﻿using System;
using System.Collections.Generic;
using System.Text;



namespace Domain.Objects.Models
{
    public class CustomerMasterViewModel : BaseViewModel
    {
        public Guid? Pk_CustomerMasterId { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public string ContactPerson { get; set; }
        public string ContactNo { get; set; }
        public string EmailId { get; set; }
        public string Address { get; set; }

    }
}
