using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Domain.Objects.Models
{
    public class BinLocationHistoryModel : BaseViewModel
    {
        public Guid? PK_BinLocationHistoryId { get; set; }
        public int Quantity { get; set; }
        public Guid FK_SortingMasterID { get; set; }
        public string CurrentLocation { get; set; }
        public string NewLocation { get; set; }
        public int Type { get; set; }
        public string itemcode { get; set; }
        public string RePackCase { get; set; }
        public string customerCode { get; set; }
        
        public string UserName { get; set; }
    }
  public class BinItemHistoryView
  {

    public Guid Pk_BinItemHistoryId { get; set; }
    public Guid FK_SortingMasterID { get; set; }
    public string RepakedCaseNo { get; set; }
    public string InvoiceNo { get; set; }
    public string CaseNo { get; set; }
    public string ItemCode { get; set; } //Part No
    public string LocationID { get; set; }
    public int Quantity { get; set; }
    public bool IsActive { get; set; }
    public bool IsDelete { get; set; }
    public Guid? CreatedBy { get; set; }
    public virtual UserMasterViewModel UserMaster { get; set; }
    public Guid? ModifiedBy { get; set; }
    public DateTime CreatedDate { get; set; }
    public DateTime ModifiedDate { get; set; }
  }
}
