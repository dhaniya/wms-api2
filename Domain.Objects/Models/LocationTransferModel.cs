﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Objects.Models
{
    public class LocationTransferModel
    {
        public Guid Pk_SortingID { get; set; }
        public string LocationID { get; set; }
        public string RepakedCaseNo { get; set; }
        public decimal Height { get; set; }
        public decimal Length { get; set; }
        public decimal Breadth { get; set; }
        public decimal Volume { get; set; }
        public decimal Weight { get; set; }
    }

    public class UpdateLocationTransferModel
    {
        public string RepakedCaseNo { get; set; }
        public Guid Pk_SortingID { get; set; }
        public string NewLocationID { get; set; }
        public Guid? ModifiedBy { get; set; }
    }

    public class searchlocationtransfer
    {
        public string LocationID { get; set; }
        public string RepakedCaseNo { get; set; }
    }
}
