﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Objects.Models
{
    public class BinMasterModel : BaseViewModel
    {
        public Guid? Pk_BinMasterId { get; set; }
        public string RackName { get; set; }
        public string LocationCode { get; set; }
        public string RowNo { get; set; }
        public string ColumnNo { get; set; }
        public bool IsUsed { get; set; }
    }

    public class BinLocationDetailModel
    {
        public int AvailableQty { get; set; }
        public int NoOfPieces { get; set; }
        public Guid FK_SortingMaster_Id { get; set; }
        public string LocationId { get; set; }
    }

    public class BinLocationModel
    {
        public string RepackedCaseNo { get; set; }
        public string ItemCode { get; set; }
        public int NoOfPieces { get; set; }
        public int Quantity { get; set; }
        public string LocationCode { get; set; }
        public Guid? CreatedBy { get; set; }
        public Guid fk_sortingid { get; set; }
        public string UserName { get; set; }
        public string Invoicenumber { get; set; }
        public string Casenumber { get; set; }
        public string Description { get; set; }

    }
    public class BinLocationModelExcel
    {
        public string ItemCode { get; set; }
        public int Quantity { get; set; }
        public string UserName{ get; set; }
        public string LocationCode { get; set; }

        public string RepackedCaseNo { get; set; }


    }


    public class AvailableQuantityModel
    {
        public int? AvailableQty { get; set; }
        public Guid fk_sortingmasterid { get; set; }
    }
}
