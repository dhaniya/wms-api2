﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Objects.Models
{
    public class PartsMasterModel :BaseViewModel
    {
        public Guid? Pk_PartsMasterID { get; set; }
        public string PartNumber { get; set; }
        public string PartDescription { get; set; }
        public decimal PartUnitPrice { get; set; }
        public decimal length { get; set; }
        public decimal Breadth { get; set; }
        public decimal Height { get; set; }
        public decimal Weight { get; set; }
    }
    public class PartsMasterModellist : BaseFilterModel
    {
        public string search { get; set; }
        public Guid? Pk_PartsMasterID { get; set; }
        public string PartNumber { get; set; }
        public string PartDescription { get; set; }
        public decimal PartUnitPrice { get; set; }
        public decimal length { get; set; }
        public decimal Breadth { get; set; }
        public decimal Height { get; set; }
        public decimal Weight { get; set; }
        public DateTime? CreatedDate { get; set; }
    }
    public class PartsMasterMismatch : PartsMasterModel
    {
        public bool isPartNumber { get; set; }
    }
}
