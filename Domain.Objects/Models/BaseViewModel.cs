﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Objects.Models
{
    public class BaseViewModel
    {
        public bool IsActive { get; set; }
        public bool IsDelete { get; set; }
        public Guid? CreatedBy { get; set; }
        public Guid? ModifiedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
    }
}
