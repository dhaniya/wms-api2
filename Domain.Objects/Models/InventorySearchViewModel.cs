using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Objects.Models
{
   public class InventorySearchViewModel:BaseFilterModel
    {
        public string InvoiceNo;
        public string LocationID;
        public string RepakedCaseNo;
        public string ItemCode;
        public string CustomerCode;
        public string CustomerName;
        public List<InventorySearchLocation> lstinventoryLocation { get; set; }
    }
    public class InventorySearchLocation
    {
        public string LocationID { get; set; }
        public List<InventorySearchRepackCase> lstRepackcase { get; set; }
    }
    public class InventorySearchRepackCase
    {
        public string InvoiceNo { get; set; }
        public Guid? SortingMasterID { get; set; }
        public string RepackCase { get; set; }
       
        public string Location { get; set; }
        public string PartNo { get; set; }
        public string Description { get; set; }
        public int? Qty { get; set; }
        public string CustomerCode { get; set; }

        public string Dimention { get; set; }
     
        public decimal? weight { get; set; }
        public List<InventorySearchItemCode> lstitems { get; set; }
    }
    public class InventorySearchItemCode
    {
        public string location { get; set; }

        public string Partno { get; set; }
        public string Description { get; set; }
        public int Qty { get; set; }
        public string CustomerCode { get; set; }
        public decimal Weight { get; set; }
        public decimal Dimention { get; set; }

    }
}
