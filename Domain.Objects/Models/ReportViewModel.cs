using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Objects.Models
{
    public class ReportViewModel
    {
    }

    public class DeliveryCollectionOrderReport
    {
        public string DONumber { get; set; }
        public string CompanyName { get; set; }
        public DateTime DateOfDeliver { get; set; }
        public string CustomerName { get; set; }
        public string CustomerCode { get; set; }
        public int TotalQuantity { get; set; }
        public int TotalCases { get; set; }
        public decimal TotalWeight { get; set; }
        public List<DeliveryCollectionOrderDetail> listdeliverycollectionorderdetail { get; set; }
    }
    public class DeliveryCollectionOrderDetail
    {
        public string PoNumber { get; set; }
        public string PartCode { get; set; }
        public string PartDescription { get; set; }
        public int Quantity { get; set; }
        public decimal Length { get; set; }
        public decimal Breadth { get; set; }
        public decimal Height { get; set; }
        public decimal m3 { get; set; }
        public decimal Weight { get; set; }
    }
    public class PartReportDetailsModel : BaseFilterModel
    {
        public string itemcode { get; set; }
        public string itemdesc { get; set; }
        public decimal? Netprice { get; set; }
        public int qtyonhand { get; set; }
        public int qtyselected { get; set; }
        public int qtyavailable { get; set; }
        public int totaldeliver { get; set; }
        public int totalrecived { get; set; }
        public string location { get; set; }
        public string repackcaseno { get; set; }
    }

    public class PartReportDetailsModelExcelExport
    {
        public string partnumber { get; set; }
        public string itemdesc { get; set; }
        public decimal? Netprice { get; set; }
        public int? qtyonhand { get; set; }
        public int? qtyselected { get; set; }
        public int? qtyavailable { get; set; }
        public int? totaldeliver { get; set; }
        public int? totalrecived { get; set; }

    }

    public class IncomingShipmentSummaryReportFilterModel : BaseFilterModel
    {
        public string ContainerNo { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public string InvoiceNo { get; set; }
        public string ShippingRefNumber { get; set; }
    }

    public class IncomingShipmentSummaryReportFilterExcelExportModel
    {
        public string ContainerNo { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public string InvoiceNo { get; set; }
        public string ShippingRefNumber { get; set; }
    }

    public class IncomingShipmentSummaryReportDetailsModel : BaseFilterModel
    {
        public string ContainerNo { get; set; }
        public DateTime? DateOfArrival { get; set; }
        public string InvoiceNo { get; set; }
        public string ShippingRefNo { get; set; }
        public string CompanyName { get; set; }
        public string SupplierName { get; set; }
        public string VesselName { get; set; }
        public string ShippingRefNumber { get; set; }
        public string RepackCaseNumber { get; set; }
        public string VesselVoyageNo { get; set; }
        public string PalletNo { get; set; }
        public string PartNo { get; set; }
        public string Description { get; set; }
        public int? Quantity { get; set; }
        public decimal? UnitPrice { get; set; }
        public string Customer { get; set; }
        public string CustomerPO { get; set; }
        public string OrderNo { get; set; }
        public decimal? Length { get; set; }
        public decimal? Breadth { get; set; }
        public decimal? Height { get; set; }

        public decimal? Weight { get; set; }
        public decimal? Volume { get; set; }
        public string Remark1 { get; set; }
        public string Remark2 { get; set; }
        public string Remark3 { get; set; }
    }

    public class IncomingShipmentReportExcelExportModel
    {
        public string InvoiceNo { get; set; }
        public DateTime? DateOfArrival { get; set; }
        public string VesselName { get; set; }
        public string VesselVoyageNo { get; set; }
        public string ContainerNo { get; set; }

        public string PalletNo { get; set; }
        public string PartNo { get; set; }

        public string Description { get; set; }
        public int? Quantity { get; set; }
        public string Customer { get; set; }

        public string CompanyName { get; set; }
        public string SupplierName { get; set; }
        public string ShippingRefNumber { get; set; }
        public string CustomerPO { get; set; }
        public string RepackCaseNumber { get; set; }
        public string OrderNo { get; set; }


        public string ShippingRefNo { get; set; }
        public decimal? UnitPrice { get; set; }


        public decimal Length { get; set; }
        public decimal? Breadth { get; set; }
        public decimal? Height { get; set; }
        public decimal? Weight { get; set; }
        public string Remark1 { get; set; }
        public string Remark2 { get; set; }
        public string Remark3 { get; set; }
    }

    public class DiscrepancyFilterModel : BaseFilterModel
    {
        public string InvoiceNo { get; set; }
    }

    public class DiscrepancyModel : BaseFilterModel
    {
        public string InvoiceNo { get; set; }
        public string PalletNo { get; set; }
        public string PartNo { get; set; }
        public string PartDescription { get; set; }

        public string customerCode { get; set; }
        public int? ReceivedQty { get; set; }
        public int ASNQty { get; set; }
        public decimal? DiscrepancyQty { get; set; }

        public int? Sortingqty { get; set; }


    }

    public class ShipmentReportFilterView : BaseFilterModel
    {
        public string ContainerNo { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public string InvoiceNo { get; set; }
        public string ShippingRefNo { get; set; }
        public string Caseno { get; set; }
        public string Partno { get; set; }
    }

    public class ShipmentReportFilterExcelView
    {
        public string ContainerNo { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public string InvoiceNo { get; set; }
        public string ShippingRefNo { get; set; }
        public string Caseno { get; set; }
        public string Partno { get; set; }
    }

    public class ShipmentReportView : BaseFilterModel
    {
        public string ContainerNo { get; set; }
        public DateTime? DateOfArrival { get; set; }
        public string InvoiceNo { get; set; }
        public string VesselName { get; set; }
        public string ShippingRefNumber { get; set; }
        public string RepackCaseNumber { get; set; }
        public string VesselVoyageNo { get; set; }
        public string PalletNo { get; set; }
        public string PartNo { get; set; }
        public string Description { get; set; }
        public int? Quantity { get; set; }
        public decimal UnitPrice { get; set; }
        public string Customer { get; set; }
        public string CustomerPO { get; set; }
        public string OrderNo { get; set; }
        public decimal Length { get; set; }
        public decimal Breadth { get; set; }
        public decimal Height { get; set; }
        public decimal Weight { get; set; }
        public decimal Volume { get; set; }
        public string Remark1 { get; set; }
        public string Remark2 { get; set; }
        public string Remark3 { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
    }

    public class ShipmentReportExcelExportView
    {
        public string InvoiceNo { get; set; }
        public DateTime? DateOfArrival { get; set; }
        public string VesselVoyageNo { get; set; }

        public string VesselName { get; set; }

        public string ContainerNo { get; set; }
        public string PalletNo { get; set; }
        public string PartNo { get; set; }
        public string Description { get; set; }

        public int? Quantity { get; set; }
        public decimal UnitPrice { get; set; }
        public string Customercode { get; set; }
        public string CustomerPO { get; set; }
        public string OrderNo { get; set; }
        public decimal Length { get; set; }
        public decimal Breadth { get; set; }
        public decimal Height { get; set; }
        public decimal Volume { get; set; }
        public string ShippingRefNumber { get; set; }
        public string RepackCaseNumber { get; set; }
        public string Remark1 { get; set; }
        public string Remark2 { get; set; }
        public string Remark3 { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
    }

    public class DeliveryListingReportFilterView : BaseFilterModel
    {
        public string CustomerName { get; set; }
        public string DoNo { get; set; }
        public string CustomerCode { get; set; }

        public DateTime? Date { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }

    }
    public class DeliveryListingReportView : BaseFilterModel
    {
        public string RepackedCaseNo { get; set; }
        public string DONumber { get; set; }
        public string CustomerCode { get; set; }
        public string CustomerPo { get; set; }
        public string PartNo { get; set; }
        public string Description { get; set; }
        public int? ShippedQuantity { get; set; }
        public int? NoOfCase { get; set; }
        public DateTime DateOfDeliver { get; set; }
        public string Remark1 { get; set; }
        public string Remark2 { get; set; }
        public string Remark3 { get; set; }
        public decimal Weight { get; set; }
        public decimal Length { get; set; }
        public decimal Breadth { get; set; }
        public decimal Height { get; set; }
        public string From { get; set; }
        public string To { get; set; }

        public string Dimension { get; set; }
        public decimal Volume { get; set; }
    }

    public class DeliveryListingSummuryReportView : BaseFilterModel
    {
        public string RepackedCaseNo { get; set; }
        public decimal Weight { get; set; }
        public decimal Volume { get; set; }
        public string DONumber { get; set; }
        public string CustomerCode { get; set; }
        public int? NoOfCase { get; set; }
        public DateTime DateOfDeliver { get; set; }
        public string From { get; set; }
        public string To { get; set; }
    }


    public class DeliveryListingReportFilterExcelExportView
    {
        public string CustomerName { get; set; }
        public string DoNo { get; set; }
        public string CustomerCode { get; set; }
        public DateTime? Date { get; set; }
    }
    public class DeliveryListingReportExcelExportView
    {
        public string DONumber { get; set; }
        public DateTime DateOfDeliver { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        public string CustomerCode { get; set; }
        public string CustomerPo { get; set; }
        public string RepackedCaseNo { get; set; }
        public string PartNo { get; set; }
        public string Description { get; set; }
        public int? ShippedQuantity { get; set; }
        public decimal Length { get; set; }
        public decimal Breadth { get; set; }

        public decimal Height { get; set; }


        public string Dimension { get; set; }
        public decimal Weight { get; set; }

        public int? NoOfCase { get; set; }


        public decimal Volume { get; set; }

        public string Remark1 { get; set; }
        public string Remark2 { get; set; }
        public string Remark3 { get; set; }
    }

    public class DeliveryListingSummaryReportExcelExportView
    {

        public string DONumber { get; set; }
        public DateTime? DateOfDeliver { get; set; }

        public string From { get; set; }
        public string To { get; set; }
        public string CustomerCode { get; set; }

        public string RepackedCaseNo { get; set; }

        public int? NoOfCase { get; set; }
        public string CustomerPo { get; set; }
        public decimal? Weight { get; set; }
        public decimal? Volume { get; set; }
    }
    public class GetPartsTransactionsListView : BaseFilterModel
    {
        public string search { get; set; }
        public string InvoiceNo { get; set; }
        public string Caseno { get; set; }//sorting item details
        public string PartNo { get; set; }
        public string PartDescription { get; set; }
        public int? Quantity { get; set; }//AsnitemDetails
        public int? ReceivedQuantity { get; set; }
        public string Repackcaseno { get; set; }
        public int? sortedQty { get; set; }

        public string DONumber { get; set; }//deliveDeliveryOrderFilterViewModel 
        public int? issuedQty { get; set; }
        public DateTime? DoNumberDate { get; set; }
        public int? Balance { get; set; }
    }
    public class GetPartsTransactionsBinList : BaseFilterModel
    {
        public string partnumber { get; set; }
        public string location { get; set; }
        public string search { get; set; }
        public string InvoiceNo { get; set; }
        public string Caseno { get; set; }//sorting item details
        public string PartNo { get; set; }
        public string PartDescription { get; set; }
        public int? Quantity { get; set; }//AsnitemDetails
        public int? ReceivedQuantity { get; set; }
        public string BinLocation { get; set; }
        public int? sortedQty { get; set; }

        //public string DONumber { get; set; }//deliveDeliveryOrderFilterViewModel 
        //public int? issuedQty { get; set; }
        //public DateTime? DoNumberDate { get; set; }
        //public int? Balance { get; set; }

    }
    public class GetPartsTransactionsBinDoList: BaseFilterModel
    {
        public string partnumber { get; set; }
        public string location { get; set; }
        public string PartNo { get; set; }
        public string DONumber { get; set; }//deliveDeliveryOrderFilterViewModel 
        public int? issuedQty { get; set; }
        public string BinLocation { get; set; }
        public DateTime? DoNumberDate { get; set; }
    }
    public class GetRealocationsListView : BaseFilterModel
    {

        public string PartNo { get; set; }
        public int Quantity { get; set; }
        public string CustomerCode { get; set; }
        public string RepackedCaseNo { get; set; }
        public string NewLocation { get; set; }
        public DateTime? Date { get; set; }
        public string User { get; set; }
    }
    public class GetStockAgingReportsListView : BaseFilterModel
    //: BaseFilterModel
    {
        public string PickingType { get; set; }
        public string LocationNumber { get; set; }
        public string LocationId { get; set; }
        public string PartCode { get; set; }
        public string PartDescription { get; set; }
        public DateTime? EarlyGoodsreceivedDate { get; set; }//1
        public DateTime? ReportRunnungDate { get; set; }//2
        public int? Aging { get; set; }//count date 1-2
        public int? OnhandQty { get; set; }//AvalibelQty
        public decimal? UnitCost { get; set; }//price unite cost
        public int? QTY0_5 { get; set; }
        public Decimal? Totalcost0_5 { get; set; }
        public int? QTY1 { get; set; }
        public Decimal? Totalcost1 { get; set; }
        public int? QTY1_5 { get; set; }
        public Decimal? Totalcost1_5 { get; set; }
        public int? QTY2 { get; set; }
        public Decimal? Totalcost2 { get; set; }
        public int? QTY2_5 { get; set; }
        public Decimal? Totalcost2_5 { get; set; }
        public int? QTY3 { get; set; }
        public Decimal? Totalcost3 { get; set; }
        public int? QTY3_5 { get; set; }
        public Decimal? Totalcost3_5 { get; set; }
        public int? QTY4 { get; set; }
        public Decimal? Totalcost4 { get; set; }
        public int? QTY4_5 { get; set; }
        public Decimal? Totalcost4_5 { get; set; }
        public int? QTY5 { get; set; }
        public Decimal? Totalcost5 { get; set; }
        public int? QTY5_5 { get; set; }
        public Decimal? Totalcost5_5 { get; set; }

    }
    public class repackbin
    {
        public List<GetStockAgingReportsListView> repack { get; set; }
        public List<GetStockAgingReportsListView> bin { get; set; }
    }
}

