﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Objects.Models
{
   public class DeliveryOrderViewModel : BaseViewModel
    {
        public Guid Pk_DeliveryOrderID { get; set; }
      
        public string DONumber { get; set; }
        public string DriverName { get; set; }
        public string VehicleNumber { get; set; }
        public int shippingmodeID { get; set; }
        public DateTime ActualDateOfDeliver { get; set; }
        public Guid Fk_PickingMasterID { get; set; }
        public Guid? Fk_CompanyID { get; set; }
        public string OrderNumber { get; set; }
        public Guid? Fk_CustomerMasterId { get; set; }
        public string CustomerName { get; set; }
        public string CustomerCode { get; set; }
        public DateTime DateOfDeliver { get; set; }
        public string RefNo { get; set; }
        public int Priority { get; set; }
        public bool isConfirm { get; set; }
        public string UserName { get; set; }
        public string Address { get; set; }
        
    }

    public class CreateDeliveryOrderViewModel
    {
        public string DriverName { get; set; }
        public string VehicleNumber { get; set; }
        public int shippingmodeID { get; set; }
        public DateTime ActualDateOfDeliver { get; set; }
        public Guid Fk_PickingMasterID { get; set; }
        public Guid Fk_CompanyID { get; set; }
        public Guid? CreatedBy { get; set; }
        public int NoOfBox { get; set; }
        public string Subject { get; set; }
        public string Address { get; set; }
        public string remarks { get; set; }
    }
}
