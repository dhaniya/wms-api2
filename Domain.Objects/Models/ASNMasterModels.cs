using System;
using System.Collections.Generic;
using System.Text;


namespace Domain.Objects.Models
{
    public class ASNMasterModels : BaseViewModel
    {
        public Guid? PK_AsnMasterID { get; set; }
        public Guid Fk_CompanyID { get; set; }
        public Guid Fk_SupplierID { get; set; }
        public DateTime DateOfArrival { get; set; }
        public string InvoiceNo { get; set; }
        public string origin { get; set; }
        public string Remark { get; set; }
        public int shippingmodeID { get; set; }
        public string ShippingmodeName { get; set; }
        public string VesselName { get; set; }
        public string VesselVoyageNo { get; set; }
        public string BillNumber { get; set; }
        public string FlightName { get; set; }
        public string FlightNumber { get; set; }
        public string AWLNumber { get; set; }
        public string TruckNumber { get; set; }
        public string DeliveryNumber { get; set; }
        public string Description { get; set; }
    }


    public class ASNandlistasnitemsModel : BaseViewModel
    {
        public Guid? PK_AsnMasterID { get; set; }
        public Guid Fk_CompanyID { get; set; }
        public string CompanyName { get; set; }
        public Guid Fk_SupplierID { get; set; }
        public string SupplierName { get; set; }
        public DateTime DateOfArrival { get; set; }
        public string InvoiceNo { get; set; }
        public string origin { get; set; }
        public string Remark { get; set; }
        public int shippingmodeID { get; set; }
        public string ShippingmodeName { get; set; }
        public string VesselName { get; set; }
        public string VesselVoyageNo { get; set; }
        public string BillNumber { get; set; }
        public string FlightName { get; set; }
        public string FlightNumber { get; set; }
        public string AWLNumber { get; set; }
        public string TruckNumber { get; set; }
        public string DeliveryNumber { get; set; }
        public string Description { get; set; }
        public List<ASNItemDetailModels> Listasnitemdetail { get; set; }
        public string UserName { get; set; }
        public string Remark1 { get; set; }
        public string Remark2 { get; set; }
        public string Remark3 { get; set; }

  }

    public class GoodsReceiptsModel 
    {
        public DateTime DateofArrival { get; set; }
        public string InvoiceNo { get; set; }
        public string ContainerNo { get; set; }
        public string CaseNo { get; set; }
        public string LocationID { get; set; }
        public string ShippingmodeName { get; set; }
        public bool? IsSorting { get; set; }
        public Guid Pk_AsnItemDetailsID { get; set; }
        public Guid? CreatedBy { get; set; }
        public string UserName { get; set; }
    }

    public class TodayAsnMasterViewModel
    {
        public Guid? PK_AsnMasterID { get; set; }
        public string CompanyName { get; set; }
        public string SupplierName { get; set; }
        public DateTime DateOfArrival { get; set; }
        public string InvoiceNo { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public bool isrecipt { get; set; }
        public string UserName { get; set; }
        public string AllReceived { get; set; }
    public string Remark { get; set; }
        public DateTime CreatedDate { get; set; }
  }

    public class GoodReceiptListViewModel
    {
        public Guid? PK_AsnMasterID { get; set; }
        public string CompanyName { get; set; }
        public string SupplierName { get; set; }
        public DateTime DateOfArrival { get; set; }
        public string InvoiceNo { get; set; }

        public string RepackedCaseNo { get; set; }
        public int Totalcontainer { get; set; }
        public int TotalScanned { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }

    }
    public class ASNDetailModal
    {
        public string InvoiceNo { get; set; }
        public string ContainerNo { get; set; }
        public string CaseNo { get; set; }
        public int Quantity { get; set; }
        public string Customer { get; set; }
        public string ItemCode { get; set; }
    }


}
