﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Objects.Models
{
    public class LocationMasterModel : BaseViewModel
    {
        public Guid Pk_LocationMasterId { get; set; }
        public string RackName { get; set; }
        public string LocationCode { get; set; }
        public int LocationType { get; set; }
        public int ZoneType { get; set; }
        public bool IsUsed { get; set; }
       
    }
}
