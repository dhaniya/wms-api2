﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Objects.Models
{
    public class UserMasterViewModel : BaseViewModel
    {
        public Guid? Pk_UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string EmailID { get; set; }
        public string ContactNumber { get; set; }
        public Guid FK_RoleID { get; set; }
        public string RoleName { get; set; }
        public string Token { get; set; }
    }

    public class LoginViewModel
	{
        public string UserName { get; set; }
        public string Password { get; set; }

    }

    public class ChangePasswordViewModel
    {
        public Guid? Userid { get; set; }
        public string OldPassword { get; set; }
        public string NewPassword { get; set; }
    }

    public enum PasswordVerificationResult
    {
        Success = 1,
        Failed = 2,
        SuccessRehashNeeded = 3
    }
}
