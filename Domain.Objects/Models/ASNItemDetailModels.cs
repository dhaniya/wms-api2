﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Objects.Models
{
    public class ASNItemDetailModels : BaseViewModel
    {
        public Guid Pk_AsnItemDetailsID { get; set; }
        public Guid Fk_AsnMasterID{ get; set; }
        public string ContainerNo { get; set; }
        public string PalletNo { get; set; } // Case No
        public string OrderNo { get; set; }
        public string SuppliedPartNo { get; set; } //Item Code
        public int Quantity { get; set; }
        public decimal UnitPrice { get; set; }
        public decimal Length { get; set; }
        public decimal Breadth { get; set; }
        public decimal Height { get; set; }
        public decimal Weight { get; set; }
        public string Customer { get; set; }
        public string PurchaseOrderNo { get; set; }
        public string Remark1 { get; set; }
        public string Remark2 { get; set; }
        public string Remark3 { get; set; }
        public bool IsSorted { get; set; }
        public bool IsRecieved { get; set; }
        public string Description { get; set; }
    }

    public class SortingItemDetail
    {
        public string InvoiceNo { get; set; }
        public string CaseNo { get; set; } //Pallet NO
        public string SuppliedPartNo { get; set; } // Item Code
        public string Customer { get; set; } // Customer Code
    }

    public class SortingItemFilter
    {
        public string InvoiceNo { get; set; }
        public string CaseNo { get; set; } //Pallet NO
        public string SuppliedPartNo { get; set; } // Item Code
        public string Customer { get; set; } // Customer Code
    }

    public class ASNDetailsMismatch: ASNItemDetailModels
    {
        public bool isCustomer { get; set; }
        public bool isPartnumber { get; set; }
        public bool isCasenumber { get; set; }
    }


}
