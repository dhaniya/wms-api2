﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Objects.Models
{
   public class BaseFilterModel
    {
        public int? Skip { get; set; }

        public int? Take { get; set; }

        public string SortColumnName { get; set; }

        public bool? IsDescending { get; set; } //i.e. asc, desc
    }
}
