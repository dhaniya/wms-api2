﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Objects.Models
{
   public class UserAccessViewModel
    {
        public Guid Pk_UserAccessId { get; set; }
        public string RoleName { get; set; }
        public string RouteUrl { get; set; }
        public bool Add { get; set; }
        public bool Edit { get; set; }
        public bool Delete { get; set; }
        public bool View { get; set; }
        public bool IsDeleted { get; set; }
        public Guid UserId { get; set; }
        public UserMasterViewModel User { get; set; }
    }
}
