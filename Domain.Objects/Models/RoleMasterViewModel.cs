﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Objects.Models
{
    public class RoleMasterViewModel : BaseViewModel
    {
        public Guid Pk_RoleMasterID { get; set; }
        public string RoleName { get; set; }
        public int levelID { get; set; }
    }
}
