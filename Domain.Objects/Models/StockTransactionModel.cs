﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Objects.Models
{
    public class StockTransactionModel : BaseViewModel
    {
        public Guid StockTransactionID { get; set; }
        public Guid? PK_SortingID { get; set; }
        public int? QuantityReceived { get; set; }
        public int? QuantityDelivered { get; set; }
        public int? QuantitySelected { get; set; }
        public int? QuantityRemaining { get; set; }
        public Guid? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public Guid? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }
}
