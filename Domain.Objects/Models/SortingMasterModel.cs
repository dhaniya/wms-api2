using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Objects.Models
{
    public class SortingMasterModel : BaseViewModel
    {
        public Guid? Pk_SortingID { get; set; }
        public string InvoiceNo { get; set; }
        public string CaseNo { get; set; }
        public string ItemCode { get; set; } //Part No
        public string NoOfPieces { get; set; }
        public string CustomerCode { get; set; }
        public string LocationID { get; set; }
        public string RepakedCaseNo { get; set; }
        public int Quantity { get; set; }
        public string Description { get; set; }
        public string SortingDescription { get; set; }
        public string UserName { get; set; }
    public int TotalQty { get; set; }
    public int Openbox { get; set; }
        public int TotalBox { get; set; }
        public bool? IsPutaway { get; set; }
    }

    public class PutAwayViewModel
    {
       public string RepakedCaseNo { get; set; }
       public string CustomerCode { get; set; }
       public decimal Weight { get; set; }
        public string LocationId { get; set; }
        public Guid? ModifiedBy { get; set; }
    }

    public class DateFilter
    {
        public DateTime FromDate{ get; set; }
        public DateTime ToDate { get; set; }
    }

    public class SortingMasterViewModal
    {
        public Guid Pk_SortingID { get; set; }
        public string InvoiceNo { get; set; }
        public string CaseNo { get; set; }
        public string ItemCode { get; set; } //Part No
        public string NoOfPieces { get; set; }
        public string CustomerCode { get; set; }
        public string SortingDescription { get; set; }
        public string LocationID { get; set; }
        public string RepakedCaseNo { get; set; }
        public int Quantity { get; set; }
        public bool IsActive { get; set; }
        public bool IsDelete { get; set; }
        public Guid? CreatedBy { get; set; }
        public Guid? ModifiedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public bool IsPutaway { get; set; }
        public bool IsBin { get; set; }
        public string UserName { get; set; }
    public string Description { get; set; }
    public int TotalQty { get; set; }
  }
}
