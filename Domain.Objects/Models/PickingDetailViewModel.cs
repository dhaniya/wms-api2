using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Objects.Models
{
    public class PickingDetailViewModel : BaseViewModel
    {
        public string PriPickingOrderNumber { get; set; }
        public Guid Pk_PickingDetailId { get; set; }
        public Guid Fk_PickingID { get; set; }
        public string CustomerName { get; set; }
        public string CustomerCode { get; set; }
        public DateTime? dateOfDeliver { get; set; }
        public int PickingType { get; set; }

        public string InvoiceNo { get; set; }
        public string RepackedCaseNo { get; set; }
        public string ItemCode { get; set; }
        public int Quantity { get; set; }
        public string Description { get; set; }
        public string PONumber { get; set; }

        public decimal Length { get; set; }
        public decimal Breadth { get; set; }
        public decimal Height { get; set; }
        public decimal Volume { get; set; }
        public decimal Weight { get; set; }
        public string Remark1 { get; set; }
        public string Remark2 { get; set; }
        public string Remark3 { get; set; }


        public string LocationId { get; set; }
        public bool IsPicked { get; set; }
        public Guid fk_SortingMaster_id { get; set; }
        public string OrigionalCase { get; set; }


        public int priority { get; set; }
    }
}
