﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Objects.Models
{
    public class DeliveryOrderViewResponceModel
    {
        public Guid Pk_DeliveryOrderID { get; set; }
        public Guid Fk_PickingMasterID { get; set; }
        public string DONumber { get; set; }
        public string DeliverName { get; set; }
        public string VehicleNumber { get; set; }
        public int shippingmodeID { get; set; }
        public DateTime ActualDateOfDeliver { get; set; }
        public string OrderNumber { get; set; }
        public string CustomerName { get; set; }
        public string CustomerCode { get; set; }
        public DateTime DateOfDeliver { get; set; }
        public string RefNo { get; set; }
        public int Priority { get; set; }
        public string Address { get; set; }
        public int TotalCases { get; set; }
        public int NoOfBox { get; set; }
        public string Remarks { get; set; }
        public string PickingOrderNo { get; set; }
        public string DriverName { get; set; }
        public string FromAddress { get; set; }
        public string ComapanyNameMaster { get; set; }
        public List<DeliveryOrderDetailsViewModel> lstdetils { get; set; }
       
    }
    public class DeliveryOrderDetailsViewModel
    {
        public Guid Pk_DeliveryOrderDetailID { get; set; }
        public Guid Fk_DeliveryOrderID { get; set; }
        public Guid? Fk_SortingMasterID { get; set; }
        public int? PickingType { get; set; }
        public string InvoiceNo { get; set; }
        public string RepackedCaseNo { get; set; }
        public string ItemCode { get; set; }
        public int Quantity { get; set; }
        public string LocationId { get; set; }
        public decimal Height { get; set; } 
        public decimal Length { get; set; }
        public decimal Breadth { get; set; }
        public decimal Weight { get; set; }
        public string PartDescription { get; set; }
        public string PONumber { get; set; }
        public string Remark1 { get; set; }
        public string Remark2 { get; set; }
        public string Remark3 { get; set; }
        public decimal M2 { get; set; }
    }

    public class DeliveryOrderFilterViewModel : BaseFilterModel
    {
        public string DONumber { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public string CustomerName { get; set; }
        public string CustomerCode { get; set; }
    }
}
