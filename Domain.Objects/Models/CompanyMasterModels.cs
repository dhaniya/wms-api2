﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Objects.Models
{
    public class CompanyMasterModels : BaseViewModel
    {
        public Guid? PK_CompanyID { get; set; }
        public string CompanyName { get; set; }
        public string CompanyContactPerson { get; set; }
        public string CompanyID { get; set; }
        public string ContactNo { get; set; }
        public string EmailID { get; set; }
        public string Address { get; set; }
        public string Description { get; set; }
        public Guid? CreatedBy { get; set; }
    }

    
}
