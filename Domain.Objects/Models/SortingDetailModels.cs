﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Objects.Models
{
    public class SortingDetailModels : BaseViewModel
    {
        public Guid Pk_SortingDetailID { get; set; }
        public Guid FK_SortingMasterID { get; set; }
        public string RepakedCaseNo { get; set; }
        public decimal Height { get; set; }
        public decimal Length { get; set; }
        public decimal Breadth { get; set; }
        public decimal Volume { get; set; }
        public decimal Weight { get; set; }
        public string UserName { get; set; }
       
    }
}
