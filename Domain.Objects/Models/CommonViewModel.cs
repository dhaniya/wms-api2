using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Objects.Models
{
    public class CommonViewModel
    {
        public RouteModel[] Routes()
        {
            return new RouteModel[]{new RouteModel {
    name= "All AsnList",
   path = "admin/AllAsnList",
   addFlag= true,
    editFlag= false,
    deleteFlag= true,
  },
  new RouteModel
  {
    name = "Goods Receipt",
    path = "admin/invoice-list",
    addFlag = true,
    editFlag = false,
    deleteFlag = true,
  },
   new RouteModel
  {
    name = "Asn Upload",
    path = "admin/exltofile",
    addFlag = true,
    editFlag = false,
    deleteFlag = false,
  },
    new RouteModel
  {
    name = "Generate Barcode",
    path = "admin/barcode",
    addFlag = true,
    editFlag = false,
    deleteFlag = false,
  },
     new RouteModel
  {
    name = "Asn Details Page",
    path = "admin/all-asn",
    addFlag = false,
    editFlag = false,
    deleteFlag = false,
  },
      new RouteModel
  {
    name = "Sorting",
    path = "admin/shorting",
    addFlag = true,
    editFlag = false,
    deleteFlag = true,
  },
       new RouteModel
  {
    name = "Put Away",
    path = "admin/putawaylist",
    addFlag = true,
    editFlag = false,
    deleteFlag = false,
  },
        new RouteModel
  {
    name = "Location Transfer",
    path = "admin/Location-Transfer",
    addFlag = true,
    editFlag = false,
    deleteFlag = false,
  },
         new RouteModel
  {
    name = "Bin Item Location Transfer",
    path = "admin/looseitemlocation",
    addFlag = true,
    editFlag = false,
    deleteFlag = false,
  },
          new RouteModel
  {
    name = "Location Search",
    path = "admin/location-by-rack",
    addFlag = false,
    editFlag = false,
    deleteFlag = false,
  },
           new RouteModel
  {
    name = "Bin Location",
    path = "admin/BinInLocation",
    addFlag = true,
    editFlag = false,
    deleteFlag = false,
  },
            new RouteModel
  {
    name = "Loose Item Transfer",
    path = "admin/looseTransfer",
    addFlag = true,
    editFlag = false,
    deleteFlag = false,
  },
             new RouteModel
  {
    name = "Pre Picking Create",
    path = "admin/picking-list",
    addFlag = true,
    editFlag = false,
    deleteFlag = false,
  },
              new RouteModel
  {
    name = "Pre Picking List",
    path = "admin/Pre-PickingList",
    addFlag = false,
    editFlag = false,
    deleteFlag = false,
  },
               new RouteModel
  {
    name = "PreOrder Details",
    path = "admin/PreOrder-Details",
    addFlag = false,
    editFlag = false,
    deleteFlag = false,
  },
                new RouteModel
  {
    name = "Bin Picking List",
    path = "admin/BinPickingList",
    addFlag = false,
    editFlag = false,
    deleteFlag = false,
  },
                 new RouteModel
  {
    name = "Measurement Sorting list",
    path = "admin/sorting-list",
    addFlag = true,
    editFlag = false,
    deleteFlag = false,
  },
                  new RouteModel
  {
    name = "Picking List",
    path = "admin/Add-pickingList",
    addFlag = false,
    editFlag = false,
    deleteFlag = false,
  },
                   new RouteModel
  {
    name = "Picking",
    path = "admin/Pre-Order-PickingList",
    addFlag = true,
    editFlag = false,
    deleteFlag = false,
  },
                    new RouteModel
  {
    name = "Do Picking List",
    path = "admin/delivery-order",
    addFlag = false,
    editFlag = false,
    deleteFlag = false,
  },
                     new RouteModel
  {
    name = "Create Do",
    path = "admin/Pre-DO-ganaration",
    addFlag = true,
    editFlag = false,
    deleteFlag = false,
  },
                      new RouteModel
  {
    name = "Delivery Order List",
    path = "admin/delivery-order-list",
    addFlag = true,
    editFlag = true,
    deleteFlag = false,
  },
                       new RouteModel
  {
    name = "Do Print",
    path = "admin/collectionOrder",
    addFlag = false,
    editFlag = false,
    deleteFlag = false,
  },
                        new RouteModel
  {
    name = "Preview DO Details",
    path = "admin/Preview-DO",
    addFlag = false,
    editFlag = false,
    deleteFlag = false,
  },
   new RouteModel
  {
    name = "Part Report",
    path = "admin/partreport",

    addFlag = false,
    editFlag = false,
    deleteFlag = false,
  },
    new RouteModel
  {
    name = "Repack case Inventory",
    path = "admin/invsearchrack",
    addFlag = false,
    editFlag = false,
    deleteFlag = false,
  },
     new RouteModel
  {
    name = "Bin Inventory",
    path = "admin/invsearchbin",
    addFlag = false,
    editFlag = false,
    deleteFlag = false,
  },
      new RouteModel
  {
    name = "User Master",
    path = "admin/user-master",
    addFlag = true,
    editFlag = true,
    deleteFlag = true,
  },
       new RouteModel
  {
    name = "Location Master",
    path = "admin/location-master",
    addFlag = true,
    editFlag = false,
    deleteFlag = false,
  },
        new RouteModel
  {
    name = "Company Master",
    path = "admin/company-master",
    addFlag = true,
    editFlag = true,
    deleteFlag = true,
  },
         new RouteModel
  {
    name = "Supplier Master",
    path = "admin/supplier-master",

    addFlag = true,
    editFlag = true,
    deleteFlag = true,
  },
          new RouteModel
  {
    name = "Part Master",
    path = "admin/part-master",

    addFlag = true,
    editFlag = true,
    deleteFlag = true,
  },
   new RouteModel
  {
    name = "Customer Master",
    path = "admin/customer-master",
    addFlag = true,
    editFlag = true,
    deleteFlag = true,
  },
    new RouteModel
  {
    name = "Bin Master",
    path = "admin/BinMaster",
    addFlag = true,
    editFlag = false,
    deleteFlag = false
  },

    new RouteModel
  {
    name = "Discrepancyreportpage",
    path = "admin/discrepancyreportpage",
    addFlag = true,
    editFlag = false,
    deleteFlag = false
  },
        new RouteModel
  {
    name = "Deliverylistingsummaryreport",
    path = "admin/deliverylistingsummary",
    addFlag = true,
    editFlag = false,
    deleteFlag = false
  },
         new RouteModel
         {
    name = "Stockcountdiscrepancyreport",
    path = "admin/stockcountmismatch",
    addFlag = true,
    editFlag = false,
    deleteFlag = false
  },
    new RouteModel
         {
    name = "Shipmentsummaryreport",
    path = "admin/shipmentsummaryreport",
    addFlag = true,
    editFlag = false,
    deleteFlag = false
  },
     new RouteModel
         {
    name = "Delivarylistingreport",
    path = "admin/deliverylistreport",
    addFlag = true,
    editFlag = false,
    deleteFlag = false
  },
    new RouteModel
         {
    name = "ShipmentReport",
    path = "admin/shipmentReport",
    addFlag = true,
    editFlag = false,
    deleteFlag = false
  },
     new RouteModel
  {
    name = "ASN DiscrepancyReport",
    path = "admin/discrepancyreport",
    addFlag = true,
    editFlag = false,
    deleteFlag = false
  },
    new RouteModel  {
    name = "StockCount",
    path = "admin/stockCount1",
    addFlag = true,
    editFlag = false,
    deleteFlag = false
  },
    new RouteModel
    {
        name= "Pending picking",
        path="admin/Panding-Picking",
        addFlag=false,
        editFlag=false,
        deleteFlag=false,
    },
    new RouteModel
    {
        name= "View DO",
        path="admin/View-DO",
        addFlag=true,
        editFlag=true,
        deleteFlag=true,
    },
    new RouteModel
    {
        name= "Re-Allocation",
        path="admin/Re-Allocation",
        addFlag=true,
        editFlag=true,
        deleteFlag=true,
    },new RouteModel
    {
        name= "sort-aging",
        path="admin/sort-aging",
        addFlag=true,
        editFlag=true,
        deleteFlag=true,
    },
                new RouteModel
    {
        name= "parttransaction",
        path="admin/parttransaction",
        addFlag=true,
        editFlag=true,
        deleteFlag=true,
    },  new RouteModel
    {
        name= "ParttransactionBin",
        path="admin/ParttransactionBin",
        addFlag=true,
        editFlag=true,
        deleteFlag=true,
    }

            };
        }
    }
    public class AllLocationMasterModel
    {
        public Guid Pk_LocationMasterId { get; set; }
        public string RackName { get; set; }
        public string LocationCode { get; set; }
        public int LocationType { get; set; }
        public int ZoneType { get; set; }
        public bool IsUsed { get; set; }
        public bool IsActive { get; set; }
        public bool IsDelete { get; set; }
        public Guid? CreatedBy { get; set; }
        public Guid? ModifiedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
    }


    public class ResponseviewModel
    {
        public string Result { get; set; }
        public string Message { get; set; }
        public int? StatusCode { get; set; }
        public bool? IsValid { get; set; }
        public object Data { get; set; }
        public Guid? pk_ID { get; set; }
    }

    public class DropdownViewModel
    {
        public Guid ID { get; set; }
        public string Value { get; set; }
    }

    public class InvoiceNoDropdownViewModel
    {
        public string ID { get; set; }
        public string Value { get; set; }
    }

    public class StringDropdownviewModel
    {
        public string ID { get; set; }
        public string Value { get; set; }
    }

    public class CustomerDataViewModal
    {
        public string Code { get; set; }
        public string ItemNo { get; set; }
        public int? Qty { get; set; }
    }
    public class StringItemcodeQuantityviewModel
    {
        public string ID { get; set; }
        public string Value { get; set; }
        public string Locationid { get; set; }
        public int Quantity { get; set; }
        public string invoiceno { get; set; }
        public Guid fk_SortingMaster_id { get; set; }
        public string OrigionalCase { get; set; }
    }
    public class LocatioinIdViewModel
    {
        public string LocationID { get; set; }
    }

    public class AvailableQuantityViewModel
    {
        public int? AvailableQuantity { get; set; }
    }

    public class QuantityDeatilViewModel
    {
        public int? TotalQuantity { get; set; }
        public int? ReceivedQuantity { get; set; }
        public int? AvailableQuantity { get; set; }
    }
    public class StringDropdpwnviewRepackedModel
    {
        public string ID { get; set; }
        public string Value { get; set; }
        public string LBH { get; set; }
        public Guid Pk_SortingID { get; set; }
        public string LocationID { get; set; }
        public int Quantity { get; set; }
    }

    public class DeliveryOrderDropdownModel
    {
        public string ID { get; set; }
        public string Value { get; set; }
    }

    public class AppSettings
    {
        public string Secret { get; set; }
    }
    public class ConnectionStrings
    {
        public string wms { get; set; }
    }

    public class PagedItemSet<T> where T : class
    {
        public int TotalResults { get; set; }

        private List<T> _Items;

        public List<T> Items
        {
            get
            {
                return _Items ?? (_Items = new List<T>());
            }
            set
            {
                _Items = value;
            }
        }
    }
}
