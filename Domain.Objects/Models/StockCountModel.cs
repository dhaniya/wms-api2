﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Objects.Models
{
    public class StockCountModel
    {
        public Guid Pk_StockCountID { get; set; }
        public string Location { get; set; }
        public string ItemCode { get; set; }
        public int RunningNo { get; set; }
        public int Qty { get; set; }
        public Guid? CreatedBy { get; set; }
        public Guid? ModifiedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
    }

    public class StockCountView
    {
        public Guid Pk_StockCountID { get; set; }
        public string Location { get; set; }
        public string ItemCode { get; set; }
        public int RunningNo { get; set; }
        public int Qty { get; set; }
        public Guid? CreatedBy { get; set; }
        public Guid? ModifiedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string UserName { get; set; }
        public int totalQty { get; set; }
        public string totalItemCode { get; set; }
        public string totalLocation { get; set; }
        public int RemainingQty { get; set; }

    }

    public class StockCountQtyview
    {
        public DateTime Date { get; set; }
        public int? stockCount { get; set; }//RunningNumber

        public string Location { get; set; }
        public string ItemCode { get; set; }
        public string PartDescripation { get; set; }
        public int? CountQuantityNumber { get; set; }//StockQty
        public int? SystemQuantity { get; set; }//RemainingQty

        public int? ExcessQty { get; set; }


        public int? ShortQty { get; set; }


        public decimal? cost { get; set; }
        public decimal? EXTcost { get; set; }
    }

    public class StockCountFilter
    {
        public int RunningNo { get; set; }
    }
}
