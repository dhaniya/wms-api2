﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Objects.Models
{
  public  class RouteModel
    {
        public string name { get; set; }
        public string path { get; set; }
        public bool addFlag { get; set; }
        public bool editFlag { get; set; }
        public bool deleteFlag { get; set; }

    }
}
