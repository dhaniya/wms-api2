using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Objects.Models
{
    public class PickingMasterViewModel : BaseViewModel
    {
        public Guid Pk_PickingMasterID { get; set; }
        public Guid? Fk_CustomerMasterId { get; set; }
        public string OrderNumber { get; set; }
        public string CustomerName { get; set; }
        public string CustomerCode { get; set; }
   
        public DateTime DateOfDeliver { get; set; }
        public DateTime dateofCreation { get; set; }

        public string RefNo { get; set; }
        public int Priority { get; set; }
        public int PickingStatus { get; set; }
        public string Invoiceno { get; set; }
        public string Locationid { get; set; }
        public string username { get; set; }
    }

    public class PickingViewModel : BaseViewModel
    {
        public Guid Pk_PickingMasterID { get; set; }
        public Guid? Fk_CustomerMasterId { get; set; }
        public string OrderNumber { get; set; }
        public string CustomerName { get; set; }
        public string CustomerCode { get; set; }
     //   public int PickingType { get; set; }
        public DateTime DateOfDeliver { get; set; }
        public string Address { get; set; }
        public string RefNo { get; set; }
        public int Priority { get; set; }
        public int PickingStatus { get; set; }
        public string RepackedCaseNo { get; set; }
        public List<PickingDetailViewModel> listpickingdetail { get; set; }
    }

    public class SearchPickingViewModel
    {
        public string OrderNo { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? DateOfDeliver { get; set; }
        public string InvoiceNo { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public string CustomerCode { get; set; }
    }

    public class UpdatePickingViewModel
    {
        public string OrderNo { get; set; }
        public int PickingType { get; set; }
        public string RepackedCaseNo { get; set; }
        public string ItemCode { get; set; }
        public Guid? ModifiedBy { get; set; }
        public Guid? PK_PickingDetails_ID { get; set; }
    }

    public class SearchPickedViewModel
    {
        public string Customer { get; set; }
        public DateTime? DateOfDeliver { get; set; }
        public string OrderNumber { get; set; }
        public string RefNumber { get; set; }
    }
   public class PickingLocationUpdate
    {
        public Guid pk_PickingMasterID { get; set; }
        public string Locationid { get; set; }
    }

}
