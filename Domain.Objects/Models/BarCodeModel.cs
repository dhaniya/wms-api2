﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Objects.Models
{
    public class BarCodeModel
    {
        public Guid Pk_BarcodeID { get; set; }
        public string CustomerCode { get; set; }
        public string BarCode { get; set; }
        public bool IsActive { get; set; }
        public bool IsDelete { get; set; }
        public Guid? CreatedBy { get; set; }
        public Guid? ModifiedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string UserName { get; set; }
    }
    public class SearchBarCodeViewModel
    {
        public string CustomerCode { get; set; }
    }
}
