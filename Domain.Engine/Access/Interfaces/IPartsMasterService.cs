﻿using Domain.Objects.Models;
using System;
using System.Collections.Generic;


namespace Domain.Engine.Access.Interfaces
{
   public interface IPartsMasterService
    {
        ResponseviewModel InsertUpdatePartsMaster(PartsMasterModel partsmastermodel);
        ResponseviewModel CheckPartNumberExistsbyPartno(string PartNumber);
        ResponseviewModel GetPartsMasterByID(Guid id);
        ResponseviewModel GetAllListPartsMaster(PartsMasterModellist filter);
        ResponseviewModel DeletePartsMasterByID(PartsMasterModel part);
        ResponseviewModel CheckPartsMasterDetails(List<PartsMasterModel> lstpartmastermodel);
        ResponseviewModel UploadPartsMaster(List<PartsMasterModel> lstpartmastermodel);
        PartsMasterModel GetDetailsBynumber(string partNumber);
    }
}
