using Domain.Objects.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Engine.Access.Interfaces
{
    public interface IBinLocationService
    {
        ResponseviewModel GetDDLRepackedCaseNo();
        ResponseviewModel GetDDLItemCodeByRepackedcaseNo(string repackedcaseno);
        ResponseviewModel GetNoofPiecesAndQtyByItemCode(string RepackedCaseNo, string ItemCode);
        ResponseviewModel InsertBinLocation(BinLocationModel binlocationmodel);
        ResponseviewModel GetSortedBinLocation(SortedBinLocationModel model);
        ResponseviewModel GetSortedBinLocationExcel();
        ResponseviewModel GetSortedBinLocationItemCode(string itemcode);
        
        ResponseviewModel GetBinAvailableQTYByItemCode(string itemcode, string locationid);
        int GetBinAvailableQuantityByItemCode(string itemcode, string locationid);
        int GetAvailableQuantity(string itemcode, string caseno);
        ResponseviewModel InsertBinLocationHistory(BinLocationHistoryModel binlocationhistorymodel);
        ResponseviewModel InsertBinLocationHistoryLooseItem(BinLocationHistoryModel binlocationhistorymodel);
        ResponseviewModel GetBinPickedItem();
        ResponseviewModel GetLoosePickedItem();
        ResponseviewModel getRePackbyCode(string customercode);
        ResponseviewModel GetBinItemLocationHistory(string locationId, string itemcode);
        ResponseviewModel DeleteBinItemLocationbyID(BinLocationHistoryModel binLocationHistory);
        ResponseviewModel DeleteBinInLocationbyID(SortingMasterModel sortingmaster);
  }
}
