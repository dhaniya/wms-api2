﻿using Domain.Objects.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Engine.Access.Interfaces
{
    public interface IReportService
    {
        ResponseviewModel PartReportDetails(PartReportDetailsModel searchmodel);
        ResponseviewModel GetPartReportExcelExport(PartReportDetailsModelExcelExport searchmodel);
        ResponseviewModel GetIncomingShipmentSummaryReport(IncomingShipmentSummaryReportFilterModel searchmodel); 
        ResponseviewModel GetIncomingShipmentSummaryReportExcelExport(IncomingShipmentSummaryReportFilterExcelExportModel searchmodel);
        ResponseviewModel Discrepancy(DiscrepancyFilterModel searchmodel);
        ResponseviewModel DiscrepancyExcelExport(DiscrepancyFilterModel searchmodel); 
         ResponseviewModel GetShipmentReport(ShipmentReportFilterView filterdata);
        ResponseviewModel GetShipmentReportExcelExport(ShipmentReportFilterExcelView filterdata);
        ResponseviewModel GetDeliveryListingReport(DeliveryListingReportFilterView filterdata);
        ResponseviewModel GetDeliveryListingSummaryReport(DeliveryListingReportFilterView filterdata);
        ResponseviewModel GetDeliveryListingReportExcelExport(DeliveryListingReportFilterExcelExportView filterdata);
        ResponseviewModel GetDeliveryListingSummaryReportExcelExport(DeliveryListingReportFilterExcelExportView filterdata);
        ResponseviewModel GetPartsTransactionsList(GetPartsTransactionsListView filterdata);
        ResponseviewModel GetReallocationList(GetRealocationsListView filterdata);
        ResponseviewModel GetPartsTransactionsListExcelExport(GetPartsTransactionsListView filterdata);
        ResponseviewModel GetPartsTransactionsBinList(GetPartsTransactionsBinList filterdata);
        ResponseviewModel GetPartsTransactionsBinDoList(GetPartsTransactionsBinDoList filterdata);
        ResponseviewModel GetRealocationsListExcelExport();
        ResponseviewModel GetStockAgingReports(GetStockAgingReportsListView filterdata);
        ResponseviewModel GetStockAgingReportsExcelExport();
        ResponseviewModel GetStockAgingFeatchSP();

    }
}
