﻿using Domain.Objects.Models;
using System;
using System.Collections.Generic;

namespace Domain.Engine.Access.Interfaces
{
    public interface IBinMasterService
    {
        ResponseviewModel InsertBinMaster(List<BinMasterModel> binmastermodel);
        ResponseviewModel GetAllBinMasterList();
        ResponseviewModel GetRackName();
        ResponseviewModel GetDLLOfBinLocationWithLocationTransfer();
        ResponseviewModel Deleteitemtransfer(Guid id);
    }
}
