﻿using Domain.Objects.Models;
using System;


namespace Domain.Engine.Access.Interfaces
{
    public interface ICustomerMasterService
    {
        ResponseviewModel InsertUpdateCustomerMaster(CustomerMasterViewModel customermasterviewmodel);
        ResponseviewModel GetCustomerMasterByID(Guid id);
        ResponseviewModel GetAllListCustomerMaster();
        ResponseviewModel DeleteCustomerMasterByID(CustomerMasterViewModel customer );
        
        ResponseviewModel GetDropdownofCustomerMaster();
        ResponseviewModel GetDropdownofCustomerCodeByID(Guid id);
        ResponseviewModel GetDuplicateCustomerCodeCheck(string code, string custid);
    }
}
