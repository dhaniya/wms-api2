﻿using Domain.Objects.Models;
using System;
using System.Collections.Generic;

namespace Domain.Engine.Access.Interfaces
{
    public interface IGoodReceiptService
    {
        ResponseviewModel InsertUpdateGoodReceipt(GoodsReceiptMasterViewModel goodsreceiptmasterviewmodel);
    }
}
