﻿using Domain.Objects.Models;
using System;
using System.Collections.Generic;

namespace Domain.Engine.Access.Interfaces
{
    public interface IBarCodeService
    {
        ResponseviewModel GetBarCodeGenerate(string customercode,int number,Guid barcode);
        ResponseviewModel GetBarCodeList(string customercode);
        ResponseviewModel RePrintBarcode(List<BarCodeModel> lstbarcode);
        List<BarCodeModel> GetBarCodeListByCustomerCode(string customercode);
        ResponseviewModel GetDDLBarcodeByCustomerCodeandinvoiceno(string customercode);
        ResponseviewModel CheckBarcodeValidOrNot(string Barcode);
        Boolean ChangeActiveByBarcode(string barcode);
        Boolean ChangeDeactiveByBarcode(string barcode);
    }
    
}
