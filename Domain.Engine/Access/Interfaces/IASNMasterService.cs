﻿using Domain.Objects.Models;
using System;
using System.Collections.Generic;

namespace Domain.Engine.Access.Interfaces
{
    public interface IASNMasterService
    {
        #region ASN 
        ResponseviewModel InsertUpdateAsnMaster(ASNMasterModels asnmastermodels);
        ResponseviewModel UploadASNandItemDetail(ASNandlistasnitemsModel asnandlistasnitemsmodel);
        ResponseviewModel GetDropdownofInvoiceNo();
        ResponseviewModel GetASNMasterDetailByInvoiceNo(string InvoiceNo);
        ResponseviewModel GetContainerNoByAsndata(string InvoiceNo, string CaseNo); 
        ResponseviewModel GetDuplicateInvoiceNoCheck(string InvoiceNo);
        ResponseviewModel GetCustomerCodes(string InvoiceNo);
        #endregion

        #region Good Receipt
        ResponseviewModel GetDropdownofInvoiceNoByDateofArrival(DateTime DateOfArrival);
        ResponseviewModel AddGoodsReceipts(GoodsReceiptsModel goodsreceiptsmodel);
        ResponseviewModel GetGoodsReceiptsByDateOfArrival(DateTime DateOfArrival);
        ResponseviewModel GetGoodsReceiptsByInvoiceNo(string InvoiceNo);
        ResponseviewModel GetDropdownofInvoiceforGoodReceipt();
        #endregion

        #region Sorting
        ResponseviewModel GetDropdownOfReceivedInvoiceNo();
        ResponseviewModel GetDropdownOfCaseNoByInvoiceNo(String invoiceno);
        ResponseviewModel GetDropdownOfCustomerCodeByAsnDetail(SortingItemDetail customeritemdetail);
        ResponseviewModel GetCustomerDataByInvoiceAndCaseNo(string invoiceno , string caseno);
        ResponseviewModel GetLocationIdByAsnDetail(SortingItemDetail customeritemdetail);
        ResponseviewModel GetAvailableQuantityByAsnDetail(SortingItemDetail sortingitemdetail);
        #endregion

        int? GetAvailableQuantityByAsnDetail(string InvoiceNo, string CaseNo, string Customer, string SuppliedPartNo);
        ResponseviewModel DeleteGoodReciptByID(string invoicenumber, string containernumber, string palletnnumber,Guid id);
        ResponseviewModel CheckASNDetails(List<ASNItemDetailModels> lstasndetails);

        ResponseviewModel GetTodayAsnMaster(TodayAsnMasterViewModel todayasnmasterviewmodel);

        ResponseviewModel GetGoodReceiptList(GoodReceiptListViewModel goodreceiptlistviewmodel);
        ResponseviewModel DeleteASN(Guid Asnid);
        ResponseviewModel ASNInfoByInvoiceNo(string invoiceno);
    }
}
