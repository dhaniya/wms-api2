﻿using Domain.Objects.Models;
using System;

namespace Domain.Engine.Access.Interfaces
{
    public interface ISupplierMasterService
    {
        ResponseviewModel InsertUpdateSupplierMaster(SupplierMasterModels suppliermastermodel);
        ResponseviewModel GetSupplierMasterByID(Guid id);
        ResponseviewModel GetAllListSupplierMaster();
        ResponseviewModel DeleteSupplierMasterByID(SupplierMasterModels supplier);
        ResponseviewModel GetDropdownofSupplierMaster();
    }
}
