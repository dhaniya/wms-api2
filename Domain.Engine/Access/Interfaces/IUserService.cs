﻿using Domain.Objects.Models;
using System;
using System.Collections.Generic;

namespace Domain.Engine.Access.Interfaces
{
   public interface IUserService
    {
        ResponseviewModel Login(LoginViewModel loginviewmodel);
        ResponseviewModel InsertUpdateUser(UserMasterViewModel usermasterviewmodel);
        ResponseviewModel ChangePassword(ChangePasswordViewModel usermasterviewmodel);
        ResponseviewModel GetDDLRoleType();
        ResponseviewModel GetUserDetailById(Guid id);
        ResponseviewModel DeleteUserById(UserMasterViewModel useru);
        ResponseviewModel GetAllUser();
    }
}
