﻿using Domain.Objects.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Engine.Access.Interfaces
{
  public  interface IUserAccessService
    {
        ResponseviewModel GetListRoutes();
        ResponseviewModel AddUserAccess(List<UserAccessViewModel> model);
        ResponseviewModel GetUserAccessByid(Guid userId);

    }
}
