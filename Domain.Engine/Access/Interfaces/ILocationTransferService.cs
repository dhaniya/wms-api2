﻿using Domain.Objects.Models;
using System;
using System.Collections.Generic;

namespace Domain.Engine.Access.Interfaces
{
    public interface ILocationTransferService
    {
        ResponseviewModel GetLocationTransferlist(searchlocationtransfer searchlocationtransfer);
        ResponseviewModel UpdateLocationBysortingid(UpdateLocationTransferModel updatelocationtransfermodel);
        ResponseviewModel GetDropdownofRepackedCaseNo();
        ResponseviewModel GetDropdownofLocationId();
    }
}
