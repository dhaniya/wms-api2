﻿using Domain.Objects.Models;
using System;
using System.Collections.Generic;

namespace Domain.Engine.Access.Interfaces
{
    public interface IASNItemDetailService
    {
        ResponseviewModel InsertUpdateAsnItemDetail(ASNItemDetailModels asnitemdetailmodels);
        List<ASNItemDetailModels> GetAsnItemsDetails(Guid? Pk_ASNMasterID);
    }
}
