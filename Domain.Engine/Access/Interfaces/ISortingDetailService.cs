﻿using Domain.Objects.Models;
using System;
using System.Collections.Generic;

namespace Domain.Engine.Access.Interfaces
{
    public interface ISortingDetailService
    {
        ResponseviewModel InsertUpdateSortingDetail(SortingDetailModels sortingdetailmodel);
        ResponseviewModel UpdateSortingDetailList(List<SortingDetailModels> Listsortingdetailmodel);
        ResponseviewModel GetAllSortingDetailList();
        ResponseviewModel GetSortingDetailByRepackedCaseNo(string RepackedCaseNo,string invoicenumber);
        ResponseviewModel DeleteSortingDetailBySortingMasterID(Guid Fk_SortingMasterID);
        ResponseviewModel GetSortingDetailByInvoiceNo(string InvoiceNo ,bool removed);
        ResponseviewModel GetSortingDetailsDemo();
    }
}
