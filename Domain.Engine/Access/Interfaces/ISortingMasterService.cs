﻿using Domain.Objects.Models;
using System;

namespace Domain.Engine.Access.Interfaces
{
    public interface ISortingMasterService
    {
        ResponseviewModel InsertUpdateSortingMaster(SortingMasterModel sortingmastermodel);
        ResponseviewModel DeleteSortingMasterbyID(SortingMasterModel sortingm);
        ResponseviewModel GetAllSortingMaster();
        int GetAvailableQuantityByAsnDetail(string InvoiceNo, string CaseNo, string Customer, string SuppliedPartNo);
        ResponseviewModel AddPutAwayBox(PutAwayViewModel putawayviewmodel);
        ResponseviewModel GetPutAwayList();
        ResponseviewModel GetPutAwayListByDate(DateTime fromdate, DateTime todate);
        ResponseviewModel GetPutAwayListInvoice(string invoice);
        ResponseviewModel GetInventorySearchForRack(InventorySearchViewModel model);
        ResponseviewModel InventorySearchForRackExcelExport(InventorySearchViewModel model); 
         ResponseviewModel GetInventorySearchForBin(InventorySearchViewModel model);
        ResponseviewModel GetInventorySearchForBinExcelExport(InventorySearchViewModel model);
    }
}
