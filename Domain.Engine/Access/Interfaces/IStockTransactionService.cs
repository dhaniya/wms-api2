﻿using Domain.Engine.Access.Impl;
using Domain.Objects.Models;
using Infrastructure.Model.Main.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Engine.Access.Interfaces
{
    public interface IStockTransactionService
    {
        ResponseviewModel InsertStockTransaction(StockTransactionModel stocktransactionmodel);
        ResponseviewModel SteponPrepickingInsert(StockTransactionModel stocktransactionmodel);
        ResponseviewModel SteponDOInsert(StockTransactionModel stocktransactionmodel);
    }
}
