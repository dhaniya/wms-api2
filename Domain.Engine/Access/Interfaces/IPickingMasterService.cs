using Domain.Objects.Models;
using System;
using System.Collections.Generic;

namespace Domain.Engine.Access.Interfaces
{
    public interface IPickingMasterService
    {
        ResponseviewModel GetDropdownofCustomerMaster();
        ResponseviewModel GetDropdownofCustomerCodeByID(Guid id);
        ResponseviewModel GetDropdownofPutAwayInvoiceNoByCustomerCode(string CustomerCode);
        ResponseviewModel GetDropdownofRepackedCaseNoByInvoiceno(string invoiceno, string CustomerCode);
        ResponseviewModel GetItemCodeByRepackedCaseNo(string repackedcaseno,string invoiceno);
        ResponseviewModel GetAvailableQuantityBySortingDetail(string itemcode, string caseno, string invoiceno);
        ResponseviewModel GetSortingdetailbyRepackedCaseno(string repackedcaseno,string itemcode, int pickingtype, string invoiceno);
        int GetAvailableQuantity(string origionalcaseno,string itemcode, string caseno, string invoiceno);
        ResponseviewModel InsertPickingMaster(PickingViewModel pickingviewmodel);
        ResponseviewModel GetPickingList(SearchPickingViewModel searchpickingviewmodel);
        ResponseviewModel GetPickingListALL(SearchPickingViewModel searchpickingviewmodel);
        ResponseviewModel GetPickingListbyCustomerCode(string customercode);
        ResponseviewModel GetPickingDetailsByPickingId(Guid id);
        int GetPickingStatusByPickingid(Guid pickingid);
        ResponseviewModel GetPendingPickingByPickingId(Guid id);
        ResponseviewModel GetPickingDetailByOrderNo(string OrderNo);
        ResponseviewModel UpdatePickingStatus(UpdatePickingViewModel updatepickingviewmodel);
        ResponseviewModel UpdatePickingMasterList(PickingDetailViewModel PickingViewModel);
    
        ResponseviewModel DeletePickingbyID(PickingMasterViewModel pickingmaster);
    string GenerateOrderNo();
    }
}
