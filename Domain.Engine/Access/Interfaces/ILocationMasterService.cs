﻿using Domain.Objects.Models;
using System;
using System.Collections.Generic;

namespace Domain.Engine.Access.Interfaces
{
    public interface ILocationMasterService
    {
        ResponseviewModel InsertLocationMaster(List<LocationMasterModel> listlocationmastermodel);
        ResponseviewModel GetAllLocationMasterList();
        ResponseviewModel GetDLLRackName();
        ResponseviewModel GetLocationMasterListbyRackName(string RackName);
        ResponseviewModel GetDLLLocationMaster(int LocationType, int ZoneType);
        Boolean ChangeActiveByLocationCode(string LocationCode);
        Boolean ChangeUnusedByLocationCode(string LocationCode);
    }
}
