using Domain.Objects.Models;
using System;
using System.Collections.Generic;

namespace Domain.Engine.Access.Interfaces
{
    public interface IDeliveryOrderService
    {
        ResponseviewModel GetPickedList(SearchPickedViewModel searchpickedviewmodel);
        ResponseviewModel GetAllPickedDetailsByPickingId(Guid id);
    ResponseviewModel GetAllpickedDetailById(Guid id); 
        ResponseviewModel GetAllPickedDetailsByPickingIdForEdit(Guid id); 
       ResponseviewModel CreateDeliveryOrder(CreateDeliveryOrderViewModel createdeliveryorderviewmodel);
        ResponseviewModel UpdateDeliveryOrder(CreateDeliveryOrderViewModel createdeliveryorderviewmodel);
        ResponseviewModel GetDeliveryOrderList(DeliveryOrderFilterViewModel modal);
        ResponseviewModel GetDeliveryOrderDropdownList();
        ResponseviewModel GetDeliveryOrderDetailList(string doorder);
        ResponseviewModel ConfirmDO(string doorder);
        ResponseviewModel UpdatePackingLocation(PickingLocationUpdate model);
        ResponseviewModel AddressDeliveryOrder();
    }
}
