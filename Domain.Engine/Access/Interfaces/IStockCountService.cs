﻿using Domain.Objects.Models;
using Infrastructure.Model.Main.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Engine.Access.Interfaces
{
  public  interface IStockCountService
    {
        ResponseviewModel GetStockCountList();
        ResponseviewModel GetRunningNumber();
        ResponseviewModel GetStockDiscrepancyData(StockCountFilter filterdata);
        ResponseviewModel GetAllRunningNumber();
        ResponseviewModel InsertUpdateStockCount(StockCountModel stockcountmodel);
        ResponseviewModel DeleteStockCountbyID(StockCountModel stockd);
    }
}
