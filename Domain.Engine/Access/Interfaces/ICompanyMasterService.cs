﻿using Domain.Objects.Models;
using System;

namespace Domain.Engine.Access.Interfaces
{
    public interface ICompanyMasterService
    {
        ResponseviewModel InsertUpdateCompanyMaster(CompanyMasterModels companymastermodels);
        ResponseviewModel GetCompanyMasterByID(Guid id);
        ResponseviewModel GetAllListCompanyMaster();
        ResponseviewModel DeleteCompanyMasterByID(CompanyMasterModels compny);
        ResponseviewModel GetDropdownofCompanyMaster();

    }
}
