﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Domain.Engine.Access.Interfaces;
using Domain.Objects.Models;
using Infrastructure.Model.Main;
using Infrastructure.Model.Main.Entities;


namespace Domain.Engine.Access.Impl
{
    public class CompanyMasterService : ICompanyMasterService
    {
        protected readonly MainDbContext _Context;
        protected MainDbContext Context
        {
            get
            {
                return (MainDbContext)_Context;
            }
        }

        public CompanyMasterService(IMainDbContext context)
        {
            _Context = (MainDbContext)context;
        }

        public ResponseviewModel InsertUpdateCompanyMaster(CompanyMasterModels companymastermodels)
        {
            ResponseviewModel result = new ResponseviewModel();
            try
            {
                if (companymastermodels.PK_CompanyID == null)
                {
                    CompanyMaster checkpartsmaster = new CompanyMaster();
                    checkpartsmaster = _Context.CompanyMaster.FirstOrDefault(d => d.CompanyID.ToLower().Trim() == companymastermodels.CompanyID.ToLower().Trim() && d.IsActive == true && d.IsDelete == false);
                    if (checkpartsmaster == null)
                    {
                        CompanyMaster companymaster = new CompanyMaster();
                        companymaster.PK_CompanyID = Guid.NewGuid();
                        companymaster.CompanyName = companymastermodels.CompanyName;
                        companymaster.CompanyID = companymastermodels.CompanyID;
                        companymaster.CompanyContactPerson = companymastermodels.CompanyContactPerson;
                        companymaster.ContactNo = companymastermodels.ContactNo;
                        companymaster.EmailID = companymastermodels.EmailID;
                        companymaster.Address = companymastermodels.Address;
                        companymaster.Description = companymastermodels.Description;
                        companymaster.IsDelete = false;
                        companymaster.IsActive = true;
                        companymaster.CreatedBy = companymastermodels.CreatedBy;
                        companymaster.CreatedDate = System.DateTime.Now;
                        companymaster.ModifiedBy = companymastermodels.CreatedBy;
                        companymaster.ModifiedDate = System.DateTime.Now;

                        _Context.AddAsync(companymaster);
                        _Context.SaveChanges();
                        result.Message = "Data Inserted Successfully";
                        result.StatusCode = 1;
                        result.IsValid = true;
                        result.Result = "success";
                        result.pk_ID = companymaster.PK_CompanyID;
                        result.Data = companymaster;
                    }
                    else
                    {
                        result.Message = "Company already exists";
                        result.StatusCode = 0;
                        result.IsValid = false;
                        result.Result = "error";
                        result.pk_ID = null;
                        result.Data = null;
                    }
                }
                else
                {
                    CompanyMaster companymaster = _Context.CompanyMaster.FirstOrDefault(d => d.PK_CompanyID == companymastermodels.PK_CompanyID && d.IsActive == true && d.IsDelete == false);
                    if (companymaster != null)
                    {
                        companymaster.CompanyName = companymastermodels.CompanyName;
                        companymaster.CompanyID = companymastermodels.CompanyID;
                        companymaster.ContactNo = companymastermodels.ContactNo;
                        companymaster.CompanyContactPerson = companymastermodels.CompanyContactPerson;
                        companymaster.EmailID = companymastermodels.EmailID;
                        companymaster.Address = companymastermodels.Address;
                        companymaster.Description = companymastermodels.Description;
                        companymaster.IsDelete = false;
                        companymaster.IsActive = true;
                        companymaster.ModifiedDate = System.DateTime.Now;
                        companymaster.ModifiedBy = companymastermodels.ModifiedBy;
                        _Context.Update(companymaster);
                        _Context.SaveChanges();
                        result.Message = "Data Updated Successfully";
                        result.StatusCode = 1;
                        result.IsValid = true;
                        result.Result = "success";
                        result.pk_ID = companymaster.PK_CompanyID;
                        result.Data = companymaster;
                    }
                    else
                    {
                        result.Message = "Record does not exists";
                        result.StatusCode = 0;
                        result.IsValid = false;
                        result.Result = "error";
                        result.pk_ID = companymaster.PK_CompanyID;
                        result.Data = null;
                    }
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.pk_ID = null;
                result.Data = null;
            }
            return result;
        }
       
        public ResponseviewModel GetCompanyMasterByID(Guid id)
        {
            ResponseviewModel result = new ResponseviewModel();
            try
            {
                var companymaster = _Context.CompanyMaster.FirstOrDefault(d => d.PK_CompanyID == id && d.IsActive == true && d.IsDelete == false);
                if (companymaster != null)
                {
                    result.Message = "Record Get Successfully";
                    result.StatusCode = 1;
                    result.IsValid = true;
                    result.Result = "success";
                    result.pk_ID = companymaster.PK_CompanyID;
                    result.Data = companymaster;
                }
                else
                {
                    result.Message = "Record does not exists";
                    result.StatusCode = 0;
                    result.IsValid = false;
                    result.Result = "error";
                    result.pk_ID = id;
                    result.Data = null;
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.pk_ID = id;
                result.Data = null;
            }
            return result;
        }
       
        public ResponseviewModel GetAllListCompanyMaster()
        {
            ResponseviewModel result = new ResponseviewModel();
            try
            {
                var companymaster = _Context.CompanyMaster.Where(d => d.IsActive == true && d.IsDelete == false).OrderByDescending(a=>a.CreatedDate).ToList();
                if (companymaster != null)
                {
                    result.Message = "Record Get Successfully";
                    result.StatusCode = 1;
                    result.IsValid = true;
                    result.Result = "success";
                    result.Data = companymaster;
                }
                else
                {
                    result.Message = "Record does not exists";
                    result.StatusCode = 0;
                    result.IsValid = false;
                    result.Result = "error";
                    result.Data = null;
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.Data = null;
            }
            return result;
        }

        public ResponseviewModel DeleteCompanyMasterByID(CompanyMasterModels compny)
        {
            ResponseviewModel result = new ResponseviewModel();
            try
            {
                var companymaster = _Context.CompanyMaster.Where(d => d.PK_CompanyID == compny.PK_CompanyID && d.IsActive == true && d.IsDelete == false).FirstOrDefault();
                if (companymaster != null)
                {
                    companymaster.IsDelete = true;
                    companymaster.CreatedBy = compny.CreatedBy;
                    _Context.Update(companymaster);
                    _Context.SaveChanges();

                    result.Message = "Record Deleted Successfully";
                    result.StatusCode = 1;
                    result.IsValid = true;
                    result.Result = "success";
                    result.pk_ID = companymaster.PK_CompanyID;
                }
                else
                {
                    result.Message = "Record does not exists";
                    result.StatusCode = 0;
                    result.IsValid = false;
                    result.Result = "error";
                    result.Data = null;
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.Data = null;
            }
            return result;
        }

        public ResponseviewModel GetDropdownofCompanyMaster()
        {
            ResponseviewModel result = new ResponseviewModel();
            try
            {
                var companymaster = _Context.CompanyMaster.Where(d => d.IsActive == true && d.IsDelete == false).Select(x=> new DropdownViewModel
                {
                    ID = x.PK_CompanyID,
                    Value = x.CompanyName
                }).ToList();

                if (companymaster.Count() > 0)
                {
                    result.Message = "Record Get Successfully";
                    result.StatusCode = 1;
                    result.IsValid = true;
                    result.Result = "success";
                    result.Data = companymaster;
                }
                else
                {
                    result.Message = "Record does not exists";
                    result.StatusCode = 0;
                    result.IsValid = false;
                    result.Result = "error";
                    result.Data = null;
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.Data = null;
            }
            return result;
        }
    }
}
