﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper;
using Domain.Engine.Access.Interfaces;
using Domain.Objects.Models;
using Infrastructure.Model.Main;
using Infrastructure.Model.Main.Entities;
using Microsoft.EntityFrameworkCore;

namespace Domain.Engine.Access.Impl
{
    public class PartsMasterService : IPartsMasterService
    {
        protected readonly MainDbContext _Context;
        private readonly IMapper _mapper;
        protected MainDbContext Context
        {
            get
            {
                return (MainDbContext)_Context;
            }
        }

        public PartsMasterService(IMainDbContext context, IMapper mapper)
        {
            _Context = (MainDbContext)context;
            _mapper = mapper;
        }

        public ResponseviewModel InsertUpdatePartsMaster(PartsMasterModel partsmastermodel)
        {
            ResponseviewModel result = new ResponseviewModel();
            try
            {
                if (partsmastermodel.Pk_PartsMasterID == null || partsmastermodel.Pk_PartsMasterID == Guid.Empty)
                {
                    PartsMaster checkpartsmaster = new PartsMaster();
                    checkpartsmaster = _Context.PartsMaster.FirstOrDefault(d => d.PartNumber.ToLower().Trim() == partsmastermodel.PartNumber.ToLower().Trim() && d.IsActive == true && d.IsDelete == false);
                    if (checkpartsmaster == null)
                    {
                        PartsMaster partsmaster = new PartsMaster();
                        partsmaster.Pk_PartsMasterID = Guid.NewGuid();
                        partsmaster.PartNumber = partsmastermodel.PartNumber;
                        partsmaster.PartDescription = partsmastermodel.PartDescription;
                        partsmaster.Height = partsmastermodel.Height;
                        partsmaster.Weight = partsmastermodel.Weight;
                        partsmaster.Breadth = partsmastermodel.Breadth;
                        partsmaster.length = partsmastermodel.length;
                        partsmaster.PartUnitPrice = partsmastermodel.PartUnitPrice;
                        partsmaster.IsDelete = false;
                        partsmaster.IsActive = true;
                        partsmaster.CreatedBy = partsmastermodel.CreatedBy;
                        partsmaster.CreatedDate = System.DateTime.Now;
                        partsmaster.ModifiedBy = partsmastermodel.CreatedBy;
                        partsmaster.ModifiedDate = System.DateTime.Now;
                        _Context.AddAsync(partsmaster);
                        _Context.SaveChanges();
                        result.Message = "Data Inserted Successfully";
                        result.StatusCode = 1;
                        result.IsValid = true;
                        result.Result = "success";
                        result.pk_ID = partsmaster.Pk_PartsMasterID;
                        result.Data = partsmaster;
                    }
                    else
                    {
                        result.Message = "Part number already exists";
                        result.StatusCode = 0;
                        result.IsValid = false;
                        result.Result = "error";
                        result.pk_ID = null;
                        result.Data = null;
                    }
                }
                else
                {
                    PartsMaster partsmaster = _Context.PartsMaster.FirstOrDefault(d => d.Pk_PartsMasterID == partsmastermodel.Pk_PartsMasterID && d.IsActive == true && d.IsDelete == false);
                    if (partsmaster != null)
                    {
                        partsmaster.PartNumber = partsmastermodel.PartNumber;
                        partsmaster.PartDescription = partsmastermodel.PartDescription;
                        partsmaster.Height = partsmastermodel.Height;
                        partsmaster.Weight = partsmastermodel.Weight;
                        partsmaster.Breadth = partsmastermodel.Breadth;
                        partsmaster.length = partsmastermodel.length;
                        partsmaster.PartUnitPrice = partsmastermodel.PartUnitPrice;
                        partsmaster.IsDelete = false;
                        partsmaster.IsActive = true;
                        partsmaster.ModifiedDate = System.DateTime.Now;
                        partsmaster.ModifiedBy = partsmastermodel.ModifiedBy;
                        _Context.Update(partsmaster);
                        _Context.SaveChanges();
                        result.Message = "Data Updated Successfully";
                        result.StatusCode = 1;
                        result.IsValid = true;
                        result.Result = "success";
                        result.pk_ID = partsmaster.Pk_PartsMasterID;
                        result.Data = partsmaster;
                    }
                    else
                    {
                        result.Message = "Record does not exists";
                        result.StatusCode = 0;
                        result.IsValid = false;
                        result.Result = "error";
                        result.pk_ID = partsmaster.Pk_PartsMasterID;
                        result.Data = null;
                    }
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.pk_ID = null;
                result.Data = null;
            }
            return result;
        }

        public ResponseviewModel CheckPartNumberExistsbyPartno(string PartNumber)
        {
            ResponseviewModel result = new ResponseviewModel();
            try
            {
                PartsMaster checkpartsmaster = new PartsMaster();
                checkpartsmaster = _Context.PartsMaster.FirstOrDefault(d => d.PartNumber.ToLower().Trim() == PartNumber.ToLower().Trim() && d.IsActive == true && d.IsDelete == false);
                if (checkpartsmaster == null)
                {
                    result.Message = "Part Number is not available.";
                    result.StatusCode = 1;
                    result.IsValid = true;
                    result.Result = "success";
                }
                else
                {
                    result.Message = "Part Number already exists.!";
                    result.StatusCode = 0;
                    result.IsValid = false;
                    result.Result = "error";
                    result.pk_ID = checkpartsmaster.Pk_PartsMasterID;
                    result.Data = checkpartsmaster;
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.pk_ID = null;
                result.Data = null;
            }
            return result;
        }
        public ResponseviewModel GetPartsMasterByID(Guid id)
        {
            ResponseviewModel result = new ResponseviewModel();
            try
            {
                var partsmaster = _Context.PartsMaster.FirstOrDefault(d => d.Pk_PartsMasterID == id && d.IsActive == true && d.IsDelete == false);
                if (partsmaster != null)
                {
                    result.Message = "Record Get Successfully";
                    result.StatusCode = 1;
                    result.IsValid = true;
                    result.Result = "success";
                    result.pk_ID = partsmaster.Pk_PartsMasterID;
                    result.Data = partsmaster;
                }
                else
                {
                    result.Message = "Record does not exists";
                    result.StatusCode = 0;
                    result.IsValid = false;
                    result.Result = "error";
                    result.pk_ID = id;
                    result.Data = null;
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.pk_ID = id;
                result.Data = null;
            }
            return result;
        }

        public ResponseviewModel GetAllListPartsMaster(PartsMasterModellist filter)
        {
            ResponseviewModel result = new ResponseviewModel();
            try
            {
                PagedItemSet<PartsMasterModellist> pagedItems = new PagedItemSet<PartsMasterModellist>();

                this._Context.Database.SetCommandTimeout(1000);
                //List<PartsMaster> partsmasterlist = new List<PartsMaster>();
                //var partsmasterlist = _Context.PartsMaster.Where(d => d.IsActive == true && d.IsDelete == false).Select(a => new PartsMasterModellist

                var partsmasterlist = _Context.PartsMaster.Where(d => d.IsActive == true && d.IsDelete == false && d.PartDescription.Contains((string.IsNullOrEmpty(filter.search) ? d.PartDescription : filter.search)) || d.PartNumber.ToLower().Contains((String.IsNullOrEmpty(filter.search) ? d.PartNumber.ToLower() : filter.search))).Select(a => new PartsMasterModellist
                {
                    PartNumber = a.PartNumber,
                    PartDescription = a.PartDescription,
                    PartUnitPrice = a.PartUnitPrice,
                    length = a.length,
                    Height = a.Height,
                    Weight = a.Weight,
                    Breadth = a.Breadth,
                    CreatedDate = a.CreatedDate,
                    Pk_PartsMasterID = a.Pk_PartsMasterID
                }).OrderByDescending(a => a.CreatedDate).AsQueryable();
                pagedItems.TotalResults = partsmasterlist.Count();
                if (filter.Skip.HasValue)
                {
                    partsmasterlist = partsmasterlist.Skip(filter.Skip.Value);
                }
                else
                {
                    partsmasterlist = partsmasterlist.Skip(0);
                }

                if (filter.Take.HasValue)
                {
                    partsmasterlist = partsmasterlist.Take(filter.Take.Value);
                }
                else
                {
                    partsmasterlist = partsmasterlist.Take(10);
                }
                pagedItems.Items = partsmasterlist.ToList();

                if (pagedItems.TotalResults > 0)
                {
                    result.Message = "Record Get Successfully";
                    result.StatusCode = 1;
                    result.IsValid = true;
                    result.Result = "success";
                    result.Data = pagedItems;
                }
                else
                {
                    result.Message = "Record does not exists";
                    result.StatusCode = 0;
                    result.IsValid = false;
                    result.Result = "error";
                    result.Data = null;
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.Data = null;
            }
            return result;
        }

        public ResponseviewModel DeletePartsMasterByID(PartsMasterModel part)
        {
            ResponseviewModel result = new ResponseviewModel();
            try
            {
                var partsmaster = _Context.PartsMaster.Where(d => d.Pk_PartsMasterID == part.Pk_PartsMasterID && d.IsActive == true && d.IsDelete == false).FirstOrDefault();
                if (partsmaster != null)
                {
                    partsmaster.IsDelete = true;
                    partsmaster.ModifiedBy = part.ModifiedBy;
                    _Context.Update(partsmaster);
                    _Context.SaveChanges();

                    result.Message = "Record Deleted Successfully";
                    result.StatusCode = 1;
                    result.IsValid = true;
                    result.Result = "success";
                    result.pk_ID = partsmaster.Pk_PartsMasterID;
                }
                else
                {
                    result.Message = "Record does not exists";
                    result.StatusCode = 0;
                    result.IsValid = false;
                    result.Result = "error";
                    result.Data = null;
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.Data = null;
            }
            return result;
        }


        public ResponseviewModel CheckPartsMasterDetails(List<PartsMasterModel> lstpartmastermodel)
        {
            ResponseviewModel result = new ResponseviewModel();
            List<PartsMasterMismatch> lstpartsmastermismatch = new List<PartsMasterMismatch>();
            try
            {
                Boolean ismismatch = false;
                PartsMasterMismatch partsmasterMismatch = new PartsMasterMismatch();
                foreach (var item in lstpartmastermodel)
                {
                    partsmasterMismatch = _mapper.Map<PartsMasterMismatch>(item);
                    if (_Context.PartsMaster.Count(c => c.PartNumber.Contains(item.PartNumber) && c.IsDelete == false) == 0)
                    {
                        ismismatch = true;
                        partsmasterMismatch.isPartNumber = true;

                    }
                    if (ismismatch)
                    {
                        lstpartsmastermismatch.Add(partsmasterMismatch);
                    }
                }
                result.Message = "Mismatch list";
                result.StatusCode = 1;
                result.IsValid = false;
                result.Result = "success";
                result.Data = lstpartsmastermismatch;
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.pk_ID = null;
                result.Data = null;
            }
            return result;
        }


        public ResponseviewModel UploadPartsMaster(List<PartsMasterModel> lstpartmastermodel)
        {
            ResponseviewModel result = new ResponseviewModel();
            try
            {
                if (lstpartmastermodel.Count > 0)
                {
                    lstpartmastermodel.ForEach(d => d.Pk_PartsMasterID = Guid.Empty);
                    int missMatchCount = 0;

                    foreach (var item in lstpartmastermodel)
                    {
                        bool isExist = false;
                        isExist = _Context.PartsMaster.Where(a => a.PartNumber.ToLower().Trim() == item.PartNumber.ToLower().Trim()).Any();
                        if (!isExist)
                        {
                            ResponseviewModel resultpartsmaster = InsertUpdatePartsMaster(item);
                        }
                        else
                        {
                            missMatchCount = missMatchCount + 1;
                        }
                    }
                    if (missMatchCount > 0)
                    {
                        result.Message = "Miss Match Found " + missMatchCount + ". data inserted successfully";
                        result.StatusCode = 1;
                        result.IsValid = true;
                        result.Result = "success";
                        result.Data = lstpartmastermodel;
                    }
                    else
                    {
                        result.Message = "data inserted  successfully";
                        result.StatusCode = 1;
                        result.IsValid = true;
                        result.Result = "success";
                        result.Data = lstpartmastermodel;
                    }
                }
                else
                {
                    result.Message = "Please insert valid data";
                    result.StatusCode = 0;
                    result.IsValid = false;
                    result.Result = "error";
                    result.pk_ID = null;
                    result.Data = null;
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.pk_ID = null;
                result.Data = null;
            }
            return result;
        }

        public PartsMasterModel GetDetailsBynumber(string partNumber)
        {
            _Context.Database.SetCommandTimeout(1000);
            PartsMasterModel checkpartsmaster = new PartsMasterModel();
            checkpartsmaster = _Context.PartsMaster.Where(d => d.PartNumber.ToLower().Trim() == partNumber.ToLower().Trim() && d.IsActive == true && d.IsDelete == false).Select(p => new PartsMasterModel
            {
                Breadth = p.Breadth,

                CreatedDate = p.CreatedDate,
                PartDescription = p.PartDescription,
                Height = p.Height,
                length = p.length,
                PartNumber = p.PartNumber,
                PartUnitPrice = p.PartUnitPrice,
                Pk_PartsMasterID = p.Pk_PartsMasterID,
                Weight = p.Weight
            }).FirstOrDefault();

            return checkpartsmaster;
        }
    }
}
