﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Domain.Engine.Access.Interfaces;
using Domain.Objects.Models;
using Infrastructure.Model.Main;
using Infrastructure.Model.Main.Entities;

namespace Domain.Engine.Access.Impl
{
    public class BarCodeService : IBarCodeService
    {
        protected readonly MainDbContext _Context;
        protected MainDbContext Context
        {
            get
            {
                return (MainDbContext)_Context;
            }
        }



        public BarCodeService(IMainDbContext context)
        {
            _Context = (MainDbContext)context;
        }


        public ResponseviewModel GetBarCodeGenerate(string customercode, int number, Guid id)
        {
            ResponseviewModel result = new ResponseviewModel();
            try
            {
                int start = 0;
                var barcode = _Context.BarcodeGenerate.Where(d => d.CustomerCode.ToLower().Trim() == customercode.ToLower().Trim() && d.IsDelete == false).OrderByDescending(a => a.CreatedDate)
                    .Max(k => k.BarCode);
                if (barcode != null)
                {
                    start = Convert.ToInt32(barcode.Replace(customercode + "-GW-", ""));


                }
                List<BarcodeGenerate> barcodes = new List<BarcodeGenerate>();
                for (int i = 1; i <= number; i++)
                {
                    barcodes.Add(new BarcodeGenerate
                    {
                        BarCode = customercode + "-GW-" + (start + i).ToString("0000"),
                        CustomerCode = customercode,
                        IsUsed = false,
                        IsActive = true,
                        IsDelete = false,
                        CreatedBy = id,
                        CreatedDate = System.DateTime.Now
                    });
                }
                _Context.BarcodeGenerate.AddRange(barcodes);
                _Context.SaveChanges();
                SaveTextFile(barcodes.Select(a => a.BarCode).ToList());
                result.Message = "Data Inserted Successfully";
                result.StatusCode = 1;
                result.IsValid = true;
                result.Result = "success";
                result.Data = barcodes;

            }

            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.pk_ID = null;
                result.Data = null;

            }
            return result;
        }

        public ResponseviewModel GetBarCodeList(string customercode)
        {
            ResponseviewModel result = new ResponseviewModel();
            try
            {
                List<BarCodeModel> barcodes = new List<BarCodeModel>();
                //var FinalArray [
               barcodes = _Context.BarcodeGenerate.Where(d => d.CustomerCode.ToLower().Trim() == customercode.ToLower().Trim() && d.IsDelete == false).OrderByDescending(k => k.CreatedDate)
                .Select(x => new BarCodeModel
                {
                    Pk_BarcodeID = x.Pk_BarcodeID,
                    BarCode = x.BarCode,
                    CustomerCode = customercode,
                    UserName = x.UserMaster.FirstName,
                }).OrderBy(p => p.CreatedDate).ToList();

                if (barcodes.Count > 0)
                {
                    result.Message = "Record Get Successfully";
                    result.StatusCode = 1;
                    result.IsValid = true;
                    result.Result = "success";
                    result.Data = barcodes;
                }
                else
                {
                    result.Message = "Record does not exists";
                    result.StatusCode = 0;
                    result.IsValid = false;
                    result.Result = "error";
                    result.pk_ID = null;
                    result.Data = null;
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.pk_ID = null;
                result.Data = null;
            }
            return result;
        }

        public ResponseviewModel RePrintBarcode(List<BarCodeModel> lstbarcode)
        {
            SaveTextFile(lstbarcode.Select(k => k.BarCode).ToList());
            ResponseviewModel result = new ResponseviewModel();
            result.Message = "Record Get Successfully";
            result.StatusCode = 1;
            result.IsValid = true;
            result.Result = "success";
            result.Data = lstbarcode;
            return result;
        }

        public Boolean ChnageActive(Guid id)
        {
            var change = _Context.BarcodeGenerate.Where(d => d.Pk_BarcodeID == id).FirstOrDefault();
            change.IsUsed = true;
            change.ModifiedDate = System.DateTime.Now;
            _Context.BarcodeGenerate.Update(change);
            _Context.SaveChanges();
            return true;
        }
        public Boolean SaveTextFile(List<String> barcodes)
        {


            // Write the string array to a new file named "WriteLines.txt".
            using (StreamWriter outputFile = new StreamWriter(Path.Combine("Barcodes", System.DateTime.Now.ToString("dddd, d MMM HHmm") + ".txt")))
            {
                foreach (string line in barcodes)
                    outputFile.WriteLine(line);
            }
            return true;
        }

        public List<BarCodeModel> GetBarCodeListByCustomerCode(string customercode)
        {
            ResponseviewModel result = new ResponseviewModel();
            List<BarCodeModel> barcodes = new List<BarCodeModel>();
            try
            {
                barcodes = _Context.BarcodeGenerate.Where(d => d.CustomerCode.ToLower().Trim() == customercode.ToLower().Trim() && d.IsActive == true && d.IsDelete == false && d.IsUsed == false).OrderByDescending(k => k.CreatedDate)
                .Select(x => new BarCodeModel
                {
                    Pk_BarcodeID = x.Pk_BarcodeID,
                    BarCode = x.BarCode
                }).OrderBy(p => p.CreatedDate).ToList();
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.pk_ID = null;
                result.Data = null;
            }
            return barcodes;

        }

        public ResponseviewModel GetDDLBarcodeByCustomerCodeandinvoiceno(string customercode)
        {
            ResponseviewModel result = new ResponseviewModel();
            List<StringDropdownviewModel> barcodemodellist = new List<StringDropdownviewModel>();
            try
            {

                barcodemodellist = _Context.BarcodeGenerate.Where(d => d.CustomerCode.ToLower().Trim() == customercode.ToLower().Trim() && d.IsUsed == false && d.IsActive == true).OrderByDescending(k => k.CreatedDate)
                .Select(x => new StringDropdownviewModel
                {
                    ID = x.BarCode,
                    Value = x.BarCode
                }).OrderBy(p => p.ID).ToList();
                if (barcodemodellist.Count > 0)
                {
                    result.Message = "Record get successfully";
                    result.StatusCode = 1;
                    result.IsValid = true;
                    result.Result = "success";
                    result.Data = barcodemodellist;
                }
                else
                {
                    result.Message = "Record does not exists";
                    result.StatusCode = 0;
                    result.IsValid = false;
                    result.Result = "error";
                    result.Data = null;
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.pk_ID = null;
                result.Data = null;
            }
            return result;
        }

        public ResponseviewModel CheckBarcodeValidOrNot(string Barcode)
        {
            ResponseviewModel result = new ResponseviewModel();
            try
            {
                BarcodeGenerate barcodegenerate = new BarcodeGenerate();
                barcodegenerate = _Context.BarcodeGenerate.Where(d => d.BarCode.ToLower().Trim() == Barcode.ToLower().Trim() && d.IsActive == true && d.IsDelete == false).FirstOrDefault();
                if (barcodegenerate != null)
                {
                    result.Message = "Your barcode is valid";
                    result.StatusCode = 1;
                    result.IsValid = true;
                    result.Result = "success";
                    result.Data = barcodegenerate;
                }
                else
                {
                    result.Message = "Your barcode is Invalid";
                    result.StatusCode = 0;
                    result.IsValid = false;
                    result.Result = "error";
                    result.Data = null;
                }

            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.pk_ID = null;
                result.Data = null;
            }

            return result;
        }


        public Boolean ChangeActiveByBarcode(string barcode)
        {
            try
            {
                var change = _Context.BarcodeGenerate.Where(d => d.BarCode.ToLower().Trim() == barcode.ToLower().Trim()).FirstOrDefault();
                change.IsUsed = true;
                change.ModifiedDate = System.DateTime.Now;
                _Context.BarcodeGenerate.Update(change);
                _Context.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public Boolean ChangeDeactiveByBarcode(string barcode)
        {
            try
            {
                var change = _Context.BarcodeGenerate.Where(d => d.BarCode.ToLower().Trim() == barcode.ToLower().Trim()).FirstOrDefault();
                change.IsUsed = false;
                change.ModifiedDate = System.DateTime.Now;
                _Context.BarcodeGenerate.Update(change);
                _Context.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }



    }
}


