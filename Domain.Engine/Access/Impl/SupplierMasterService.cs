﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Domain.Engine.Access.Interfaces;
using Domain.Objects.Models;
using Infrastructure.Model.Main;
using Infrastructure.Model.Main.Entities;


namespace Domain.Engine.Access.Impl
{
    public class SupplierMasterService : ISupplierMasterService
    {
        protected readonly MainDbContext _Context;
        protected MainDbContext Context
        {
            get
            {
                return (MainDbContext)_Context;
            }
        }

        public SupplierMasterService(IMainDbContext context)

        {
            _Context = (MainDbContext)context;
        }

        public ResponseviewModel InsertUpdateSupplierMaster(SupplierMasterModels suppliermastermodel)
        {
            ResponseviewModel result = new ResponseviewModel();
            try
            {
                if (suppliermastermodel.Pk_SupplierID == null)
                {
                    SupplierMaster checkpartsmaster = new SupplierMaster();
                    checkpartsmaster = _Context.SupplierMaster.FirstOrDefault(d => d.SupplierCode == suppliermastermodel.SupplierCode && d.IsActive == true && d.IsDelete == false);
                    if (checkpartsmaster == null)
                    {
                        SupplierMaster supplierMaster = new SupplierMaster();
                        supplierMaster.Pk_SupplierID = Guid.NewGuid();
                        supplierMaster.SupplierName = suppliermastermodel.SupplierName;
                        supplierMaster.SupplierCode = suppliermastermodel.SupplierCode;
                        supplierMaster.ContactPerson = suppliermastermodel.ContactPerson;
                        supplierMaster.ContactNo = suppliermastermodel.ContactNo;
                        supplierMaster.EmailID = suppliermastermodel.EmailID;
                        supplierMaster.Address = suppliermastermodel.Address;
                        supplierMaster.Description = suppliermastermodel.Description;
                        supplierMaster.IsDelete = false;
                        supplierMaster.IsActive = true;
                        supplierMaster.CreatedBy = suppliermastermodel.CreatedBy;
                        supplierMaster.CreatedDate = System.DateTime.Now;
                        supplierMaster.ModifiedBy = suppliermastermodel.CreatedBy;
                        supplierMaster.ModifiedDate = System.DateTime.Now;

                        _Context.AddAsync(supplierMaster);
                        _Context.SaveChanges();
                        result.Message = "Data Inserted Successfully";
                        result.StatusCode = 1;
                        result.IsValid = true;
                        result.Result = "success";
                        result.pk_ID = supplierMaster.Pk_SupplierID;
                        result.Data = supplierMaster;
                    }
                    else
                    {
                        result.Message = "Supplier Code already exists";
                        result.StatusCode = 0;
                        result.IsValid = false;
                        result.Result = "error";
                        result.pk_ID = null;
                        result.Data = null;
                    }
                }
                else
                {
                    SupplierMaster suppliermaster = _Context.SupplierMaster.FirstOrDefault(d => d.Pk_SupplierID == suppliermastermodel.Pk_SupplierID&& d.IsActive == true && d.IsDelete == false);
                    if (suppliermaster != null)
                    {
                        suppliermaster.SupplierName = suppliermastermodel.SupplierName;
                        suppliermaster.SupplierCode = suppliermastermodel.SupplierCode;
                        suppliermaster.ContactNo = suppliermastermodel.ContactNo;
                        suppliermaster.ContactPerson = suppliermastermodel.ContactPerson;
                        suppliermaster.EmailID = suppliermastermodel.EmailID;
                        suppliermaster.Address = suppliermastermodel.Address;
                        suppliermaster.Description = suppliermastermodel.Description;
                        suppliermaster.IsDelete = false;
                        suppliermaster.IsActive = true;
                        suppliermaster.ModifiedDate = System.DateTime.Now;
                        suppliermaster.ModifiedBy = suppliermastermodel.ModifiedBy;
                        _Context.Update(suppliermaster);
                        _Context.SaveChanges();
                        result.Message = "Data Updated Successfully";
                        result.StatusCode = 1;
                        result.IsValid = true;
                        result.Result = "success";
                        result.pk_ID = suppliermaster.Pk_SupplierID;
                        result.Data = suppliermaster;
                    }
                    else
                    {
                        result.Message = "Record does not exists";
                        result.StatusCode = 0;
                        result.IsValid = false;
                        result.Result = "error";
                        result.pk_ID = suppliermaster.Pk_SupplierID;
                        result.Data = null;
                    }
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.pk_ID = null;
                result.Data = null;
            }
            return result;
        }

        public ResponseviewModel GetSupplierMasterByID(Guid id)
        {
            ResponseviewModel result = new ResponseviewModel();
            try
            {
                var suppliermaster = _Context.SupplierMaster.FirstOrDefault(d => d.Pk_SupplierID == id && d.IsActive == true && d.IsDelete == false);
                if (suppliermaster != null)
                {
                    result.Message = "Record Get Successfully";
                    result.StatusCode = 1;
                    result.IsValid = true;
                    result.Result = "success";
                    result.pk_ID = suppliermaster.Pk_SupplierID;
                    result.Data = suppliermaster;
                }
                else
                {
                    result.Message = "Record does not exists";
                    result.StatusCode = 0;
                    result.IsValid = false;
                    result.Result = "error";
                    result.pk_ID = id;
                    result.Data = null;
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.pk_ID = id;
                result.Data = null;
            }
            return result;
        }

        public ResponseviewModel GetAllListSupplierMaster()
        {
            ResponseviewModel result = new ResponseviewModel();
            try
            {
                var suppliermaster =_Context.SupplierMaster.Where(d => d.IsActive == true && d.IsDelete == false).OrderByDescending(a=>a.CreatedDate).ToList();
                if (suppliermaster != null)
                {
                    result.Message = "Record Get Successfully";
                    result.StatusCode = 1;
                    result.IsValid = true;
                    result.Result = "success";
                    result.Data = suppliermaster;
                }
                else
                {
                    result.Message = "Record does not exists";
                    result.StatusCode = 0;
                    result.IsValid = false;
                    result.Result = "error";
                    result.Data = null;
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.Data = null;
            }
            return result;
        }

        public ResponseviewModel DeleteSupplierMasterByID(SupplierMasterModels supplier)
        {
            ResponseviewModel result = new ResponseviewModel();
            try
            {
                var suppliermaster =_Context.SupplierMaster.Where(d => d.Pk_SupplierID == supplier.Pk_SupplierID && d.IsActive == true && d.IsDelete == false).FirstOrDefault();
                if (suppliermaster != null)
                {
                    suppliermaster.IsDelete = true;
                    suppliermaster.ModifiedBy = supplier.ModifiedBy;
                    _Context.Update(suppliermaster);
                    _Context.SaveChanges();

                    result.Message = "Record Deleted Successfully";
                    result.StatusCode = 1;
                    result.IsValid = true;
                    result.Result = "success";
                    result.pk_ID = suppliermaster.Pk_SupplierID;
                }
                else
                {
                    result.Message = "Record does not exists";
                    result.StatusCode = 0;
                    result.IsValid = false;
                    result.Result = "error";
                    result.Data = null;
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.Data = null;
            }
            return result;
        }


        public ResponseviewModel GetDropdownofSupplierMaster()
        {
            ResponseviewModel result = new ResponseviewModel();
            try
            {
                var suppliermaster = _Context.SupplierMaster.Where(d => d.IsActive == true && d.IsDelete == false).Select(x => new DropdownViewModel
                {
                    ID = x.Pk_SupplierID,
                    Value = x.SupplierName
                }).ToList();

                if (suppliermaster.Count() > 0)
                {
                    result.Message = "Record Get Successfully";
                    result.StatusCode = 1;
                    result.IsValid = true;
                    result.Result = "success";
                    result.Data = suppliermaster;
                }
                else
                {
                    result.Message = "Record does not exists";
                    result.StatusCode = 0;
                    result.IsValid = false;
                    result.Result = "error";
                    result.Data = null;
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.Data = null;
            }
            return result;
        }

    }
}
