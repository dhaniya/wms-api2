using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Domain.Engine.Access.Interfaces;
using Domain.Objects.Models;
using Infrastructure.Model.Main;
using Infrastructure.Model.Main.Entities;
using Microsoft.EntityFrameworkCore;

namespace Domain.Engine.Access.Impl
{
    public class SortingMasterService : ISortingMasterService
    {
        protected readonly MainDbContext _Context;
        ResponseviewModel result = new ResponseviewModel();
        private readonly ISortingDetailService _SortingDetailService;
        private readonly IASNMasterService _ASNMasterService;
        private readonly IBarCodeService _BarCodeService;
        private readonly ILocationMasterService _locationmasterService;
        private readonly IStockTransactionService _stockTransactionService;
        private readonly IPartsMasterService _partsMasterService;


        protected MainDbContext Context
        {
            get
            {
                return (MainDbContext)_Context;
            }
        }
        public SortingMasterService(IMainDbContext context, ISortingDetailService sortingDetailService, IBarCodeService barcodeservice, IASNMasterService asnmasterservice, ILocationMasterService locationmasterservice, IStockTransactionService stockTransactionService, IPartsMasterService partsMasterService)
        {
            _SortingDetailService = sortingDetailService;
            _Context = (MainDbContext)context;
            _BarCodeService = barcodeservice;
            _ASNMasterService = asnmasterservice;
            _locationmasterService = locationmasterservice;
            _stockTransactionService = stockTransactionService;
            _partsMasterService = partsMasterService;
        }

        public ResponseviewModel InsertUpdateSortingMaster(SortingMasterModel sortingmastermodel)
        {
            try
            {
                if (sortingmastermodel.Pk_SortingID == null)
                {
                    int availableqty = GetAvailableQuantityByAsnDetail(sortingmastermodel.InvoiceNo, sortingmastermodel.CaseNo, sortingmastermodel.CustomerCode, sortingmastermodel.ItemCode);
                    if (availableqty >= sortingmastermodel.Quantity)
                    {
                        ASNItemDetails asndetail = new ASNItemDetails();
                        asndetail = _Context.ASNItemDetails.Where(a => a.PalletNo == sortingmastermodel.CaseNo && a.Customer == sortingmastermodel.CustomerCode && a.ASNMaster.InvoiceNo == sortingmastermodel.InvoiceNo && a.SuppliedPartNo == sortingmastermodel.ItemCode && a.Quantity != a.ReceivedQty && a.IsDelete == false).FirstOrDefault();
                        if (asndetail.ReceivedQty != null)
                        {
                            asndetail.ReceivedQty = asndetail.ReceivedQty + sortingmastermodel.Quantity;
                            _Context.Update(asndetail);
                        }
                        else
                        {
                            asndetail.ReceivedQty = sortingmastermodel.Quantity;
                            _Context.Update(asndetail);
                        }


                        SortingMaster checkcasenousedornot = new SortingMaster();
                        checkcasenousedornot = _Context.SortingMaster.Where(d => d.RepakedCaseNo.Trim().ToLower() == sortingmastermodel.RepakedCaseNo.Trim().ToLower() && d.IsActive == true && d.IsDelete == false && d.ItemCode.Trim() == sortingmastermodel.ItemCode.Trim() && d.InvoiceNo.Trim() == sortingmastermodel.InvoiceNo.Trim() && d.CaseNo.ToLower().Trim() == sortingmastermodel.CaseNo.ToLower().Trim() && d.IsBin == false).FirstOrDefault();
                        if (checkcasenousedornot == null)
                        {
                            SortingMaster sortingmaster = new SortingMaster();
                            sortingmaster.Pk_SortingID = Guid.NewGuid();
                            sortingmaster.CaseNo = sortingmastermodel.CaseNo;
                            sortingmaster.InvoiceNo = sortingmastermodel.InvoiceNo;
                            sortingmaster.ItemCode = sortingmastermodel.ItemCode.Trim();
                            sortingmaster.NoOfPieces = sortingmastermodel.NoOfPieces;
                            sortingmaster.CustomerCode = sortingmastermodel.CustomerCode;
                            sortingmaster.LocationID = sortingmastermodel.LocationID.Trim();
                            sortingmaster.RepakedCaseNo = sortingmastermodel.RepakedCaseNo;
                            sortingmaster.Quantity = sortingmastermodel.Quantity;
                            sortingmaster.IsDelete = false;
                            sortingmaster.IsActive = true;
                            sortingmaster.CreatedBy = sortingmastermodel.CreatedBy;
                            sortingmaster.CreatedDate = System.DateTime.Now;
                            sortingmaster.ModifiedBy = sortingmastermodel.CreatedBy;
                            sortingmaster.ModifiedDate = System.DateTime.Now;
                            sortingmaster.IsPutaway = false;
                            sortingmaster.IsBin = false;
                            sortingmaster.SortingDescription = _partsMasterService.GetDetailsBynumber(sortingmastermodel.ItemCode) == null ? "" : _partsMasterService.GetDetailsBynumber(sortingmastermodel.ItemCode).PartDescription;
                            _Context.AddAsync(sortingmaster);
                            _Context.SaveChanges();
                            //add stock on stock transection
                            StockTransactionModel stockTransactionModel = new StockTransactionModel();
                            stockTransactionModel.CreatedBy = sortingmaster.CreatedBy;
                            stockTransactionModel.PK_SortingID = sortingmaster.Pk_SortingID;
                            stockTransactionModel.QuantityReceived = sortingmastermodel.Quantity;
                            stockTransactionModel.QuantityRemaining = sortingmaster.Quantity;
                            _stockTransactionService.InsertStockTransaction(stockTransactionModel);

                            //Insert Default data in sorting detail table
                            SortingDetailModels sortingdetailmodel = new SortingDetailModels();
                            //  var sortdetails=  _Context.SortingDetail.Where(k => k.RepakedCaseNo.Trim() == sortingmaster.RepakedCaseNo.Trim()).FirstOrDefault();
                            sortingdetailmodel.FK_SortingMasterID = sortingmaster.Pk_SortingID;
                            //  sortingdetailmodel.Pk_SortingDetailID = sortdetails == null ? Guid.Empty : sortdetails.Pk_SortingDetailID;
                            sortingdetailmodel.RepakedCaseNo = sortingmaster.RepakedCaseNo;
                            sortingdetailmodel.Height = 0;
                            sortingdetailmodel.Weight = 0;
                            sortingdetailmodel.Breadth = 0;
                            sortingdetailmodel.Volume = 0;
                            sortingdetailmodel.Weight = 0;
                            sortingdetailmodel.CreatedBy = sortingmastermodel.CreatedBy;
                            _SortingDetailService.InsertUpdateSortingDetail(sortingdetailmodel);

                            result.Message = "Data Inserted Successfully";
                            result.StatusCode = 1;
                            result.IsValid = true;
                            result.Result = "success";
                            result.pk_ID = sortingmaster.Pk_SortingID;
                            result.Data = sortingmaster;

                            int currentavailableqty = GetAvailableQuantityByAsnDetail(sortingmastermodel.InvoiceNo, sortingmastermodel.CaseNo, sortingmastermodel.CustomerCode, sortingmastermodel.ItemCode);
                            if (currentavailableqty == 0)
                            {
                                var LocationID = _Context.ASNItemDetails.FirstOrDefault(d => d.ASNMaster.InvoiceNo.ToLower() == sortingmastermodel.InvoiceNo.ToLower() && d.PalletNo.ToLower() == sortingmastermodel.CaseNo.ToLower() && d.IsRecieved == true && d.IsActive == true && d.IsDelete == false).LocationId;
                                if (LocationID != null)
                                    _locationmasterService.ChangeUnusedByLocationCode(LocationID);
                            }
                        }
                        else
                        {
                            checkcasenousedornot.Quantity = checkcasenousedornot.Quantity + sortingmastermodel.Quantity;

                            _Context.SaveChanges();
                            StockTransactionModel stockTransactionModel = new StockTransactionModel();
                            stockTransactionModel.CreatedBy = sortingmastermodel.CreatedBy;
                            stockTransactionModel.PK_SortingID = checkcasenousedornot.Pk_SortingID;
                            stockTransactionModel.QuantityReceived = checkcasenousedornot.Quantity;
                            stockTransactionModel.QuantityRemaining = checkcasenousedornot.Quantity;
                            _stockTransactionService.InsertStockTransaction(stockTransactionModel);
                            result.Message = "Data Inserted Successfully";
                            result.StatusCode = 1;
                            result.IsValid = true;
                            result.Result = "success";
                            result.pk_ID = checkcasenousedornot.Pk_SortingID;
                            result.Data = checkcasenousedornot;
                        }
                    }
                    else
                    {
                        result.Message = "Please insert valid quantity";
                        result.StatusCode = 0;
                        result.IsValid = false;
                        result.Result = "error";
                        result.pk_ID = null;
                        result.Data = null;
                    }
                }
                else
                {


                    SortingMaster sortingmaster = _Context.SortingMaster.FirstOrDefault(d => d.Pk_SortingID == sortingmastermodel.Pk_SortingID && d.IsActive == true && d.IsDelete == false);
                    if (sortingmaster != null)
                    {
                        int availableqty = GetAvailableQuantityByAsnDetail(sortingmastermodel.InvoiceNo, sortingmastermodel.CaseNo, sortingmastermodel.CustomerCode, sortingmastermodel.ItemCode);
                        if (availableqty >= sortingmastermodel.Quantity)
                        {
                            sortingmaster.CaseNo = sortingmastermodel.CaseNo;
                            sortingmaster.InvoiceNo = sortingmastermodel.InvoiceNo;
                            sortingmaster.ItemCode = sortingmastermodel.ItemCode.Trim();
                            //sortingmaster.NoOfPieces = sortingmastermodel.NoOfPieces;
                            sortingmaster.CustomerCode = sortingmastermodel.CustomerCode;
                            sortingmaster.LocationID = sortingmastermodel.LocationID.Trim();
                            sortingmaster.RepakedCaseNo = sortingmastermodel.RepakedCaseNo;
                            sortingmaster.Quantity = sortingmastermodel.Quantity;
                            sortingmaster.IsDelete = false;
                            sortingmaster.IsActive = true;
                            sortingmaster.IsPutaway = false;
                            sortingmaster.IsBin = false;

                            sortingmaster.ModifiedDate = System.DateTime.Now;
                            sortingmaster.ModifiedBy = sortingmastermodel.ModifiedBy;
                            _Context.Update(sortingmaster);
                            _Context.SaveChanges();

                            // var barcodes = _BarCodeService.GetBarCodeListByCustomerCode(sortingmastermodel.InvoiceNo, sortingmastermodel.RepakedCaseNo).Data;

                            result.Message = "Data Updated Successfully";
                            result.StatusCode = 1;
                            result.IsValid = true;
                            result.Result = "success";
                            result.pk_ID = sortingmaster.Pk_SortingID;
                            result.Data = sortingmaster;
                        }
                        else
                        {
                            result.Message = "Please insert valid quantity";
                            result.StatusCode = 0;
                            result.IsValid = false;
                            result.Result = "error";
                            result.pk_ID = null;
                            result.Data = null;
                        }
                    }
                    else
                    {
                        result.Message = "Record does not exists";
                        result.StatusCode = 0;
                        result.IsValid = false;
                        result.Result = "error";
                        result.pk_ID = sortingmaster.Pk_SortingID;
                        result.Data = null;
                    }
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.pk_ID = null;
                result.Data = null;
            }
            return result;
        }

        public ResponseviewModel DeleteSortingMasterbyID(SortingMasterModel sortingm)
        {
            try
            {
                SortingMaster sortingmaster = _Context.SortingMaster.FirstOrDefault(d => d.Pk_SortingID == sortingm.Pk_SortingID && d.IsActive == true && d.IsDelete == false);
                if (sortingmaster != null)
                {
                    if (sortingmaster != null)
                    {
                        int Qty = sortingmaster.Quantity;
                        List<ASNItemDetails> asndetail = new List<ASNItemDetails>();
                        asndetail = _Context.ASNItemDetails.Where(a => a.PalletNo == sortingmaster.CaseNo && a.Customer == sortingmaster.CustomerCode && a.ASNMaster.InvoiceNo == sortingmaster.InvoiceNo && a.SuppliedPartNo == sortingmaster.ItemCode).ToList();
                        foreach (var item in asndetail)
                        {
                            if (item.Quantity <= Qty)
                            {
                                item.ReceivedQty = item.ReceivedQty - item.Quantity;
                                Qty = Qty - item.Quantity;
                                _Context.Update(item);

                                if (Qty <= 0)
                                {
                                    break;
                                }

                            }
                            else
                            {
                                item.ReceivedQty = item.ReceivedQty - Qty;
                                _Context.Update(item);
                            }

                        }
                        sortingmaster.IsDelete = true;
                        sortingmaster.IsActive = false;
                        sortingmaster.ModifiedBy = sortingm.ModifiedBy;
                        sortingmaster.ModifiedDate = System.DateTime.Now;
                        _Context.Update(sortingmaster);
                        _Context.SaveChanges();
                        //Barcode update as a unused
                        _BarCodeService.ChangeDeactiveByBarcode(sortingmaster.RepakedCaseNo);

                        //Set sorting qty to 0

                        StockTransactionModel stockTransactionModel = new StockTransactionModel();
                        stockTransactionModel.CreatedBy = sortingmaster.CreatedBy;
                        stockTransactionModel.PK_SortingID = sortingmaster.Pk_SortingID;
                        stockTransactionModel.QuantityReceived = 0;
                        stockTransactionModel.QuantityRemaining = 0;
                        _stockTransactionService.InsertStockTransaction(stockTransactionModel);
                        //location update as used
                        var locationid = _Context.ASNItemDetails.FirstOrDefault(d => d.ASNMaster.InvoiceNo == sortingmaster.InvoiceNo && d.PalletNo == sortingmaster.CaseNo).LocationId;
                        if (locationid != null)
                            _locationmasterService.ChangeActiveByLocationCode(locationid);
                        //soft delete sorting table data 
                        var sortingdetailresult = _SortingDetailService.DeleteSortingDetailBySortingMasterID(sortingm.Pk_SortingID.Value);
                        result.Message = "Data Deleted Successfully";
                        result.StatusCode = 1;
                        result.IsValid = true;
                        result.Result = "success";
                        result.pk_ID = sortingmaster.Pk_SortingID;
                    }
                    else
                    {
                        result.Message = "Record does not exists";
                        result.StatusCode = 0;
                        result.IsValid = false;
                        result.Result = "error";
                        result.pk_ID = sortingm.Pk_SortingID;
                        result.Data = null;
                    }
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.pk_ID = null;
                result.Data = null;
            }
            return result;
        }


        public ResponseviewModel GetAllSortingMaster()
        {
            try
            {
                List<SortingMasterViewModal> listsortingmaster = new List<SortingMasterViewModal>();
                listsortingmaster = _Context.SortingMaster.Include(a => a.UserMaster).Where(d => d.ModifiedDate.Date == System.DateTime.Now.Date && d.IsActive == true && d.IsDelete == false && d.IsBin == false).Select(a => new SortingMasterViewModal
                {
                    Pk_SortingID = a.Pk_SortingID,
                    InvoiceNo = a.InvoiceNo,
                    CaseNo = a.CaseNo,
                    ItemCode = a.ItemCode,
                    NoOfPieces = a.NoOfPieces,
                    CustomerCode = a.CustomerCode,
                    LocationID = a.LocationID,
                    RepakedCaseNo = a.RepakedCaseNo,
                    Quantity = _Context.StockTransaction.Where(x => x.PK_SortingID == a.Pk_SortingID).FirstOrDefault().QuantityRemaining ?? 0,
                    IsActive = a.IsActive,
                    IsDelete = a.IsDelete,
                    CreatedBy = a.CreatedBy,
                    ModifiedBy = a.ModifiedBy,
                    CreatedDate = a.CreatedDate,
                    ModifiedDate = a.ModifiedDate,
                    IsPutaway = a.IsPutaway,
                    IsBin = a.IsBin,
                    SortingDescription = a.SortingDescription,
                    UserName = a.UserMaster.FirstName,
                    TotalQty = _Context.BinItemHistory.Where(k => k.FK_SortingMasterID == k.FK_SortingMasterID).FirstOrDefault().Quantity
                }).OrderBy(x => x.ModifiedDate).ToList();
                foreach (var item in listsortingmaster)
                {
                    var ttlqty = _Context.BinItemHistory.Where(a => a.FK_SortingMasterID == item.Pk_SortingID).Any();
                    if (ttlqty == true)
                    {
                        int qty = _Context.BinItemHistory.Where(a => a.FK_SortingMasterID == item.Pk_SortingID).Select(a => a.Quantity).Sum();
                        item.TotalQty = item.Quantity + qty;
                    }
                    else
                    {
                        item.TotalQty = item.Quantity;
                    }
                }

                if (listsortingmaster.Count > 0)
                {

                    result.Message = "Record Get Successfully";
                    result.StatusCode = 1;
                    result.IsValid = true;
                    result.Result = "success";
                    result.Data = listsortingmaster;
                }
                else
                {
                    result.Message = "Record does not exists";
                    result.StatusCode = 0;
                    result.IsValid = false;
                    result.Result = "error";
                    result.Data = null;
                }

            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.pk_ID = null;
                result.Data = null;
            }
            return result;
        }


        public int GetAvailableQuantityByAsnDetail(string InvoiceNo, string CaseNo, string Customer, string SuppliedPartNo)
        {
            int AvailableQuantity = 0;
            try
            {
                int totalquantity = 0;
                int sortedquantity = 0;

                totalquantity = _Context.ASNItemDetails.Where(d => d.ASNMaster.InvoiceNo.ToLower().Trim() == InvoiceNo.ToLower().Trim() && d.PalletNo.ToLower().Trim() == CaseNo.ToLower().Trim() && d.Customer.ToLower().Trim() == Customer.ToLower().Trim() && d.SuppliedPartNo.ToLower().Trim() == SuppliedPartNo.ToLower().Trim() && d.IsRecieved == true && d.IsActive == true && d.IsDelete == false).Sum(x => x.Quantity);
                sortedquantity = _Context.SortingMaster.Where(d => d.InvoiceNo.ToLower().Trim() == InvoiceNo.ToLower().Trim() && d.CaseNo.ToLower().Trim() == CaseNo.ToLower().Trim() && d.CustomerCode.ToLower().Trim() == Customer.ToLower().Trim() && d.ItemCode.ToLower().Trim() == SuppliedPartNo.ToLower().Trim() && d.IsActive == true && d.IsDelete == false).Sum(x => x.Quantity);
                //Remain to count sorted quantity
                AvailableQuantity = totalquantity - sortedquantity; // get available quantity by sum of quantity and sum of sorted quantity

            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.Data = null;
            }
            return AvailableQuantity;
        }
        public int GetAvailableQuantityItemCode(string caseno, string partnumber)
        {
            int availablequantity = 0;
            try
            {
                int totalquantity = 0;
                int pickingquantity = 0;
                totalquantity = _Context.SortingMaster.Where(d => d.RepakedCaseNo.ToLower().Trim() == caseno.ToLower().Trim() && d.ItemCode.ToLower().Trim() == partnumber.ToLower().Trim() && d.IsBin == false && d.IsActive == true && d.IsDelete == false && d.IsPutaway == true).Sum(x => x.Quantity);
                pickingquantity = _Context.PickingDetail.Where(d => d.RepackedCaseNo.ToLower().Trim() == caseno.ToLower().Trim() && d.ItemCode.ToLower().Trim() == partnumber.ToLower().Trim() && d.IsActive == true && d.IsDelete == false && d.PickingType != 3).Sum(x => x.Quantity);
                //Remain to count sorted quantity
                availablequantity = totalquantity - pickingquantity;
                if (availablequantity <= 0)
                {
                    availablequantity = 0;
                }
            }
            catch (Exception ex)
            {
                availablequantity = 0;
            }
            return availablequantity;
        }
        public int GetAvailableQuantityItemCodeBin(string location, string partnumber)
        {
            int availablequantity = 0;
            try
            {
                int totalquantity = 0;
                int pickingquantity = 0;
                totalquantity = _Context.SortingMaster.Where(d => d.LocationID.ToLower().Trim() == location.ToLower().Trim() && d.ItemCode.ToLower().Trim() == partnumber.ToLower().Trim() && d.IsBin == true && d.IsActive == true && d.IsDelete == false).Sum(x => x.Quantity);
                pickingquantity = _Context.PickingDetail.Where(d => d.ItemCode.ToLower().Trim() == partnumber.ToLower().Trim() && d.LocationId.Trim() == location.ToLower().Trim() && d.IsActive == true && d.IsDelete == false && d.PickingType == 3).Sum(x => x.Quantity);
                //Remain to count sorted quantity
                availablequantity = totalquantity - pickingquantity;
                if (availablequantity <= 0)
                {
                    availablequantity = 0;
                }
            }
            catch (Exception ex)
            {
                availablequantity = 0;
            }
            return availablequantity;
        }

        public int GetAvailableQuantityBy(string InvoiceNo, string CaseNo, string Customer, string SuppliedPartNo)
        {
            int AvailableQuantity = 0;
            try
            {
                int totalquantity = 0;
                int sortedquantity = 0;

                totalquantity = _Context.ASNItemDetails.Where(d => d.ASNMaster.InvoiceNo.ToLower().Trim() == InvoiceNo.ToLower().Trim() && d.PalletNo.ToLower().Trim() == CaseNo.ToLower().Trim() && d.Customer.ToLower().Trim() == Customer.ToLower().Trim() && d.SuppliedPartNo.ToLower().Trim() == SuppliedPartNo.ToLower().Trim() && d.IsRecieved == true && d.IsActive == true && d.IsDelete == false).Sum(x => x.Quantity);
                sortedquantity = _Context.SortingMaster.Where(d => d.InvoiceNo.ToLower().Trim() == InvoiceNo.ToLower().Trim() && d.CaseNo.ToLower().Trim() == CaseNo.ToLower().Trim() && d.CustomerCode.ToLower().Trim() == Customer.ToLower().Trim() && d.ItemCode.ToLower().Trim() == SuppliedPartNo.ToLower().Trim() && d.IsActive == true && d.IsDelete == false).Sum(x => x.Quantity);
                //Remain to count sorted quantity
                AvailableQuantity = totalquantity - sortedquantity; // get available quantity by sum of quantity and sum of sorted quantity

            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.Data = null;
            }
            return AvailableQuantity;
        }

        public ResponseviewModel AddPutAwayBox(PutAwayViewModel putawayviewmodel)
        {
            try
            {
                var sortingmaster = _Context.SortingMaster.Where(d => d.RepakedCaseNo.ToLower().Trim() == putawayviewmodel.RepakedCaseNo.ToLower().Trim() && d.IsActive == true && d.IsDelete == false && d.IsBin != true && d.IsPutaway == false).ToList();
                if (sortingmaster.Count != 0)
                {
                    //check repacked no already used or not
                    var isputaway = _Context.SortingMaster.Where(d => d.RepakedCaseNo.ToLower().Trim() == putawayviewmodel.RepakedCaseNo.ToLower().Trim() && d.IsPutaway == false && d.IsActive == true && d.IsDelete == false && d.IsBin != true && d.Quantity > 0).ToList();
                    if (isputaway.Count != 0)
                    {
                        foreach (var item in sortingmaster)
                        {
                            item.IsPutaway = true;
                            item.LocationID = putawayviewmodel.LocationId;
                            item.ModifiedDate = System.DateTime.Now;
                            item.ModifiedBy = putawayviewmodel.ModifiedBy;
                            _Context.Update(item);
                            _Context.SaveChanges();
                            //update barcode as a is used
                            _BarCodeService.ChangeActiveByBarcode(item.RepakedCaseNo);
                        }
                        _locationmasterService.ChangeActiveByLocationCode(putawayviewmodel.LocationId);
                        putawayviewmodel = _Context.SortingMaster.Where(d => d.RepakedCaseNo.ToLower().Trim() == putawayviewmodel.RepakedCaseNo.ToLower().Trim() && d.IsActive == true && d.IsDelete == false && d.IsPutaway == false && d.IsBin == false).Select(x => new PutAwayViewModel
                        {
                            RepakedCaseNo = x.RepakedCaseNo,
                            CustomerCode = x.CustomerCode,
                            Weight = _Context.SortingDetail.FirstOrDefault(z => z.FK_SortingMasterID == x.Pk_SortingID && z.IsActive == true && z.IsDelete == false).Weight,
                            LocationId = x.LocationID
                        }).FirstOrDefault();


                        result.Message = "Data Updated Successfully";
                        result.StatusCode = 1;
                        result.IsValid = true;
                        result.Result = "success";
                        result.Data = putawayviewmodel;
                    }
                    else
                    {
                        result.Message = "RepackedCaseNo already used.!";
                        result.StatusCode = 0;
                        result.IsValid = false;
                        result.Result = "error";
                        result.pk_ID = null;
                        result.Data = null;
                    }

                }
                else
                {
                    result.Message = "Record does not exists";
                    result.StatusCode = 0;
                    result.IsValid = false;
                    result.Result = "error";
                    result.pk_ID = null;
                    result.Data = null;
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.pk_ID = null;
                result.Data = null;
            }
            return result;
        }
        public ResponseviewModel GetPutAwayList()
        {
            try
            {
                var sortingmasterlist = _Context.SortingMaster.Where(d => d.ModifiedDate.Date == System.DateTime.Now.Date && d.IsPutaway == true && d.IsActive == true && d.IsDelete == false && d.IsBin != true && d.Quantity > 0).OrderByDescending(k => k.ModifiedDate).Select(a =>
                      new
                      {
                          InvoiceNo = a.InvoiceNo,
                          CaseNo = a.CaseNo,
                          CustomerCode = a.CustomerCode,
                          LocationID = a.LocationID,
                          UserName = _Context.UserMaster.Where(x => x.Pk_UserId == a.ModifiedBy).FirstOrDefault().UserName,
                          RepakedCaseNo = a.RepakedCaseNo

                      }).ToList().GroupBy(k => k.RepakedCaseNo).Select(c => new
                      {
                          InvoiceNo = c.FirstOrDefault().InvoiceNo,
                          CaseNo = c.FirstOrDefault().CaseNo,
                          CustomerCode = c.FirstOrDefault().CustomerCode,
                          LocationID = c.FirstOrDefault().LocationID,
                          UserName = c.FirstOrDefault().UserName,
                          RepakedCaseNo = c.Key
                      }).ToList();

                if (sortingmasterlist != null)
                {
                    result.Message = "Record get successfully";
                    result.StatusCode = 1;
                    result.IsValid = true;
                    result.Result = "success";
                    result.Data = sortingmasterlist;
                }
                else
                {
                    result.Message = "Record does not exists";
                    result.StatusCode = 0;
                    result.IsValid = false;
                    result.Result = "error";
                    result.pk_ID = null;
                    result.Data = null;
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.pk_ID = null;
                result.Data = null;
            }
            return result;
        }

        public ResponseviewModel GetPutAwayListByDate(DateTime fromdate, DateTime todate)
        {
            try
            {

                DateTime from = Convert.ToDateTime(fromdate);
                DateTime to = Convert.ToDateTime(todate);
                var sortingmasterlist = _Context.SortingMaster.Where(d => d.ModifiedDate.Date >= (from != DateTime.MinValue ? from : d.ModifiedDate.Date) && d.ModifiedDate.Date <= (to != DateTime.MinValue ? to : d.ModifiedDate.Date) && d.IsPutaway == true && d.IsActive == true && d.IsDelete == false && d.IsBin != true && d.Quantity > 0).OrderByDescending(k => k.ModifiedDate).Select(a =>
                      new
                      {
                          InvoiceNo = a.InvoiceNo,
                          CaseNo = a.CaseNo,
                          CustomerCode = a.CustomerCode,
                          LocationID = a.LocationID,
                          UserName = a.UserMaster.FirstName,
                          RepakedCaseNo = a.RepakedCaseNo

                      }).ToList().GroupBy(k => k.RepakedCaseNo).Select(c => new
                      {
                          InvoiceNo = c.FirstOrDefault().InvoiceNo,
                          CaseNo = c.FirstOrDefault().CaseNo,
                          CustomerCode = c.FirstOrDefault().CustomerCode,
                          LocationID = c.FirstOrDefault().LocationID,
                          UserName = c.FirstOrDefault().UserName,
                          RepakedCaseNo = c.Key
                      }).ToList();

                if (sortingmasterlist != null)
                {
                    result.Message = "Record get successfully";
                    result.StatusCode = 1;
                    result.IsValid = true;
                    result.Result = "success";
                    result.Data = sortingmasterlist;
                }
                else
                {
                    result.Message = "Record does not exists";
                    result.StatusCode = 0;
                    result.IsValid = false;
                    result.Result = "error";
                    result.pk_ID = null;
                    result.Data = null;
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.pk_ID = null;
                result.Data = null;
            }
            return result;
        }

        public ResponseviewModel GetPutAwayListInvoice(string invoice)
        {
            try
            {
                var sortingmasterlist = _Context.SortingMaster.Where(d => d.InvoiceNo.ToLower().Trim() == invoice.ToLower().Trim() && d.IsPutaway == true && d.IsActive == true && d.IsDelete == false).OrderByDescending(k => k.CreatedDate).Select(a =>
                     new
                     {
                         InvoiceNo = a.InvoiceNo,
                         CaseNo = a.CaseNo,
                         CustomerCode = a.CustomerCode,
                         LocationID = a.LocationID,
                         UserName = a.UserMaster.FirstName,
                         RepakedCaseNo = a.RepakedCaseNo

                     }).ToList().GroupBy(k => k.RepakedCaseNo).Select(c => new
                     {
                         InvoiceNo = c.FirstOrDefault().InvoiceNo,
                         CaseNo = c.FirstOrDefault().CaseNo,
                         CustomerCode = c.FirstOrDefault().CustomerCode,
                         LocationID = c.FirstOrDefault().LocationID,
                         UserName = c.FirstOrDefault().UserName,
                         RepakedCaseNo = c.Key
                     }).ToList();

                if (sortingmasterlist != null)
                {
                    result.Message = "Record get successfully";
                    result.StatusCode = 1;
                    result.IsValid = true;
                    result.Result = "success";
                    result.Data = sortingmasterlist;
                }
                else
                {
                    result.Message = "Record does not exists";
                    result.StatusCode = 0;
                    result.IsValid = false;
                    result.Result = "error";
                    result.pk_ID = null;
                    result.Data = null;
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.pk_ID = null;
                result.Data = null;
            }
            return result;
        }

        public ResponseviewModel GetInventorySearchForRack(InventorySearchViewModel model)
        {
            try
            {
                PagedItemSet<InventorySearchRepackCase> pagedItems = new PagedItemSet<InventorySearchRepackCase>();
                var InventorySearch = _Context.StockTransaction.Where(k => k.SortingMaster.InvoiceNo == (!string.IsNullOrEmpty(model.InvoiceNo) ? model.InvoiceNo : k.SortingMaster.InvoiceNo)
                && k.SortingMaster.LocationID == (model.LocationID != "" ? model.LocationID : k.SortingMaster.LocationID)
                && k.SortingMaster.RepakedCaseNo == (model.RepakedCaseNo != "" ? model.RepakedCaseNo : k.SortingMaster.RepakedCaseNo)
                && k.SortingMaster.ItemCode == (model.ItemCode != "" ? model.ItemCode : k.SortingMaster.ItemCode)
                && k.SortingMaster.CustomerCode == (model.CustomerCode != "" ? model.CustomerCode : k.SortingMaster.CustomerCode)
                && k.SortingMaster.IsDelete == false && k.SortingMaster.IsBin == false && k.SortingMaster.IsPutaway == true
                && (k.QuantitySelected > 0 ? k.QuantityRemaining == k.QuantityRemaining : k.QuantityRemaining > 0)).Select(b => new InventorySearchRepackCase
                {
                    SortingMasterID = b.PK_SortingID,
                    Location = b.SortingMaster.LocationID,
                    InvoiceNo = b.SortingMaster.InvoiceNo,
                    RepackCase = b.SortingMaster.RepakedCaseNo,
                    PartNo = b.SortingMaster.ItemCode,
                    Description = b.SortingMaster.SortingDescription,
                    //Description = _partsMasterService.GetDetailsBynumber(b.SortingMaster.ItemCode).PartDescription,
                    Qty = b.QuantityRemaining ?? 0,
                    //Dimention = b.Length * b.Height * b.Breadth,
                    CustomerCode = b.SortingMaster.CustomerCode,
                    // weight = _Context.SortingDetail.Where(p => p.FK_SortingMasterID == b.PK_SortingID).FirstOrDefault().Weight?? default(decimal)
                }).OrderByDescending(k => k.RepackCase).AsQueryable();
                pagedItems.TotalResults = InventorySearch.Count();// _Context.ASNItemDetails.Where(k => k.ContainerNo == (searchmodel.ContainerNo != "" ? searchmodel.ContainerNo : k.ContainerNo) && k.ASNMaster.DateOfArrival.Date == (searchmodel.DateOfArrival != null ? searchmodel.DateOfArrival : k.ASNMaster.DateOfArrival.Date) && k.ASNMaster.InvoiceNo == (searchmodel.InvoiceNo != "" ? searchmodel.InvoiceNo : k.ASNMaster.InvoiceNo) && k.ASNMaster.Remark == (searchmodel.ShippingRefNumber != "" ? searchmodel.ShippingRefNumber : k.ASNMaster.Remark)).Count();
                if (model.Skip.HasValue)
                {
                    InventorySearch = InventorySearch.Skip(model.Skip.Value);
                }
                else
                {
                    InventorySearch = InventorySearch.Skip(0);
                }

                if (model.Take.HasValue)
                {
                    InventorySearch = InventorySearch.Take(model.Take.Value);
                }
                else
                {
                    InventorySearch = InventorySearch.Take(10);
                }
                pagedItems.Items = InventorySearch.ToList();
                decimal d = 0;
                foreach (var item in pagedItems.Items)
                {
                    item.weight = _Context.SortingDetail.Where(p => p.RepakedCaseNo == item.RepackCase).FirstOrDefault() != null ? _Context.SortingDetail.Where(p => p.FK_SortingMasterID == item.SortingMasterID).FirstOrDefault().Weight : d;

                    var Data = _Context.SortingDetail.Where(a => a.RepakedCaseNo == item.RepackCase).FirstOrDefault();
                    if (Data != null)
                    {
                        item.Dimention = Data.Length + " X " + Data.Breadth + " X " + Data.Height;
                        item.weight = Data.Weight;
                    }
                    else
                    {
                        item.Dimention = "";
                        item.weight = 0;
                    }

                }

                if (pagedItems.TotalResults > 0)
                {
                    result.Message = "Re cord Get Successfully";
                    result.StatusCode = 1;
                    result.IsValid = true;
                    result.Result = "success";
                    result.Data = pagedItems;
                }
                else
                {
                    result.Message = "Record does not exists";
                    result.StatusCode = 0;
                    result.IsValid = false;
                    result.Result = "error";
                    result.Data = null;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;

        }

        public ResponseviewModel InventorySearchForRackExcelExport(InventorySearchViewModel model)
        {
            try
            {
                var InventorySearch = _Context.StockTransaction.Where(k => k.SortingMaster.IsDelete == false
                && k.SortingMaster.InvoiceNo == (model.InvoiceNo != null ? model.InvoiceNo : k.SortingMaster.InvoiceNo)
                && k.SortingMaster.CustomerCode == (model.CustomerCode != null ? model.CustomerCode : k.SortingMaster.CustomerCode)
                && k.SortingMaster.LocationID == (model.LocationID != "" ? model.LocationID : k.SortingMaster.LocationID)
                && k.SortingMaster.RepakedCaseNo == (model.RepakedCaseNo != "" ? model.RepakedCaseNo : k.SortingMaster.RepakedCaseNo)
                && k.SortingMaster.ItemCode == (model.ItemCode != "" ? model.ItemCode : k.SortingMaster.ItemCode)
                //&& k.SortingMaster.IsDelete == false
                && (k.QuantitySelected > 0 ? k.QuantityRemaining == k.QuantityRemaining : k.QuantityRemaining > 0)
                && k.SortingMaster.IsBin == false && k.SortingMaster.IsPutaway == true)
                .Select(b => new InventorySearchRepackCase
                {
                    SortingMasterID = b.PK_SortingID,
                    Location = b.SortingMaster.LocationID,
                    InvoiceNo = b.SortingMaster.InvoiceNo,
                    RepackCase = b.SortingMaster.RepakedCaseNo,
                    PartNo = b.SortingMaster.ItemCode,
                    Description = b.SortingMaster.SortingDescription,
                    //Description = _partsMasterService.GetDetailsBynumber(b.SortingMaster.ItemCode).PartDescription,
                    Qty = b.QuantityRemaining ?? 0,
                    //Dimention = b.SortingMaster.Length + " X " + b.Breadth + " X " + b.Height,
                    CustomerCode = b.SortingMaster.CustomerCode,
                    weight = _Context.SortingDetail.Where(p => p.FK_SortingMasterID == b.PK_SortingID).FirstOrDefault().Weight
                    //weight = b.Weight
                }).ToList();
                //InventorySearch = InventorySearch.Take(2);
                //var InventorySearch1 = InventorySearch.ToList();
                decimal d = 0;
                foreach (var item in InventorySearch)
                {
                    item.weight = _Context.SortingDetail.Where(p => p.RepakedCaseNo == item.RepackCase).FirstOrDefault() != null ? _Context.SortingDetail.Where(p => p.FK_SortingMasterID == item.SortingMasterID).FirstOrDefault().Weight : d;

                    var Data = _Context.SortingDetail.Where(a => a.RepakedCaseNo == item.RepackCase).FirstOrDefault();
                    if (Data != null)
                    {
                        item.Dimention = Data.Length + " X " + Data.Breadth + " X " + Data.Height;
                        item.weight = Data.Weight;
                    }
                    else
                    {
                        item.Dimention = "";
                        item.weight = 0;
                    }

                }

                if (InventorySearch.Count > 0)
                {
                    result.Message = "Record Get Successfully";
                    result.StatusCode = 1;
                    result.IsValid = true;
                    result.Result = "success";
                    result.Data = InventorySearch;
                }
                else
                {
                    result.Message = "Record does not exists";
                    result.StatusCode = 0;
                    result.IsValid = false;
                    result.Result = "error";
                    result.Data = null;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        public ResponseviewModel GetInventorySearchForBin(InventorySearchViewModel model)
        {

            InventorySearchViewModel viewModel = new InventorySearchViewModel();
            PagedItemSet<InventorySearchItemCode> pagedItems = new PagedItemSet<InventorySearchItemCode>();
            viewModel.lstinventoryLocation = new List<InventorySearchLocation>();
            var query = _Context.StockTransaction.Where(k => k.SortingMaster.LocationID.ToLower().Trim() == (model.LocationID.ToLower().Trim() != "" ? model.LocationID.ToLower().Trim() : k.SortingMaster.LocationID.ToLower().Trim()) && k.SortingMaster.IsDelete == false && k.SortingMaster.IsBin == true && k.SortingMaster.ItemCode.ToLower().Trim() == (model.ItemCode.ToLower().Trim() != "" ? model.ItemCode.ToLower().Trim() : k.SortingMaster.ItemCode.ToLower().Trim()) && ((k.QuantityRemaining > 0) || (k.QuantitySelected > 0))).AsQueryable();
            pagedItems.TotalResults = query.Count();
            if (model.Skip.HasValue)
            {
                query = query.Skip(model.Skip.Value);
            }
            else
            {
                query = query.Skip(0);
            }

            if (model.Take.HasValue)
            {
                query = query.Take(model.Take.Value);
            }
            else
            {
                query = query.Take(10);
            }

            pagedItems.Items = query.Select(k => new InventorySearchItemCode
            {
                CustomerCode = k.SortingMaster.CustomerCode,
                Description = k.SortingMaster.SortingDescription,
                location = k.SortingMaster.LocationID,
                Partno = k.SortingMaster.ItemCode,
                Qty = k.QuantityRemaining.HasValue ? k.QuantityRemaining.Value : 0,
                //Qty=k.SortingMaster.Quantity

            }).OrderByDescending(a => a.Partno).ThenBy(a => a.location).ToList();

            result.Data = pagedItems;
            result.Message = "Data Get";
            result.StatusCode = 1;
            result.IsValid = true;
            result.Result = "success";
            //  var final = new List<InventorySearchItemCode>();


            //if (model.RackNo != "")
            //{
            //    InventorySearchViewModel viewModel = new InventorySearchViewModel();
            //    PagedItemSet<InventorySearchLocation> pagedItems = new PagedItemSet<InventorySearchLocation>();
            //    viewModel.lstinventoryLocation = new List<InventorySearchLocation>();
            //    var query = _Context.SortingMaster.Where(k => k.LocationID.ToLower().Trim() == model.RackNo.ToLower().Trim() && k.IsDelete == false && k.IsBin == true).GroupBy(g => g.LocationID).AsQueryable();
            //    pagedItems.TotalResults = query.Count();
            //    if (model.Skip.HasValue)
            //    {
            //        query = query.Skip(model.Skip.Value);
            //    }
            //    else
            //    {
            //        query = query.Skip(0);
            //    }

            //    if (model.Take.HasValue)
            //    {
            //        query = query.Take(model.Take.Value);
            //    }
            //    else
            //    {
            //        query = query.Take(10);
            //    }
            //    viewModel.lstinventoryLocation.Add(new InventorySearchLocation
            //    {
            //        LocationID = model.RackNo,
            //        lstRepackcase = query.Select(
            //        a => new InventorySearchRepackCase
            //        {
            //            RepackCase = a.Key,
            //            lstitems = a.Select(b => new InventorySearchItemCode
            //            {
            //                Partno = b.ItemCode,
            //                Description = _partsMasterService.GetDetailsBynumber(b.ItemCode).PartDescription,
            //                Qty = b.Quantity


            //            }).ToList()

            //        }).ToList()
            //    });
            //    foreach (var location in viewModel.lstinventoryLocation)
            //    {
            //        foreach (var repackCase in location.lstRepackcase)
            //        {
            //            foreach (var itemcodes in repackCase.lstitems)
            //            {
            //                itemcodes.Qty = GetAvailableQuantityItemCodeBin(repackCase.RepackCase, itemcodes.Partno);
            //            }
            //        }

            //    }
            //    pagedItems.Items = viewModel.lstinventoryLocation;
            //    result.Data = pagedItems;
            //    result.Message = "Data Get";
            //    result.StatusCode = 1;
            //    result.IsValid = true;
            //    result.Result = "success";

            //}

            //else if (model.Partnumber != "")
            //{
            //    InventorySearchViewModel viewModel = new InventorySearchViewModel();
            //    viewModel.lstinventoryLocation = new List<InventorySearchLocation>();
            //    PagedItemSet<InventorySearchLocation> pagedItems = new PagedItemSet<InventorySearchLocation>();
            //    var query = _Context.SortingMaster.Where(k => k.ItemCode.ToLower().Trim() == model.Partnumber.ToLower().Trim() && k.IsDelete == false && k.IsBin == true).GroupBy(g => g.LocationID).AsQueryable();
            //    pagedItems.TotalResults = query.Count();
            //    if (model.Skip.HasValue)
            //    {
            //        query = query.Skip(model.Skip.Value);
            //    }
            //    else
            //    {
            //        query = query.Skip(0);
            //    }

            //    if (model.Take.HasValue)
            //    {
            //        query = query.Take(model.Take.Value);
            //    }
            //    else
            //    {
            //        query = query.Take(10);
            //    }
            //    viewModel.lstinventoryLocation.AddRange(query.Select(
            //        a => new InventorySearchLocation
            //        {
            //            LocationID = a.Key,
            //            lstRepackcase = query.Select(
            //        b => new InventorySearchRepackCase
            //        {
            //            RepackCase = b.Key,
            //            lstitems = b.Select(c => new InventorySearchItemCode
            //            {
            //                Partno = c.ItemCode,
            //                Description = c.SortingDescription,
            //                Qty = c.Quantity


            //            }).ToList()

            //        }).ToList()

            //        }));
            //    var final = new List<InventorySearchItemCode>();
            //    foreach (var location in viewModel.lstinventoryLocation)
            //    {
            //        foreach (var repackCase in location.lstRepackcase)
            //        {
            //            foreach (var itemcodes in repackCase.lstitems)
            //            {

            //                itemcodes.Qty = GetAvailableQuantityItemCodeBin(repackCase.RepackCase, itemcodes.Partno);
            //                if (itemcodes.Qty>0)
            //                {
            //                    final.Add(itemcodes);
            //                }
            //            }
            //        }
            //    }

            //    pagedItems.Items = viewModel.lstinventoryLocation;
            //    result.Data = pagedItems;
            //    result.Message = "Data Get";
            //    result.StatusCode = 1;
            //    result.IsValid = true;
            //    result.Result = "success";
            //}
            //else
            //{
            //    InventorySearchViewModel viewModel = new InventorySearchViewModel();
            //    PagedItemSet<InventorySearchLocation> pagedItems = new PagedItemSet<InventorySearchLocation>();
            //    viewModel.lstinventoryLocation = new List<InventorySearchLocation>();
            //    var query = _Context.SortingMaster.Where(k => k.IsDelete == false && k.IsBin == true).GroupBy(g => g.LocationID).AsQueryable();
            //    pagedItems.TotalResults = query.Count();
            //    if (model.Skip.HasValue)
            //    {
            //        query = query.Skip(model.Skip.Value);
            //    }
            //    else
            //    {
            //        query = query.Skip(0);
            //    }

            //    if (model.Take.HasValue)
            //    {
            //        query = query.Take(model.Take.Value);
            //    }
            //    else
            //    {
            //        query = query.Take(10);
            //    }
            //    viewModel.lstinventoryLocation.Add(new InventorySearchLocation
            //    {
            //        //LocationID = model.RackNo,
            //        lstRepackcase = query.Select(
            //        a => new InventorySearchRepackCase
            //        {
            //            RepackCase = a.Key,
            //            lstitems = a.Select(b => new InventorySearchItemCode
            //            {
            //                Partno = b.ItemCode,
            //                Description = b.SortingDescription,
            //                Qty = b.Quantity
            //            }).ToList()

            //        }).ToList()
            //    });
            //    foreach (var location in viewModel.lstinventoryLocation)
            //    {
            //        foreach (var repackCase in location.lstRepackcase)
            //        {
            //            foreach (var itemcodes in repackCase.lstitems)
            //            {
            //                itemcodes.Qty = GetAvailableQuantityItemCodeBin(repackCase.RepackCase, itemcodes.Partno);
            //            }
            //        }

            //    }
            //    pagedItems.Items = viewModel.lstinventoryLocation;
            //    result.Data = pagedItems;
            //    result.Message = "Data Get";
            //    result.StatusCode = 1;
            //    result.IsValid = true;
            //    result.Result = "success";

            //}
            return result;

        }

        public ResponseviewModel GetInventorySearchForBinExcelExport(InventorySearchViewModel model)
        {
            //if (model.LocationID != "")
            //{
            //    InventorySearchViewModel viewModel = new InventorySearchViewModel();
            //    PagedItemSet<InventorySearchLocation> pagedItems = new PagedItemSet<InventorySearchLocation>();
            //    viewModel.lstinventoryLocation = new List<InventorySearchLocation>();
            //    var query = _Context.SortingMaster.Where(k => k.LocationID.ToLower().Trim() == model.LocationID.ToLower().Trim() && k.IsDelete == false && k.IsBin == true).GroupBy(g => g.LocationID).ToList();
            //    viewModel.lstinventoryLocation.Add(new InventorySearchLocation
            //    {
            //        LocationID = model.LocationID,
            //        lstRepackcase = query.Select(
            //        a => new InventorySearchRepackCase
            //        {
            //            RepackCase = a.Key,
            //            lstitems = a.Select(b => new InventorySearchItemCode
            //            {
            //                Partno = b.ItemCode,
            //                Description = _partsMasterService.GetDetailsBynumber(b.ItemCode).PartDescription,
            //                Qty = b.Quantity


            //            }).ToList()

            //        }).ToList()
            //    });
            //    foreach (var location in viewModel.lstinventoryLocation)
            //    {
            //        foreach (var repackCase in location.lstRepackcase)
            //        {
            //            foreach (var itemcodes in repackCase.lstitems)
            //            {
            //                itemcodes.Qty = GetAvailableQuantityItemCodeBin(repackCase.RepackCase, itemcodes.Partno);
            //            }
            //        }

            //    }
            //    pagedItems.Items = viewModel.lstinventoryLocation;
            //    result.Data = pagedItems;
            //    result.Message = "Data Get";
            //    result.StatusCode = 1;
            //    result.IsValid = true;
            //    result.Result = "success";

            //}

            //else if (model.ItemCode != "")
            //{
            //    InventorySearchViewModel viewModel = new InventorySearchViewModel();
            //    viewModel.lstinventoryLocation = new List<InventorySearchLocation>();
            //    PagedItemSet<InventorySearchLocation> pagedItems = new PagedItemSet<InventorySearchLocation>();
            //    var query = _Context.SortingMaster.Where(k => k.ItemCode.ToLower().Trim() == model.ItemCode.ToLower().Trim() && k.IsDelete == false && k.IsBin == true).GroupBy(g => g.LocationID).ToList();
            //    viewModel.lstinventoryLocation.AddRange(query.Select(
            //        a => new InventorySearchLocation
            //        {
            //            LocationID = a.Key,
            //            lstRepackcase = query.Select(
            //        b => new InventorySearchRepackCase
            //        {
            //            RepackCase = b.Key,
            //            lstitems = b.Select(c => new InventorySearchItemCode
            //            {
            //                Partno = c.ItemCode,
            //                Description = c.SortingDescription,
            //                Qty = c.Quantity


            //            }).ToList()

            //        }).ToList()

            //        }));
            //    foreach (var location in viewModel.lstinventoryLocation)
            //    {
            //        foreach (var repackCase in location.lstRepackcase)
            //        {
            //            foreach (var itemcodes in repackCase.lstitems)
            //            {
            //                itemcodes.Qty = GetAvailableQuantityItemCodeBin(repackCase.RepackCase, itemcodes.Partno);
            //            }
            //        }
            //    }

            //    pagedItems.Items = viewModel.lstinventoryLocation;
            //    result.Data = pagedItems;
            //    result.Message = "Data Get";
            //    result.StatusCode = 1;
            //    result.IsValid = true;
            //    result.Result = "success";
            //}
            //else
            //{
            //    InventorySearchViewModel viewModel = new InventorySearchViewModel();
            //    PagedItemSet<InventorySearchLocation> pagedItems = new PagedItemSet<InventorySearchLocation>();
            //    viewModel.lstinventoryLocation = new List<InventorySearchLocation>();
            //    var query = _Context.SortingMaster.Where(k => k.IsDelete == false && k.IsBin == true).GroupBy(g => g.LocationID).ToList();
            //    viewModel.lstinventoryLocation.Add(new InventorySearchLocation
            //    {
            //        //LocationID = model.RackNo,
            //        lstRepackcase = query.Select(
            //        a => new InventorySearchRepackCase
            //        {
            //            RepackCase = a.Key,
            //            lstitems = a.Select(b => new InventorySearchItemCode
            //            {
            //                Partno = b.ItemCode,
            //                Description = b.SortingDescription,
            //                Qty = b.Quantity
            //            }).ToList()

            //        }).ToList()
            //    });
            //    foreach (var location in viewModel.lstinventoryLocation)
            //    {
            //        foreach (var repackCase in location.lstRepackcase)
            //        {
            //            foreach (var itemcodes in repackCase.lstitems)
            //            {
            //                itemcodes.Qty = GetAvailableQuantityItemCodeBin(repackCase.RepackCase, itemcodes.Partno);
            //            }
            //        }

            //    }
            //    pagedItems.Items = viewModel.lstinventoryLocation;
            //    result.Data = pagedItems;
            //    result.Message = "Data Get";
            //    result.StatusCode = 1;
            //    result.IsValid = true;
            //    result.Result = "success";

            //}

            InventorySearchViewModel viewModel = new InventorySearchViewModel();
            PagedItemSet<InventorySearchItemCode> pagedItems = new PagedItemSet<InventorySearchItemCode>();
            viewModel.lstinventoryLocation = new List<InventorySearchLocation>();
            var query = _Context.StockTransaction.Where(k => k.SortingMaster.LocationID.ToLower().Trim() == (model.LocationID.ToLower().Trim() != "" ? model.LocationID.ToLower().Trim() : k.SortingMaster.LocationID.ToLower().Trim()) && k.SortingMaster.IsDelete == false && k.SortingMaster.IsBin == true && k.SortingMaster.ItemCode.ToLower().Trim() == (model.ItemCode.ToLower().Trim() != "" ? model.ItemCode.ToLower().Trim() : k.SortingMaster.ItemCode.ToLower().Trim()) && ((k.QuantityRemaining > 0) || (k.QuantitySelected > 0))).AsQueryable();
            pagedItems.Items = query.Select(k => new InventorySearchItemCode
            {
                CustomerCode = k.SortingMaster.CustomerCode,
                Description = k.SortingMaster.SortingDescription,
                location = k.SortingMaster.LocationID,
                Partno = k.SortingMaster.ItemCode,
                Qty = k.QuantityRemaining.HasValue ? k.QuantityRemaining.Value : 0,
            }).ToList();

            result.Data = pagedItems;
            result.Message = "Data Get";
            result.StatusCode = 1;
            result.IsValid = true;
            result.Result = "success";
            return result;

        }
    }
}
