using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Domain.Engine.Access.Interfaces;
using Domain.Objects.Models;
using Infrastructure.Model.Main;
using Infrastructure.Model.Main.Entities;

namespace Domain.Engine.Access.Impl
{
  public class LocationMasterService : ILocationMasterService
  {
    protected readonly MainDbContext _Context;
    ResponseviewModel result = new ResponseviewModel();
    protected MainDbContext Context
    {
      get
      {
        return (MainDbContext)_Context;
      }
    }

    public LocationMasterService(IMainDbContext context)
    {
      _Context = (MainDbContext)context;
    }

    public ResponseviewModel InsertLocationMaster(List<LocationMasterModel> listlocationmastermodel)
    {
      try
      {
        LocationMaster existlocationmaster = new LocationMaster();
        existlocationmaster = _Context.LocationMaster.Where(d => d.RackName.ToLower().Trim() == listlocationmastermodel.FirstOrDefault().RackName.ToLower().Trim()).FirstOrDefault();
        if (existlocationmaster == null)
        {
          foreach (var item in listlocationmastermodel)
          {
            LocationMaster locationmaster = new LocationMaster();
            locationmaster.RackName = item.RackName;
            locationmaster.LocationCode = item.LocationCode;
            locationmaster.LocationType = item.LocationType;
            locationmaster.ZoneType = item.ZoneType;
            locationmaster.IsUsed = false;
            locationmaster.IsActive = true;
            locationmaster.IsDelete = false;
            locationmaster.CreatedBy = item.CreatedBy;
            locationmaster.CreatedDate = System.DateTime.Now;
            locationmaster.ModifiedDate = System.DateTime.Now;
            _Context.Add(locationmaster);
            _Context.SaveChanges();
          }
          result.Message = "Data inserted successfully";
          result.StatusCode = 1;
          result.IsValid = true;
          result.Result = "success";
          result.Data = listlocationmastermodel;
        }
        else
        {
          result.Message = "Rack name already exists.!";
          result.StatusCode = 0;
          result.IsValid = false;
          result.Result = "error";
          result.pk_ID = null;
          result.Data = null;
        }
      }
      catch (Exception ex)
      {
        result.Message = ex.ToString();
        result.StatusCode = 0;
        result.IsValid = false;
        result.Result = "error";
        result.pk_ID = null;
        result.Data = null;

      }
      return result;
    }

    public ResponseviewModel GetAllLocationMasterList()
    {
      try
      {
        List<LocationMaster> listlocationmaster = new List<LocationMaster>();
        listlocationmaster = _Context.LocationMaster.Where(d => d.IsActive == true && d.IsDelete == false).OrderBy(x => x.LocationCode).ToList();
        if (listlocationmaster.Count > 0)
        {
          result.Message = "Record Get Successfully";
          result.StatusCode = 1;
          result.IsValid = true;
          result.Result = "success";
          result.Data = listlocationmaster;
        }
        else
        {
          result.Message = "Record does not exists.!";
          result.StatusCode = 0;
          result.IsValid = false;
          result.Result = "error";
          result.pk_ID = null;
          result.Data = null;
        }
      }
      catch (Exception ex)
      {
        result.Message = ex.ToString();
        result.StatusCode = 0;
        result.IsValid = false;
        result.Result = "error";
        result.pk_ID = null;
        result.Data = null;
      }
      return result;
    }


    public ResponseviewModel GetDLLRackName()
    {
      try
      {
        var RackNameList = _Context.LocationMaster.Where(d => d.IsActive == true && d.IsDelete == false).OrderBy(x => x.RackName).Select(x => new
        {
          ID = x.RackName,
          Value = x.RackName,
          LocationType = x.LocationType,
          ZoneType = x.ZoneType
        }).Distinct().ToList();
        if (RackNameList.Count > 0)
        {
          result.Message = "Record Get Successfully";
          result.StatusCode = 1;
          result.IsValid = true;
          result.Result = "success";
          result.Data = RackNameList;
        }
        else
        {
          result.Message = "Record does not exists";
          result.StatusCode = 0;
          result.IsValid = false;
          result.Result = "error";
          result.pk_ID = null;
          result.Data = null;

        }
      }
      catch (Exception ex)
      {
        result.Message = ex.ToString();
        result.StatusCode = 0;
        result.IsValid = false;
        result.Result = "error";
        result.pk_ID = null;
        result.Data = null;

      }
      return result;
    }


    public ResponseviewModel GetLocationMasterListbyRackName(string RackName)
    {
      try
      {
        List<AllLocationMasterModel> listlocationmaster = new List<AllLocationMasterModel>();
        var listlocationmaster1 = _Context.LocationMaster.Where(d => d.RackName.ToLower().Trim() == RackName.ToLower().Trim() && d.IsActive == true && d.IsDelete == false).OrderBy(x => x.LocationCode).Select(a => new AllLocationMasterModel
        {
          LocationCode = a.LocationCode,
          Pk_LocationMasterId = a.Pk_LocationMasterId,
          IsUsed = a.IsUsed,
          IsActive = a.IsActive,
          IsDelete = a.IsDelete
        }).ToList();

        var listlocationmaster2 = Context.BinMaster.Where(d => d.RackName.ToLower().Trim() == RackName.ToLower().Trim() && d.IsActive == true && d.IsDelete == false).OrderBy(x => x.LocationCode).Select(a => new AllLocationMasterModel
        {
          LocationCode = a.LocationCode,
          Pk_LocationMasterId = a.Pk_BinMasterId,
          IsUsed = a.IsUsed,
          IsActive = a.IsActive,
          IsDelete = a.IsDelete
        }).ToList();
        listlocationmaster = listlocationmaster1.Union(listlocationmaster2).OrderBy(a => a.LocationCode).ToList();
        if (listlocationmaster.Count > 0)
        {
          result.Message = "Record Get Successfully";
          result.StatusCode = 1;
          result.IsValid = true;
          result.Result = "success";
          result.Data = listlocationmaster;
        }
        else
        {
          result.Message = "Record does not exists.!";
          result.StatusCode = 0;
          result.IsValid = false;
          result.Result = "error";
          result.pk_ID = null;
          result.Data = null;
        }
      }
      catch (Exception ex)
      {
        result.Message = ex.ToString();
        result.StatusCode = 0;
        result.IsValid = false;
        result.Result = "error";
        result.pk_ID = null;
        result.Data = null;
      }
      return result;
    }

    public ResponseviewModel GetDLLLocationMaster(int LocationType, int ZoneType)
    {
      try
      {
        var LocationMasterList = _Context.LocationMaster.Where(d => d.LocationType == LocationType && d.ZoneType == ZoneType && d.IsActive == true && d.IsDelete == false).OrderBy(x => x.LocationCode).Select(x => new StringDropdownviewModel
        {
          ID = x.LocationCode,
          Value = x.LocationCode,
        }).Distinct().ToList();
        if (LocationMasterList.Count > 0)
        {
          result.Message = "Record Get Successfully";
          result.StatusCode = 1;
          result.IsValid = true;
          result.Result = "success";
          result.Data = LocationMasterList;
        }
        else
        {
          result.Message = "Record does not exists";
          result.StatusCode = 0;
          result.IsValid = false;
          result.Result = "error";
          result.pk_ID = null;
          result.Data = null;
        }
      }
      catch (Exception ex)
      {
        result.Message = ex.ToString();
        result.StatusCode = 0;
        result.IsValid = false;
        result.Result = "error";
        result.pk_ID = null;
        result.Data = null;

      }
      return result;
    }

    public Boolean ChangeActiveByLocationCode(string LocationCode)
    {
      try
      {
        var change = _Context.LocationMaster.Where(d => d.LocationCode.ToLower().Trim() == LocationCode.ToLower().Trim()).FirstOrDefault();
        change.IsUsed = true;
        change.ModifiedDate = System.DateTime.Now;
        _Context.LocationMaster.Update(change);
        _Context.SaveChanges();
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public Boolean ChangeUnusedByLocationCode(string LocationCode)
    {
      try
      {
                if (!_Context.StockTransaction.Where(k => k.SortingMaster.LocationID == LocationCode && k.SortingMaster.IsBin != true && k.SortingMaster.IsDelete == false && k.QuantityRemaining > 0).Any())
                {
                    var change = _Context.LocationMaster.Where(d => d.LocationCode.ToLower().Trim() == LocationCode.ToLower().Trim()).FirstOrDefault();

                    if (change != null)
                    {
                        change.IsUsed = false;
                        change.ModifiedDate = System.DateTime.Now;
                        _Context.LocationMaster.Update(change);
                        _Context.SaveChanges();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
      }
      catch (Exception)
      {
        return false;
      }
    }
  }
}
