using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper;
using System.Threading.Tasks;
using Domain.Engine.Access.Interfaces;
using Microsoft.EntityFrameworkCore;
using Domain.Objects.Models;
using Infrastructure.Model.Main;
using Infrastructure.Model.Main.Entities;


namespace Domain.Engine.Access.Impl
{
    public class ASNMasterService : IASNMasterService
    {
        protected readonly MainDbContext _Context;
        private readonly IASNItemDetailService _AsnItemDetailService;
        private readonly IGoodReceiptService _GoodReceiptService;
        private readonly IBarCodeService _BarCodeService;
        private readonly ILocationMasterService _locationMasterService;
        ResponseviewModel result = new ResponseviewModel();
        private readonly IMapper _mapper;
        protected MainDbContext Context
        {
            get
            {
                return (MainDbContext)_Context;
            }
        }
        public ASNMasterService(IMainDbContext context, IASNItemDetailService asnitemdetailservice, IGoodReceiptService goodreceiptservice, IBarCodeService barcodeservice, IMapper mapper, ILocationMasterService locationMasterService)
        {
            _Context = (MainDbContext)context;
            _AsnItemDetailService = asnitemdetailservice;
            _GoodReceiptService = goodreceiptservice;
            _BarCodeService = barcodeservice;
            _mapper = mapper;
            _locationMasterService = locationMasterService;
        }

        public ResponseviewModel InsertUpdateAsnMaster(ASNMasterModels asnmastermodels)
        {

            try
            {
                if (asnmastermodels.PK_AsnMasterID == null)
                {
                    ASNMaster asnmaster = new ASNMaster();
                    asnmaster.PK_AsnMasterID = Guid.NewGuid();
                    asnmaster.CompanyMaster.PK_CompanyID = asnmastermodels.Fk_CompanyID;
                    asnmaster.SupplierMaster.Pk_SupplierID = asnmastermodels.Fk_SupplierID;
                    asnmaster.DateOfArrival = asnmastermodels.DateOfArrival;
                    asnmaster.InvoiceNo = asnmastermodels.InvoiceNo.Trim();
                    asnmaster.origin = asnmastermodels.origin;
                    asnmaster.Remark = asnmastermodels.Remark;
                    asnmaster.shippingmodeID = asnmastermodels.shippingmodeID;
                    asnmaster.ShippingmodeName = asnmastermodels.ShippingmodeName;
                    asnmaster.VesselName = asnmastermodels.VesselName;
                    asnmaster.VesselVoyageNo = asnmastermodels.VesselVoyageNo;
                    asnmaster.BillNumber = asnmastermodels.BillNumber;
                    asnmaster.FlightName = asnmastermodels.FlightName;
                    asnmaster.FlightNumber = asnmastermodels.FlightNumber;
                    asnmaster.AWLNumber = asnmastermodels.AWLNumber;
                    asnmaster.TruckNumber = asnmastermodels.TruckNumber;
                    asnmaster.DeliveryNumber = asnmastermodels.DeliveryNumber;
                    asnmaster.Description = asnmastermodels.Description;
                    asnmaster.IsDelete = false;
                    asnmaster.IsActive = true;
                    asnmaster.CreatedBy = asnmastermodels.CreatedBy;
                    asnmaster.CreatedDate = System.DateTime.Now;
                    asnmaster.ModifiedBy = asnmastermodels.CreatedBy;
                    asnmaster.ModifiedDate = System.DateTime.Now;
                    _Context.AddAsync(asnmaster);
                    _Context.SaveChanges();
                    result.Message = "Data Inserted Successfully";
                    result.StatusCode = 1;
                    result.IsValid = true;
                    result.Result = "success";
                    result.pk_ID = asnmaster.PK_AsnMasterID;
                    result.Data = asnmaster;
                }
                else
                {
                    ASNMaster asnmaster = _Context.ASNMaster.FirstOrDefault(d => d.PK_AsnMasterID == asnmastermodels.PK_AsnMasterID && d.IsActive == true && d.IsDelete == false);
                    if (asnmaster != null)
                    {
                        asnmaster.CompanyMaster.PK_CompanyID = asnmastermodels.Fk_CompanyID;
                        asnmaster.SupplierMaster.Pk_SupplierID = asnmastermodels.Fk_SupplierID;
                        asnmaster.DateOfArrival = asnmastermodels.DateOfArrival;
                        asnmaster.InvoiceNo = asnmastermodels.InvoiceNo;
                        asnmaster.origin = asnmastermodels.origin;
                        asnmaster.Remark = asnmastermodels.Remark;
                        asnmaster.shippingmodeID = asnmastermodels.shippingmodeID;
                        asnmaster.ShippingmodeName = asnmastermodels.ShippingmodeName;
                        asnmaster.VesselName = asnmastermodels.VesselName;
                        asnmaster.VesselVoyageNo = asnmastermodels.VesselVoyageNo;
                        asnmaster.BillNumber = asnmastermodels.BillNumber;
                        asnmaster.FlightName = asnmastermodels.FlightName;
                        asnmaster.FlightNumber = asnmastermodels.FlightNumber;
                        asnmaster.AWLNumber = asnmastermodels.AWLNumber;
                        asnmaster.TruckNumber = asnmastermodels.TruckNumber;
                        asnmaster.DeliveryNumber = asnmastermodels.DeliveryNumber;
                        asnmaster.Description = asnmastermodels.Description;
                        asnmaster.IsDelete = false;
                        asnmaster.IsActive = true;
                        asnmaster.ModifiedDate = System.DateTime.Now;
                        asnmaster.CreatedBy = asnmastermodels.CreatedBy;

                        asnmaster.ModifiedBy = asnmastermodels.ModifiedBy;
                        _Context.Update(asnmaster);
                        _Context.SaveChanges();
                        result.Message = "Data Updated Successfully";
                        result.StatusCode = 1;
                        result.IsValid = true;
                        result.Result = "success";
                        result.pk_ID = asnmaster.PK_AsnMasterID;
                        result.Data = asnmaster;
                    }
                    else
                    {
                        result.Message = "Record does not exists";
                        result.StatusCode = 0;
                        result.IsValid = false;
                        result.Result = "error";
                        result.pk_ID = asnmaster.PK_AsnMasterID;
                        result.Data = null;
                    }
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.pk_ID = null;
                result.Data = null;
            }
            return result;
        }

        public ResponseviewModel UploadASNandItemDetail(ASNandlistasnitemsModel asnandlistasnitemsmodel)
        {
            try
            {

                var CheckInvoiceAvailable = _Context.ASNMaster.Where(d => d.InvoiceNo.ToLower().Trim() == asnandlistasnitemsmodel.InvoiceNo.ToLower().Trim() && d.IsDelete != true).FirstOrDefault();
                if (CheckInvoiceAvailable == null)
                {
                    ASNMaster asnmaster = new ASNMaster();
                    asnmaster.PK_AsnMasterID = Guid.NewGuid();
                    asnmaster.FK_CompanyID = asnandlistasnitemsmodel.Fk_CompanyID;
                    asnmaster.FK_SupplierID = asnandlistasnitemsmodel.Fk_SupplierID;
                    asnmaster.DateOfArrival = asnandlistasnitemsmodel.DateOfArrival;
                    asnmaster.InvoiceNo = asnandlistasnitemsmodel.InvoiceNo;
                    asnmaster.origin = asnandlistasnitemsmodel.origin;
                    asnmaster.Remark = asnandlistasnitemsmodel.Remark;
                    asnmaster.shippingmodeID = asnandlistasnitemsmodel.shippingmodeID;
                    asnmaster.ShippingmodeName = asnandlistasnitemsmodel.ShippingmodeName;
                    asnmaster.VesselName = asnandlistasnitemsmodel.VesselName;
                    asnmaster.VesselVoyageNo = asnandlistasnitemsmodel.VesselVoyageNo;
                    asnmaster.BillNumber = asnandlistasnitemsmodel.BillNumber;
                    asnmaster.FlightName = asnandlistasnitemsmodel.FlightName;
                    asnmaster.FlightNumber = asnandlistasnitemsmodel.FlightNumber;
                    asnmaster.AWLNumber = asnandlistasnitemsmodel.AWLNumber;
                    asnmaster.TruckNumber = asnandlistasnitemsmodel.TruckNumber;
                    asnmaster.DeliveryNumber = asnandlistasnitemsmodel.DeliveryNumber;
                    asnmaster.Description = asnandlistasnitemsmodel.Description;
                    asnmaster.IsDelete = false;
                    asnmaster.IsActive = true;
                    asnmaster.CreatedBy = asnandlistasnitemsmodel.CreatedBy;
                    asnmaster.CreatedDate = System.DateTime.Now;
                    asnmaster.ModifiedBy = asnandlistasnitemsmodel.CreatedBy;
                    asnmaster.ModifiedDate = System.DateTime.Now;
                    _Context.AddAsync(asnmaster);
                    _Context.SaveChanges();
                    foreach (var item in asnandlistasnitemsmodel.Listasnitemdetail)
                    {
                        item.Fk_AsnMasterID = asnmaster.PK_AsnMasterID;
                        item.CreatedBy = asnandlistasnitemsmodel.CreatedBy;
                        ResponseviewModel result = _AsnItemDetailService.InsertUpdateAsnItemDetail(item);

                    }
                    result.Message = "Data Inserted Successfully";
                    result.StatusCode = 1;
                    result.IsValid = true;
                    result.Result = "success";
                    result.pk_ID = asnmaster.PK_AsnMasterID;
                    result.Data = asnmaster;
                }
                else
                {
                    result.Message = "Invoice Number already exists.!";
                    result.StatusCode = 0;
                    result.IsValid = false;
                    result.Result = "error";
                    result.pk_ID = null;
                    result.Data = null;
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.pk_ID = null;
                result.Data = null;
            }
            return result;
        }

        public ResponseviewModel GetASNMasterDetailByInvoiceNo(string InvoiceNo)
        {
            try
            {
                var CheckInvoiceAvailable = _Context.ASNMaster.Where(d => d.InvoiceNo.ToLower().Trim() == InvoiceNo.ToLower().Trim() && d.IsActive == true && d.IsDelete == false).FirstOrDefault();
                if (CheckInvoiceAvailable != null)
                {
                    ASNandlistasnitemsModel asnandlistasnitemsmodel = _Context.ASNMaster.Where(d => d.InvoiceNo.ToLower().Trim() == InvoiceNo.ToLower().Trim() && d.IsActive == true && d.IsDelete == false).Select(x => new ASNandlistasnitemsModel
                    {
                        PK_AsnMasterID = x.PK_AsnMasterID,

                        Fk_CompanyID = x.FK_CompanyID,
                        Fk_SupplierID = x.FK_SupplierID,
                        CompanyName = _Context.CompanyMaster.FirstOrDefault(d => d.PK_CompanyID == x.FK_CompanyID).CompanyName,
                        SupplierName = _Context.SupplierMaster.FirstOrDefault(d => d.Pk_SupplierID == x.FK_SupplierID).SupplierName,
                        DateOfArrival = x.DateOfArrival,
                        InvoiceNo = x.InvoiceNo,
                        origin = x.origin,
                        Remark = x.Remark,
                        shippingmodeID = x.shippingmodeID,
                        ShippingmodeName = x.ShippingmodeName,
                        VesselName = x.VesselName,
                        VesselVoyageNo = x.VesselVoyageNo,
                        BillNumber = x.BillNumber,
                        FlightName = x.FlightName,
                        FlightNumber = x.FlightNumber,
                        AWLNumber = x.AWLNumber,
                        TruckNumber = x.TruckNumber,
                        DeliveryNumber = x.DeliveryNumber,
                        Description = x.Description,
                        IsDelete = x.IsDelete,
                        IsActive = x.IsActive,
                        UserName = x.UserMaster.FirstName,
                        Remark1 = _Context.ASNItemDetails.Where(a => a.FK_ASNMasterID == x.PK_AsnMasterID).FirstOrDefault().Remark1,
                        Remark2 = _Context.ASNItemDetails.Where(a => a.FK_ASNMasterID == x.PK_AsnMasterID).FirstOrDefault().Remark2,
                        Remark3 = _Context.ASNItemDetails.Where(a => a.FK_ASNMasterID == x.PK_AsnMasterID).FirstOrDefault().Remark3,
                    }).OrderByDescending(a => a.CreatedDate).FirstOrDefault();

                    asnandlistasnitemsmodel.Listasnitemdetail = _AsnItemDetailService.GetAsnItemsDetails(asnandlistasnitemsmodel.PK_AsnMasterID).OrderByDescending(a => a.CreatedDate).ToList();

                    result.Message = "Record Get Successfully";
                    result.StatusCode = 1;
                    result.IsValid = true;
                    result.Result = "success";
                    result.Data = asnandlistasnitemsmodel;
                }
                else
                {
                    result.Message = "Record does not exists.!";
                    result.StatusCode = 0;
                    result.IsValid = false;
                    result.Result = "error";
                    result.pk_ID = null;
                    result.Data = null;
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.Data = null;
            }
            return result;
        }


        public ResponseviewModel GetContainerNoByAsndata(string InvoiceNo, string CaseNo)
        {
            try
            {
                var ContainerNo = _Context.ASNItemDetails.Where(d => d.ASNMaster.InvoiceNo.ToLower().Trim() == InvoiceNo.ToLower().Trim() && d.PalletNo.ToLower().Trim() == CaseNo.ToLower().Trim() && d.IsActive == true && d.IsDelete == false).FirstOrDefault().ContainerNo;
                if (ContainerNo != null)
                {
                    result.Message = "Record Get Successfully";
                    result.StatusCode = 1;
                    result.IsValid = true;
                    result.Result = "success";
                    result.Data = ContainerNo;
                }
                else
                {
                    result.Message = "Record does not exists.!";
                    result.StatusCode = 0;
                    result.IsValid = false;
                    result.Result = "error";
                    result.pk_ID = null;
                    result.Data = null;
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.Data = null;
            }
            return result;
        }

        public ResponseviewModel GetDropdownofInvoiceNo()
        {
            ResponseviewModel result = new ResponseviewModel();
            try
            {
                var Asnmaster = _Context.ASNMaster.Where(d => d.IsActive == true && d.IsDelete == false).OrderByDescending(d => d.CreatedDate).Select(x => new InvoiceNoDropdownViewModel
                {
                    ID = x.InvoiceNo,
                    Value = x.InvoiceNo
                }).ToList();

                if (Asnmaster.Count() > 0)
                {
                    result.Message = "Record Get Successfully";
                    result.StatusCode = 1;
                    result.IsValid = true;
                    result.Result = "success";
                    result.Data = Asnmaster;
                }
                else
                {
                    result.Message = "Record does not exists";
                    result.StatusCode = 0;
                    result.IsValid = false;
                    result.Result = "error";
                    result.Data = null;
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.Data = null;
            }
            return result;
        }

        public ResponseviewModel GetDropdownofInvoiceNoByDateofArrival(DateTime DateOfArrival)
        {
            ResponseviewModel result = new ResponseviewModel();
            try
            {
                var Asnmaster = _Context.ASNMaster.Where(d => d.DateOfArrival.Date == DateOfArrival.Date && d.IsActive == true && d.IsDelete == false).Select(x => new InvoiceNoDropdownViewModel
                {
                    ID = x.InvoiceNo,
                    Value = x.InvoiceNo
                }).ToList();

                if (Asnmaster.Count() > 0)
                {
                    result.Message = "Record Get Successfully";
                    result.StatusCode = 1;
                    result.IsValid = true;
                    result.Result = "success";
                    result.Data = Asnmaster;
                }
                else
                {
                    result.Message = "Record does not exists";
                    result.StatusCode = 0;
                    result.IsValid = false;
                    result.Result = "error";
                    result.Data = null;
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.Data = null;
            }
            return result;
        }

        //need to change this api (only display is not received invoice no)
        public ResponseviewModel GetDropdownofInvoiceforGoodReceipt()
        {
            ResponseviewModel result = new ResponseviewModel();
            try
            {
                var asnitemdetail = _Context.ASNMaster.Where(d => d.IsActive == true && d.IsDelete == false).OrderByDescending(a => a.CreatedDate).Select(x => new InvoiceNoDropdownViewModel
                {
                    ID = x.InvoiceNo,
                    Value = x.InvoiceNo
                }).ToList();
                List<InvoiceNoDropdownViewModel> lstfinalinv = new List<InvoiceNoDropdownViewModel>();
                foreach (var item in asnitemdetail)
                {
                    //List<StringDropdownviewModel> lstdata =(List < StringDropdownviewModel >) GetDropdownOfCaseNoByInvoiceNo(item.ID).Data;
                    //if (lstdata!=null)
                    // {
                    lstfinalinv.Add(item);
                    //}
                }
                if (asnitemdetail.Count() > 0)
                {
                    result.Message = "Record Get Successfully";
                    result.StatusCode = 1;
                    result.IsValid = true;
                    result.Result = "success";
                    result.Data = lstfinalinv;
                }
                else
                {
                    result.Message = "Record does not exists";
                    result.StatusCode = 0;
                    result.IsValid = false;
                    result.Result = "error";
                    result.Data = null;
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.Data = null;
            }
            return result;
        }

        public ResponseviewModel AddGoodsReceipts(GoodsReceiptsModel goodsreceiptsmodel)
        {
            ResponseviewModel result = new ResponseviewModel();
            try
            {
                if (goodsreceiptsmodel.ShippingmodeName.ToLower() != "sea")
                {

                    var GoodsReceipts = _Context.GoodsReceiptMaster.Where(d => d.ASNMaster.InvoiceNo.ToLower().Trim() == goodsreceiptsmodel.InvoiceNo.ToLower().Trim() && d.PalletNo.ToLower().Trim() == goodsreceiptsmodel.CaseNo.ToLower().Trim() && d.IsActive == true && d.IsDelete == false).FirstOrDefault();
                    if (GoodsReceipts == null)
                    {

                        var location = _Context.LocationMaster.Where(a => a.LocationCode == goodsreceiptsmodel.LocationID).FirstOrDefault();
                        if (location != null)
                        {
                            location.IsUsed = true;
                            location.ModifiedDate = System.DateTime.Now;
                            location.ModifiedBy = goodsreceiptsmodel.CreatedBy;
                            _Context.SaveChanges();
                        }

                        //Guid FkAsnID = _Context.ASNMaster.FirstOrDefault(x => x.InvoiceNo == goodsreceiptsmodel.InvoiceNo).PK_AsnMasterID;
                        List<ASNItemDetails> listasnitemdetails = new List<ASNItemDetails>();
                        listasnitemdetails = _Context.ASNItemDetails.Where(d => d.ASNMaster.InvoiceNo.ToLower().Trim() == goodsreceiptsmodel.InvoiceNo.ToLower().Trim() && d.PalletNo.ToLower().Trim() == goodsreceiptsmodel.CaseNo.ToLower().Trim() && d.IsActive == true && d.IsDelete == false).ToList();
                        if (listasnitemdetails.Count > 0)
                        {
                            foreach (var item in listasnitemdetails)
                            {
                                ASNItemDetails asnitemdetails = _Context.ASNItemDetails.Where(d => d.Pk_AsnItemDetailsID == item.Pk_AsnItemDetailsID).FirstOrDefault();
                                asnitemdetails.LocationId = goodsreceiptsmodel.LocationID;
                                asnitemdetails.IsRecieved = true;
                                asnitemdetails.ModifiedDate = System.DateTime.Now;

                                asnitemdetails.CreatedBy = goodsreceiptsmodel.CreatedBy;
                                _Context.Update(asnitemdetails);
                                _Context.SaveChanges();
                                GoodsReceiptMasterViewModel goodsreceiptmasterviewmodel = new GoodsReceiptMasterViewModel();
                                goodsreceiptmasterviewmodel.FK_ASNMasterID = asnitemdetails.FK_ASNMasterID;
                                goodsreceiptmasterviewmodel.ContainerNo = asnitemdetails.ContainerNo;
                                goodsreceiptmasterviewmodel.PalletNo = asnitemdetails.PalletNo;
                                goodsreceiptmasterviewmodel.OrderNo = asnitemdetails.OrderNo;
                                goodsreceiptmasterviewmodel.SuppliedPartNo = asnitemdetails.SuppliedPartNo;
                                goodsreceiptmasterviewmodel.Quantity = asnitemdetails.Quantity;
                                goodsreceiptmasterviewmodel.UnitPrice = asnitemdetails.UnitPrice;
                                goodsreceiptmasterviewmodel.Length = asnitemdetails.Length;
                                goodsreceiptmasterviewmodel.Breadth = asnitemdetails.Breadth;
                                goodsreceiptmasterviewmodel.Height = asnitemdetails.Height;
                                goodsreceiptmasterviewmodel.Weight = asnitemdetails.Weight;
                                goodsreceiptmasterviewmodel.Customer = asnitemdetails.Customer;
                                goodsreceiptmasterviewmodel.PurchaseOrderNo = asnitemdetails.PurchaseOrderNo;
                                goodsreceiptmasterviewmodel.Remark1 = asnitemdetails.Remark1;
                                goodsreceiptmasterviewmodel.Remark2 = asnitemdetails.Remark2;
                                goodsreceiptmasterviewmodel.Remark3 = asnitemdetails.Remark3;
                                goodsreceiptmasterviewmodel.Description = asnitemdetails.Description;
                                goodsreceiptmasterviewmodel.LocationId = goodsreceiptsmodel.LocationID;
                                goodsreceiptmasterviewmodel.CreatedBy = goodsreceiptsmodel.CreatedBy.HasValue ? goodsreceiptsmodel.CreatedBy.Value : Guid.Empty;
                                var insertgoodreceiptresult = _GoodReceiptService.InsertUpdateGoodReceipt(goodsreceiptmasterviewmodel);
                                //location code as ISUsed
                                bool IsUsed = _locationMasterService.ChangeActiveByLocationCode(goodsreceiptsmodel.LocationID);
                            }
                            result.Message = "Goods Receipts Added Successfully";
                            result.StatusCode = 1;
                            result.IsValid = true;
                            result.Result = "success";
                            result.Data = GetGoodsReceiptsByInvoiceNo(goodsreceiptsmodel.InvoiceNo);
                        }
                        else
                        {
                            result.Message = "Record does not exists";
                            result.StatusCode = 0;
                            result.IsValid = false;
                            result.Result = "error";
                            result.Data = null;
                        }
                    }
                    else
                    {
                        result.Message = "The Case no is already scanned.Please scan the correct case no";
                        result.StatusCode = 0;
                        result.IsValid = false;
                        result.Result = "error";
                        result.Data = null;
                    }
                }
                else
                {

                    var GoodsReceipts = _Context.GoodsReceiptMaster.Where(d => d.ASNMaster.InvoiceNo.ToLower().Trim() == goodsreceiptsmodel.InvoiceNo.ToLower().Trim() && d.ContainerNo.ToLower().Trim() == goodsreceiptsmodel.ContainerNo.ToLower().Trim() && d.PalletNo.ToLower().Trim() == goodsreceiptsmodel.CaseNo.ToLower().Trim()).FirstOrDefault();
                    if (GoodsReceipts == null)
                    {
                        //Guid FkAsnID = _Context.ASNMaster.FirstOrDefault(x => x.InvoiceNo == goodsreceiptsmodel.InvoiceNo).PK_AsnMasterID;
                        List<ASNItemDetails> listasnitemdetails = new List<ASNItemDetails>();
                        listasnitemdetails = _Context.ASNItemDetails.Where(d => d.ASNMaster.InvoiceNo.ToLower().Trim() == goodsreceiptsmodel.InvoiceNo.ToLower().Trim() && d.ContainerNo.ToLower().Trim() == goodsreceiptsmodel.ContainerNo.ToLower().Trim() && d.PalletNo.ToLower().Trim() == goodsreceiptsmodel.CaseNo.ToLower().Trim() && d.IsActive == true && d.IsDelete == false).ToList();
                        if (listasnitemdetails.Count > 0)
                        {
                            foreach (var item in listasnitemdetails)
                            {
                                ASNItemDetails asnitemdetails = _Context.ASNItemDetails.Where(d => d.Pk_AsnItemDetailsID == item.Pk_AsnItemDetailsID).FirstOrDefault();
                                asnitemdetails.LocationId = goodsreceiptsmodel.LocationID;
                                asnitemdetails.IsRecieved = true;
                                asnitemdetails.ModifiedDate = System.DateTime.Now;
                                _Context.Update(asnitemdetails);
                                _Context.SaveChanges();
                                GoodsReceiptMasterViewModel goodsreceiptmasterviewmodel = new GoodsReceiptMasterViewModel();
                                goodsreceiptmasterviewmodel.FK_ASNMasterID = asnitemdetails.FK_ASNMasterID;
                                goodsreceiptmasterviewmodel.ContainerNo = asnitemdetails.ContainerNo;
                                goodsreceiptmasterviewmodel.PalletNo = asnitemdetails.PalletNo;
                                goodsreceiptmasterviewmodel.OrderNo = asnitemdetails.OrderNo;
                                goodsreceiptmasterviewmodel.SuppliedPartNo = asnitemdetails.SuppliedPartNo;
                                goodsreceiptmasterviewmodel.Quantity = asnitemdetails.Quantity;
                                goodsreceiptmasterviewmodel.UnitPrice = asnitemdetails.UnitPrice;
                                goodsreceiptmasterviewmodel.Length = asnitemdetails.Length;
                                goodsreceiptmasterviewmodel.Breadth = asnitemdetails.Breadth;
                                goodsreceiptmasterviewmodel.Height = asnitemdetails.Height;
                                goodsreceiptmasterviewmodel.Weight = asnitemdetails.Weight;
                                goodsreceiptmasterviewmodel.Customer = asnitemdetails.Customer;
                                goodsreceiptmasterviewmodel.PurchaseOrderNo = asnitemdetails.PurchaseOrderNo;
                                goodsreceiptmasterviewmodel.Remark1 = asnitemdetails.Remark1;
                                goodsreceiptmasterviewmodel.Remark2 = asnitemdetails.Remark2;
                                goodsreceiptmasterviewmodel.Remark3 = asnitemdetails.Remark3;
                                goodsreceiptmasterviewmodel.Description = asnitemdetails.Description;
                                goodsreceiptmasterviewmodel.CreatedBy = goodsreceiptsmodel.CreatedBy.HasValue ? goodsreceiptsmodel.CreatedBy.Value : Guid.Empty;
                                var insertgoodreceiptresult = _GoodReceiptService.InsertUpdateGoodReceipt(goodsreceiptmasterviewmodel);
                                //location code as ISUsed
                                bool IsUsed = _locationMasterService.ChangeActiveByLocationCode(goodsreceiptsmodel.LocationID);
                            }
                            result.Message = "Goods Receipts Added Successfully";
                            result.StatusCode = 1;
                            result.IsValid = true;
                            result.Result = "success";
                            result.Data = GetGoodsReceiptsByInvoiceNo(goodsreceiptsmodel.InvoiceNo);
                        }
                        else
                        {
                            result.Message = "Record does not exists";
                            result.StatusCode = 0;
                            result.IsValid = false;
                            result.Result = "error";
                            result.Data = null;
                        }
                    }
                    else
                    {
                        result.Message = "The Case no is already scanned.Please scan the correct case no";
                        result.StatusCode = 0;
                        result.IsValid = false;
                        result.Result = "error";
                        result.Data = null;
                    }
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.Data = null;
            }
            return result;
        }
        public ResponseviewModel GetGoodsReceiptsByDateOfArrival(DateTime DateOfArrival)
        {
            ResponseviewModel result = new ResponseviewModel();
            try
            {

                var GoodsReceiptsList = _Context.ASNItemDetails.Where(d => d.ASNMaster.DateOfArrival.Date == DateOfArrival.Date && d.IsRecieved == true && d.IsActive == true && d.IsDelete == false).Select(x => new GoodsReceiptsModel
                {
                    InvoiceNo = x.ASNMaster.InvoiceNo,
                    DateofArrival = x.ASNMaster.DateOfArrival,
                    ContainerNo = x.ContainerNo,
                    CaseNo = x.PalletNo,
                    LocationID = x.LocationId
                }).ToList();

                if (GoodsReceiptsList.Count > 0)
                {

                    result.Message = "Goods Receipts Added Successfully";
                    result.StatusCode = 1;
                    result.IsValid = true;
                    result.Result = "success";
                    result.Data = GoodsReceiptsList;
                }
                else
                {
                    result.Message = "Record does not exists";
                    result.StatusCode = 0;
                    result.IsValid = false;
                    result.Result = "error";
                    result.Data = null;
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.Data = null;
            }
            return result;
        }
        public ResponseviewModel GetGoodsReceiptsByInvoiceNo(string InvoiceNo)
        {
            ResponseviewModel result = new ResponseviewModel();
            try
            {
                var shippigmode = _Context.ASNMaster.Where(k => k.InvoiceNo.ToLower().Trim() == InvoiceNo.ToLower().Trim()).FirstOrDefault().ShippingmodeName;
                var GoodsReceiptsList = _Context.ASNItemDetails.Where(d => d.ASNMaster.InvoiceNo.ToLower().Trim() == InvoiceNo.ToLower().Trim() && d.IsRecieved == true && d.IsActive == true && d.IsDelete == false).OrderByDescending(s => s.ModifiedDate).Select(x => new GoodsReceiptsModel
                {
                    InvoiceNo = x.ASNMaster.InvoiceNo,
                    DateofArrival = x.ASNMaster.DateOfArrival,
                    ContainerNo = x.ContainerNo,
                    CaseNo = x.PalletNo,
                    LocationID = x.LocationId,
                    ShippingmodeName = shippigmode,
                    UserName = _Context.GoodsReceiptMaster.Where(g => g.ASNMaster.PK_AsnMasterID == x.FK_ASNMasterID).FirstOrDefault().UserMaster.FirstName
                }).Distinct().ToList();
                foreach (var item in GoodsReceiptsList)
                {
                    var data = _Context.SortingMaster.Any(a => a.InvoiceNo == item.InvoiceNo && a.CaseNo == item.CaseNo && a.IsDelete!=true);

                    if (data == true)
                    {
                        item.IsSorting = true;
                    }
                    else
                    {
                        item.IsSorting = false;
                    }
                }
                var totalcontainer = _Context.ASNItemDetails.Where(d => d.ASNMaster.InvoiceNo.ToLower().Trim() == InvoiceNo.ToLower().Trim() && d.IsActive == true && d.IsDelete == false).OrderByDescending(s => s.ModifiedDate).GroupBy(x => x.PalletNo).ToList().Count();
                var totalscanned = GoodsReceiptsList.Count;
                if (GoodsReceiptsList.Count > 0)
                {

                    result.Message = "Goods Receipts Added Successfully";
                    result.StatusCode = 1;
                    result.IsValid = true;
                    result.Result = "success";
                    result.Data = new { data = GoodsReceiptsList, totalcontainer = totalcontainer, totalscanned = totalscanned };
                }
                else
                {
                    result.Message = "Record does not exists";
                    result.StatusCode = 0;
                    result.IsValid = false;
                    result.Result = "error";
                    result.Data = null;
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.Data = null;
            }
            return result;
        }
        #region Sorting

        public ResponseviewModel GetDropdownOfReceivedInvoiceNo()
        {
            ResponseviewModel result = new ResponseviewModel();
            try
            {

                var Asnmaster = _Context.ASNItemDetails.Where(d => d.IsRecieved == true && d.IsActive == true && d.IsDelete == false).OrderByDescending(k => k.CreatedDate).Select(x => new InvoiceNoDropdownViewModel
                {
                    ID = x.ASNMaster.InvoiceNo,
                    Value = x.ASNMaster.InvoiceNo
                }).Distinct().ToList();

                if (Asnmaster.Count > 0)
                {

                    result.Message = "Record Get Successfully";
                    result.StatusCode = 1;
                    result.IsValid = true;
                    result.Result = "success";
                    result.Data = Asnmaster;
                }
                else
                {
                    result.Message = "Record does not exists";
                    result.StatusCode = 0;
                    result.IsValid = false;
                    result.Result = "error";
                    result.Data = null;
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.Data = null;
            }
            return result;
        }
        public ResponseviewModel GetDropdownOfCaseNoByInvoiceNo(String invoiceno)
        {
            ResponseviewModel result = new ResponseviewModel();
            try
            {
                var Asnmaster = _Context.ASNItemDetails.Where(d => d.ASNMaster.InvoiceNo.ToLower() == invoiceno.ToLower() && d.IsRecieved == true && d.IsActive == true && d.IsDelete == false).ToList();
                List<StringDropdownviewModel> casenolist = new List<StringDropdownviewModel>();
                foreach (var item in Asnmaster)
                {
                    if (GetAvailableQuantityByAsnDetail(invoiceno, item.PalletNo, item.Customer, item.SuppliedPartNo) > 0)
                    {
                        if (casenolist.Count(a => a.ID == item.PalletNo) == 0)
                        {
                            StringDropdownviewModel caseno = new StringDropdownviewModel();
                            caseno.ID = item.PalletNo;
                            caseno.Value = item.PalletNo;
                            casenolist.Add(caseno);
                        }
                    }

                }

                if (casenolist.Count > 0)
                {

                    result.Message = "Record Get Successfully";
                    result.StatusCode = 1;
                    result.IsValid = true;
                    result.Result = "success";
                    result.Data = casenolist;
                }
                else
                {
                    result.Message = "Record does not exists";
                    result.StatusCode = 0;
                    result.IsValid = false;
                    result.Result = "error";
                    result.Data = null;
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.Data = null;
            }
            return result;
        }

        public ResponseviewModel GetDropdownOfCustomerCodeByAsnDetail(SortingItemDetail sortingitemdetail)
        {
            ResponseviewModel result = new ResponseviewModel();
            try
            {
                List<BarCodeModel> lstbarcodes = new List<BarCodeModel>();
                var Asnmaster = _Context.ASNItemDetails.Where(d => d.ASNMaster.InvoiceNo.ToLower().Trim() == sortingitemdetail.InvoiceNo.ToLower().Trim() && d.SuppliedPartNo.ToLower().Trim() == sortingitemdetail.SuppliedPartNo.ToLower().Trim() && d.PalletNo.ToLower().Trim() == sortingitemdetail.CaseNo.ToLower().Trim() && d.IsRecieved == true && d.IsActive == true && d.IsDelete == false).GroupBy(m => m.Customer).ToList();
                List<StringDropdownviewModel> customercodelist = new List<StringDropdownviewModel>();
                foreach (var item in Asnmaster)
                {

                    StringDropdownviewModel customercode = new StringDropdownviewModel();
                    sortingitemdetail.Customer = item.FirstOrDefault().Customer;
                    //check partucular customer wise quantity is more than 0 is not
                    int? availableqty = GetAvailableQuantityByAsnDetail(sortingitemdetail.InvoiceNo, sortingitemdetail.CaseNo, sortingitemdetail.Customer, sortingitemdetail.SuppliedPartNo);
                    if (availableqty > 0)//if more than zero then it will able to add customer
                    {
                        customercode.ID = item.FirstOrDefault().Customer;
                        customercode.Value = item.FirstOrDefault().Customer;
                        customercodelist.Add(customercode);
                    }
                    lstbarcodes.AddRange(_BarCodeService.GetBarCodeListByCustomerCode(item.FirstOrDefault().Customer));
                }



                if (customercodelist.Count > 0)
                {
                    result.Message = "Record Get Successfully";
                    result.StatusCode = 1;
                    result.IsValid = true;
                    result.Result = "success";
                    result.Data = new { customercode = customercodelist, barcode = lstbarcodes };
                }

                else
                {
                    result.Message = "Record does not exists";
                    result.StatusCode = 0;
                    result.IsValid = false;
                    result.Result = "error";
                    result.Data = null;
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.Data = null;
            }
            return result;
        }

        // Get Customer Data by InvoiceNo And Case No 
        public ResponseviewModel GetCustomerDataByInvoiceAndCaseNo(string invoicenno, string caseno)
        {
            ResponseviewModel result = new ResponseviewModel();
            try
            {
                List<BarCodeModel> lstbarcodes = new List<BarCodeModel>();
                var Asnmaster = _Context.ASNItemDetails.Where(d => d.ASNMaster.InvoiceNo.ToLower() == invoicenno.ToLower() && d.PalletNo.ToLower() == caseno.ToLower() && d.IsRecieved == true && d.IsActive == true && d.IsDelete == false).ToList();
                List<CustomerDataViewModal> customercodelist = new List<CustomerDataViewModal>();
                var Customer = "";
                foreach (var item in Asnmaster)
                {
                    CustomerDataViewModal customercode = new CustomerDataViewModal();
                    Customer = item.Customer;


                    //check partucular customer wise quantity is more than 0 is not
                    int? availableqty = item.Quantity - (item.ReceivedQty == null ? 0 : item.ReceivedQty);
                    //   GetAvailableQuantityByInvoiceNoAndCaseNo(invoicenno, caseno, Customer,item.SuppliedPartNo);
                    if (availableqty > 0)//if more than zero then it will able to add customer
                    {
                        customercode.Code = item.Customer;
                        customercode.ItemNo = item.SuppliedPartNo;
                        customercode.Qty = availableqty;
                        customercodelist.Add(customercode);
                    }
                }



                if (customercodelist.Count > 0)
                {
                    result.Message = "Record Get Successfully";
                    result.StatusCode = 1;
                    result.IsValid = true;
                    result.Result = "success";
                    result.Data = customercodelist;
                }

                else
                {
                    result.Message = "Record does not exists";
                    result.StatusCode = 0;
                    result.IsValid = false;
                    result.Result = "error";
                    result.Data = null;
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.Data = null;
            }
            return result;
        }

        public ResponseviewModel GetLocationIdByAsnDetail(SortingItemDetail sortingitemdetail)
        {
            ResponseviewModel result = new ResponseviewModel();
            try
            {
                LocatioinIdViewModel locationidviewmodel = new LocatioinIdViewModel();
                locationidviewmodel.LocationID = _Context.ASNItemDetails.FirstOrDefault(d => d.ASNMaster.InvoiceNo.ToLower() == sortingitemdetail.InvoiceNo.ToLower() && d.PalletNo.ToLower() == sortingitemdetail.CaseNo.ToLower() && d.IsRecieved == true && d.IsActive == true && d.IsDelete == false).LocationId;

                if (locationidviewmodel.LocationID != null)
                {
                    result.Message = "Record Get Successfully";
                    result.StatusCode = 1;
                    result.IsValid = true;
                    result.Result = "success";
                    result.Data = locationidviewmodel;
                }
                else
                {
                    result.Message = "Record does not exists";
                    result.StatusCode = 0;
                    result.IsValid = false;
                    result.Result = "error";
                    result.Data = null;
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.Data = null;
            }
            return result;
        }

        public ResponseviewModel GetAvailableQuantityByAsnDetail(SortingItemDetail sortingitemdetail)
        {
            ResponseviewModel result = new ResponseviewModel();
            try
            {
                int totalquantity = 0;
                int? sortedquantity = 0;

                totalquantity = _Context.ASNItemDetails.Where(d => d.ASNMaster.InvoiceNo.ToLower() == sortingitemdetail.InvoiceNo.ToLower() && d.PalletNo.ToLower() == sortingitemdetail.CaseNo.ToLower() && d.Customer.ToLower() == sortingitemdetail.Customer.ToLower() && d.SuppliedPartNo.ToLower() == sortingitemdetail.SuppliedPartNo.ToLower() && d.IsRecieved == true && d.IsActive == true && d.IsDelete == false && d.Quantity != d.ReceivedQty).FirstOrDefault().Quantity;
                sortedquantity = _Context.ASNItemDetails.Where(d => d.ASNMaster.InvoiceNo.ToLower() == sortingitemdetail.InvoiceNo.ToLower() && d.PalletNo.ToLower() == sortingitemdetail.CaseNo.ToLower() && d.Customer.ToLower() == sortingitemdetail.Customer.ToLower() && d.SuppliedPartNo.ToLower() == sortingitemdetail.SuppliedPartNo.ToLower() && d.IsRecieved == true && d.IsActive == true && d.IsDelete == false && d.Quantity != d.ReceivedQty).FirstOrDefault().ReceivedQty;
                //sortedquantity = _Context.SortingMaster.Where(d => d.InvoiceNo.ToLower() == sortingitemdetail.InvoiceNo.ToLower() && d.CaseNo.ToLower() == sortingitemdetail.CaseNo.ToLower() && d.CustomerCode.ToLower() == sortingitemdetail.Customer.ToLower() && d.ItemCode.ToLower() == sortingitemdetail.SuppliedPartNo.ToLower() && d.IsActive == true && d.IsDelete == false).Sum(x => x.Quantity);
                //Remain to count sorted quantity
                AvailableQuantityViewModel availablequantityviewmodel = new AvailableQuantityViewModel();
                availablequantityviewmodel.AvailableQuantity = totalquantity - (sortedquantity == null ? 0 : sortedquantity); // get available quantity by sum of quantity and sum of sorted quantity
                if (availablequantityviewmodel.AvailableQuantity > 0)
                {

                    result.Message = "Record Get Successfully";
                    result.StatusCode = 1;
                    result.IsValid = true;
                    result.Result = "success";
                    result.Data = availablequantityviewmodel;
                }
                else
                {
                    availablequantityviewmodel.AvailableQuantity = 0;
                    result.Message = "Record does not exists";
                    result.StatusCode = 0;
                    result.IsValid = false;
                    result.Result = "error";
                    result.Data = availablequantityviewmodel;
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.Data = null;
            }
            return result;
        }
        #endregion

        public int? GetAvailableQuantityByAsnDetail(string InvoiceNo, string CaseNo, string Customer, string SuppliedPartNo)
        {
            int? AvailableQuantity = 0;
            try
            {
                int totalquantity = 0;
                int? sortedquantity = 0;

                totalquantity = _Context.ASNItemDetails.Where(d => d.ASNMaster.InvoiceNo.ToLower().Trim() == InvoiceNo.ToLower().Trim() && d.PalletNo.ToLower().Trim() == CaseNo.ToLower().Trim() && d.Customer.ToLower().Trim() == Customer.ToLower().Trim() && d.SuppliedPartNo.ToLower().Trim() == SuppliedPartNo.ToLower().Trim() && d.IsRecieved == true && d.IsActive == true && d.IsDelete == false && d.Quantity != d.ReceivedQty).FirstOrDefault().Quantity;
                sortedquantity = _Context.ASNItemDetails.Where(d => d.ASNMaster.InvoiceNo.ToLower().Trim() == InvoiceNo.ToLower().Trim() && d.PalletNo.ToLower().Trim() == CaseNo.ToLower().Trim() && d.Customer.ToLower().Trim() == Customer.ToLower().Trim() && d.SuppliedPartNo.ToLower().Trim() == SuppliedPartNo.ToLower().Trim() && d.IsRecieved == true && d.IsActive == true && d.IsDelete == false && d.Quantity != d.ReceivedQty).FirstOrDefault().ReceivedQty;
                // sortedquantity = _Context.SortingMaster.Where(d => d.InvoiceNo.ToLower().Trim() == InvoiceNo.ToLower().Trim() && d.CaseNo.ToLower().Trim() == CaseNo.ToLower().Trim() && d.CustomerCode.ToLower().Trim() == Customer.ToLower().Trim() && d.ItemCode.ToLower().Trim() == SuppliedPartNo.ToLower().Trim() && d.IsActive == true && d.IsDelete == false).Sum(x => x.Quantity);
                //Remain to count sorted quantity
                AvailableQuantity = totalquantity - (sortedquantity == null ? 0 : sortedquantity); // get available quantity by sum of quantity and sum of sorted quantity

            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.Data = null;
            }
            return AvailableQuantity;
        }

        public QuantityDeatilViewModel GetQuantityByAsnDetail(string InvoiceNo, string CaseNo, string Customer, string SuppliedPartNo)
        {

            QuantityDeatilViewModel availablequantityviewmodel = new QuantityDeatilViewModel();
            try
            {
                int totalquantity = 0;
                int? sortedquantity = 0;

                availablequantityviewmodel.TotalQuantity = _Context.ASNItemDetails.Where(d => d.ASNMaster.InvoiceNo.ToLower().Trim() == InvoiceNo.ToLower().Trim() && d.PalletNo.ToLower().Trim() == CaseNo.ToLower().Trim() && d.Customer.ToLower().Trim() == Customer.ToLower().Trim() && d.SuppliedPartNo.ToLower().Trim() == SuppliedPartNo.ToLower().Trim() && d.IsActive == true && d.IsDelete == false).FirstOrDefault().Quantity;
                availablequantityviewmodel.ReceivedQuantity = _Context.ASNItemDetails.Where(d => d.ASNMaster.InvoiceNo.ToLower().Trim() == InvoiceNo.ToLower().Trim() && d.PalletNo.ToLower().Trim() == CaseNo.ToLower().Trim() && d.Customer.ToLower().Trim() == Customer.ToLower().Trim() && d.SuppliedPartNo.ToLower().Trim() == SuppliedPartNo.ToLower().Trim() && d.IsActive == true && d.IsDelete == false).FirstOrDefault().ReceivedQty;
                // sortedquantity = _Context.SortingMaster.Where(d => d.InvoiceNo.ToLower().Trim() == InvoiceNo.ToLower().Trim() && d.CaseNo.ToLower().Trim() == CaseNo.ToLower().Trim() && d.CustomerCode.ToLower().Trim() == Customer.ToLower().Trim() && d.ItemCode.ToLower().Trim() == SuppliedPartNo.ToLower().Trim() && d.IsActive == true && d.IsDelete == false).Sum(x => x.Quantity);
                //Remain to count sorted quantity
                availablequantityviewmodel.AvailableQuantity = totalquantity - (sortedquantity == null ? 0 : sortedquantity); // get available quantity by sum of quantity and sum of sorted quantity

            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.Data = availablequantityviewmodel;
            }
            return availablequantityviewmodel;
        }

        public int? GetAvailableQuantityByInvoiceNoAndCaseNo(string InvoiceNo, string CaseNo, string Customer)
        {
            int? AvailableQuantity = 0;
            try
            {
                int totalquantity = 0;
                int? sortedquantity = 0;

                totalquantity = _Context.ASNItemDetails.Where(d => d.ASNMaster.InvoiceNo.ToLower().Trim() == InvoiceNo.ToLower().Trim() && d.PalletNo.ToLower().Trim() == CaseNo.ToLower().Trim() && d.Customer.ToLower().Trim() == Customer.ToLower().Trim() && d.IsRecieved == true && d.IsActive == true && d.IsDelete == false).FirstOrDefault().Quantity;
                sortedquantity = _Context.ASNItemDetails.Where(d => d.ASNMaster.InvoiceNo.ToLower().Trim() == InvoiceNo.ToLower().Trim() && d.PalletNo.ToLower().Trim() == CaseNo.ToLower().Trim() && d.Customer.ToLower().Trim() == Customer.ToLower().Trim() && d.IsRecieved == true && d.IsActive == true && d.IsDelete == false).FirstOrDefault().ReceivedQty;
                //Remain to count sorted quantity
                AvailableQuantity = totalquantity - (sortedquantity == null ? 0 : sortedquantity); // get available quantity by sum of quantity and sum of sorted quantity
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.Data = null;
            }
            return AvailableQuantity;
        }

        public ResponseviewModel DeleteGoodReciptByID(string invoicenumber, string palletnnumber, string containernumber, Guid id)
        {
            ResponseviewModel result = new ResponseviewModel();
            try
            {
                var GoodsReceiptsList = _Context.ASNItemDetails.Where(d => d.ASNMaster.InvoiceNo.ToLower().Trim() == invoicenumber.ToLower().Trim() && d.PalletNo.ToLower().Trim() == palletnnumber.ToLower().Trim() && d.ContainerNo.ToLower().Trim() == containernumber.ToLower().Trim()).ToList();
                foreach (var item in GoodsReceiptsList)
                {
                    var goodrecipt = _Context.GoodsReceiptMaster.Where(k => k.FK_ASNMasterID == item.FK_ASNMasterID).ToList();
                    _Context.GoodsReceiptMaster.RemoveRange(goodrecipt);
                    item.IsRecieved = false;
                    item.ModifiedBy = id;
                    item.ModifiedDate = System.DateTime.Now;
                    _Context.ASNItemDetails.Update(item);
                }
                _Context.SaveChanges();
                //Location code free when we delete good recipt
                if (GoodsReceiptsList.Count > 0)
                {
                    bool freelocationcode = _locationMasterService.ChangeUnusedByLocationCode(GoodsReceiptsList.FirstOrDefault().LocationId);
                }

                result.Message = "Goods Receipts Deleted Successfully";
                result.StatusCode = 1;
                result.IsValid = true;
                result.Result = "success";
                result.Data = GoodsReceiptsList;

            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.Data = null;
            }
            return result;
        }

        public ResponseviewModel GetDuplicateInvoiceNoCheck(string InvoiceNo)
        {
            ResponseviewModel result = new ResponseviewModel();
            try
            {
                Boolean isavailable = false;
                ASNMaster invoicenos = new ASNMaster();
                invoicenos = _Context.ASNMaster.Where(d => d.InvoiceNo.ToLower().Trim() == InvoiceNo.ToLower().Trim() && d.IsActive == true && d.IsDelete == false).FirstOrDefault();


                if (invoicenos != null)
                {
                    isavailable = true;
                    result.Message = "Code already exists";
                    result.StatusCode = 1;
                    result.IsValid = true;
                    result.Result = "success";
                    result.Data = InvoiceNo;
                }
                else
                {
                    result.Message = "Code not Match";
                    result.StatusCode = 0;
                    result.IsValid = false;
                    result.Result = "error";
                    result.Data = null;
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.pk_ID = null;
                result.Data = null;
            }
            return result;
        }
        public ResponseviewModel GetCustomerCodes(string InvoiceNo)
        {
            ResponseviewModel result = new ResponseviewModel();
            try
            {
                Boolean isavailable = false;

                var invoicenos = _Context.ASNItemDetails.Where(d => d.ASNMaster.InvoiceNo.ToLower().Trim() == InvoiceNo.ToLower().Trim() && d.IsActive == true && d.IsDelete == false).Select(s => new
                {
                    code = s.Customer
                }).Distinct().ToList();

                if (invoicenos != null)
                {
                    isavailable = true;
                    result.Message = "Code get Successfully";
                    result.StatusCode = 1;
                    result.IsValid = true;
                    result.Result = "success";
                    result.Data = invoicenos;
                }
                else
                {
                    result.Message = "Code not Match";
                    result.StatusCode = 0;
                    result.IsValid = false;
                    result.Result = "error";
                    result.Data = null;
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.pk_ID = null;
                result.Data = null;
            }
            return result;
        }

        public ResponseviewModel CheckASNDetails(List<ASNItemDetailModels> lstasndetails)
        {
            ResponseviewModel result = new ResponseviewModel();
            List<ASNDetailsMismatch> lstmismatch = new List<ASNDetailsMismatch>();
            try
            {
                Boolean ismismatch = false;
                ASNDetailsMismatch sNDetailsMismatch = new ASNDetailsMismatch();
                foreach (var item in lstasndetails)
                {
                    ismismatch = false;
                    sNDetailsMismatch = _mapper.Map<ASNDetailsMismatch>(item);
                    if (_Context.CustomerMaster.AsEnumerable().Count(c => c.Code.ToLower().Trim().Split(',', StringSplitOptions.RemoveEmptyEntries).Contains(item.Customer.ToLower().Trim()) && c.IsDelete == false) == 0)
                    {
                        ismismatch = true;
                        sNDetailsMismatch.isCustomer = true;
                    }
                    if (!_Context.PartsMaster.Any(c => c.PartNumber.ToLower().Trim() == item.SuppliedPartNo.ToLower().Trim() && c.IsDelete == false))
                    {
                        ismismatch = true;
                        sNDetailsMismatch.isPartnumber = true;
                    }
                    //if (_Context.ASNItemDetails.Count(c => c.PalletNo == item.PalletNo && c.IsDelete == false) > 0)
                    //{
                    //    ismismatch = true;
                    //    sNDetailsMismatch.isCasenumber = true;

                    //}
                    if (ismismatch)
                    {
                        lstmismatch.Add(sNDetailsMismatch);
                    }
                }

                result.Message = "Mismatch list";
                result.StatusCode = 1;
                result.IsValid = false;
                result.Result = "success";
                result.Data = lstmismatch;
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.pk_ID = null;
                result.Data = null;
            }
            return result;
        }


        public ResponseviewModel GetTodayAsnMaster(TodayAsnMasterViewModel todayasnmasterviewmodel)
        {
            try
            {
                List<TodayAsnMasterViewModel> listasnmaster = new List<TodayAsnMasterViewModel>();
                if (todayasnmasterviewmodel.CompanyName == null && todayasnmasterviewmodel.SupplierName == null && todayasnmasterviewmodel.InvoiceNo == null && todayasnmasterviewmodel.FromDate == null && todayasnmasterviewmodel.ToDate == null)
                {
                    var data = _Context.ASNItemDetails.Where(d => d.IsDelete == false).OrderByDescending(c => c.CreatedDate).GroupBy(a => new { a.ASNMaster.InvoiceNo, a.ASNMaster.DateOfArrival, a.ASNMaster.FK_CompanyID, a.ASNMaster.FK_SupplierID, a.ASNMaster.CreatedBy, a.ASNMaster.PK_AsnMasterID }).ToList();

                    foreach (var item in data)
                    {
                        var info = _Context.ASNMaster.Where(a => a.PK_AsnMasterID == item.Key.PK_AsnMasterID).OrderByDescending(p => p.CreatedDate).Select(x => new TodayAsnMasterViewModel
                        {

                            PK_AsnMasterID = x.PK_AsnMasterID,
                            CompanyName = x.CompanyMaster.CompanyName,
                            SupplierName = x.SupplierMaster.SupplierName,
                            DateOfArrival = x.DateOfArrival,
                            InvoiceNo = x.InvoiceNo,
                            UserName = x.UserMaster.FirstName,
                            Remark = x.Remark,
                            CreatedDate = x.CreatedDate,
                            isrecipt = _Context.GoodsReceiptMaster.Where(k => k.ASNMaster.InvoiceNo.ToLower().Trim() == x.InvoiceNo.ToLower().Trim() && k.IsDelete == false).Any(),
                        }).FirstOrDefault();
                        listasnmaster.Add(info);
                    }
                }

                else
                {
                    // listasnmaster = _Context.ASNMaster.Where(d=> d.InvoiceNo.ToLower().Trim() == d.InvoiceNo.ToLower().Trim()= todayasnmasterviewmodel.FromDate)).Select(x => new TodayAsnMasterViewModel
                    //listasnmaster = _Context.ASNMaster.Where(d => d.todayasnmasterviewmodel.InvoiceNo.ToLower().Trim() == (d.InvoiceNo.ToLower().Trim() != "" ? d.InvoiceNo.ToLower().Trim() : d.todayasnmasterviewmodel.Invoice.ToLower().Trim()) && k.SortingMaster.IsDelete == false && k.SortingMaster.IsBin == true && k.SortingMaster.ItemCode.ToLower().Trim() == (model.ItemCode.ToLower().Trim() != "" ? model.ItemCode.ToLower().Trim() : k.SortingMaster.ItemCode.ToLower().Trim()) && (k.QuantitySelected > 0 ? k.QuantityRemaining == k.QuantityRemaining : k.QuantityRemaining > 0)).ToList();
                    //listasnmaster = _Context.ASNMaster.Where(d => (todayasnmasterviewmodel.CompanyName != "" ? d.CompanyMaster.CompanyName.Contains(todayasnmasterviewmodel.CompanyName) : true) && (todayasnmasterviewmodel.SupplierName != "" ? d.SupplierMaster.SupplierName.Contains(todayasnmasterviewmodel.SupplierName) : true) && (todayasnmasterviewmodel.InvoiceNo != "" ? d.InvoiceNo.Contains(todayasnmasterviewmodel.InvoiceNo) : true) || (d.CreatedDate <= (todayasnmasterviewmodel.ToDate != null ? todayasnmasterviewmodel.ToDate : d.CreatedDate) &&
                    //                                               d.CreatedDate >= (todayasnmasterviewmodel.FromDate != null ? todayasnmasterviewmodel.FromDate : d.CreatedDate))).Select(x => new TodayAsnMasterViewModel

                    listasnmaster = _Context.ASNMaster.Where(d => d.InvoiceNo == (todayasnmasterviewmodel.InvoiceNo != null ? todayasnmasterviewmodel.InvoiceNo : d.InvoiceNo)
                                                                && d.CompanyMaster.CompanyName == (todayasnmasterviewmodel.CompanyName != null ? todayasnmasterviewmodel.CompanyName : d.CompanyMaster.CompanyName)
                                                                && d.SupplierMaster.SupplierName == (todayasnmasterviewmodel.SupplierName != null ? todayasnmasterviewmodel.SupplierName : d.SupplierMaster.SupplierName)
                                                                && (d.CreatedDate <= (todayasnmasterviewmodel.ToDate != null ? todayasnmasterviewmodel.ToDate : d.CreatedDate) &&
                                                                    (d.CreatedDate >= (todayasnmasterviewmodel.FromDate != null ? todayasnmasterviewmodel.FromDate : d.CreatedDate))
                                                                    && d.IsDelete == false)

                                                                    ).OrderByDescending(a => a.CreatedDate)
                                                                    .Select(x => new TodayAsnMasterViewModel
                                                                    {
                                                                        CreatedDate = x.CreatedDate,
                                                                        PK_AsnMasterID = x.PK_AsnMasterID,
                                                                        CompanyName = x.CompanyMaster.CompanyName,
                                                                        SupplierName = x.SupplierMaster.SupplierName,
                                                                        DateOfArrival = x.DateOfArrival,
                                                                        InvoiceNo = x.InvoiceNo,
                                                                        UserName = x.UserMaster.FirstName,
                                                                        Remark = x.Remark,
                                                                        isrecipt = _Context.GoodsReceiptMaster.Where(k => k.ASNMaster.InvoiceNo.ToLower().Trim() == x.InvoiceNo.ToLower().Trim() && k.IsDelete == false).Any(),
                                                                    }
                                                                    ).ToList();
                }

                foreach (var item in listasnmaster)
                {
                    if (_Context.ASNItemDetails.Any(a => a.FK_ASNMasterID == item.PK_AsnMasterID && a.IsRecieved != true))
                    {
                        item.AllReceived = "Pending";
                    }
                    else
                    {
                        item.AllReceived = "Complete";
                    }
                }
                if (listasnmaster.Count > 0)
                {
                    result.Message = "Record Get Successfully";
                    result.StatusCode = 1;
                    result.IsValid = true;
                    result.Result = "success";
                    result.Data = listasnmaster.OrderByDescending(k => k.CreatedDate);
                }
                else
                {
                    result.Message = "Record does not exits";
                    result.StatusCode = 0;
                    result.IsValid = false;
                    result.Result = "error";
                    result.pk_ID = null;
                    result.Data = null;
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.pk_ID = null;
                result.Data = null;
            }
            return result;
        }


        public ResponseviewModel GetGoodReceiptList(GoodReceiptListViewModel goodreceiptlistviewmodel)
        {
            try
            {
                List<GoodReceiptListViewModel> listasnmaster = new List<GoodReceiptListViewModel>();
                if (goodreceiptlistviewmodel.CompanyName == null && goodreceiptlistviewmodel.SupplierName == null && goodreceiptlistviewmodel.InvoiceNo == null && goodreceiptlistviewmodel.FromDate == null && goodreceiptlistviewmodel.ToDate == null)
                {
                    listasnmaster = _Context.ASNItemDetails.Where(d => d.IsRecieved == true && d.IsActive == true && d.IsDelete == false).OrderByDescending(x => x.ModifiedDate).Select(x => new GoodReceiptListViewModel
                    {
                        PK_AsnMasterID = x.ASNMaster.PK_AsnMasterID,
                        CompanyName = x.ASNMaster.CompanyMaster.CompanyName,
                        SupplierName = x.ASNMaster.SupplierMaster.SupplierName,
                        DateOfArrival = x.ASNMaster.DateOfArrival,
                        RepackedCaseNo = x.PalletNo,
                        InvoiceNo = x.ASNMaster.InvoiceNo,
                        Totalcontainer = _Context.ASNItemDetails.Where(s => s.PalletNo == x.PalletNo).Count(),
                        TotalScanned = _Context.ASNItemDetails.Where(s => s.PalletNo == x.PalletNo && s.IsRecieved == true).Count()
                    }).Distinct().ToList();
                }
                else
                {
                    listasnmaster = _Context.ASNItemDetails.Where(d => d.IsRecieved == true && d.IsActive == true && d.IsDelete == false && d.ASNMaster.CompanyMaster.CompanyName.Contains(goodreceiptlistviewmodel.CompanyName) || d.ASNMaster.SupplierMaster.SupplierName.Contains(goodreceiptlistviewmodel.SupplierName) || d.ASNMaster.InvoiceNo.Contains(goodreceiptlistviewmodel.InvoiceNo) || (d.ASNMaster.DateOfArrival <= goodreceiptlistviewmodel.ToDate && d.ASNMaster.DateOfArrival >= goodreceiptlistviewmodel.FromDate)).OrderByDescending(x => x.ModifiedDate).Select(x => new GoodReceiptListViewModel
                    {
                        PK_AsnMasterID = x.ASNMaster.PK_AsnMasterID,
                        CompanyName = x.ASNMaster.CompanyMaster.CompanyName,
                        SupplierName = x.ASNMaster.SupplierMaster.SupplierName,
                        DateOfArrival = x.ASNMaster.DateOfArrival,
                        RepackedCaseNo = x.PalletNo,
                        InvoiceNo = x.ASNMaster.InvoiceNo,
                        Totalcontainer = _Context.ASNItemDetails.Where(s => s.PalletNo.ToLower().Trim() == x.PalletNo.ToLower().Trim()).Count(),
                        TotalScanned = _Context.ASNItemDetails.Where(s => s.PalletNo.ToLower().Trim() == x.PalletNo.ToLower().Trim() && s.IsRecieved == true).Count()
                    }).Distinct().ToList();
                }
                if (listasnmaster.Count > 0)
                {
                    result.Message = "Record Get Successfully";
                    result.StatusCode = 1;
                    result.IsValid = true;
                    result.Result = "success";
                    result.Data = listasnmaster;
                }
                else
                {
                    result.Message = "Record does not exits";
                    result.StatusCode = 0;
                    result.IsValid = false;
                    result.Result = "error";
                    result.pk_ID = null;
                    result.Data = null;
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.pk_ID = null;
                result.Data = null;
            }
            return result;
        }

        public ResponseviewModel DeleteASN(Guid Asnid)
        {
            try
            {
                var AsnMasterData = _Context.ASNMaster.Where(a => a.PK_AsnMasterID == Asnid).FirstOrDefault();
                if (AsnMasterData != null)
                {
                    AsnMasterData.IsActive = false;
                    AsnMasterData.IsDelete = true;
                    AsnMasterData.ModifiedDate = System.DateTime.Now;

                    var Asnitemdata = _Context.ASNItemDetails.Where(a => a.FK_ASNMasterID == Asnid).ToList();
                    if (Asnitemdata.Count > 0)
                    {
                        foreach (var item in Asnitemdata)
                        {
                            item.IsActive = false;
                            item.IsDelete = true;
                            item.ModifiedDate = System.DateTime.Now;
                            //_Context.ASNMaster.Update(item.ASNMaster);
                        }
                    }
                    _Context.SaveChanges();
                    if (Asnitemdata.Count > 0)
                    {
                        result.Message = "Record Deleted Successfully";
                        result.StatusCode = 1;
                        result.IsValid = true;
                        result.Result = "success";
                        result.Data = AsnMasterData;
                    }
                    else
                    {
                        result.Message = "Record does not exits";
                        result.StatusCode = 0;
                        result.IsValid = false;
                        result.Result = "error";
                        result.pk_ID = null;
                        result.Data = null;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        public ResponseviewModel ASNInfoByInvoiceNo(String invoiceno)
        {
            ResponseviewModel result = new ResponseviewModel();
            try
            {
                List<ASNDetailModal> ASNDetail = new List<ASNDetailModal>();
                ASNDetail = _Context.ASNItemDetails.Where(d => d.ASNMaster.InvoiceNo.ToLower() == invoiceno.ToLower() && d.IsActive == true && d.IsDelete == false && d.IsRecieved == false).Select(a => new ASNDetailModal
                {
                    ContainerNo = a.ContainerNo,
                    CaseNo = a.PalletNo,
                    InvoiceNo = a.ASNMaster.InvoiceNo,
                    Quantity = a.Quantity,
                    Customer = a.Customer,
                    ItemCode = a.SuppliedPartNo
                }).ToList();

                if (ASNDetail != null)
                {
                    result.Message = "Record Get Successfully";
                    result.StatusCode = 1;
                    result.IsValid = true;
                    result.Result = "success";
                    result.Data = ASNDetail;
                }
                else
                {
                    result.Message = "Record does not exists";
                    result.StatusCode = 0;
                    result.IsValid = false;
                    result.Result = "error";
                    result.Data = null;
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.Data = null;
            }
            return result;
        }
    }
}
