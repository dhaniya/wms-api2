using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Domain.Engine.Access.Interfaces;
using Domain.Objects.Models;
using Infrastructure.Model.Main;
using Infrastructure.Model.Main.Entities;

namespace Domain.Engine.Access.Impl
{
  public class CustomerMasterService : ICustomerMasterService
  {
    protected readonly MainDbContext _Context;
    protected MainDbContext Context
    {
      get
      {
        return (MainDbContext)_Context;
      }
    }

    public CustomerMasterService(IMainDbContext context)

    {
      _Context = (MainDbContext)context;
    }

    public ResponseviewModel InsertUpdateCustomerMaster(CustomerMasterViewModel customermasterviewmodel)
    {
      ResponseviewModel result = new ResponseviewModel();
      try
      {

        string[] AllCustomerCode = customermasterviewmodel.Code.Split(",");
        customermasterviewmodel.Code = "";
        foreach (var item in AllCustomerCode)
        {
          string val = item.Trim();
          customermasterviewmodel.Code += val + ',';
        }
        customermasterviewmodel.Code = customermasterviewmodel.Code.Remove(customermasterviewmodel.Code.Length - 1);
        if (customermasterviewmodel.Pk_CustomerMasterId == null)
        {
          CustomerMaster customermaster = new CustomerMaster();
          customermaster.Pk_CustomerMasterId = Guid.NewGuid();
          customermaster.Name = customermasterviewmodel.Name;
          customermaster.Code = customermasterviewmodel.Code.Trim();
          customermaster.Description = customermasterviewmodel.Description;
          customermaster.ContactPerson = customermasterviewmodel.ContactPerson;
          customermaster.ContactNo = customermasterviewmodel.ContactNo;
          customermaster.Address = customermasterviewmodel.Address;
          customermaster.Description = customermasterviewmodel.Description;
          customermaster.EmailId = customermasterviewmodel.EmailId;
          customermaster.IsDelete = false;
          customermaster.IsActive = true;
          customermaster.CreatedBy = customermasterviewmodel.CreatedBy;
          customermaster.CreatedDate = System.DateTime.Now;
          customermaster.ModifiedBy = customermasterviewmodel.CreatedBy;
          customermaster.ModifiedDate = System.DateTime.Now;

          _Context.AddAsync(customermaster);
          _Context.SaveChanges();
          result.Message = "Data Inserted Successfully";
          result.StatusCode = 1;
          result.IsValid = true;
          result.Result = "success";
          result.pk_ID = customermaster.Pk_CustomerMasterId;
          result.Data = customermaster;
        }
        else
        {
          CustomerMaster customermaster = _Context.CustomerMaster.FirstOrDefault(d => d.Pk_CustomerMasterId == customermasterviewmodel.Pk_CustomerMasterId && d.IsActive == true && d.IsDelete == false);
          if (customermaster != null)
          {
            customermaster.Name = customermasterviewmodel.Name;
            customermaster.Code = customermasterviewmodel.Code.Trim();
            customermaster.Description = customermasterviewmodel.Description;
            customermaster.ContactPerson = customermasterviewmodel.ContactPerson;
            customermaster.ContactNo = customermasterviewmodel.ContactNo;
            customermaster.Address = customermasterviewmodel.Address;
            customermaster.Description = customermasterviewmodel.Description;
            customermaster.EmailId = customermasterviewmodel.EmailId;
            customermaster.IsDelete = false;
            customermaster.IsActive = true;
            customermaster.ModifiedDate = System.DateTime.Now;
            customermaster.ModifiedBy = customermasterviewmodel.ModifiedBy;
            _Context.Update(customermaster);
            _Context.SaveChanges();
            result.Message = "Data Updated Successfully";
            result.StatusCode = 1;
            result.IsValid = true;
            result.Result = "success";
            result.pk_ID = customermaster.Pk_CustomerMasterId;
            result.Data = customermaster;
          }
          else
          {
            result.Message = "Record does not exists";
            result.StatusCode = 0;
            result.IsValid = false;
            result.Result = "error";
            result.pk_ID = customermaster.Pk_CustomerMasterId;
            result.Data = null;
          }
        }
      }
      catch (Exception ex)
      {
        result.Message = ex.ToString();
        result.StatusCode = 0;
        result.IsValid = false;
        result.Result = "error";
        result.pk_ID = null;
        result.Data = null;
      }
      return result;
    }

    public ResponseviewModel GetCustomerMasterByID(Guid id)
    {
      ResponseviewModel result = new ResponseviewModel();
      try
      {
        var customermaster = _Context.CustomerMaster.FirstOrDefault(d => d.Pk_CustomerMasterId == id && d.IsActive == true && d.IsDelete == false);
        if (customermaster != null)
        {
          result.Message = "Record Get Successfully";
          result.StatusCode = 1;
          result.IsValid = true;
          result.Result = "success";
          result.pk_ID = customermaster.Pk_CustomerMasterId;
          result.Data = customermaster;
        }
        else
        {
          result.Message = "Record does not exists";
          result.StatusCode = 0;
          result.IsValid = false;
          result.Result = "error";
          result.pk_ID = id;
          result.Data = null;
        }
      }
      catch (Exception ex)
      {
        result.Message = ex.ToString();
        result.StatusCode = 0;
        result.IsValid = false;
        result.Result = "error";
        result.pk_ID = id;
        result.Data = null;
      }
      return result;
    }

    public ResponseviewModel GetAllListCustomerMaster()
    {
      ResponseviewModel result = new ResponseviewModel();
      try
      {
        var customermaster = _Context.CustomerMaster.Where(d => d.IsActive == true && d.IsDelete == false).ToList();
        if (customermaster != null)
        {
          result.Message = "Record Get Successfully";
          result.StatusCode = 1;
          result.IsValid = true;
          result.Result = "success";
          result.Data = customermaster.OrderByDescending(a => a.CreatedDate);
        }
        else
        {
          result.Message = "Record does not exists";
          result.StatusCode = 0;
          result.IsValid = false;
          result.Result = "error";
          result.Data = null;
        }
      }
      catch (Exception ex)
      {
        result.Message = ex.ToString();
        result.StatusCode = 0;
        result.IsValid = false;
        result.Result = "error";
        result.Data = null;
      }
      return result;
    }

    public ResponseviewModel DeleteCustomerMasterByID(CustomerMasterViewModel customer)
    {
      ResponseviewModel result = new ResponseviewModel();
      try
      {

        var customermaster = _Context.CustomerMaster.Where(d => d.Pk_CustomerMasterId == customer.Pk_CustomerMasterId && d.IsActive == true && d.IsDelete == false).FirstOrDefault();
        if (customermaster != null)
        {
          customermaster.IsDelete = true;
          customermaster.ModifiedBy = customer.ModifiedBy;
          customermaster.ModifiedDate = System.DateTime.Now;
          _Context.Update(customermaster);
          _Context.SaveChanges();

          result.Message = "Record Deleted Successfully";
          result.StatusCode = 1;
          result.IsValid = true;
          result.Result = "success";
          result.pk_ID = customermaster.Pk_CustomerMasterId;
        }
        else
        {
          result.Message = "Record does not exists";
          result.StatusCode = 0;
          result.IsValid = false;
          result.Result = "error";
          result.Data = null;
        }
      }
      catch (Exception ex)
      {
        result.Message = ex.ToString();
        result.StatusCode = 0;
        result.IsValid = false;
        result.Result = "error";
        result.Data = null;
      }
      return result;
    }

    public ResponseviewModel GetDropdownofCustomerMaster()
    {
      ResponseviewModel result = new ResponseviewModel();
      try
      {
        var customermaster = _Context.CustomerMaster.Where(d => d.IsActive == true && d.IsDelete == false).Select(x => new DropdownViewModel
        {
          ID = x.Pk_CustomerMasterId,
          Value = x.Name
        }).ToList();

        if (customermaster.Count() > 0)
        {
          result.Message = "Record Get Successfully";
          result.StatusCode = 1;
          result.IsValid = true;
          result.Result = "success";
          result.Data = customermaster;
        }
        else
        {
          result.Message = "Record does not exists";
          result.StatusCode = 0;
          result.IsValid = false;
          result.Result = "error";
          result.Data = null;
        }
      }
      catch (Exception ex)
      {
        result.Message = ex.ToString();
        result.StatusCode = 0;
        result.IsValid = false;
        result.Result = "error";
        result.Data = null;
      }
      return result;
    }

    public ResponseviewModel GetDropdownofCustomerCodeByID(Guid id)
    {
      ResponseviewModel result = new ResponseviewModel();
      try
      {
        CustomerMaster customermaster = new CustomerMaster();
        customermaster = _Context.CustomerMaster.Where(d => d.Pk_CustomerMasterId == id && d.IsActive == true && d.IsDelete == false).FirstOrDefault();

        if (customermaster != null)
        {
          var customercode = customermaster.Code;
          List<StringDropdownviewModel> StringDropdownviewModellist = new List<StringDropdownviewModel>();
          string[] customercodelist = customercode.Split(',');
          for (int i = 0; i < customercodelist.Length; i++)
          {
            StringDropdownviewModel stringdropdown = new StringDropdownviewModel();
            stringdropdown.ID = customercodelist[i];
            stringdropdown.Value = customercodelist[i];
            StringDropdownviewModellist.Add(stringdropdown);
          }

          result.Message = "Record Get Successfully";
          result.StatusCode = 1;
          result.IsValid = true;
          result.Result = "success";
          result.Data = StringDropdownviewModellist;
        }
        else
        {
          result.Message = "Record does not exists";
          result.StatusCode = 0;
          result.IsValid = false;
          result.Result = "error";
          result.Data = null;
        }
      }
      catch (Exception ex)
      {
        result.Message = ex.ToString();
        result.StatusCode = 0;
        result.IsValid = false;
        result.Result = "error";
        result.Data = null;
      }
      return result;
    }

    public ResponseviewModel GetDuplicateCustomerCodeCheck(string code, string custid)
    {

      ResponseviewModel result = new ResponseviewModel();
      try
      {

        Guid cguid = custid == "null" ? default(Guid) : Guid.Parse(custid);

        Boolean isavailable = false;
        var customercode = code.Split(',');
        foreach (var item in customercode)
        {
          if (item == "")
          {
            continue;
          }
          CustomerMaster customercodes = new CustomerMaster();
          if (cguid != default(Guid))
          {
            customercodes = _Context.CustomerMaster.Where(d => d.IsActive == true && d.IsDelete == false && d.Pk_CustomerMasterId != cguid).AsEnumerable().Where(d => d.Code.Split(',', StringSplitOptions.RemoveEmptyEntries).Contains(item)).FirstOrDefault();

          }
          else
          {
            customercodes = _Context.CustomerMaster.Where(d => d.IsActive == true && d.IsDelete == false).AsEnumerable().Where(d => d.Code.Split(',', StringSplitOptions.RemoveEmptyEntries).Contains(item)).FirstOrDefault();
          }

          if (customercodes != null)
          {
            isavailable = true;
          }
        }
        if (isavailable)
        {
          result.Message = "Code already exists";
          result.StatusCode = 1;
          result.IsValid = true;
          result.Result = "success";
          result.Data = customercode;
        }
        else
        {
          result.Message = "Code not Match";
          result.StatusCode = 0;
          result.IsValid = false;
          result.Result = "error";
          result.Data = null;
        }

      }
      catch (Exception ex)
      {
        result.Message = ex.ToString();
        result.StatusCode = 0;
        result.IsValid = false;
        result.Result = "error";
        result.pk_ID = null;
        result.Data = null;
      }
      return result;
    }
  }
}
