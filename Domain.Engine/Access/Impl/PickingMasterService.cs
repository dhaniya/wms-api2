using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Domain.Engine.Access.Interfaces;
using Domain.Objects.Models;
using Infrastructure.Model.Main;
using Infrastructure.Model.Main.Entities;

namespace Domain.Engine.Access.Impl
{
    public class PickingMasterService : IPickingMasterService
    {
        protected readonly MainDbContext _Context;
        protected readonly IStockTransactionService _stockTransactionService;
        ResponseviewModel result = new ResponseviewModel();
        protected MainDbContext Context
        {
            get
            {
                return (MainDbContext)_Context;
            }
        }
        public PickingMasterService(IMainDbContext context, IStockTransactionService stockTransactionService)
        {
            _Context = (MainDbContext)context;
            _stockTransactionService = stockTransactionService;
        }

        public ResponseviewModel GetDropdownofCustomerMaster()
        {

            try
            {
                List<DropdownViewModel> dropdownlist = new List<DropdownViewModel>();
                //Add All Customer field in Dropdowmviewmodel
                DropdownViewModel dropdownviewmodel = new DropdownViewModel();
                dropdownviewmodel.ID = Guid.Empty;
                dropdownviewmodel.Value = "All Customer";
                dropdownlist.Add(dropdownviewmodel);
                List<DropdownViewModel> customermaster = new List<DropdownViewModel>();
                customermaster = _Context.CustomerMaster.Where(d => d.IsActive == true && d.IsDelete == false).Select(x => new DropdownViewModel
                {
                    ID = x.Pk_CustomerMasterId,
                    Value = x.Name
                }).OrderBy(x => x.Value).ToList();

                if (customermaster.Count() > 0)
                {
                    dropdownlist.AddRange(customermaster);
                    result.Message = "Record Get Successfully";
                    result.StatusCode = 1;
                    result.IsValid = true;
                    result.Result = "success";
                    result.Data = dropdownlist;
                }
                else
                {
                    result.Message = "Record does not exists";
                    result.StatusCode = 0;
                    result.IsValid = false;
                    result.Result = "error";
                    result.Data = null;
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.Data = null;
            }
            return result;
        }

        public ResponseviewModel GetDropdownofCustomerCodeByID(Guid id)
        {
            try
            {
                if (id == Guid.Empty)
                {
                    List<CustomerMaster> customermasterlist = new List<CustomerMaster>();
                    customermasterlist = _Context.CustomerMaster.Where(d => d.IsActive == true && d.IsDelete == false).ToList();
                    List<StringDropdownviewModel> StringDropdownviewModelmultilist = new List<StringDropdownviewModel>();
                    if (customermasterlist.Count > 0)
                    {
                        foreach (var item in customermasterlist)
                        {
                            var customercode = item.Code;
                            string[] customercodelist = customercode.Split(',');
                            for (int i = 0; i < customercodelist.Length; i++)
                            {
                                StringDropdownviewModel stringdropdown = new StringDropdownviewModel();
                                stringdropdown.ID = customercodelist[i];
                                stringdropdown.Value = customercodelist[i];
                                StringDropdownviewModelmultilist.Add(stringdropdown);
                            }
                        }
                        result.Message = "Record Get Successfully";
                        result.StatusCode = 1;
                        result.IsValid = true;
                        result.Result = "success";
                        result.Data = StringDropdownviewModelmultilist;
                    }
                    else
                    {
                        result.Message = "Record does not exists";
                        result.StatusCode = 0;
                        result.IsValid = false;
                        result.Result = "error";
                        result.Data = null;
                    }
                }
                else
                {
                    CustomerMaster customermaster = new CustomerMaster();
                    customermaster = _Context.CustomerMaster.Where(d => d.Pk_CustomerMasterId == id && d.IsActive == true && d.IsDelete == false).FirstOrDefault();
                    if (customermaster != null)
                    {
                        var customercode = customermaster.Code;
                        List<StringDropdownviewModel> StringDropdownviewModellist = new List<StringDropdownviewModel>();
                        string[] customercodelist = customercode.Split(',');
                        for (int i = 0; i < customercodelist.Length; i++)
                        {
                            StringDropdownviewModel stringdropdown = new StringDropdownviewModel();
                            stringdropdown.ID = customercodelist[i];
                            stringdropdown.Value = customercodelist[i];
                            StringDropdownviewModellist.Add(stringdropdown);
                        }
                        result.Message = "Record Get Successfully";
                        result.StatusCode = 1;
                        result.IsValid = true;
                        result.Result = "success";
                        result.Data = StringDropdownviewModellist;
                    }
                    else
                    {
                        result.Message = "Record does not exists";
                        result.StatusCode = 0;
                        result.IsValid = false;
                        result.Result = "error";
                        result.Data = null;
                    }
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.Data = null;
            }
            return result;
        }

        public ResponseviewModel GetDropdownofPutAwayInvoiceNoByCustomerCode(string code)
        {
            try
            {
                var CustomerCode = code.Split(',');
                var putawayinvoices = _Context.StockTransaction.Where(d => CustomerCode.Contains(d.SortingMaster.CustomerCode) && d.SortingMaster.IsPutaway == true && d.SortingMaster.IsActive == true && d.SortingMaster.IsDelete == false && d.QuantityRemaining > 0).OrderByDescending(k => k.CreatedDate)
                .Select(x => new
                {
                    ID = x.SortingMaster.InvoiceNo,
                    Value = x.SortingMaster.InvoiceNo,
                    Date= x.CreatedDate
                  
                }).OrderByDescending(k=>k.Date).Distinct().ToList();

                if (putawayinvoices.Count > 0)
                {
                    //var lst = putawayinvoices.Select(y => new StringDropdownviewModel
                    //{
                    //    ID = y.ID,
                    //    Value = y.Value
                    //}).Distinct().ToList();
                    List<StringDropdownviewModel> lstdropdown = new List<StringDropdownviewModel>();
                    foreach (var item in putawayinvoices.OrderByDescending(k=>k.Date))
                    {
                        if (!lstdropdown.Any(k => k.ID == item.ID))
                        {
                            lstdropdown.Add(new StringDropdownviewModel { ID = item.ID, Value = item.Value });
                        }
                    }
                    result.Message = "Record Get Successfully";
                    result.StatusCode = 1;
                    result.IsValid = true;
                    result.Result = "success";
                    result.Data = lstdropdown ;
                }
                else
                {
                    result.Message = "Record does not exists";
                    result.StatusCode = 0;
                    result.IsValid = false;
                    result.Result = "error";
                    result.Data = null;
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.Data = null;
            }
            return result;
        }

        public ResponseviewModel GetDropdownofRepackedCaseNoByInvoiceno(string invoiceno, string customercode)
        {
            try
            {
                string[] custcode = customercode.Split(',');
                List<StringDropdpwnviewRepackedModel> liststringdropdownviewmodel = new List<StringDropdpwnviewRepackedModel>();
                List<StringDropdpwnviewRepackedModel> masterstringdropdownviewmodel = new List<StringDropdpwnviewRepackedModel>();
                liststringdropdownviewmodel = _Context.StockTransaction.Where(d => d.SortingMaster.InvoiceNo.ToLower().Trim() == invoiceno.ToLower().Trim() && custcode.Contains(d.SortingMaster.CustomerCode) && d.SortingMaster.IsPutaway == true && d.SortingMaster.IsActive == true && d.SortingMaster.IsDelete == false && d.SortingMaster.IsBin != true && d.QuantityRemaining > 0)
                .Select(x => new StringDropdpwnviewRepackedModel
                {
                    ID = x.SortingMaster.RepakedCaseNo,

                    Value = x.SortingMaster.RepakedCaseNo,
                    LocationID = x.SortingMaster.LocationID,

                }).Distinct().ToList();
                if (liststringdropdownviewmodel.Count > 0)
                {
                    foreach (var item in liststringdropdownviewmodel)
                    {
                        //  PickingDetail pickingDetail = new PickingDetail();
                        // pickingDetail = _Context.PickingDetail.Where(d => d.RepackedCaseNo.ToLower().Trim() == item.ID.ToLower().Trim()).FirstOrDefault();
                        ////var sortingdetails = _Context.SortingDetail.Where(k => k.RepakedCaseNo.ToLower().Trim() == item.ID.ToLower().Trim()).FirstOrDefault();
                        // if (pickingDetail == null)
                        // {
                        //  StringDropdpwnviewRepackedModel stringviewmodel = new StringDropdpwnviewRepackedModel();
                        //  stringviewmodel.ID = item.ID;
                        //  stringviewmodel.Value = item.Value;
                        // stringviewmodel.Pk_SortingID = item.Pk_SortingID;
                        //  stringviewmodel.LocationID = item.LocationID;
                        // stringviewmodel.Quantity =   GetAvailableQuantityRepack(item.ID,invoiceno);

                        //stringviewmodel.LBH = Math.Round(sortingdetails.Length,0) + "*" + Math.Round(sortingdetails.Breadth,0) + "*" + (int)Math.Round(sortingdetails.Height,0);
                        // masterstringdropdownviewmodel.Add(stringviewmodel);
                        //   }
                        //  else
                        //  {
                        //  if (pickingDetail.PickingType == 2)
                        // {
                        int availableQuantity = GetAvailableQuantityRepack(item.ID, invoiceno);
                        if (availableQuantity > 0)
                        {
                            StringDropdpwnviewRepackedModel stringviewmodel = new StringDropdpwnviewRepackedModel();
                            stringviewmodel.ID = item.ID;
                            stringviewmodel.Value = item.Value;
                            stringviewmodel.Pk_SortingID = item.Pk_SortingID;
                            stringviewmodel.LocationID = item.LocationID;
                            stringviewmodel.Quantity = availableQuantity;
                            //stringviewmodel.LBH = sortingdetails.Length + "*" + sortingdetails.Breadth + "*" + sortingdetails.Height;
                            masterstringdropdownviewmodel.Add(stringviewmodel);
                        }
                        //  }
                        //  }
                    }
                    if (masterstringdropdownviewmodel.Count > 0)
                    {
                        result.Message = "Record Get Successfully";
                        result.StatusCode = 1;
                        result.IsValid = true;
                        result.Result = "success";
                        result.Data = masterstringdropdownviewmodel;
                    }
                    else
                    {
                        result.Message = "Record does not exists";
                        result.StatusCode = 0;
                        result.IsValid = false;
                        result.Result = "error";
                        result.Data = null;
                    }
                }
                else
                {
                    result.Message = "Record does not exists";
                    result.StatusCode = 0;
                    result.IsValid = false;
                    result.Result = "error";
                    result.Data = null;
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.Data = null;
            }
            return result;
        }

        public ResponseviewModel GetItemCodeByRepackedCaseNo(string repackedcaseno, string invoice)
        {
            try
            {
                var putawayinvoices = _Context.SortingMaster.Where(d => d.RepakedCaseNo.ToLower().Trim() == repackedcaseno.ToLower().Trim() && d.IsPutaway == true && d.IsActive == true && d.IsDelete == false && d.IsBin == false && d.InvoiceNo.ToLower().Trim() == invoice.ToLower().Trim())
                .Select(x => new StringItemcodeQuantityviewModel
                {
                    ID = x.ItemCode,
                    Value = x.ItemCode,
                    Locationid = x.LocationID,
                    invoiceno = x.InvoiceNo,
                    OrigionalCase = x.CaseNo,
                    fk_SortingMaster_id = x.Pk_SortingID


                }).Distinct().ToList();

                foreach (var item in putawayinvoices)
                {
                    item.Quantity = _Context.StockTransaction.Where(pk => pk.PK_SortingID == item.fk_SortingMaster_id).FirstOrDefault() != null ? _Context.StockTransaction.Where(pk => pk.PK_SortingID == item.fk_SortingMaster_id).FirstOrDefault().QuantityRemaining.Value : 0;// GetAvailableQuantity(item.OrigionalCase, item.Value, repackedcaseno, item.invoiceno);
                }
                if (putawayinvoices.Count > 0)
                {
                    result.Message = "Record Get Successfully";
                    result.StatusCode = 1;
                    result.IsValid = true;
                    result.Result = "success";
                    result.Data = putawayinvoices;
                }
                else
                {
                    result.Message = "Record does not exists";
                    result.StatusCode = 0;
                    result.IsValid = false;
                    result.Result = "error";
                    result.Data = null;
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.Data = null;
            }
            return result;
        }

        public ResponseviewModel GetAvailableQuantityBySortingDetail(string itemcode, string caseno, string invoiceno)
        {
            try
            {
                int totalquantity = 0;
                int pickingquantity = 0;
                totalquantity = _Context.SortingMaster.Where(d => d.InvoiceNo.ToLower().Trim() == invoiceno.ToLower().Trim() && d.RepakedCaseNo.ToLower().Trim() == caseno.ToLower().Trim() && d.ItemCode.ToLower().Trim() == itemcode.ToLower().Trim()).Sum(x => x.Quantity);
                pickingquantity = _Context.PickingDetail.Where(d => d.InvoiceNo.ToLower().Trim() == invoiceno.ToLower().Trim() && d.RepackedCaseNo.ToLower().Trim() == caseno.ToLower().Trim() && d.ItemCode.ToLower().Trim() == itemcode.ToLower().Trim() && d.IsActive == true && d.IsDelete == false).Sum(x => x.Quantity);
                //Remain to count sorted quantity
                AvailableQuantityViewModel availablequantityviewmodel = new AvailableQuantityViewModel();
                availablequantityviewmodel.AvailableQuantity = totalquantity - pickingquantity;
                if (availablequantityviewmodel.AvailableQuantity > 0)
                {
                    result.Message = "Record Get Successfully";
                    result.StatusCode = 1;
                    result.IsValid = true;
                    result.Result = "success";
                    result.Data = availablequantityviewmodel;
                }
                else
                {
                    availablequantityviewmodel.AvailableQuantity = 0;
                    result.Message = "Record does not exists";
                    result.StatusCode = 0;
                    result.IsValid = false;
                    result.Result = "error";
                    result.Data = availablequantityviewmodel;
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.Data = null;
            }
            return result;
        }

        public ResponseviewModel GetSortingdetailbyRepackedCaseno(string repackedcaseno, string itemcode, int pickingtype, string invoiceno)
        {
            try
            {

                if (pickingtype == 1)
                {
                    if (repackedcaseno != null)
                    {
                        // SortingMaster sortingmaster = new SortingMaster();
                        var sortingmaster = _Context.SortingMaster.Where(d => d.RepakedCaseNo.ToLower().Trim() == repackedcaseno.ToLower().Trim() && d.InvoiceNo.ToLower().Trim() == invoiceno.ToLower().Trim() && d.IsPutaway == true && d.IsActive == true && d.IsDelete == false && d.IsBin == false && d.Quantity > 0).ToList();
                        if (sortingmaster.Count != 0)
                        {
                            List<PickingDetailViewModel> pickingdetailviewmodel = new List<PickingDetailViewModel>();
                            foreach (var item in sortingmaster)
                            {
                                pickingdetailviewmodel.AddRange(_Context.SortingDetail.Where(d => d.FK_SortingMasterID == item.Pk_SortingID && d.IsActive == true && d.IsDelete == false).Select(x => new PickingDetailViewModel
                                {
                                    InvoiceNo = x.SortingMaster.InvoiceNo,
                                    RepackedCaseNo = x.SortingMaster.RepakedCaseNo,
                                    ItemCode = x.SortingMaster.ItemCode,
                                    Quantity = 0,
                                    LocationId = x.SortingMaster.LocationID,
                                    Height = x.Height,
                                    Length = x.Length,
                                    Breadth = x.Breadth,
                                    Weight = x.Weight,
                                    fk_SortingMaster_id = item.Pk_SortingID,
                                    OrigionalCase = item.CaseNo

                                }).ToList());

                            }

                            foreach (var item in pickingdetailviewmodel)
                            {
                                //item.Quantity = GetAvailableQuantity(item.OrigionalCase, item.ItemCode, item.RepackedCaseNo, item.InvoiceNo);
                                item.Quantity = _Context.StockTransaction.Where(a => a.PK_SortingID == item.fk_SortingMaster_id).FirstOrDefault().QuantityRemaining??0;
                            }


                            result.Message = "Record Get Successfully";
                            result.StatusCode = 1;
                            result.IsValid = true;
                            result.Result = "success";
                            result.Data = pickingdetailviewmodel;
                        }
                        else
                        {
                            result.Message = "Repacked case no is can't create as a put away.";
                            result.StatusCode = 0;
                            result.IsValid = false;
                            result.Result = "error";
                            result.pk_ID = null;
                            result.Data = null;
                        }
                    }
                    else
                    {
                        result.Message = "Please select repacked case no";
                        result.StatusCode = 0;
                        result.IsValid = false;
                        result.Result = "error";
                        result.pk_ID = null;
                        result.Data = null;
                    }
                }
                else
                {
                    PickingDetailViewModel pickingdetailviewmodel = new PickingDetailViewModel();
                    pickingdetailviewmodel = _Context.SortingDetail.Where(d => d.RepakedCaseNo.ToLower().Trim() == repackedcaseno.ToLower().Trim() && d.SortingMaster.ItemCode.ToLower().Trim() == itemcode.ToLower().Trim() && d.SortingMaster.IsPutaway == true && d.IsActive == true && d.IsDelete == false)
                    .Select(x => new PickingDetailViewModel
                    {
                        LocationId = x.SortingMaster.LocationID,
                        Length = x.Length,
                        Weight = x.Weight,
                        Height = x.Height,
                        Breadth = x.Breadth,
                        ItemCode = x.SortingMaster.ItemCode,
                        RepackedCaseNo = x.SortingMaster.RepakedCaseNo,
                    }).FirstOrDefault();
                    if (pickingdetailviewmodel != null)
                    {
                        result.Message = "Record Get Successfully";
                        result.StatusCode = 1;
                        result.IsValid = true;
                        result.Result = "success";
                        result.Data = pickingdetailviewmodel;
                    }
                    else
                    {
                        result.Message = "Record does not exists";
                        result.StatusCode = 0;
                        result.IsValid = false;
                        result.Result = "error";
                        result.Data = null;
                    }
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.Data = null;
            }
            return result;
        }

        public ResponseviewModel InsertPickingMaster(PickingViewModel pickingviewmodel)
        {
            try
            {
                if (pickingviewmodel.listpickingdetail.Count > 0)
                {
                    PickingMaster pickingmaster = new PickingMaster();
                    pickingmaster.Pk_PickingMasterID = Guid.NewGuid();
                    pickingmaster.OrderNumber = GenerateOrderNo();
                    if (pickingviewmodel.Fk_CustomerMasterId == Guid.Empty)
                        pickingmaster.Fk_CustomerMasterId = null;
                    else
                        pickingmaster.Fk_CustomerMasterId = pickingviewmodel.Fk_CustomerMasterId;
                    pickingmaster.CustomerName = pickingviewmodel.CustomerName;
                    pickingmaster.CustomerCode = pickingviewmodel.CustomerCode;
                    pickingmaster.DateOfDeliver = pickingviewmodel.DateOfDeliver;
                    pickingmaster.RefNo = pickingviewmodel.RefNo;
                    pickingmaster.Priority = pickingviewmodel.Priority;
                    pickingmaster.IsDelete = false;
                    pickingmaster.IsActive = true;
                    pickingmaster.CreatedBy = pickingviewmodel.CreatedBy;
                    pickingmaster.CreatedDate = System.DateTime.Now;
                    pickingmaster.ModifiedBy = pickingviewmodel.CreatedBy;
                    pickingmaster.ModifiedDate = System.DateTime.Now;

                    pickingmaster.PickingStatus = 0;//No picking
                    _Context.AddAsync(pickingmaster);
                    _Context.SaveChanges();
                    //Insert Default data in sorting detail table
                    foreach (var item in pickingviewmodel.listpickingdetail)
                    {
                        var sorm = _Context.SortingMaster.Where(so => so.Pk_SortingID == item.fk_SortingMaster_id).FirstOrDefault();
                        int availableqty = 0;
                        if (item.PickingType == 3)
                        {
                            availableqty = _Context.StockTransaction.Where(k => k.PK_SortingID == item.fk_SortingMaster_id).Sum(q => q.QuantityRemaining).Value;// GetAvailableQuantityBin(item.ItemCode, item.LocationId);
                        }
                        else
                        {
                            availableqty = _Context.StockTransaction.Where(k => k.PK_SortingID == item.fk_SortingMaster_id).Sum(q => q.QuantityRemaining).Value;
                        }


                        if (availableqty >= item.Quantity)
                        {
                            PickingDetail pickingdetail = new PickingDetail();
                            pickingdetail.Pk_PickingDetailId = Guid.NewGuid();
                            pickingdetail.Fk_PickingID = pickingmaster.Pk_PickingMasterID;
                            pickingdetail.PickingType = item.PickingType; //1 : Repacked Case No, 2 : loose Items
                            pickingdetail.InvoiceNo = item.InvoiceNo;
                            pickingdetail.RepackedCaseNo = item.RepackedCaseNo;
                            //pickingdetail.Fk_PickingID = item.Fk_PickingID;
                            pickingdetail.Fk_SortingMasterID = item.fk_SortingMaster_id;
                            pickingdetail.ItemCode = item.ItemCode;
                            pickingdetail.Quantity = item.Quantity;
                            pickingdetail.LocationId = item.LocationId;
                            pickingdetail.Height = item.Height;
                            pickingdetail.Length = item.Length;
                            pickingdetail.Breadth = item.Breadth;
                            pickingdetail.Weight = item.Weight;
                            pickingdetail.IsPicked = false;
                            pickingdetail.IsDelete = false;
                            pickingdetail.IsActive = true;
                            pickingdetail.CreatedBy = item.CreatedBy;
                            pickingdetail.CreatedDate = System.DateTime.Now;
                            pickingdetail.ModifiedBy = item.CreatedBy;
                            pickingdetail.ModifiedDate = System.DateTime.Now;
                            _Context.AddAsync(pickingdetail);
                            _Context.SaveChanges();

                            // Update quantity
                            //if (item.PickingType==3)
                            //{
                            //    var lstsortdetails = _Context.SortingMaster.Where(d => d.ItemCode == item.ItemCode && d.LocationID == item.LocationId && d.IsBin == true && d.IsActive == true && d.IsDelete == false).ToList();
                            //    int qtyselected = item.Quantity;
                            //    foreach (var itmlstsortdetails in lstsortdetails)
                            //    {
                            //        if (itmlstsortdetails.Quantity<=qtyselected)
                            //        {
                            //            StockTransactionModel stockTransactionModel = new StockTransactionModel();
                            //            stockTransactionModel.CreatedBy = item.CreatedBy;
                            //            stockTransactionModel.PK_SortingID = itmlstsortdetails.Pk_SortingID;
                            //            stockTransactionModel.QuantitySelected = itmlstsortdetails.Quantity;

                            //            _stockTransactionService.SteponPrepickingInsert(stockTransactionModel);
                            //            qtyselected = qtyselected - itmlstsortdetails.Quantity;
                            //        }

                            //    }

                            //}
                            //else
                            //{
                            StockTransactionModel stockTransactionModel = new StockTransactionModel();
                            stockTransactionModel.CreatedBy = item.CreatedBy;
                            stockTransactionModel.PK_SortingID = item.fk_SortingMaster_id;
                            stockTransactionModel.QuantitySelected = item.Quantity;

                            _stockTransactionService.SteponPrepickingInsert(stockTransactionModel);
                            //  }


                        }

                    }
                    result.Message = "Data Inserted Successfully";
                    result.StatusCode = 1;
                    result.IsValid = true;
                    result.Result = "success";
                    result.pk_ID = pickingmaster.Pk_PickingMasterID;
                    result.Data = pickingmaster;
                }
                else
                {
                    result.Message = "Please add some picking item details in picking";
                    result.StatusCode = 0;
                    result.IsValid = false;
                    result.Result = "error";
                    result.pk_ID = null;
                    result.Data = null;
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.pk_ID = null;
                result.Data = null;
            }
            return result;
        }

        public int GetAvailableQuantity(string origionalcase, string itemcode, string caseno, string invoiceno)
        {
            int availablequantity = 0;
            try
            {
                int totalquantity = 0;
                int pickingquantity = 0;
                totalquantity = _Context.SortingMaster.Where(d => d.InvoiceNo.ToLower().Trim() == invoiceno.ToLower().Trim() && d.RepakedCaseNo.ToLower().Trim() == caseno.ToLower().Trim() && d.ItemCode.ToLower().Trim() == itemcode.ToLower().Trim() && d.CaseNo == origionalcase && d.IsBin == false && d.IsActive == true && d.IsDelete == false && d.IsPutaway == true).Sum(x => x.Quantity);
                pickingquantity = _Context.PickingDetail.Where(d => d.InvoiceNo.ToLower().Trim() == invoiceno.ToLower().Trim() && d.RepackedCaseNo.ToLower().Trim() == caseno.ToLower().Trim() && d.ItemCode.ToLower().Trim() == itemcode.ToLower().Trim() && d.IsActive == true && d.IsDelete == false).Sum(x => x.Quantity);
                //Remain to count sorted quantity
                availablequantity = totalquantity - pickingquantity;
                if (availablequantity <= 0)
                {
                    availablequantity = 0;
                }
            }
            catch (Exception ex)
            {
                availablequantity = 0;
            }
            return availablequantity;
        }
        public int GetAvailableQuantityRepack(string caseno, string invoiceno)
        {
            int availablequantity = 0;
            try
            {
                int totalquantity = 0;
                int pickingquantity = 0;
                // totalquantity = _Context.SortingMaster.Where(d => d.InvoiceNo.ToLower().Trim() == invoiceno.ToLower().Trim() && d.RepakedCaseNo.ToLower().Trim() == caseno.ToLower().Trim() &&  d.IsBin == false && d.IsActive == true && d.IsDelete == false && d.IsPutaway == true).Sum(x => x.Quantity);
                // pickingquantity = _Context.PickingDetail.Where(d => d.InvoiceNo.ToLower().Trim() == invoiceno.ToLower().Trim() && d.RepackedCaseNo.ToLower().Trim() == caseno.ToLower().Trim()  && d.IsActive == true && d.IsDelete == false).Sum(x => x.Quantity);
                //Remain to count sorted quantity
                availablequantity = _Context.StockTransaction.Where(d => d.SortingMaster.InvoiceNo.ToLower().Trim() == invoiceno.ToLower().Trim() && d.SortingMaster.RepakedCaseNo.ToLower().Trim() == caseno.ToLower().Trim() && d.SortingMaster.IsBin == false && d.SortingMaster.IsActive == true && d.SortingMaster.IsDelete == false && d.SortingMaster.IsPutaway == true).Sum(k => k.QuantityRemaining).Value;
                if (availablequantity <= 0)
                {
                    availablequantity = 0;
                }
            }
            catch (Exception ex)
            {
                availablequantity = 0;
            }
            return availablequantity;
        }

        public int GetAvailableQuantityBin(string itemcode, string location)
        {
            int availablequantity = 0;
            try
            {
                int totalquantity = 0;
                int pickingquantity = 0;
                totalquantity = _Context.SortingMaster.Where(d => d.ItemCode.ToLower().Trim() == itemcode.ToLower().Trim() && d.LocationID.ToLower().Trim() == location.ToLower().Trim() && d.IsBin == true && d.IsActive == true && d.IsDelete == false && d.IsPutaway == true).Sum(x => x.Quantity);
                pickingquantity = _Context.PickingDetail.Where(d => d.ItemCode.ToLower().Trim() == itemcode.ToLower().Trim() && d.PickingType == 3 && d.LocationId.ToLower().Trim() == location.ToLower().Trim() && d.IsActive == true && d.IsDelete == false).Sum(x => x.Quantity);
                //Remain to count sorted quantity
                availablequantity = totalquantity - pickingquantity;
                if (availablequantity <= 0)
                {
                    availablequantity = 0;
                }
            }
            catch (Exception ex)
            {
                availablequantity = 0;
            }
            return availablequantity;
        }

        public ResponseviewModel GetPickingList(SearchPickingViewModel searchpickingviewmodel)
        {
            try
            {
                List<PickingMasterViewModel> Masterlistpickingmasterviewmodel = new List<PickingMasterViewModel>();
                List<PickingMasterViewModel> listpickingmasterviewmodel = new List<PickingMasterViewModel>();

                if (searchpickingviewmodel.OrderNo == "" && searchpickingviewmodel.FromDate == null && searchpickingviewmodel.ToDate == null && searchpickingviewmodel.CustomerCode == "")
                {
                    var data = _Context.PickingMaster.Where(k => (k.PickingStatus == 0 || k.PickingStatus == 1) && k.IsActive == true && k.IsDelete == false)
                        //&&_Context.PickingDetail.Where(k=>k.ItemCode!=null)
                        .OrderByDescending(k => k.CreatedDate)
                             .Select(x => new PickingMasterViewModel
                             {
                                 Pk_PickingMasterID = x.Pk_PickingMasterID,
                                 username = x.UserMaster.FirstName,
                                 OrderNumber = x.OrderNumber,
                                 CustomerName = x.CustomerName,
                                 CustomerCode = x.CustomerCode,
                                 DateOfDeliver = x.DateOfDeliver,
                                 RefNo = x.RefNo,
                                 Priority = x.Priority,
                                 PickingStatus = x.PickingStatus,
                                 CreatedDate = x.CreatedDate,
                                 ModifiedDate = x.ModifiedDate,
                                 Invoiceno = _Context.PickingDetail.FirstOrDefault(d => d.Fk_PickingID == x.Pk_PickingMasterID && d.IsActive == true && d.IsDelete == false).InvoiceNo
                             }).OrderByDescending(p => p.CreatedDate).ToList();
                    foreach (var item in data)
                    {
                        var a = _Context.PickingDetail.Where(j => j.PickingMaster.OrderNumber.Trim() == item.OrderNumber.Trim()).Any();
                        if (a)
                        {
                            listpickingmasterviewmodel.Add(item);
                        }
                    }

                }
                else
                {
                    var cCode = searchpickingviewmodel.CustomerCode.Split(',');
                    DateTime from = Convert.ToDateTime(searchpickingviewmodel.FromDate);
                    DateTime to = Convert.ToDateTime(searchpickingviewmodel.ToDate);
                    var data1 = _Context.PickingMaster.Where(k => k.OrderNumber == (searchpickingviewmodel.OrderNo != "" ? searchpickingviewmodel.OrderNo : k.OrderNumber) && (searchpickingviewmodel.CustomerCode != "" ? cCode.Contains(k.CustomerCode) : true) &&
                                                                             (k.DateOfDeliver.Date >= (searchpickingviewmodel.FromDate != null ? searchpickingviewmodel.FromDate : k.DateOfDeliver.Date) &&
                                                                              k.DateOfDeliver.Date <= (searchpickingviewmodel.ToDate != null ? searchpickingviewmodel.ToDate : k.DateOfDeliver.Date)) &&
                                                                              k.IsActive == true && k.IsDelete == false && (k.PickingStatus == 0 || k.PickingStatus == 1))
                                                                              .OrderByDescending(k => k.CreatedDate)
                            .Select(x => new PickingMasterViewModel
                            {
                                Pk_PickingMasterID = x.Pk_PickingMasterID,
                                username = x.UserMaster.FirstName,
                                OrderNumber = x.OrderNumber,
                                CustomerName = x.CustomerName,
                                CustomerCode = x.CustomerCode,
                                DateOfDeliver = x.DateOfDeliver,
                                RefNo = x.RefNo,
                                Priority = x.Priority,
                                PickingStatus = x.PickingStatus,
                                CreatedDate = x.CreatedDate,
                                ModifiedDate = x.ModifiedDate,
                                Invoiceno = _Context.PickingDetail.FirstOrDefault(d => d.Fk_PickingID == x.Pk_PickingMasterID && d.IsActive == true && d.IsDelete == false).InvoiceNo
                            }).OrderByDescending(p => p.CreatedDate).ToList();
                    foreach (var item in data1)
                    {
                        var a = _Context.PickingDetail.Where(j => j.PickingMaster.OrderNumber.Trim() == item.OrderNumber.Trim()).Any();
                        if (a)
                        {
                            listpickingmasterviewmodel.Add(item);
                        }
                    }
                }


                if (listpickingmasterviewmodel.Count > 0)
                {
                    foreach (var item in listpickingmasterviewmodel)
                    {
                        item.PickingStatus = GetPickingStatusByPickingid(item.Pk_PickingMasterID);
                        if (item.PickingStatus != 2)
                        {
                            Masterlistpickingmasterviewmodel.Add(item);
                        }
                    }
                    result.Message = "Record Get Successfully";
                    result.StatusCode = 1;
                    result.IsValid = true;
                    result.Result = "success";
                    result.Data = Masterlistpickingmasterviewmodel;
                }
                else
                {
                    result.Message = "Record does not exists";
                    result.StatusCode = 0;
                    result.IsValid = false;
                    result.Result = "error";
                    result.Data = null;
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.Data = null;
            }
            return result;
        }
        public ResponseviewModel GetPickingListALL(SearchPickingViewModel searchpickingviewmodel)
        {
            try
            {
                List<PickingMasterViewModel> Masterlistpickingmasterviewmodel = new List<PickingMasterViewModel>();
                List<PickingMasterViewModel> listpickingmasterviewmodel = new List<PickingMasterViewModel>();
                List<PickingDetailViewModel> listpickingdetailviewmodel = new List<PickingDetailViewModel>();

                if (searchpickingviewmodel.OrderNo == "" && searchpickingviewmodel.FromDate == null && searchpickingviewmodel.ToDate == null && searchpickingviewmodel.CustomerCode.Contains(""))
                {
                    //(k => (k.PickingStatus == 0 || k.PickingStatus == 1) &&
                    var data = _Context.PickingMaster.Where(k => k.IsActive == true && k.IsDelete == false && k.CustomerCode.Contains(searchpickingviewmodel.CustomerCode) && (k.CreatedDate.Date >= System.DateTime.Now.AddDays(-7).Date)
                             ).Select(x => new PickingMasterViewModel
                             {
                                 Pk_PickingMasterID = x.Pk_PickingMasterID,
                                 username = x.UserMaster.FirstName,
                                 OrderNumber = x.OrderNumber,
                                 CustomerName = x.CustomerName,
                                 CustomerCode = x.CustomerCode,
                                 DateOfDeliver = x.DateOfDeliver,
                                 RefNo = x.RefNo,
                                 Priority = x.Priority,
                                 PickingStatus = x.PickingStatus,
                                 dateofCreation = x.CreatedDate,
                                 CreatedDate = x.CreatedDate,
                                 ModifiedDate = x.ModifiedDate,
                                 Invoiceno = _Context.PickingDetail.FirstOrDefault(d => d.Fk_PickingID == x.Pk_PickingMasterID && d.IsActive == true && d.IsDelete == false).InvoiceNo
                             }).OrderByDescending(p => p.CreatedDate).ToList();
                    foreach (var item in data)
                    {
                        var a = _Context.PickingDetail.Where(j => j.PickingMaster.OrderNumber.Trim() == item.OrderNumber.Trim()).Any();
                        if (a)
                        {
                            listpickingmasterviewmodel.Add(item);
                        }
                    }
                }
                else
                {
                    var cCode = searchpickingviewmodel.CustomerCode.Split(',');
                    var data = _Context.PickingMaster.Where(k => k.OrderNumber == (searchpickingviewmodel.OrderNo != "" ? searchpickingviewmodel.OrderNo : k.OrderNumber) && (searchpickingviewmodel.CustomerCode != "" ? cCode.Contains(k.CustomerCode) : true) &&
                                                                                     k.DateOfDeliver >= (searchpickingviewmodel.FromDate != null ? searchpickingviewmodel.FromDate : System.DateTime.MinValue) &&
                                                                                      k.DateOfDeliver <= (searchpickingviewmodel.ToDate != null ? searchpickingviewmodel.ToDate : System.DateTime.MaxValue) &&
                                                                                      k.IsActive == true && k.IsDelete == false)
                                                                                      .OrderByDescending(k => k.CreatedDate)
                            .Select(x => new PickingMasterViewModel
                            {
                                Pk_PickingMasterID = x.Pk_PickingMasterID,
                                username = x.UserMaster.FirstName,
                                OrderNumber = x.OrderNumber,
                                CustomerName = x.CustomerName,
                                CustomerCode = x.CustomerCode,
                                DateOfDeliver = x.DateOfDeliver,
                                RefNo = x.RefNo,
                                Priority = x.Priority,
                                PickingStatus = x.PickingStatus,
                                dateofCreation = x.CreatedDate,
                                CreatedDate = x.CreatedDate,
                                ModifiedDate = x.ModifiedDate,
                                Invoiceno = _Context.PickingDetail.FirstOrDefault(d => d.Fk_PickingID == x.Pk_PickingMasterID && d.IsActive == true && d.IsDelete == false).InvoiceNo
                            }).OrderByDescending(p => p.CreatedDate).ToList();
                    foreach (var item in data)
                    {
                        var a = _Context.PickingDetail.Where(j => j.PickingMaster.OrderNumber.Trim() == item.OrderNumber.Trim()).Any();
                        if (a)
                        {
                            listpickingmasterviewmodel.Add(item);
                        }
                    }
                }

                if (listpickingmasterviewmodel.Count > 0)
                {
                    foreach (var item in listpickingmasterviewmodel)
                    {
                        item.PickingStatus = GetPickingStatusByPickingid(item.Pk_PickingMasterID);
                        Masterlistpickingmasterviewmodel.Add(item);

                    }
                    result.Message = "Record Get Successfully";
                    result.StatusCode = 1;
                    result.IsValid = true;
                    result.Result = "success";
                    result.Data = listpickingmasterviewmodel;
                }
                else
                {
                    result.Message = "Record does not exists";
                    result.StatusCode = 0;
                    result.IsValid = false;
                    result.Result = "error";
                    result.Data = null;
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.Data = null;
            }
            return result;
        }

        public ResponseviewModel GetPickingListbyCustomerCode(string customercode)
        {
            try
            {
                List<PickingMasterViewModel> Masterlistpickingmasterviewmodel = new List<PickingMasterViewModel>();
                List<PickingMasterViewModel> listpickingmasterviewmodel = new List<PickingMasterViewModel>();
                if (customercode != null)
                {
                    listpickingmasterviewmodel = _Context.PickingMaster.Where(d => d.CustomerCode.Contains(customercode) && d.IsActive == true && d.IsDelete == false && d.PickingStatus != 2).OrderByDescending(k => k.CreatedDate)
                    .Select(x => new PickingMasterViewModel
                    {
                        Pk_PickingMasterID = x.Pk_PickingMasterID,
                        OrderNumber = x.OrderNumber,
                        CustomerName = x.CustomerName,
                        CustomerCode = x.CustomerCode,
                        DateOfDeliver = x.DateOfDeliver,
                        RefNo = x.RefNo,
                        Priority = x.Priority,
                        PickingStatus = x.PickingStatus,
                        CreatedDate = x.CreatedDate,
                        ModifiedDate = x.ModifiedDate,
                        Invoiceno = _Context.PickingDetail.FirstOrDefault(d => d.Fk_PickingID == x.Pk_PickingMasterID && d.IsActive == true && d.IsDelete == false).InvoiceNo
                    }).OrderByDescending(p => p.CreatedDate).ToList();
                }
                else
                {
                    listpickingmasterviewmodel = _Context.PickingMaster.Where(d => d.ModifiedDate.Date == System.DateTime.Now.Date && d.IsActive == true && d.IsDelete == false && d.PickingStatus != 2).OrderByDescending(k => k.CreatedDate)
                     .Select(x => new PickingMasterViewModel
                     {
                         Pk_PickingMasterID = x.Pk_PickingMasterID,
                         OrderNumber = x.OrderNumber,
                         CustomerName = x.CustomerName,
                         CustomerCode = x.CustomerCode,
                         DateOfDeliver = x.DateOfDeliver,
                         RefNo = x.RefNo,
                         Priority = x.Priority,
                         PickingStatus = x.PickingStatus,
                         CreatedDate = x.CreatedDate,
                         ModifiedDate = x.ModifiedDate,
                         Invoiceno = _Context.PickingDetail.FirstOrDefault(d => d.Fk_PickingID == x.Pk_PickingMasterID && d.IsActive == true && d.IsDelete == false).InvoiceNo
                     }).OrderByDescending(p => p.CreatedDate).ToList();
                }
                if (listpickingmasterviewmodel.Count > 0)
                {
                    foreach (var item in listpickingmasterviewmodel)
                    {
                        item.PickingStatus = GetPickingStatusByPickingid(item.Pk_PickingMasterID);
                        Masterlistpickingmasterviewmodel.Add(item);
                    }
                    result.Message = "Record Get Successfully";
                    result.StatusCode = 1;
                    result.IsValid = true;
                    result.Result = "success";
                    result.Data = listpickingmasterviewmodel;
                }
                else
                {
                    result.Message = "Record does not exists";
                    result.StatusCode = 0;
                    result.IsValid = false;
                    result.Result = "error";
                    result.Data = null;
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.Data = null;
            }
            return result;
        }

        public ResponseviewModel GetPickingDetailsByPickingId(Guid id)
        {
            try
            {
                List<PickingDetailViewModel> listpickingdetailviewmodel = new List<PickingDetailViewModel>();
                listpickingdetailviewmodel = _Context.PickingDetail.Where(d => d.Fk_PickingID == id && d.IsActive == true && d.IsDelete == false).OrderByDescending(k => k.CreatedDate)
                .Select(x => new PickingDetailViewModel
                {
                    Pk_PickingDetailId = x.Pk_PickingDetailId,
                    InvoiceNo = x.InvoiceNo,
                    Fk_PickingID = x.Fk_PickingID,
                    PickingType = Convert.ToInt16(x.PickingType),
                    CustomerCode = x.SortingMaster.CustomerCode,
                    fk_SortingMaster_id = x.SortingMaster.Pk_SortingID,
                    RepackedCaseNo = x.RepackedCaseNo,
                    ItemCode = x.ItemCode,
                    Quantity = x.Quantity,
                    LocationId = x.LocationId,
                    Height = x.Height,
                    Length = x.Length,
                    Breadth = x.Breadth,
                    Weight = x.Weight
                }).ToList();

                if (listpickingdetailviewmodel.Count > 0)
                {
                    result.Message = "Record Get Successfully";
                    result.StatusCode = 1;
                    result.IsValid = true;
                    result.Result = "success";
                    result.Data = listpickingdetailviewmodel;
                }
                else
                {
                    result.Message = "Record does not exists";
                    result.StatusCode = 0;
                    result.IsValid = false;
                    result.Result = "error";
                    result.Data = null;
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.Data = null;
            }
            return result;
        }

        public int GetPickingStatusByPickingid(Guid pickingid)
        {
            int pickingstatus = 0;//0 : No picking ,1:Partialpicking,2:Picked
            try
            {

                int totalpickingcount = 0;
                int ispickedcount = 0;
                totalpickingcount = _Context.PickingDetail.Where(d => d.Fk_PickingID == pickingid && d.IsActive == true && d.IsDelete == false).Count();
                ispickedcount = _Context.PickingDetail.Where(d => d.Fk_PickingID == pickingid && d.IsPicked == true && d.IsActive == true && d.IsDelete == false).Count();
                if (ispickedcount == 0)
                {
                    pickingstatus = 0;//0 : No picking ,1:Partialpicking,2:Picked
                    PickingMaster pickingmaster = new PickingMaster();
                    pickingmaster = _Context.PickingMaster.Where(d => d.Pk_PickingMasterID == pickingid && d.IsActive == true && d.IsDelete == false).FirstOrDefault();
                    if (pickingmaster != null)
                    {
                        pickingmaster.PickingStatus = pickingstatus;
                        _Context.Update(pickingmaster);
                        _Context.SaveChanges();
                    }
                }
                else if (totalpickingcount == ispickedcount)
                {
                    pickingstatus = 2;////0 : No picking ,1:Partialpicking,2:Picked
                    PickingMaster pickingmaster = new PickingMaster();
                    pickingmaster = _Context.PickingMaster.Where(d => d.Pk_PickingMasterID == pickingid && d.IsActive == true && d.IsDelete == false).FirstOrDefault();
                    if (pickingmaster != null)
                    {
                        pickingmaster.PickingStatus = pickingstatus;
                        _Context.Update(pickingmaster);
                        _Context.SaveChanges();
                    }
                }
                else
                {
                    pickingstatus = 1;//0 : No picking ,1:Partialpicking,2:Picked
                    PickingMaster pickingmaster = new PickingMaster();
                    pickingmaster = _Context.PickingMaster.Where(d => d.Pk_PickingMasterID == pickingid && d.IsActive == true && d.IsDelete == false).FirstOrDefault();
                    if (pickingmaster != null)
                    {
                        pickingmaster.PickingStatus = pickingstatus;
                        _Context.Update(pickingmaster);
                        _Context.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
            }
            return pickingstatus;
        }

        public ResponseviewModel GetPendingPickingByPickingId(Guid id)
        {

            try
            {
                List<PickingDetailViewModel> masterlistpickingdetail = new List<PickingDetailViewModel>();
                List<PickingDetail> listpickingdetail = new List<PickingDetail>();
                listpickingdetail = _Context.PickingDetail.Where(d => d.Fk_PickingID == id && d.IsActive == true && d.IsDelete == false && d.IsPicked == false).OrderByDescending(k => k.CreatedDate).ToList();
                foreach (var item in listpickingdetail)
                {
                    PickingDetailViewModel pickingdetailviewmodel = new PickingDetailViewModel();
                    if (item.PickingType == 1)
                    {
                        PickingDetailViewModel existrepackedcaseno = new PickingDetailViewModel();
                        existrepackedcaseno = masterlistpickingdetail.Where(d => d.RepackedCaseNo.ToLower().Trim() == item.RepackedCaseNo.ToLower().Trim()).FirstOrDefault();
                        if (existrepackedcaseno == null)
                        {
                            pickingdetailviewmodel.RepackedCaseNo = item.RepackedCaseNo;
                            pickingdetailviewmodel.LocationId = item.LocationId;
                            pickingdetailviewmodel.ItemCode = "All Items";
                            pickingdetailviewmodel.Quantity = item.Quantity;
                            masterlistpickingdetail.Add(pickingdetailviewmodel);
                        }
                        else
                        {
                            int toalquantity = existrepackedcaseno.Quantity;
                            pickingdetailviewmodel.RepackedCaseNo = item.RepackedCaseNo;
                            pickingdetailviewmodel.LocationId = item.LocationId;
                            pickingdetailviewmodel.ItemCode = "All Items";
                            pickingdetailviewmodel.Quantity = toalquantity + item.Quantity;
                            masterlistpickingdetail.Remove(existrepackedcaseno);
                            masterlistpickingdetail.Add(pickingdetailviewmodel);
                        }
                    }
                    else
                    {
                        pickingdetailviewmodel.RepackedCaseNo = item.RepackedCaseNo;
                        pickingdetailviewmodel.LocationId = item.LocationId;
                        pickingdetailviewmodel.ItemCode = item.ItemCode;
                        pickingdetailviewmodel.Quantity = item.Quantity;
                        masterlistpickingdetail.Add(pickingdetailviewmodel);
                    }
                }
                if (masterlistpickingdetail.Count > 0)
                {
                    result.Message = "Record Get Successfully";
                    result.StatusCode = 1;
                    result.IsValid = true;
                    result.Result = "success";
                    result.Data = masterlistpickingdetail;
                }
                else
                {
                    result.Message = "Record does not exists";
                    result.StatusCode = 0;
                    result.IsValid = false;
                    result.Result = "error";
                    result.Data = null;
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.Data = null;
            }
            return result;
        }

        public ResponseviewModel GetPickingDetailByOrderNo(string OrderNo)
        {
            try
            {
                List<PickingDetailViewModel> listpickingdetailviewmodel = new List<PickingDetailViewModel>();
                listpickingdetailviewmodel = _Context.PickingDetail.Where(d => d.PickingMaster.OrderNumber.ToLower().Trim() == OrderNo.ToLower().Trim() && d.PickingType != 1 && d.IsPicked == false && d.IsActive == true && d.IsDelete == false).OrderByDescending(d => d.CreatedDate)
                .Select(x => new PickingDetailViewModel
                {
                    Pk_PickingDetailId = x.Pk_PickingDetailId,
                    Fk_PickingID = x.Fk_PickingID,
                    PickingType = Convert.ToInt16(x.PickingType),
                    InvoiceNo = x.InvoiceNo,
                    RepackedCaseNo = x.RepackedCaseNo,
                    ItemCode = x.ItemCode,
                    Quantity = x.Quantity,
                    LocationId = x.LocationId,
                    Height = x.Height,
                    Length = x.Length,
                    Breadth = x.Breadth,
                    Weight = x.Weight
                }).ToList();
                var repackboxlisting = _Context.PickingDetail.Where(d => d.PickingMaster.OrderNumber.ToLower().Trim() == OrderNo.ToLower().Trim() && d.PickingType == 1 && d.IsPicked == false && d.IsActive == true && d.IsDelete == false).OrderByDescending(d => d.CreatedDate).GroupBy(g => g.RepackedCaseNo)
                .Select(x => new PickingDetailViewModel
                {

                    PickingType = Convert.ToInt16(x.FirstOrDefault().PickingType),
                    InvoiceNo = x.FirstOrDefault().InvoiceNo,
                    RepackedCaseNo = x.FirstOrDefault().RepackedCaseNo,
                    ItemCode = "All Items",
                    Quantity = x.Sum(q => q.Quantity),
                    LocationId = x.FirstOrDefault().LocationId,
                    Height = x.FirstOrDefault().Height,
                    Length = x.FirstOrDefault().Length,
                    Breadth = x.FirstOrDefault().Breadth,
                    Weight = x.FirstOrDefault().Weight
                }).ToList();
                listpickingdetailviewmodel.AddRange(repackboxlisting);
                if (listpickingdetailviewmodel.Count > 0)
                {
                    result.Message = "Record Get Successfully";
                    result.StatusCode = 1;
                    result.IsValid = true;
                    result.Result = "success";
                    result.Data = listpickingdetailviewmodel;
                }
                else
                {
                    result.Message = "Record does not exists";
                    result.StatusCode = 0;
                    result.IsValid = false;
                    result.Result = "error";
                    result.Data = null;
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.Data = null;
            }
            return result;
        }

        public ResponseviewModel UpdatePickingStatus(UpdatePickingViewModel updatepickingviewmodel)
        {
            try
            {
                if (updatepickingviewmodel.PickingType == 1)
                {
                    if (updatepickingviewmodel.OrderNo != null && updatepickingviewmodel.RepackedCaseNo != null)
                    {
                        List<PickingDetail> listpickingdetail = new List<PickingDetail>();
                        listpickingdetail = _Context.PickingDetail.Where(d => d.PickingType == 1 && d.PickingMaster.OrderNumber.ToLower().Trim() == updatepickingviewmodel.OrderNo.ToLower().Trim() && d.RepackedCaseNo.ToLower().Trim() == updatepickingviewmodel.RepackedCaseNo.ToLower().Trim()).ToList();
                        if (listpickingdetail.Count > 0)
                        {
                            foreach (var item in listpickingdetail)
                            {
                                item.IsPicked = true;
                                item.ModifiedBy = updatepickingviewmodel.ModifiedBy;
                                _Context.Update(item);
                                _Context.SaveChanges();
                            }
                            result.Message = "All item picked";
                            result.StatusCode = 1;
                            result.IsValid = true;
                            result.Result = "success";
                            result.Data = listpickingdetail;
                        }
                        else
                        {
                            result.Message = "Record does not exist";
                            result.StatusCode = 0;
                            result.IsValid = false;
                            result.Result = "error";
                            result.Data = null;
                        }
                    }
                    else
                    {
                        result.Message = "Please enter valid order no or case no";
                        result.StatusCode = 0;
                        result.IsValid = false;
                        result.Result = "error";
                        result.Data = null;
                    }
                }
                else if (updatepickingviewmodel.PickingType == 2)
                {
                    PickingDetail pickingdetail = new PickingDetail();
                    pickingdetail = _Context.PickingDetail.FirstOrDefault(d => d.PickingType == 2 && d.Pk_PickingDetailId == updatepickingviewmodel.PK_PickingDetails_ID);
                    if (pickingdetail != null)
                    {
                        pickingdetail.IsPicked = true;
                        _Context.Update(pickingdetail);
                        _Context.SaveChanges();
                        result.Message = "All item picked";
                        result.StatusCode = 1;
                        result.IsValid = true;
                        result.Result = "success";
                        result.Data = pickingdetail;
                    }
                    else
                    {
                        result.Message = "Record does not exist";
                        result.StatusCode = 0;
                        result.IsValid = false;
                        result.Result = "error";
                        result.Data = null;
                    }
                }
                else if (updatepickingviewmodel.PickingType == 3)
                {
                    PickingDetail pickingdetail = new PickingDetail();
                    pickingdetail = _Context.PickingDetail.FirstOrDefault(d => d.PickingType == 3 && d.Pk_PickingDetailId == updatepickingviewmodel.PK_PickingDetails_ID);
                    if (pickingdetail != null)
                    {
                        pickingdetail.IsPicked = true;
                        _Context.Update(pickingdetail);
                        _Context.SaveChanges();
                        result.Message = "All item picked";
                        result.StatusCode = 1;
                        result.IsValid = true;
                        result.Result = "success";
                        result.Data = pickingdetail;
                    }
                    else
                    {
                        result.Message = "Record does not exist";
                        result.StatusCode = 0;
                        result.IsValid = false;
                        result.Result = "error";
                        result.Data = null;
                    }
                }
                else
                {
                    result.Message = "Please select valid picking type";
                    result.StatusCode = 0;
                    result.IsValid = false;
                    result.Result = "error";
                    result.Data = null;
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.Data = null;
            }
            return result;
        }

        public ResponseviewModel UpdatePickingMasterList(PickingDetailViewModel PickingViewModel)
        {
            try
            {
                PickingDetail pickingdetail = new PickingDetail();
                pickingdetail.Pk_PickingDetailId = Guid.NewGuid();
                pickingdetail.Fk_PickingID = PickingViewModel.Fk_PickingID;
                pickingdetail.PickingType = PickingViewModel.PickingType; //1 : Repacked Case No, 2 : loose Items
                pickingdetail.InvoiceNo = PickingViewModel.InvoiceNo;
                pickingdetail.RepackedCaseNo = PickingViewModel.RepackedCaseNo;
                //pickingdetail.Fk_PickingID = item.Fk_PickingID;
                pickingdetail.Fk_SortingMasterID = PickingViewModel.fk_SortingMaster_id;
                pickingdetail.ItemCode = PickingViewModel.ItemCode;
                pickingdetail.Quantity = PickingViewModel.Quantity;
                pickingdetail.LocationId = PickingViewModel.LocationId;
                pickingdetail.Height = PickingViewModel.Height;
                pickingdetail.Length = PickingViewModel.Length;
                pickingdetail.Breadth = PickingViewModel.Breadth;
                pickingdetail.Weight = PickingViewModel.Weight;
                pickingdetail.IsPicked = false;
                pickingdetail.IsDelete = false;
                pickingdetail.IsActive = true;
                pickingdetail.CreatedBy = PickingViewModel.CreatedBy;
                pickingdetail.CreatedDate = System.DateTime.Now;
                pickingdetail.ModifiedBy = PickingViewModel.CreatedBy;
                pickingdetail.ModifiedDate = System.DateTime.Now;
                _Context.PickingDetail.Add(pickingdetail);
                _Context.SaveChanges();
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.Data = null;
            }
            return result;
        }



        public string GenerateOrderNo()
        {
            string oid = _Context.PickingMaster.Max(k => k.OrderNumber);
            if (oid == null)
            {
                oid = "PR0000";
            }
            oid = oid.Substring(2);
            int a = Convert.ToInt32(oid);
            return "PR" + (a + 1).ToString("0000");
            //string new_tid = System.Guid.NewGuid().ToString();
            //new_tid = new_tid.Replace("-", string.Empty).ToLower();
            //new_tid = new_tid.Substring(0, 15).Replace("a", "1");
            //new_tid = new_tid.Substring(0, 15).Replace("b", "2");
            //new_tid = new_tid.Substring(0, 15).Replace("c", "3");
            //new_tid = new_tid.Substring(0, 15).Replace("d", "4");
            //new_tid = new_tid.Substring(0, 15).Replace("e", "5");
            //new_tid = new_tid.Substring(0, 15).Replace("f", "6");
            //new_tid = new_tid.Substring(0, 15).Replace("g", "14");
            //new_tid = new_tid.Substring(0, 15).Replace("h", "8");
            //new_tid = new_tid.Substring(0, 15).Replace("i", "9");
            //new_tid = new_tid.Substring(0, 15).Replace("j", "0");
            //new_tid = new_tid.Substring(0, 15).Replace("k", "1");
            //new_tid = new_tid.Substring(0, 15).Replace("l", "2");
            //new_tid = new_tid.Substring(0, 15).Replace("m", "3");
            //new_tid = new_tid.Substring(0, 15).Replace("n", "4");
            //new_tid = new_tid.Substring(0, 15).Replace("o", "5");
            //new_tid = new_tid.Substring(0, 15).Replace("p", "0");
            //new_tid = new_tid.Substring(0, 15).Replace("q", "9");
            //new_tid = new_tid.Substring(0, 15).Replace("r", "8");
            //new_tid = new_tid.Substring(0, 15).Replace("s", "14");
            //new_tid = new_tid.Substring(0, 15).Replace("t", "6");
            //new_tid = new_tid.Substring(0, 15).Replace("u", "5");
            //new_tid = new_tid.Substring(0, 15).Replace("v", "4");
            //new_tid = new_tid.Substring(0, 15).Replace("w", "3");
            //new_tid = new_tid.Substring(0, 15).Replace("x", "2");
            //new_tid = new_tid.Substring(0, 15).Replace("y", "1");
            //new_tid = new_tid.Substring(0, 15).Replace("z", "0");
            ////  return (new_tid + DateTime.Now.Millisecond.ToString()).Substring(2);
            //return (new_tid + DateTime.Now.Millisecond.ToString()).Substring(10);
        }


        public ResponseviewModel DeletePickingbyID(PickingMasterViewModel pickingmaster)
        {
            try
            {
                PickingMaster pickingmasterdata = _Context.PickingMaster.FirstOrDefault(d => d.Pk_PickingMasterID == pickingmaster.Pk_PickingMasterID && d.IsActive == true && d.IsDelete == false);
                if (pickingmasterdata != null)
                {
                    pickingmasterdata.IsActive = false;
                    pickingmasterdata.IsDelete = true;
                    pickingmasterdata.ModifiedBy = pickingmasterdata.ModifiedBy;
                    pickingmasterdata.ModifiedDate = System.DateTime.Now;
                    _Context.Update(pickingmasterdata);
                    _Context.SaveChanges();


                    result.Message = "Record Delete Successfully";
                    result.StatusCode = 1;
                    result.IsValid = true;
                    result.Result = "success";
                    result.Data = pickingmaster;
                }
                {

                }
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.pk_ID = null;
                result.Data = null;
            }
            return result;
        }
    }
}
