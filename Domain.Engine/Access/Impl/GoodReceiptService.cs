﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Domain.Engine.Access.Interfaces;
using Domain.Objects.Models;
using Infrastructure.Model.Main;
using Infrastructure.Model.Main.Entities;


namespace Domain.Engine.Access.Impl
{
    public class GoodReceiptService : IGoodReceiptService
    {
        protected readonly MainDbContext _Context;
        protected MainDbContext Context
        {
            get
            {
                return (MainDbContext)_Context;
            }
        }

        public GoodReceiptService(IMainDbContext context,ILocationMasterService locationMasterService)
        {
            _Context = (MainDbContext)context;
        }


        public ResponseviewModel InsertUpdateGoodReceipt(GoodsReceiptMasterViewModel goodsreceiptmasterviewmodel)
        {
            ResponseviewModel result = new ResponseviewModel();
            try
            {
                if (goodsreceiptmasterviewmodel.Pk_GoodsReceiptID == Guid.Empty)
                {
                    GoodsReceiptMaster goodsreceiptmaster = new GoodsReceiptMaster();
                    goodsreceiptmaster.Pk_GoodsReceiptID = Guid.NewGuid();
                    goodsreceiptmaster.FK_ASNMasterID = goodsreceiptmasterviewmodel.FK_ASNMasterID;
                    goodsreceiptmaster.ContainerNo = goodsreceiptmasterviewmodel.ContainerNo;
                    goodsreceiptmaster.PalletNo = goodsreceiptmasterviewmodel.PalletNo;
                    goodsreceiptmaster.OrderNo = goodsreceiptmasterviewmodel.OrderNo;
                    goodsreceiptmaster.SuppliedPartNo = goodsreceiptmasterviewmodel.SuppliedPartNo;
                    goodsreceiptmaster.Quantity = goodsreceiptmasterviewmodel.Quantity;
                    goodsreceiptmaster.UnitPrice = goodsreceiptmasterviewmodel.UnitPrice;
                    goodsreceiptmaster.Length = goodsreceiptmasterviewmodel.Length;
                    goodsreceiptmaster.Breadth = goodsreceiptmasterviewmodel.Breadth;
                    goodsreceiptmaster.Height = goodsreceiptmasterviewmodel.Height;
                    goodsreceiptmaster.Weight = goodsreceiptmasterviewmodel.Weight;
                    goodsreceiptmaster.Customer = goodsreceiptmasterviewmodel.Customer;
                    goodsreceiptmaster.PurchaseOrderNo = goodsreceiptmasterviewmodel.PurchaseOrderNo;
                    goodsreceiptmaster.Remark1 = goodsreceiptmasterviewmodel.Remark1;
                    goodsreceiptmaster.Remark2 = goodsreceiptmasterviewmodel.Remark2;
                    goodsreceiptmaster.Remark3 = goodsreceiptmasterviewmodel.Remark3;
                    goodsreceiptmaster.Description = goodsreceiptmasterviewmodel.Description;
                    goodsreceiptmaster.IsDelete = false;
                    goodsreceiptmaster.IsActive = true;
                    goodsreceiptmaster.IsSorted = false;
                    goodsreceiptmaster.CreatedBy = goodsreceiptmasterviewmodel.CreatedBy;
                    goodsreceiptmaster.CreatedDate = System.DateTime.Now;
                    goodsreceiptmaster.ModifiedBy = goodsreceiptmasterviewmodel.CreatedBy;
                    goodsreceiptmaster.ModifiedDate = System.DateTime.Now;
                    _Context.AddAsync(goodsreceiptmaster);
                    _Context.SaveChanges();
                   
                    result.Message = "Data Inserted Successfully";
                    result.StatusCode = 1;
                    result.IsValid = true;
                    result.Result = "success";
                    result.pk_ID = goodsreceiptmaster.Pk_GoodsReceiptID;
                    result.Data = goodsreceiptmaster;
                }
                else
                {
                    GoodsReceiptMaster goodsreceiptmaster = _Context.GoodsReceiptMaster.FirstOrDefault(d => d.Pk_GoodsReceiptID == goodsreceiptmasterviewmodel.Pk_GoodsReceiptID && d.IsActive == true && d.IsDelete == false);
                    if (goodsreceiptmaster != null)
                    {
                        goodsreceiptmaster.FK_ASNMasterID = goodsreceiptmasterviewmodel.FK_ASNMasterID;
                        goodsreceiptmaster.ContainerNo = goodsreceiptmasterviewmodel.ContainerNo;
                        goodsreceiptmaster.PalletNo = goodsreceiptmasterviewmodel.PalletNo;
                        goodsreceiptmaster.OrderNo = goodsreceiptmasterviewmodel.OrderNo;
                        goodsreceiptmaster.SuppliedPartNo = goodsreceiptmasterviewmodel.SuppliedPartNo;
                        goodsreceiptmaster.Quantity = goodsreceiptmasterviewmodel.Quantity;
                        goodsreceiptmaster.UnitPrice = goodsreceiptmasterviewmodel.UnitPrice;
                        goodsreceiptmaster.Length = goodsreceiptmasterviewmodel.Length;
                        goodsreceiptmaster.Breadth = goodsreceiptmasterviewmodel.Breadth;
                        goodsreceiptmaster.Height = goodsreceiptmasterviewmodel.Height;
                        goodsreceiptmaster.Weight = goodsreceiptmasterviewmodel.Weight;
                        goodsreceiptmaster.Customer = goodsreceiptmasterviewmodel.Customer;
                        goodsreceiptmaster.PurchaseOrderNo = goodsreceiptmasterviewmodel.PurchaseOrderNo;
                        goodsreceiptmaster.Remark1 = goodsreceiptmasterviewmodel.Remark1;
                        goodsreceiptmaster.Remark2 = goodsreceiptmasterviewmodel.Remark2;
                        goodsreceiptmaster.Remark3 = goodsreceiptmasterviewmodel.Remark3;
                        goodsreceiptmaster.Description = goodsreceiptmasterviewmodel.Description;
                        goodsreceiptmaster.IsDelete = false;
                        goodsreceiptmaster.IsActive = true;
                        goodsreceiptmaster.ModifiedDate = System.DateTime.Now;
                        goodsreceiptmaster.ModifiedBy = goodsreceiptmasterviewmodel.ModifiedBy;
                        _Context.Update(goodsreceiptmaster);
                        _Context.SaveChanges();

                        
                        result.Message = "Data Updated Successfully";
                        result.StatusCode = 1;
                        result.IsValid = true;
                        result.Result = "success";
                        result.pk_ID = goodsreceiptmaster.Pk_GoodsReceiptID;
                        result.Data = goodsreceiptmaster;
                    }
                    else
                    {
                        result.Message = "Record does not exists";
                        result.StatusCode = 0;
                        result.IsValid = false;
                        result.Result = "error";
                        result.pk_ID = goodsreceiptmasterviewmodel.Pk_GoodsReceiptID;
                        result.Data = null;
                    }
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.pk_ID = null;
                result.Data = null;
            }
            return result;
        }

    }
}
