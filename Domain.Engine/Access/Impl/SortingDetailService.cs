using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Domain.Engine.Access.Interfaces;
using Domain.Objects.Models;
using Infrastructure.Model.Main;
using Infrastructure.Model.Main.Entities;
using Microsoft.EntityFrameworkCore;

namespace Domain.Engine.Access.Impl
{
    public class SortingDetailService : ISortingDetailService
    {
        protected readonly MainDbContext _Context;
        protected MainDbContext Context
        {
            get
            {
                return (MainDbContext)_Context;
            }
        }

        public SortingDetailService(IMainDbContext context)
        {
            _Context = (MainDbContext)context;
        }

        public ResponseviewModel InsertUpdateSortingDetail(SortingDetailModels sortingdetailmodel)
        {
            ResponseviewModel result = new ResponseviewModel();
            try
            {
                if (sortingdetailmodel.Pk_SortingDetailID == Guid.Empty)
                {
                    SortingDetail sortingdetail = new SortingDetail();
                    sortingdetail.Pk_SortingDetailID = Guid.NewGuid();
                    sortingdetail.FK_SortingMasterID = sortingdetailmodel.FK_SortingMasterID;
                    sortingdetail.RepakedCaseNo = sortingdetailmodel.RepakedCaseNo;
                    sortingdetail.Length = sortingdetailmodel.Length;
                    sortingdetail.Breadth = sortingdetailmodel.Breadth;
                    sortingdetail.Height = sortingdetailmodel.Height;
                    sortingdetail.Weight = sortingdetailmodel.Weight;
                    sortingdetail.Volume = sortingdetailmodel.Volume;
                    sortingdetail.CreatedBy = sortingdetailmodel.CreatedBy;
                    sortingdetail.IsDelete = false;
                    sortingdetail.IsActive = true;
                    sortingdetail.CreatedBy = sortingdetailmodel.CreatedBy;
                    sortingdetail.CreatedDate = System.DateTime.Now;
                    sortingdetail.ModifiedBy = sortingdetailmodel.ModifiedBy;
                    sortingdetail.ModifiedDate = System.DateTime.Now;
                    _Context.AddAsync(sortingdetail);
                    _Context.SaveChanges();
                    result.Message = "Data Inserted Successfully";
                    result.StatusCode = 1;
                    result.IsValid = true;
                    result.Result = "success";
                    result.pk_ID = sortingdetail.Pk_SortingDetailID;
                    result.Data = sortingdetail;
                }
                else
                {
                    SortingDetail sortingdetail = _Context.SortingDetail.FirstOrDefault(d => d.Pk_SortingDetailID == sortingdetailmodel.Pk_SortingDetailID && d.IsActive == true && d.IsDelete == false);
                    if (sortingdetail != null)
                    {
                        sortingdetail.FK_SortingMasterID = sortingdetailmodel.FK_SortingMasterID;
                        sortingdetail.RepakedCaseNo = sortingdetailmodel.RepakedCaseNo;
                        sortingdetail.Length = sortingdetailmodel.Length;
                        sortingdetail.Breadth = sortingdetailmodel.Breadth;
                        sortingdetail.Height = sortingdetailmodel.Height;
                        sortingdetail.Weight = sortingdetailmodel.Weight;
                        sortingdetail.Volume = sortingdetailmodel.Volume;
                        sortingdetail.IsDelete = false;
                        sortingdetail.IsActive = true;
                        sortingdetail.ModifiedBy = sortingdetailmodel.CreatedBy;
                        sortingdetail.ModifiedDate = System.DateTime.Now;
                        _Context.Update(sortingdetail);
                        _Context.SaveChanges();
                        result.Message = "Data Updated Successfully";
                        result.StatusCode = 1;
                        result.IsValid = true;
                        result.Result = "success";
                        result.pk_ID = sortingdetail.Pk_SortingDetailID;
                        result.Data = sortingdetail;
                    }
                    else
                    {
                        result.Message = "Record does not exists";
                        result.StatusCode = 0;
                        result.IsValid = false;
                        result.Result = "error";
                        result.pk_ID = sortingdetailmodel.Pk_SortingDetailID;
                        result.Data = null;
                    }
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.pk_ID = null;
                result.Data = null;
            }
            return result;
        }
        public ResponseviewModel UpdateSortingDetailList(List<SortingDetailModels> Listsortingdetailmodel)
        {
            ResponseviewModel result = new ResponseviewModel();
            try
            {
                if (Listsortingdetailmodel.Count > 0)
                {
                    foreach (var item in Listsortingdetailmodel)
                    {
                        var sortingdetail = _Context.SortingDetail.Where(d => d.RepakedCaseNo.ToLower().Trim() == item.RepakedCaseNo.ToLower().Trim() && d.IsActive == true && d.IsDelete == false).ToList();


                        if (sortingdetail != null)
                        {

                            foreach (var item2 in sortingdetail)
                            {
                                item2.Length = item.Length;
                                item2.Breadth = item.Breadth;
                                item2.Height = item.Height;
                                item2.Weight = item.Weight;
                                item2.Volume = item.Volume;
                                item2.IsDelete = false;
                                item2.IsActive = true;
                                item2.ModifiedBy = item.ModifiedBy;
                                item2.ModifiedDate = System.DateTime.Now;
                                _Context.Update(item2);
                                _Context.SaveChanges();

                            }

                            result.Message = "Data Updated Successfully";
                            result.StatusCode = 1;
                            result.IsValid = true;
                            result.Result = "success";
                            result.pk_ID = item.Pk_SortingDetailID;
                            result.Data = sortingdetail;
                        }
                        else
                        {
                            result.Message = "Record does not exists";
                            result.StatusCode = 0;
                            result.IsValid = false;
                            result.Result = "error";
                            result.Data = null;
                        }
                    }
                }
                else
                {
                    result.Message = "Record does not exists";
                    result.StatusCode = 0;
                    result.IsValid = false;
                    result.Result = "error";
                    result.Data = null;
                }

            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.pk_ID = null;
                result.Data = null;
            }
            return result;
        }
        public ResponseviewModel GetAllSortingDetailList()
        {
            ResponseviewModel result = new ResponseviewModel();
            try
            {
                List<SortingDetailModels> listsortingdetailmodel = new List<SortingDetailModels>();
                listsortingdetailmodel = _Context.SortingDetail.Where(d => d.IsActive == true && d.IsDelete == false).Select(x => new SortingDetailModels
                {
                    Pk_SortingDetailID = x.Pk_SortingDetailID,
                    FK_SortingMasterID = x.FK_SortingMasterID,
                    RepakedCaseNo = x.RepakedCaseNo,
                    Height = x.Height,
                    Weight = x.Weight,
                    Length = x.Length,
                    Breadth = x.Breadth,
                    IsActive = x.IsActive,
                    IsDelete = x.IsDelete,
                    CreatedDate = x.CreatedDate
                }).OrderByDescending(x => x.CreatedDate).ToList();
                if (listsortingdetailmodel.Count > 0)
                {

                    result.Message = "Record Get Successfully";
                    result.StatusCode = 1;
                    result.IsValid = true;
                    result.Result = "success";
                    result.Data = listsortingdetailmodel;
                }
                else
                {
                    result.Message = "Record does not exists";
                    result.StatusCode = 0;
                    result.IsValid = false;
                    result.Result = "error";
                    result.Data = null;
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.pk_ID = null;
                result.Data = null;
            }
            return result;
        }
        public ResponseviewModel GetSortingDetailByRepackedCaseNo(string RepackedCaseNo, string invoicenumber)
        {
            ResponseviewModel result = new ResponseviewModel();
            try
            {
                List<SortingDetailModels> sortingdetailmodel = new List<SortingDetailModels>();
                var query = _Context.SortingDetail.Include(k => k.UserMaster).Where(d => d.IsActive == true && d.IsDelete == false);
                if (!String.IsNullOrEmpty(RepackedCaseNo))
                {
                    query = query.Where(k => k.RepakedCaseNo.ToLower().Trim() == RepackedCaseNo.ToLower().Trim());
                }
                if (!String.IsNullOrEmpty(invoicenumber))
                {
                    var qpk = _Context.SortingMaster.Where(k => k.InvoiceNo.ToLower().Trim() == invoicenumber.ToLower().Trim()).Select(k => k.Pk_SortingID);
                    query = query.Where(k => qpk.Contains(k.FK_SortingMasterID));
                }
                sortingdetailmodel = query.Select(x => new SortingDetailModels
                {
                    Pk_SortingDetailID = x.Pk_SortingDetailID,
                    FK_SortingMasterID = x.FK_SortingMasterID,
                    RepakedCaseNo = x.RepakedCaseNo,
                    Height = x.Height,
                    Weight = x.Weight,
                    Length = x.Length,
                    Breadth = x.Breadth,
                    IsActive = x.IsActive,
                    IsDelete = x.IsDelete,
                    CreatedDate = x.CreatedDate,
                    UserName = x.UserMaster.FirstName
                }).OrderByDescending(d => d.CreatedDate).ToList();
                if (sortingdetailmodel.Count > 0)
                {

                    result.Message = "Record Get Successfully";
                    result.StatusCode = 1;
                    result.IsValid = true;
                    result.Result = "success";
                    result.Data = sortingdetailmodel;
                    result.pk_ID = sortingdetailmodel[0].Pk_SortingDetailID;
                }
                else
                {
                    result.Message = "Record does not exists";
                    result.StatusCode = 0;
                    result.IsValid = false;
                    result.Result = "error";
                    result.Data = null;
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.pk_ID = null;
                result.Data = null;
            }
            return result;
        }

        public ResponseviewModel DeleteSortingDetailBySortingMasterID(Guid Fk_SortingMasterID)
        {
            ResponseviewModel result = new ResponseviewModel();
            try
            {
                SortingDetail sortingdetail = _Context.SortingDetail.FirstOrDefault(d => d.FK_SortingMasterID == Fk_SortingMasterID && d.IsActive == true && d.IsDelete == false);
                if (sortingdetail != null)
                {
                    sortingdetail.IsDelete = true;
                    sortingdetail.IsActive = false;
                    sortingdetail.ModifiedDate = System.DateTime.Now;
                    _Context.Update(sortingdetail);
                    _Context.SaveChanges();
                    result.Message = "Data Deleted Successfully";
                    result.StatusCode = 1;
                    result.IsValid = true;
                    result.Result = "success";
                    result.pk_ID = sortingdetail.Pk_SortingDetailID;
                    result.Data = sortingdetail;
                }
                else
                {
                    result.Message = "Record does not exists";
                    result.StatusCode = 0;
                    result.IsValid = false;
                    result.Result = "error";
                    result.pk_ID = Fk_SortingMasterID;
                    result.Data = null;
                }

            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.pk_ID = null;
                result.Data = null;
            }
            return result;
        }

        public ResponseviewModel GetSortingDetailByInvoiceNo(string InvoiceNo, bool removed)
        {
            ResponseviewModel result = new ResponseviewModel();
            try
            {
                var TotalBox = _Context.ASNItemDetails.Where(a => a.ASNMaster.InvoiceNo.Trim() == InvoiceNo.Trim() && a.IsDelete == false).GroupBy(a => a.PalletNo).Count();
                var OpenBox = _Context.ASNItemDetails.Where(a => a.ASNMaster.InvoiceNo.Trim() == InvoiceNo.Trim() && (a.ReceivedQty != null || a.ReceivedQty > 0) && a.IsDelete == false).GroupBy(a => a.PalletNo).Count();

                if (removed == false)
                {
                    List<SortingMasterModel> sortingdetailmodel = new List<SortingMasterModel>();
                    sortingdetailmodel = _Context.StockTransaction.Where(d => d.SortingMaster.InvoiceNo.ToLower().Trim() == InvoiceNo.ToLower().Trim() && d.SortingMaster.IsBin == false && d.SortingMaster.IsDelete == false && d.SortingMaster.IsActive == true).Select(x => new SortingMasterModel
                    {
                        CaseNo = x.SortingMaster.CaseNo,
                        Pk_SortingID = x.SortingMaster.Pk_SortingID,
                        CreatedBy = x.SortingMaster.CreatedBy,
                        CustomerCode = x.SortingMaster.CustomerCode,
                        Description = x.SortingMaster.Description,
                        InvoiceNo = x.SortingMaster.InvoiceNo,
                        ItemCode = x.SortingMaster.ItemCode,
                        NoOfPieces = x.SortingMaster.NoOfPieces,
                        LocationID = x.SortingMaster.LocationID,
                        Quantity = x.QuantityRemaining??0,
                        RepakedCaseNo = x.SortingMaster.RepakedCaseNo,
                        IsActive = x.SortingMaster.IsActive,
                        IsDelete = x.SortingMaster.IsDelete,
                        CreatedDate = x.SortingMaster.CreatedDate,
                        UserName = x.SortingMaster.UserMaster.FirstName,
                        TotalQty= x.SortingMaster.Quantity,
                        Openbox = OpenBox,
                        TotalBox = TotalBox,
                        IsPutaway = x.SortingMaster.IsPutaway
                    }).OrderByDescending(d => d.CreatedDate).ToList();

                    foreach (var item in sortingdetailmodel)
                    {
                        var ttlqty = _Context.BinItemHistory.Where(a => a.FK_SortingMasterID == item.Pk_SortingID).Any();
                        if (ttlqty == true)
                        {
                            int qty = _Context.BinItemHistory.Where(a => a.FK_SortingMasterID == item.Pk_SortingID).Select(a => a.Quantity).Sum();
                            item.TotalQty = item.Quantity + qty;
                        }
                       
                    }
                    if (sortingdetailmodel.Count > 0)
                    {

                        result.Message = "Record Get Successfully";
                        result.StatusCode = 1;
                        result.IsValid = true;
                        result.Result = "success";
                        result.Data = sortingdetailmodel;
                        result.pk_ID = sortingdetailmodel[0].Pk_SortingID;
                    }
                    else
                    {
                        result.Message = "Record does not exists";
                        result.StatusCode = 0;
                        result.IsValid = false;
                        result.Result = "error";
                        result.Data = null;
                    }
                }
                else if (removed == true)
                {
                    List<SortingMasterModel> sortingdetailmodel = new List<SortingMasterModel>();
                    sortingdetailmodel = _Context.StockTransaction.Where(d => d.SortingMaster.InvoiceNo.ToLower().Trim() == InvoiceNo.ToLower().Trim() && d.SortingMaster.IsBin == false).Select(x => new SortingMasterModel
                    //sortingdetailmodel = _Context.SortingMaster.Where(d => d.InvoiceNo.ToLower().Trim() == InvoiceNo.ToLower().Trim() && d.IsBin == false && d.IsDelete == false && d.IsActive == true).Select(x => new SortingMasterModel

                    {
                        CaseNo = x.SortingMaster.CaseNo,
                        Pk_SortingID = x.SortingMaster.Pk_SortingID,
                        CreatedBy = x.SortingMaster.CreatedBy,
                        CustomerCode = x.SortingMaster.CustomerCode,
                        Description = x.SortingMaster.Description,
                        InvoiceNo = x.SortingMaster.InvoiceNo,
                        ItemCode = x.SortingMaster.ItemCode,
                        NoOfPieces = x.SortingMaster.NoOfPieces,
                        LocationID = x.SortingMaster.LocationID,
                        Quantity = x.QuantityRemaining??0,
                        RepakedCaseNo = x.SortingMaster.RepakedCaseNo,
                        IsActive = x.SortingMaster.IsActive,
                        IsDelete = x.SortingMaster.IsDelete,
                        CreatedDate = x.SortingMaster.CreatedDate,
                        UserName = x.SortingMaster.UserMaster.FirstName,
                    }).OrderByDescending(d => d.CreatedDate).ToList();
                    foreach (var item in sortingdetailmodel)
                    {
                        var ttlqty = _Context.BinItemHistory.Where(a => a.FK_SortingMasterID == item.Pk_SortingID).Any();
                        if (ttlqty == true)
                        {
                            int qty = _Context.BinItemHistory.Where(a => a.FK_SortingMasterID == item.Pk_SortingID).Select(a => a.Quantity).Sum();
                            item.TotalQty = item.Quantity + qty;
                        }
                        else
                        {
                            item.TotalQty = item.Quantity;
                        }
                    }
                    if (sortingdetailmodel.Count > 0)
                    {

                        result.Message = "Record Get Successfully";
                        result.StatusCode = 1;
                        result.IsValid = true;
                        result.Result = "success";
                        result.Data = sortingdetailmodel;
                        result.pk_ID = sortingdetailmodel[0].Pk_SortingID;
                    }
                    else
                    {
                        result.Message = "Record does not exists";
                        result.StatusCode = 0;
                        result.IsValid = false;
                        result.Result = "error";
                        result.Data = null;
                    }
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.pk_ID = null;
                result.Data = null;
            }
            return result;
        }


        public ResponseviewModel GetSortingDetailsDemo()
        {
            ResponseviewModel result = new ResponseviewModel();
            try
            {
                var model = _Context.StockTransaction.Where(k => k.SortingMaster.IsDelete == false && k.QuantityRemaining >0 ).Select(p=>p.SortingMaster).ToList();

                foreach (var item in model)
                {
                    if (item.IsBin == true)
                    {
                     var r=   _Context.BinMaster.Where(p => p.LocationCode.ToLower().Trim() == item.LocationID.ToLower().Trim()).FirstOrDefault();
                        if (r != null)
                        {
                            r.IsUsed = true;
                        }
                        var b = _Context.LocationMaster.Where(p => p.LocationCode.ToLower().Trim() == item.LocationID.ToLower().Trim()).FirstOrDefault();
                        if(b!=null)
                        {
                            b.IsUsed = true;
                        }
                    }
                    else
                    {
                        var r = _Context.LocationMaster.Where(p => p.LocationCode == item.LocationID).FirstOrDefault();
                        if (r != null)
                        {
                            r.IsUsed = true;
                        }
                    }
                }
                _Context.SaveChanges();
                result.Message = "Record Get Successfully";
                result.StatusCode = 1;
                result.IsValid = true;
                result.Result = "success";
                result.Data = model;
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.pk_ID = null;
                result.Data = null;
            }

            return result;
        }
    }

}