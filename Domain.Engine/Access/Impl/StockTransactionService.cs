﻿using Domain.Engine.Access.Interfaces;
using Domain.Objects.Models;
using Infrastructure.Model.Main;
using Infrastructure.Model.Main.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Domain.Engine.Access.Impl
{
    public class StockTransactionService : IStockTransactionService
    {
        protected readonly MainDbContext _Context;
        ResponseviewModel result = new ResponseviewModel();
        protected MainDbContext Context
        {
            get
            {
                return (MainDbContext)_Context;
            }
        }
        public StockTransactionService(IMainDbContext context)
        {
            _Context = (MainDbContext)context;
        }

        public ResponseviewModel InsertStockTransaction(StockTransactionModel stocktransactionmodel)
        {
            try
            {
                if(stocktransactionmodel.StockTransactionID == Guid.Empty)
                {
                    
                    StockTransaction stock = new StockTransaction();
                    stock = _Context.StockTransaction.Where(s => s.PK_SortingID == stocktransactionmodel.PK_SortingID).FirstOrDefault();
                    if (stock==null)
                    {
                        stock = new StockTransaction();
                        stock.StockTransactionID = Guid.NewGuid();
                        stock.PK_SortingID = stocktransactionmodel.PK_SortingID;
                        stock.QuantityReceived = stocktransactionmodel.QuantityReceived;
                        stock.QuantityDelivered = 0;// stocktransactionmodel.QuantityDelivered;
                        stock.QuantitySelected = 0;// stocktransactionmodel.QuantitySelected;
                        stock.QuantityRemaining = stocktransactionmodel.QuantityRemaining;
                        stock.CreatedBy = stocktransactionmodel.CreatedBy;
                        stock.CreatedDate = System.DateTime.Now;
                        stock.ModifiedBy = stocktransactionmodel.CreatedBy;
                        stock.ModifiedDate = System.DateTime.Now;

                        _Context.AddAsync(stock);
                    }
                    else
                    {
                       
                        stock.QuantityReceived =  stocktransactionmodel.QuantityReceived;
                       
                        stock.QuantityRemaining = stock.QuantityReceived- stock.QuantitySelected;
                       
                        stock.ModifiedBy = stocktransactionmodel.CreatedBy;
                        stock.ModifiedDate = System.DateTime.Now;

                        _Context.Update(stock);
                    }
                    
                    _Context.SaveChanges();

                    result.Message = "Data Inserted Successfully";
                    result.StatusCode = 1;
                    result.IsValid = true;
                    result.Result = "success";
                    result.pk_ID = stock.StockTransactionID;
                    result.Data = stock;
                }
                else
                {
                    result.Message = "Record does not exists";
                    result.StatusCode = 0;
                    result.IsValid = false;
                    result.Result = "error";
                    result.Data = null;
                }
            }
            catch(Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.pk_ID = null;
                result.Data = null;
            }
            return result;
        }
        public ResponseviewModel SteponPrepickingInsert(StockTransactionModel stocktransactionmodel)
        {
            StockTransaction stock = new StockTransaction();
            stock = _Context.StockTransaction.Where(s => s.PK_SortingID.Value.ToString().ToLower().Trim() == stocktransactionmodel.PK_SortingID.Value.ToString().ToLower().Trim()).FirstOrDefault();
            if (stock != null)
            {
              
               
                
                stock.QuantitySelected = stock.QuantitySelected+ stocktransactionmodel.QuantitySelected;
                stock.QuantityRemaining = stock.QuantityReceived-stock.QuantityDelivered- stock.QuantitySelected;
                
                stock.ModifiedBy = stocktransactionmodel.CreatedBy;
                stock.ModifiedDate = System.DateTime.Now;

                _Context.Update(stock);
                _Context.SaveChanges();
                result.Message = "Data Inserted Successfully";
                result.StatusCode = 1;
                result.IsValid = true;
                result.Result = "success";
                result.pk_ID = stock.StockTransactionID;
                result.Data = stock;
            }
            else
            {
                result.Message = "Record does not exists";
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.Data = null;
            }
            return result;
        }
        public ResponseviewModel SteponDOInsert(StockTransactionModel stocktransactionmodel)
        {
            StockTransaction stock = new StockTransaction();
            stock = _Context.StockTransaction.Where(s => s.PK_SortingID.Value.ToString().ToLower().Trim() == stocktransactionmodel.PK_SortingID.Value.ToString().ToLower().Trim()).FirstOrDefault();
            if (stock != null)
            {



                stock.QuantityDelivered = stock.QuantityDelivered+ stocktransactionmodel.QuantityDelivered;
                stock.QuantitySelected = stock.QuantitySelected - stocktransactionmodel.QuantityDelivered;
               

                stock.ModifiedBy = stocktransactionmodel.CreatedBy;
                stock.ModifiedDate = System.DateTime.Now;

                _Context.Update(stock);
                _Context.SaveChanges();
                result.Message = "Data Inserted Successfully";
                result.StatusCode = 1;
                result.IsValid = true;
                result.Result = "success";
                result.pk_ID = stock.StockTransactionID;
                result.Data = stock;
            }
            else
            {
                result.Message = "Record does not exists";
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.Data = null;
            }
            return result;
        }
    }
}
