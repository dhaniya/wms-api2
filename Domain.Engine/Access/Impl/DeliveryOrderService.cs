using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper;
using Domain.Engine.Access.Interfaces;
using Domain.Objects.Models;
using Infrastructure.Model.Main;
using Infrastructure.Model.Main.Entities;

namespace Domain.Engine.Access.Impl
{
    public class DeliveryOrderService : IDeliveryOrderService
    {
        ResponseviewModel data = new ResponseviewModel();
        protected readonly MainDbContext _Context;
        private readonly ILocationMasterService _locationmasterservice;
        private readonly IMapper _mapper;
        private readonly IStockTransactionService _stockTransactionService;
        protected MainDbContext Context
        {
            get
            {
                return (MainDbContext)_Context;
            }
        }

        public DeliveryOrderService(IMainDbContext context, ILocationMasterService locationmasterservice, IMapper mapper, IStockTransactionService stockTransactionService)
        {
            _Context = (MainDbContext)context;
            _locationmasterservice = locationmasterservice;
            _mapper = mapper;
            _stockTransactionService = stockTransactionService;
        }

        public ResponseviewModel GetPickedList(SearchPickedViewModel searchpickedviewmodel)
        {
            try
            {
                List<PickingMasterViewModel> listpickingmasterviewmodel = new List<PickingMasterViewModel>();
                List<PickingMasterViewModel> listpickingmasterviewmodelnew = new List<PickingMasterViewModel>();
                if (searchpickedviewmodel.Customer == null && searchpickedviewmodel.DateOfDeliver == null && searchpickedviewmodel.OrderNumber == null && searchpickedviewmodel.RefNumber == null)
                {
                    listpickingmasterviewmodel = _Context.PickingMaster.Where(d => d.IsActive == true && d.IsDelete == false && d.IsDeliverOrder == false).OrderByDescending(d => d.CreatedDate)
                    .Select(x => new PickingMasterViewModel
                    {
                        Pk_PickingMasterID = x.Pk_PickingMasterID,
                        OrderNumber = x.OrderNumber,
                        CustomerName = x.CustomerName,
                        Fk_CustomerMasterId = x.Fk_CustomerMasterId,
                        CustomerCode = x.CustomerCode,
                        DateOfDeliver = x.DateOfDeliver,
                        RefNo = x.RefNo,
                        Priority = x.Priority,
                        CreatedDate = x.CreatedDate,
                        ModifiedDate = x.ModifiedDate,
                        Locationid = x.LocationId,
                        username = x.UserMaster.FirstName
                    }).OrderByDescending(p => p.CreatedDate).ToList();


                    foreach (var item in listpickingmasterviewmodel)
                    {
                        var st = GetPickingStatusByPickingid(item.Pk_PickingMasterID);
                        if (st == 2)
                        {
                            listpickingmasterviewmodelnew.Add(item);
                        }

                    }
                }
                else
                {
                    listpickingmasterviewmodel = _Context.PickingMaster.Where(d => d.IsDeliverOrder == false && d.IsActive == true && (d.CustomerName.Contains(searchpickedviewmodel.Customer) || d.OrderNumber.Contains(searchpickedviewmodel.OrderNumber) || d.DateOfDeliver.Date == searchpickedviewmodel.DateOfDeliver || d.RefNo == searchpickedviewmodel.RefNumber) && d.IsActive == true && d.IsDelete == false).OrderByDescending(d => d.CreatedDate)
                   .Select(x => new PickingMasterViewModel
                   {
                       Pk_PickingMasterID = x.Pk_PickingMasterID,
                       OrderNumber = x.OrderNumber,
                       CustomerName = x.CustomerName,
                       CustomerCode = x.CustomerCode,
                       DateOfDeliver = x.DateOfDeliver,
                       RefNo = x.RefNo,
                       Priority = x.Priority,
                       CreatedDate = x.CreatedDate,
                       ModifiedDate = x.ModifiedDate,
                       username = x.UserMaster.FirstName
                   }).OrderByDescending(p => p.CreatedDate).ToList();
                    foreach (var item in listpickingmasterviewmodel)
                    {
                        var st = GetPickingStatusByPickingid(item.Pk_PickingMasterID);
                        if (st == 2)
                        {
                            listpickingmasterviewmodelnew.Add(item);
                        }

                    }
                }
                if (listpickingmasterviewmodelnew.Count > 0)
                {
                    data.Message = "Record Get Successfully";
                    data.StatusCode = 1;
                    data.IsValid = true;
                    data.Result = "success";
                    data.Data = listpickingmasterviewmodelnew.OrderByDescending(k => k.ModifiedDate);
                }
                else
                {
                    data.Message = "Record does not exists";
                    data.StatusCode = 0;
                    data.IsValid = false;
                    data.Result = "error";
                    data.Data = null;
                }
            }
            catch (Exception ex)
            {
                data.Message = ex.ToString();
                data.StatusCode = 0;
                data.IsValid = false;
                data.Result = "error";
                data.Data = null;
            }
            return data;
        }

        public ResponseviewModel GetAllPickedDetailsByPickingId(Guid id)
        {
            try
            {
                PickingViewModel pickingviewmodel = new PickingViewModel();
                pickingviewmodel = _Context.PickingMaster.Where(d => d.Pk_PickingMasterID == id && d.IsActive == true && d.IsDelete == false).OrderByDescending(d => d.CreatedDate)
                .Select(x => new PickingViewModel
                {
                    Pk_PickingMasterID = x.Pk_PickingMasterID,
                    Fk_CustomerMasterId = x.Fk_CustomerMasterId,
                    OrderNumber = x.OrderNumber,
                    CustomerName = x.CustomerName,
                    CustomerCode = x.CustomerCode,
                    DateOfDeliver = x.DateOfDeliver,
                    RefNo = x.RefNo,
                    Priority = x.Priority,
                    PickingStatus = x.PickingStatus
                }).FirstOrDefault();

                if (pickingviewmodel != null)
                {
                    pickingviewmodel.listpickingdetail = _Context.PickingDetail.Where(d => d.Fk_PickingID == pickingviewmodel.Pk_PickingMasterID && d.IsActive == true && d.IsDelete == false).OrderByDescending(d => d.CreatedDate)
                    .Select(x => new PickingDetailViewModel
                    {
                        PriPickingOrderNumber = x.PickingMaster.OrderNumber,
                        dateOfDeliver = x.PickingMaster.DateOfDeliver,
                        CustomerName = x.PickingMaster.CustomerName,
                        CustomerCode = x.PickingMaster.CustomerCode,
                        Pk_PickingDetailId = x.Pk_PickingDetailId,
                        InvoiceNo = x.InvoiceNo,
                        Fk_PickingID = x.Fk_PickingID,
                        PickingType = Convert.ToInt16(x.PickingType),
                        RepackedCaseNo = x.RepackedCaseNo,
                        ItemCode = x.ItemCode,
                        Quantity = x.Quantity,
                        LocationId = x.LocationId,
                        Description = x.SortingMaster.SortingDescription,
                        Height = x.Height,
                        Length = x.Length,
                        Breadth = x.Breadth,
                        Volume = x.Length * x.Breadth * x.Height,
                        Weight = x.Weight,
                        PONumber = String.Join(",", _Context.ASNItemDetails.Where(a => a.SuppliedPartNo == x.ItemCode && a.ASNMaster.InvoiceNo == x.InvoiceNo && a.PalletNo == x.SortingMaster.CaseNo).Select(a => a.PurchaseOrderNo).ToList()),
                        Remark1 = String.Join(",", _Context.ASNItemDetails.Where(a => a.SuppliedPartNo == x.ItemCode && a.ASNMaster.InvoiceNo == x.InvoiceNo && a.PalletNo == x.SortingMaster.CaseNo).Select(a => a.Remark1).ToList()),
                        Remark2 = String.Join(",", _Context.ASNItemDetails.Where(a => a.SuppliedPartNo == x.ItemCode && a.ASNMaster.InvoiceNo == x.InvoiceNo && a.PalletNo == x.SortingMaster.CaseNo).Select(a => a.Remark2).ToList()),
                        Remark3 = String.Join(",", _Context.ASNItemDetails.Where(a => a.SuppliedPartNo == x.ItemCode && a.ASNMaster.InvoiceNo == x.InvoiceNo && a.PalletNo == x.SortingMaster.CaseNo).Select(a => a.Remark2).ToList())
                    }).ToList();

                    data.Message = "Record Get Successfully";
                    data.StatusCode = 1;
                    data.IsValid = true;
                    data.Result = "success";
                    data.Data = pickingviewmodel;
                }
                else
                {
                    data.Message = "Record does not exists";
                    data.StatusCode = 0;
                    data.IsValid = false;
                    data.Result = "error";
                    data.Data = null;
                }
            }
            catch (Exception ex)
            {
                data.Message = ex.ToString();
                data.StatusCode = 0;
                data.IsValid = false;
                data.Result = "error";
                data.Data = null;
            }
            return data;
        }


        public ResponseviewModel GetAllpickedDetailById(Guid id)
        {
            try
            {
                PickingViewModel pickingviewmodel = new PickingViewModel();
                PickingDetailViewModel pickingDetailViewModel = new PickingDetailViewModel();
                pickingviewmodel = _Context.PickingMaster.Where(d => d.Pk_PickingMasterID == id && d.IsActive == true && d.IsDelete == false).OrderByDescending(d => d.CreatedDate)
                 //&& _Context.PickingDetail.Where(d => d.Fk_PickingID ==id )
                 .Select(x => new PickingViewModel
                 {
                     Pk_PickingMasterID = x.Pk_PickingMasterID,
                     Fk_CustomerMasterId = x.Fk_CustomerMasterId,
                     OrderNumber = x.OrderNumber,
                     CustomerName = x.CustomerName,
                     CustomerCode = x.CustomerCode,
                     DateOfDeliver = x.DateOfDeliver,
                     RefNo = x.RefNo,
                     Priority = x.Priority,
                     PickingStatus = x.PickingStatus,
                 }).FirstOrDefault();

                if (pickingviewmodel != null)
                {
                    pickingviewmodel.listpickingdetail = _Context.PickingDetail.Where(d => d.Fk_PickingID == pickingviewmodel.Pk_PickingMasterID && d.IsActive == true && d.IsDelete == false).OrderByDescending(d => d.CreatedDate)
                    .Select(x => new PickingDetailViewModel
                    {
                        Pk_PickingDetailId = x.Pk_PickingDetailId,
                        InvoiceNo = x.InvoiceNo,
                        Fk_PickingID = x.Fk_PickingID,
                        PickingType = Convert.ToInt16(x.PickingType),
                        PONumber = _Context.ASNItemDetails.Where(a => a.ASNMaster.InvoiceNo == x.InvoiceNo && a.Customer == x.PickingMaster.CustomerCode && a.SuppliedPartNo == x.ItemCode && a.IsDelete == false && a.IsRecieved == true).FirstOrDefault().PurchaseOrderNo ?? "",
                        Description = x.SortingMaster.Description,
                        Remark1 = _Context.ASNItemDetails.Where(a => a.ASNMaster.InvoiceNo == x.InvoiceNo && a.Customer == x.PickingMaster.CustomerCode && a.SuppliedPartNo == x.ItemCode && a.IsDelete == false && a.IsRecieved == true).FirstOrDefault().Remark1 ?? "",
                        Remark2 = _Context.ASNItemDetails.Where(a => a.ASNMaster.InvoiceNo == x.InvoiceNo && a.Customer == x.PickingMaster.CustomerCode && a.SuppliedPartNo == x.ItemCode && a.IsDelete == false && a.IsRecieved == true).FirstOrDefault().Remark2 ?? "",
                        Remark3 = _Context.ASNItemDetails.Where(a => a.ASNMaster.InvoiceNo == x.InvoiceNo && a.Customer == x.PickingMaster.CustomerCode && a.SuppliedPartNo == x.ItemCode && a.IsDelete == false && a.IsRecieved == true).FirstOrDefault().Remark3 ?? "",
                        RepackedCaseNo = x.RepackedCaseNo,
                        ItemCode = x.ItemCode,
                        CustomerCode = x.SortingMaster.CustomerCode,
                        Volume = x.Height * x.Length * x.Breadth,
                        Quantity = x.Quantity,
                        LocationId = x.LocationId,
                        Height = x.Height,
                        Length = x.Length,
                        Breadth = x.Breadth,
                        Weight = x.Weight,


                    }).ToList();

                    data.Message = "Record Get Successfully";
                    data.StatusCode = 1;
                    data.IsValid = true;
                    data.Result = "success";
                    data.Data = pickingviewmodel;
                }
                else
                {
                    data.Message = "Record does not exists";
                    data.StatusCode = 0;
                    data.IsValid = false;
                    data.Result = "error";
                    data.Data = null;
                }
            }
            catch (Exception ex)
            {
                data.Message = ex.ToString();
                data.StatusCode = 0;
                data.IsValid = false;
                data.Result = "error";
                data.Data = null;
            }
            return data;
        }


        public ResponseviewModel GetAllPickedDetailsByPickingIdForEdit(Guid id)
        {
            try
            {
                DeliveryOrder deliveryOrdermodal = new DeliveryOrder();
                var data = _Context.PickingMaster.Where(d => d.Pk_PickingMasterID == id && d.IsActive == true && d.IsDelete == false).OrderByDescending(d => d.CreatedDate).FirstOrDefault();
                if (data != null)
                {
                    deliveryOrdermodal = _Context.DeliveryOrder.Where(d => d.Fk_PickingMasterID == data.Pk_PickingMasterID && d.IsActive == true && d.IsDelete == false).OrderByDescending(d => d.CreatedDate)
                    .Select(x => new DeliveryOrder
                    {
                        DriverName = x.DriverName,
                        VehicleNumber = x.VehicleNumber,
                        shippingmodeID = x.shippingmodeID,
                        NoOfBox = x.NoOfBox,
                        ActualDateOfDeliver = x.ActualDateOfDeliver,
                        Subject = x.Subject,
                        Address = x.Address,
                        Remarks=x.Remarks,
                        Fk_CompanyID=x.Fk_CompanyID
                    }).FirstOrDefault();

                    this.data.Message = "Record Get Successfully";
                    this.data.StatusCode = 1;
                    this.data.IsValid = true;
                    this.data.Result = "success";
                    this.data.Data = deliveryOrdermodal;
                }
                else
                {
                    this.data.Message = "Record does not exists";
                    this.data.StatusCode = 0;
                    this.data.IsValid = false;
                    this.data.Result = "error";
                    this.data.Data = null;
                }
            }
            catch (Exception ex)
            {
                data.Message = ex.ToString();
                data.StatusCode = 0;
                data.IsValid = false;
                data.Result = "error";
                data.Data = null;
            }
            return data;
        }


        public ResponseviewModel CreateDeliveryOrder(CreateDeliveryOrderViewModel createdeliveryorderviewmodel)
        {
            try
            {
                if (_Context.PickingMaster.FirstOrDefault(d => d.Pk_PickingMasterID == createdeliveryorderviewmodel.Fk_PickingMasterID).IsDeliverOrder)
                {
                    data.Message = "Delivery Order already Created";
                    data.StatusCode = 0;
                    data.IsValid = false;
                    data.Result = "error";
                    data.pk_ID = null;
                    data.Data = null;
                }
                else
                {
                    DeliveryOrderViewModel deliveryorderviewmodel = new DeliveryOrderViewModel();
                    deliveryorderviewmodel = _Context.PickingMaster.Where(d => d.Pk_PickingMasterID == createdeliveryorderviewmodel.Fk_PickingMasterID && d.IsActive == true && d.IsDelete == false)
                    .Select(x => new DeliveryOrderViewModel
                    {
                        Fk_PickingMasterID = x.Pk_PickingMasterID,
                        Fk_CustomerMasterId = x.Fk_CustomerMasterId,
                        OrderNumber = x.OrderNumber,
                        CustomerName = x.CustomerName,
                        CustomerCode = x.CustomerCode,
                        DateOfDeliver = x.DateOfDeliver,
                        RefNo = x.RefNo,
                        Priority = x.Priority,
                    }).FirstOrDefault();
                    var pickmaster = _Context.PickingMaster.Where(a => a.Pk_PickingMasterID == createdeliveryorderviewmodel.Fk_PickingMasterID).FirstOrDefault();
                    pickmaster.IsDeliverOrder = true;
                    pickmaster.ModifiedDate = System.DateTime.Now;
                    _Context.Update(pickmaster);
                    List<PickingDetailViewModel> listpickingdetailviewmodel = new List<PickingDetailViewModel>();
                    listpickingdetailviewmodel = _Context.PickingDetail.Where(d => d.Fk_PickingID == deliveryorderviewmodel.Fk_PickingMasterID && d.IsActive == true && d.IsDelete == false)
                       .Select(x => new PickingDetailViewModel
                       {
                           Pk_PickingDetailId = x.Pk_PickingDetailId,
                           InvoiceNo = x.InvoiceNo,
                           Fk_PickingID = x.Fk_PickingID,
                           PickingType = Convert.ToInt16(x.PickingType),
                           RepackedCaseNo = x.RepackedCaseNo,
                           ItemCode = x.ItemCode,
                           Quantity = x.Quantity,
                           LocationId = x.LocationId,
                           Height = x.Height,
                           Length = x.Length,
                           Breadth = x.Breadth,
                           Weight = x.Weight,
                           fk_SortingMaster_id = x.Fk_SortingMasterID.HasValue ? x.Fk_SortingMasterID.Value : Guid.Empty
                       }).ToList();


                    if (deliveryorderviewmodel != null)
                    {
                        if (listpickingdetailviewmodel.Count > 0)
                        {
                            DeliveryOrder deliveryorder = new DeliveryOrder();
                            deliveryorder.Pk_DeliveryOrderID = Guid.NewGuid();
                            deliveryorder.DONumber = GenerateOrderNo();
                            deliveryorder.NoOfBox = createdeliveryorderviewmodel.NoOfBox;
                            deliveryorder.VehicleNumber = createdeliveryorderviewmodel.VehicleNumber;
                            deliveryorder.DriverName = createdeliveryorderviewmodel.DriverName;
                            deliveryorder.Remarks = createdeliveryorderviewmodel.remarks;
                            deliveryorder.shippingmodeID = createdeliveryorderviewmodel.shippingmodeID;
                            deliveryorder.ActualDateOfDeliver = createdeliveryorderviewmodel.ActualDateOfDeliver;
                            deliveryorder.Fk_CustomerMasterId = deliveryorderviewmodel.Fk_CustomerMasterId;
                            deliveryorder.Fk_PickingMasterID = deliveryorderviewmodel.Fk_PickingMasterID;
                            deliveryorder.CustomerName = deliveryorderviewmodel.CustomerName;
                            deliveryorder.CustomerCode = deliveryorderviewmodel.CustomerCode;
                            deliveryorder.DateOfDeliver = deliveryorderviewmodel.DateOfDeliver;
                            deliveryorder.RefNo = deliveryorderviewmodel.RefNo;
                            deliveryorder.Priority = deliveryorderviewmodel.Priority;
                            deliveryorder.Address = createdeliveryorderviewmodel.Address;
                            deliveryorder.Fk_CompanyID = createdeliveryorderviewmodel.Fk_CompanyID;
                            deliveryorder.IsDelete = false;
                            deliveryorder.IsActive = true;
                            deliveryorder.IsConfirm = false;
                            deliveryorder.CreatedBy = createdeliveryorderviewmodel.CreatedBy;
                            deliveryorder.CreatedDate = System.DateTime.Now;
                            deliveryorder.ModifiedBy = createdeliveryorderviewmodel.CreatedBy;
                            deliveryorder.ModifiedDate = System.DateTime.Now;
                            deliveryorder.Subject = createdeliveryorderviewmodel.Subject;
                            _Context.DeliveryOrder.Add(deliveryorder);
                            _Context.SaveChanges();
                            foreach (var item in listpickingdetailviewmodel)
                            {
                                DeliveryOrderDetail deliveryorderdetail = new DeliveryOrderDetail();
                                deliveryorderdetail.Pk_DeliveryOrderDetailID = Guid.NewGuid();
                                deliveryorderdetail.Fk_DeliveryOrderID = deliveryorder.Pk_DeliveryOrderID;
                                deliveryorderdetail.Fk_SortingMasterID = item.fk_SortingMaster_id;
                                deliveryorderdetail.PickingType = item.PickingType; //1 : Repacked Case No, 2 : loose Items
                                deliveryorderdetail.InvoiceNo = item.InvoiceNo;
                                deliveryorderdetail.RepackedCaseNo = item.RepackedCaseNo;
                                deliveryorderdetail.ItemCode = item.ItemCode;
                                deliveryorderdetail.Quantity = item.Quantity;
                                deliveryorderdetail.LocationId = item.LocationId;
                                deliveryorderdetail.Height = item.Height;
                                deliveryorderdetail.Length = item.Length;
                                deliveryorderdetail.Breadth = item.Breadth;
                                deliveryorderdetail.Weight = item.Weight;
                                deliveryorderdetail.IsDelete = false;
                                deliveryorderdetail.IsActive = true;
                                deliveryorderdetail.CreatedBy = createdeliveryorderviewmodel.CreatedBy;
                                deliveryorderdetail.CreatedDate = System.DateTime.Now;
                                deliveryorderdetail.ModifiedBy = createdeliveryorderviewmodel.CreatedBy;
                                deliveryorderdetail.ModifiedDate = System.DateTime.Now;
                                _Context.AddAsync(deliveryorderdetail);
                                _Context.SaveChanges();

                                int totalavailableqty = GetAvailableQuantityofPicking(deliveryorderdetail.RepackedCaseNo, deliveryorderdetail.InvoiceNo);
                                if (totalavailableqty == 0)
                                {
                                    if (item.PickingType != 3)
                                    {
                                        var locationId = _Context.SortingMaster.FirstOrDefault(d => d.RepakedCaseNo.ToLower().Trim() == deliveryorderdetail.RepackedCaseNo.ToLower().Trim()).LocationID;
                                        if (locationId != null)
                                            _locationmasterservice.ChangeUnusedByLocationCode(locationId);
                                    }
                                }
                                //StockTransactionModel stockTransactionModel = new StockTransactionModel();
                                //stockTransactionModel.CreatedBy = item.CreatedBy;
                                //stockTransactionModel.PK_SortingID = item.fk_SortingMaster_id;
                                //stockTransactionModel.QuantityDelivered = item.Quantity;

                                //_stockTransactionService.SteponDOInsert(stockTransactionModel);
                            }



                            data.Message = "Data Inserted Successfully";
                            data.StatusCode = 1;
                            data.IsValid = true;
                            data.Result = "success";
                            data.pk_ID = deliveryorder.Pk_DeliveryOrderID;
                            data.Data = deliveryorder;
                        }
                        else
                        {
                            data.Message = "Please more than one item";
                            data.StatusCode = 0;
                            data.IsValid = false;
                            data.Result = "error";
                            data.pk_ID = null;
                            data.Data = null;
                        }
                    }
                    else
                    {
                        data.Message = "Insert valid data";
                        data.StatusCode = 0;
                        data.IsValid = false;
                        data.Result = "error";
                        data.pk_ID = null;
                        data.Data = null;
                    }
                }
            }
            catch (Exception ex)
            {
                data.Message = ex.ToString();
                data.StatusCode = 0;
                data.IsValid = false;
                data.Result = "error";
                data.pk_ID = null;
                data.Data = null;
            }
            return data;
        }

        public ResponseviewModel UpdateDeliveryOrder(CreateDeliveryOrderViewModel createdeliveryorderviewmodel)
        {
            try
            {

                DeliveryOrderViewModel deliveryorderviewmodel = new DeliveryOrderViewModel();
                deliveryorderviewmodel = _Context.PickingMaster.Where(d => d.Pk_PickingMasterID == createdeliveryorderviewmodel.Fk_PickingMasterID && d.IsActive == true && d.IsDelete == false)
                .Select(x => new DeliveryOrderViewModel
                {
                    Fk_PickingMasterID = x.Pk_PickingMasterID,
                    Fk_CustomerMasterId = x.Fk_CustomerMasterId,
                    OrderNumber = x.OrderNumber,
                    CustomerName = x.CustomerName,
                    CustomerCode = x.CustomerCode,
                    DateOfDeliver = x.DateOfDeliver,
                    RefNo = x.RefNo,
                    Priority = x.Priority
                }).FirstOrDefault();
                var pickmaster = _Context.PickingMaster.Where(a => a.Pk_PickingMasterID == createdeliveryorderviewmodel.Fk_PickingMasterID).FirstOrDefault();
                pickmaster.IsDeliverOrder = true;
                pickmaster.ModifiedDate = System.DateTime.Now;
                _Context.Update(pickmaster);
                List<PickingDetailViewModel> listpickingdetailviewmodel = new List<PickingDetailViewModel>();
                listpickingdetailviewmodel = _Context.PickingDetail.Where(d => d.Fk_PickingID == deliveryorderviewmodel.Fk_PickingMasterID && d.IsActive == true && d.IsDelete == false)
                   .Select(x => new PickingDetailViewModel
                   {
                       Pk_PickingDetailId = x.Pk_PickingDetailId,
                       InvoiceNo = x.InvoiceNo,
                       Fk_PickingID = x.Fk_PickingID,
                       PickingType = Convert.ToInt16(x.PickingType),
                       RepackedCaseNo = x.RepackedCaseNo,
                       ItemCode = x.ItemCode,
                       Quantity = x.Quantity,
                       LocationId = x.LocationId,
                       Height = x.Height,
                       Length = x.Length,
                       Breadth = x.Breadth,
                       Weight = x.Weight,
                       fk_SortingMaster_id = x.Fk_SortingMasterID.HasValue ? x.Fk_SortingMasterID.Value : Guid.Empty
                   }).ToList();


                if (deliveryorderviewmodel != null)
                {
                    if (listpickingdetailviewmodel.Count > 0)
                    {


                        DeliveryOrder deliveryorder = new DeliveryOrder();
                        deliveryorder = _Context.DeliveryOrder.Where(d => d.Fk_PickingMasterID == createdeliveryorderviewmodel.Fk_PickingMasterID && d.IsActive == true && d.IsDelete == false).FirstOrDefault();
                        if (deliveryorder != null)
                        {
                            //deliveryorder.DONumber = GenerateOrderNo();
                            deliveryorder.NoOfBox = createdeliveryorderviewmodel.NoOfBox;
                            deliveryorder.VehicleNumber = createdeliveryorderviewmodel.VehicleNumber;
                            deliveryorder.DriverName = createdeliveryorderviewmodel.DriverName;
                            deliveryorder.shippingmodeID = createdeliveryorderviewmodel.shippingmodeID;
                            deliveryorder.ActualDateOfDeliver = createdeliveryorderviewmodel.ActualDateOfDeliver;
                            deliveryorder.Remarks = createdeliveryorderviewmodel.remarks;
                            deliveryorder.Fk_CompanyID = createdeliveryorderviewmodel.Fk_CompanyID;
                            //deliveryorder.Fk_CustomerMasterId = deliveryorderviewmodel.Fk_CustomerMasterId;
                            //deliveryorder.Fk_PickingMasterID = deliveryorderviewmodel.Fk_PickingMasterID;
                            //deliveryorder.CustomerName = deliveryorderviewmodel.CustomerName;
                            //deliveryorder.CustomerCode = deliveryorderviewmodel.CustomerCode;
                            //deliveryorder.DateOfDeliver = deliveryorderviewmodel.DateOfDeliver;
                            //deliveryorder.RefNo = deliveryorderviewmodel.RefNo;
                            //deliveryorder.Priority = deliveryorderviewmodel.Priority;
                            deliveryorder.Address = createdeliveryorderviewmodel.Address;
                            //deliveryorder.IsDelete = false;
                            //deliveryorder.IsActive = true;
                            //deliveryorder.IsConfirm = false;
                            //deliveryorder.CreatedBy = createdeliveryorderviewmodel.CreatedBy;
                            //deliveryorder.CreatedDate = System.DateTime.Now;
                            deliveryorder.ModifiedBy = createdeliveryorderviewmodel.CreatedBy;
                            deliveryorder.ModifiedDate = System.DateTime.Now;
                            //deliveryorder.Subject = createdeliveryorderviewmodel.Subject;
                            _Context.DeliveryOrder.Update(deliveryorder);
                            _Context.SaveChanges();
                        }



                        data.Message = "Data Updated Successfully";
                        data.StatusCode = 1;
                        data.IsValid = true;
                        data.Result = "success";
                        data.pk_ID = deliveryorder.Pk_DeliveryOrderID;
                        data.Data = deliveryorder;
                    }
                    else
                    {
                        data.Message = "Please more than one item";
                        data.StatusCode = 0;
                        data.IsValid = false;
                        data.Result = "error";
                        data.pk_ID = null;
                        data.Data = null;
                    }
                }
                else
                {
                    data.Message = "Insert valid data";
                    data.StatusCode = 0;
                    data.IsValid = false;
                    data.Result = "error";
                    data.pk_ID = null;
                    data.Data = null;
                }
            }
            catch (Exception ex)
            {
                data.Message = ex.ToString();
                data.StatusCode = 0;
                data.IsValid = false;
                data.Result = "error";
                data.pk_ID = null;
                data.Data = null;
            }
            return data;
        }





        public ResponseviewModel GetDeliveryOrderList(DeliveryOrderFilterViewModel searchmodel)
        {
            PagedItemSet<DeliveryOrderViewModel> pagedItems = new PagedItemSet<DeliveryOrderViewModel>();
            var cCode = searchmodel.CustomerCode.Split(',');
            var DeliveryOrderList = _Context.DeliveryOrder.Where(d => d.DONumber == (searchmodel.DONumber != "" ? searchmodel.DONumber : d.DONumber) &&
                                                           (searchmodel.CustomerCode != "" ? cCode.Contains(d.CustomerCode) : true) &&
                                                          d.CustomerName == (searchmodel.CustomerName != "" ? searchmodel.CustomerName : d.CustomerName) &&
                                                          //d.CustomerCode == (searchmodel.CustomerCode != "" ? searchmodel.CustomerCode : d.CustomerCode) &&
                                                          d.DateOfDeliver >= (searchmodel.FromDate != null ? searchmodel.FromDate : d.DateOfDeliver) &&
                                                          d.DateOfDeliver <= (searchmodel.ToDate != null ? searchmodel.ToDate : d.DateOfDeliver)
                                                          && d.IsActive == true && d.IsDelete == false).OrderByDescending(p => p.DONumber).Select(x => new DeliveryOrderViewModel
                                                          {
                                                              Pk_DeliveryOrderID = x.Pk_DeliveryOrderID,
                                                              Fk_PickingMasterID = x.Fk_PickingMasterID,
                                                              DONumber = x.DONumber,
                                                              DriverName = x.DriverName,
                                                              VehicleNumber = x.VehicleNumber,
                                                              shippingmodeID = x.shippingmodeID,
                                                              ActualDateOfDeliver = x.ActualDateOfDeliver,
                                                              OrderNumber = x.OrderNumber,
                                                              CustomerName = x.CustomerName,
                                                              CustomerCode = x.CustomerCode,
                                                              DateOfDeliver = x.DateOfDeliver,
                                                              RefNo = x.RefNo,
                                                              Priority = x.Priority,
                                                              isConfirm = x.IsConfirm,
                                                              UserName = x.UserMaster.FirstName
                                                          }).AsQueryable();
            pagedItems.TotalResults = DeliveryOrderList.Count();
            if (searchmodel.Skip.HasValue)
            {
                DeliveryOrderList = DeliveryOrderList.Skip(searchmodel.Skip.Value);
            }
            else
            {
                DeliveryOrderList = DeliveryOrderList.Skip(0);
            }

            if (searchmodel.Take.HasValue)
            {
                DeliveryOrderList = DeliveryOrderList.Take(searchmodel.Take.Value);
            }
            else
            {
                DeliveryOrderList = DeliveryOrderList.Take(10);
            }
            pagedItems.Items = DeliveryOrderList.ToList();

            if (pagedItems.TotalResults > 0)
            {
                data.Message = "Record Get Successfully";
                data.StatusCode = 1;
                data.IsValid = true;
                data.Result = "success";
                data.Data = pagedItems;
            }
            else
            {
                data.Message = "Record does not exists";
                data.StatusCode = 0;
                data.IsValid = false;
                data.Result = "error";
                data.Data = null;
            }
            return data;

        }

        public int GetAvailableQuantityofPicking(string caseno, string invoiceno)
        {
            int availablequantity = 0;
            try
            {
                int totalquantity = 0;
                int pickingquantity = 0;
                totalquantity = _Context.SortingMaster.Where(d => d.InvoiceNo.ToLower().Trim() == invoiceno.ToLower().Trim() && d.RepakedCaseNo.ToLower().Trim() == caseno.ToLower().Trim() && d.IsActive == true && d.IsDelete == false && d.IsPutaway == true).Sum(x => x.Quantity);
                pickingquantity = _Context.PickingDetail.Where(d => d.InvoiceNo.ToLower().Trim() == invoiceno.ToLower().Trim() && d.RepackedCaseNo.ToLower().Trim() == caseno.ToLower().Trim() && d.IsActive == true && d.IsDelete == false).Sum(x => x.Quantity);
                //Remain to count sorted quantity
                availablequantity = totalquantity - pickingquantity;
                if (availablequantity <= 0)
                {
                    availablequantity = 0;
                }
            }
            catch (Exception ex)
            {
                availablequantity = 0;
            }
            return availablequantity;
        }


        public string GenerateOrderNo()
        {
            string oid = _Context.DeliveryOrder.Max(k => k.DONumber);
            if (oid == null)
            {
                oid = "DO0000";
            }
            oid = oid.Substring(2);
            int a = Convert.ToInt32(oid);
            return "DO" + (a + 1).ToString("0000");
            //string new_tid = System.Guid.NewGuid().ToString();
            //new_tid = new_tid.Replace("-", string.Empty).ToLower();
            //new_tid = new_tid.Substring(0, 15).Replace("a", "1");
            //new_tid = new_tid.Substring(0, 15).Replace("b", "2");
            //new_tid = new_tid.Substring(0, 15).Replace("c", "3");
            //new_tid = new_tid.Substring(0, 15).Replace("d", "4");
            //new_tid = new_tid.Substring(0, 15).Replace("e", "5");
            //new_tid = new_tid.Substring(0, 15).Replace("f", "6");
            //new_tid = new_tid.Substring(0, 15).Replace("g", "14");
            //new_tid = new_tid.Substring(0, 15).Replace("h", "8");
            //new_tid = new_tid.Substring(0, 15).Replace("i", "9");
            //new_tid = new_tid.Substring(0, 15).Replace("j", "0");
            //new_tid = new_tid.Substring(0, 15).Replace("k", "1");
            //new_tid = new_tid.Substring(0, 15).Replace("l", "2");
            //new_tid = new_tid.Substring(0, 15).Replace("m", "3");
            //new_tid = new_tid.Substring(0, 15).Replace("n", "4");
            //new_tid = new_tid.Substring(0, 15).Replace("o", "5");
            //new_tid = new_tid.Substring(0, 15).Replace("p", "0");
            //new_tid = new_tid.Substring(0, 15).Replace("q", "9");
            //new_tid = new_tid.Substring(0, 15).Replace("r", "8");
            //new_tid = new_tid.Substring(0, 15).Replace("s", "14");
            //new_tid = new_tid.Substring(0, 15).Replace("t", "6");
            //new_tid = new_tid.Substring(0, 15).Replace("u", "5");
            //new_tid = new_tid.Substring(0, 15).Replace("v", "4");
            //new_tid = new_tid.Substring(0, 15).Replace("w", "3");
            //new_tid = new_tid.Substring(0, 15).Replace("x", "2");
            //new_tid = new_tid.Substring(0, 15).Replace("y", "1");
            //new_tid = new_tid.Substring(0, 15).Replace("z", "0");
            ////  return (new_tid + DateTime.Now.Millisecond.ToString()).Substring(2);
            //return (new_tid + DateTime.Now.Millisecond.ToString()).Substring(10);
        }

        public ResponseviewModel GetDeliveryOrderDropdownList()
        {
            try
            {
                var Deliveryorder = _Context.PickingMaster.Where(d => d.ModifiedDate.Date == System.DateTime.Now.Date && d.IsActive == true && d.IsDelete == false).OrderByDescending(k => k.CreatedDate)
                .Select(x => new DeliveryOrderDropdownModel
                {
                    ID = x.OrderNumber,
                    Value = x.OrderNumber,
                }).ToList();

                if (Deliveryorder.Count() > 0)
                {
                    data.Message = "Record Get Successfully";
                    data.StatusCode = 1;
                    data.IsValid = true;
                    data.Result = "success";
                    data.Data = Deliveryorder;
                }
                else
                {
                    data.Message = "Record does not exists";
                    data.StatusCode = 0;
                    data.IsValid = false;
                    data.Result = "error";
                    data.Data = null;
                }
            }
            catch (Exception ex)
            {
                data.Message = ex.ToString();
                data.StatusCode = 0;
                data.IsValid = false;
                data.Result = "error";
                data.Data = null;
            }
            return data;
        }

        public int GetPickingStatusByPickingid(Guid pickingid)
        {
            int pickingstatus = 0;//0 : No picking ,1:Partialpicking,2:Picked
            try
            {

                int totalpickingcount = 0;
                int ispickedcount = 0;
                totalpickingcount = _Context.PickingDetail.Where(d => d.Fk_PickingID == pickingid && d.IsActive == true && d.IsDelete == false).Count();
                ispickedcount = _Context.PickingDetail.Where(d => d.Fk_PickingID == pickingid && d.IsPicked == true && d.IsActive == true && d.IsDelete == false).Count();
                if (ispickedcount == 0)
                {
                    pickingstatus = 0;//0 : No picking ,1:Partialpicking,2:Picked
                    PickingMaster pickingmaster = new PickingMaster();
                    pickingmaster = _Context.PickingMaster.Where(d => d.Pk_PickingMasterID == pickingid && d.IsActive == true && d.IsDelete == false).FirstOrDefault();
                    if (pickingmaster != null)
                    {
                        pickingmaster.PickingStatus = pickingstatus;
                        _Context.Update(pickingmaster);
                        _Context.SaveChanges();
                    }
                }
                else if (totalpickingcount == ispickedcount)
                {
                    pickingstatus = 2;////0 : No picking ,1:Partialpicking,2:Picked
                    PickingMaster pickingmaster = new PickingMaster();
                    pickingmaster = _Context.PickingMaster.Where(d => d.Pk_PickingMasterID == pickingid && d.IsActive == true && d.IsDelete == false).FirstOrDefault();
                    if (pickingmaster != null)
                    {
                        pickingmaster.PickingStatus = pickingstatus;
                        _Context.Update(pickingmaster);
                        _Context.SaveChanges();
                    }
                }
                else
                {
                    pickingstatus = 1;//0 : No picking ,1:Partialpicking,2:Picked
                    PickingMaster pickingmaster = new PickingMaster();
                    pickingmaster = _Context.PickingMaster.Where(d => d.Pk_PickingMasterID == pickingid && d.IsActive == true && d.IsDelete == false).FirstOrDefault();
                    if (pickingmaster != null)
                    {
                        pickingmaster.PickingStatus = pickingstatus;
                        _Context.Update(pickingmaster);
                        _Context.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
            }
            return pickingstatus;
        }

        public ResponseviewModel GetDeliveryOrderDetailList(string donumber)
        {
            try
            {

                var listdeliveryorderdetailviewmodel = _Context.DeliveryOrder.Where(d => d.IsActive == true && d.IsDelete == false && d.DONumber.ToLower().Trim() == donumber.ToLower().Trim()).FirstOrDefault();
                DeliveryOrderViewResponceModel k = _mapper.Map<DeliveryOrderViewResponceModel>(listdeliveryorderdetailviewmodel);
                k.Address = listdeliveryorderdetailviewmodel.Address;
                k.Fk_PickingMasterID = listdeliveryorderdetailviewmodel.Fk_PickingMasterID;
                if (listdeliveryorderdetailviewmodel.Fk_CompanyID != null)
                {
                    k.ComapanyNameMaster = _Context.CompanyMaster.Where(a => a.PK_CompanyID == listdeliveryorderdetailviewmodel.Fk_CompanyID).FirstOrDefault().CompanyName ?? "";
                    k.FromAddress = _Context.CompanyMaster.Where(a => a.PK_CompanyID == listdeliveryorderdetailviewmodel.Fk_CompanyID).FirstOrDefault().Address;

                }
                k.PickingOrderNo = _Context.PickingMaster.Where(p => p.Pk_PickingMasterID == k.Fk_PickingMasterID).FirstOrDefault().OrderNumber ?? "";
                k.DriverName = listdeliveryorderdetailviewmodel.DriverName;
                k.Remarks = listdeliveryorderdetailviewmodel.Remarks;
                if (k != null)
                {
                    var lstdodetails = _Context.DeliveryOrderDetail.Where(p => p.Fk_DeliveryOrderID == k.Pk_DeliveryOrderID)
                        .ToList();
                    //k.lstdetils = lstdodetails.Where(a=>a.Quantity>0).Select(p => _mapper.Map<DeliveryOrderDetailsViewModel>(p)).ToList();
                    k.lstdetils = lstdodetails.Select(p => _mapper.Map<DeliveryOrderDetailsViewModel>(p)).ToList();
                    foreach (var item in k.lstdetils)
                    {
                        var sortingmaster = _Context.SortingMaster.Where(sort => sort.Pk_SortingID == item.Fk_SortingMasterID).FirstOrDefault();
                        if (item.PickingType == 1)
                        {

                            var asndetails = _Context.ASNItemDetails.Where(asn => asn.PalletNo == sortingmaster.CaseNo && asn.SuppliedPartNo == sortingmaster.ItemCode && asn.ASNMaster.InvoiceNo == sortingmaster.InvoiceNo && asn.Customer == sortingmaster.CustomerCode).FirstOrDefault();
                            item.Remark1 = asndetails != null ? asndetails.Remark1 : "";
                            item.Remark2 = asndetails != null ? asndetails.Remark2 : "";
                            item.Remark3 = asndetails != null ? asndetails.Remark3 : "";
                            item.PONumber = asndetails != null ? asndetails.PurchaseOrderNo : "";

                            var volume = _Context.SortingDetail.Where(r => r.FK_SortingMasterID == sortingmaster.Pk_SortingID).FirstOrDefault();
                            item.Length = volume.Length;
                            item.Breadth = volume.Breadth;
                            item.Height = volume.Height;
                            item.Weight = volume.Weight;
                            item.M2 = volume.Height * volume.Length * volume.Breadth;
                        }
                        item.PartDescription = sortingmaster.SortingDescription;


                    }
                    data.Message = "Data Get Successfully";
                    data.StatusCode = 1;
                    data.IsValid = true;
                    data.Result = "success";
                    data.Data = k;
                }
                else
                {
                    data.Message = "Record does not exists.!";
                    data.StatusCode = 0;
                    data.IsValid = false;
                    data.Result = "error";
                    data.pk_ID = null;
                    data.Data = null;
                }
            }
            catch (Exception ex)
            {
                data.Message = ex.ToString();
                data.StatusCode = 0;
                data.IsValid = false;
                data.Result = "error";
                data.pk_ID = null;
                data.Data = null;
            }
            return data;
        }
        public ResponseviewModel ConfirmDO(string donumber)
        {
            try
            {

                var listdeliveryorderdetailviewmodel = _Context.DeliveryOrder.Where(d => d.IsActive == true && d.IsDelete == false && d.DONumber.ToLower().Trim() == donumber.ToLower().Trim() && d.IsConfirm != true).FirstOrDefault();

                DeliveryOrderViewResponceModel k = _mapper.Map<DeliveryOrderViewResponceModel>(listdeliveryorderdetailviewmodel);


                if (k != null)
                {
                    listdeliveryorderdetailviewmodel.IsConfirm = true;
                    listdeliveryorderdetailviewmodel.ModifiedDate = System.DateTime.Now;
                    _Context.Update(listdeliveryorderdetailviewmodel);
                    _Context.SaveChanges();
                    var lstdodetails = _Context.DeliveryOrderDetail.Where(p => p.Fk_DeliveryOrderID == k.Pk_DeliveryOrderID).ToList();
                    foreach (var item in lstdodetails)
                    {
                        StockTransactionModel stockTransactionModel = new StockTransactionModel();
                        stockTransactionModel.CreatedBy = item.CreatedBy;
                        stockTransactionModel.PK_SortingID = item.Fk_SortingMasterID;
                        stockTransactionModel.QuantityDelivered = item.Quantity;

                        _stockTransactionService.SteponDOInsert(stockTransactionModel);
                    }
                    k.lstdetils = lstdodetails.Select(p => _mapper.Map<DeliveryOrderDetailsViewModel>(p)).ToList();
                    data.Message = "Data Get Successfully";
                    data.StatusCode = 1;
                    data.IsValid = true;
                    data.Result = "success";
                    data.Data = k;
                }
                else
                {
                    data.Message = "Record does not exists.!";
                    data.StatusCode = 0;
                    data.IsValid = false;
                    data.Result = "error";
                    data.pk_ID = null;
                    data.Data = null;
                }
            }
            catch (Exception ex)
            {
                data.Message = ex.ToString();
                data.StatusCode = 0;
                data.IsValid = false;
                data.Result = "error";
                data.pk_ID = null;
                data.Data = null;
            }
            return data;
        }
        public ResponseviewModel UpdatePackingLocation(PickingLocationUpdate model)
        {
            var pickingMasters = _Context.PickingMaster.Where(d => d.IsActive == true && d.Pk_PickingMasterID == model.pk_PickingMasterID && d.IsDelete == false && d.IsDeliverOrder == false).FirstOrDefault();
            pickingMasters.LocationId = model.Locationid;
            _Context.PickingMaster.Update(pickingMasters);
            _Context.SaveChanges();
            if (pickingMasters != null)
            {

                data.Message = "Location Updated Successfully";
                data.StatusCode = 1;
                data.IsValid = true;
                data.Result = "success";
                data.Data = pickingMasters;
            }
            else
            {
                data.Message = "Record does not exists.!";
                data.StatusCode = 0;
                data.IsValid = false;
                data.Result = "error";
                data.pk_ID = null;
                data.Data = null;
            }
            return data;
        }
        public ResponseviewModel AddressDeliveryOrder()
        {
            //List<DeliveryOrder>  = new List<DeliveryOrder>();
            DeliveryOrderViewModel deliveryorderviewmodel = new DeliveryOrderViewModel();
            var AddressDetails = _Context.DeliveryOrder.Where(d => d.Address != null && d.Address != "" && d.Address.ToLower() != "null").
                Select(x => new DeliveryOrderViewModel
                {
                    Address = x.Address
                }).ToList();

            data.Message = "Record is exists.!";
            data.StatusCode = 1;
            data.IsValid = true;
            data.Result = "Success";
            data.pk_ID = null;
            data.Data = AddressDetails;
            return data;
        }
    }
}
