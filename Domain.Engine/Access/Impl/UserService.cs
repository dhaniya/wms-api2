﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper;
using System.IdentityModel.Tokens.Jwt;
using Domain.Engine.Access.Interfaces;
using Domain.Objects.Models;
using Infrastructure.Model.Main;
using Infrastructure.Model.Main.Entities;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;
using System.Security.Cryptography;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace Domain.Engine.Access.Impl
{
    public class UserService : IUserService
    {

        protected readonly MainDbContext _Context;
        private readonly AppSettings _appSettings;
        ResponseviewModel result = new ResponseviewModel();
        IPasswordHasher<UserMaster> passwordHasher = new PasswordHasher<UserMaster>();

        protected MainDbContext Context
        {
            get
            {
                return (MainDbContext)_Context;
            }
        }

        public UserService(IMainDbContext context, IOptions<AppSettings> appSettings)
        {
            _Context = (MainDbContext)context;
            _appSettings = appSettings.Value;
        }

        public ResponseviewModel Login(LoginViewModel loginviewmodel)
        {
            try
            {
               
                UserMaster usermaster = new UserMaster();
                usermaster = _Context.UserMaster.Where(d => d.UserName.ToLower().Trim() == loginviewmodel.UserName.ToLower().Trim() && d.IsActive == true && d.IsDelete == false).FirstOrDefault();
                if (usermaster != null)
                {
                    string verifypassword = passwordHasher.VerifyHashedPassword(usermaster, usermaster.Password, loginviewmodel.Password).ToString();
                    if (verifypassword.ToLower() == "success")
                    {
                     var d = _Context.UserAccess.AsNoTracking().Where(u => u.UserId == usermaster.Pk_UserId).Select(b=> new
                        {
                         name= b.RoleName,
                         url= b.RouteUrl,
                         add= b.Add,
                         edit=b.Edit,
                         delete=b.Delete,
                         view=b.View
                        }).ToList();
                        // authentication successful so generate jwt token
                        var tokenHandler = new JwtSecurityTokenHandler();
                        var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
                        var tokenDescriptor = new SecurityTokenDescriptor
                        {
                            Subject = new ClaimsIdentity(new Claim[]
                            {
                         new Claim(ClaimTypes.Name, usermaster.Pk_UserId.ToString()),
                         new Claim(ClaimTypes.Role, usermaster.RoleName),
                          new Claim("Access", Newtonsoft.Json.JsonConvert.SerializeObject( d)),
                            }),
                            Expires = DateTime.UtcNow.AddDays(1),
                            SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
                        };
                        var token = tokenHandler.CreateToken(tokenDescriptor);
                        usermaster.Token = tokenHandler.WriteToken(token);
                        usermaster.Password = null;
                        result.Message = "Login Successfully";
                        result.StatusCode = 1;
                        result.Result = "success";
                        result.IsValid = true;
                        result.Data = usermaster;
                        result.pk_ID = usermaster.Pk_UserId;
                    }
                    else
                    {
                        result.Message = "Password is not invalid";
                        result.StatusCode = 0;
                        result.Result = "error";
                        result.IsValid = false;
                    }
                }
                else
                {
                    result.Message = "UserName is not invalid";
                    result.StatusCode = 0;
                    result.Result = "error";
                    result.IsValid = false;
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.Result = "error";
                result.IsValid = false;
            }
            return result;
        }

        public ResponseviewModel InsertUpdateUser(UserMasterViewModel usermasterviewmodel)
        {

            try
            {
                if (usermasterviewmodel.Pk_UserId == null)
                {
                    UserMaster checkusernameexits = new UserMaster();
                    checkusernameexits = _Context.UserMaster.Where(d => d.UserName.ToLower().Trim() == usermasterviewmodel.UserName.ToLower().Trim() && d.IsActive == true && d.IsDelete == false).FirstOrDefault();
                    if (checkusernameexits == null)
                    {
                        UserMaster usermaster = new UserMaster();
                        usermaster.Pk_UserId = new Guid();
                        usermaster.FirstName = usermasterviewmodel.FirstName;
                        usermaster.LastName = usermasterviewmodel.LastName;
                        usermaster.EmailID = usermasterviewmodel.LastName;
                        usermaster.UserName = usermasterviewmodel.UserName;
                        usermaster.ContactNumber = usermasterviewmodel.ContactNumber;
                        usermaster.FK_RoleID = usermasterviewmodel.FK_RoleID;
                        usermaster.RoleName = usermasterviewmodel.RoleName;
                        usermaster.IsActive = true;
                        usermaster.IsDelete = false;
                        usermaster.CreatedBy = null;
                        usermaster.ModifiedBy = null;
                        usermaster.ModifiedDate = System.DateTime.Now;
                        usermaster.CreatedDate = System.DateTime.Now;
                        usermaster.Password = passwordHasher.HashPassword(usermaster, usermasterviewmodel.Password);
                        _Context.UserMaster.Add(usermaster);
                        _Context.SaveChanges();
                        result.Message = "User Registered Successfully";
                        result.StatusCode = 1;
                        result.IsValid = true;
                        result.Result = "success";
                        result.pk_ID = usermaster.Pk_UserId;
                        result.Data = usermaster;
                    }
                    else
                    {
                        result.Message = "Record does not exits";
                        result.StatusCode = 0;
                        result.Result = "error";
                        result.IsValid = false;
                    }
                }
                else
                {
                    UserMaster checkusermaster = new UserMaster();
                    checkusermaster = _Context.UserMaster.Where(d => d.Pk_UserId == usermasterviewmodel.Pk_UserId).FirstOrDefault();
                    if (checkusermaster != null)
                    {
                        checkusermaster.FirstName = usermasterviewmodel.FirstName;
                        checkusermaster.LastName = usermasterviewmodel.LastName;
                        checkusermaster.EmailID = usermasterviewmodel.LastName;
                        checkusermaster.UserName = usermasterviewmodel.UserName;
                        checkusermaster.ContactNumber = usermasterviewmodel.ContactNumber;
                        checkusermaster.FK_RoleID = usermasterviewmodel.FK_RoleID;
                        checkusermaster.RoleName = usermasterviewmodel.RoleName;
                        checkusermaster.ModifiedBy = null;
                        checkusermaster.ModifiedDate = System.DateTime.Now;
                        // checkusermaster.Password = passwordHasher.HashPassword(checkusermaster, usermasterviewmodel.Password);
                        _Context.UserMaster.Update(checkusermaster);
                        _Context.SaveChanges();
                        result.Message = "User Registered Successfully";
                        result.StatusCode = 1;
                        result.IsValid = true;
                        result.Result = "success";
                        result.pk_ID = checkusermaster.Pk_UserId;
                        result.Data = checkusermaster;

                    }
                    else
                    {
                        result.Message = "Record does not exits";
                        result.StatusCode = 0;
                        result.Result = "error";
                        result.IsValid = false;
                    }
                }
            }

            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.Result = "error";
                result.IsValid = false;
            }
            return result;
        }

        // Change Password
        public ResponseviewModel ChangePassword(ChangePasswordViewModel modal)
        {

            try
            {
                UserMaster usermaster = new UserMaster();
                var oldpassword = _Context.UserMaster.Where(a => a.Pk_UserId == modal.Userid).FirstOrDefault().Password;
                usermaster = _Context.UserMaster.Where(d => d.Pk_UserId == modal.Userid).FirstOrDefault();
                //  var passHas = passwordHasher.HashPassword(usermaster, modal.OldPassword);
                var isOldVerify = passwordHasher.VerifyHashedPassword(usermaster, oldpassword, modal.OldPassword).ToString();
                if (isOldVerify.ToLower() == "success")
                {
                   
                    

                        usermaster.Password = passwordHasher.HashPassword(usermaster, modal.NewPassword);
                        _Context.SaveChanges();
                        result.Message = "Password Changed Successfully";
                        result.StatusCode = 1;
                        result.IsValid = true;
                        result.Result = "success";
                        result.pk_ID = usermaster.Pk_UserId;
                   
                }
                else
                {
                    result.Message = "Please Enter Valid Old Password";
                    result.StatusCode = 0;
                    result.Result = "error";
                    result.IsValid = false;
                }
            }

            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.Result = "error";
                result.IsValid = false;
            }
            return result;
        }


        public ResponseviewModel GetAllUser()
        {
            try
            {
                List<UserMasterViewModel> usermasterviewmodellist = new List<UserMasterViewModel>();
                usermasterviewmodellist = _Context.UserMaster.Where(d => d.IsActive == true && d.IsDelete == false).Select(x => new UserMasterViewModel
                {
                    Pk_UserId = x.Pk_UserId,
                    FirstName = x.FirstName,
                    LastName = x.LastName,
                    UserName = x.UserName,
                    EmailID = x.EmailID,
                    ContactNumber = x.ContactNumber,
                    RoleName = x.RoleName,
                    IsActive = x.IsActive,
                    IsDelete = x.IsDelete,
                    CreatedDate = x.CreatedDate,
                    ModifiedDate = x.ModifiedDate
                }).ToList();
                if (usermasterviewmodellist.Count > 0)
                {
                    result.Message = "Record get successfully";
                    result.StatusCode = 1;
                    result.Result = "success";
                    result.IsValid = true;
                    result.Data = usermasterviewmodellist;
                }
                else
                {
                    result.Message = "Record does not exits";
                    result.StatusCode = 0;
                    result.Result = "error";
                    result.IsValid = false;
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.Result = "error";
                result.IsValid = false;
            }
            return result;
        }

        public ResponseviewModel GetUserDetailById(Guid id)
        {
            try
            {
                UserMasterViewModel usermasterviewmodel = new UserMasterViewModel();
                usermasterviewmodel = _Context.UserMaster.Where(d => d.Pk_UserId == id && d.IsActive == true && d.IsDelete == false).Select(x => new UserMasterViewModel
                {
                    Pk_UserId = x.Pk_UserId,
                    FirstName = x.FirstName,
                    LastName = x.LastName,
                    UserName = x.UserName,
                    EmailID = x.EmailID,
                    ContactNumber = x.ContactNumber,
                    RoleName = x.RoleName,
                    IsActive = x.IsActive,
                    IsDelete = x.IsDelete,
                    CreatedDate = x.CreatedDate,
                    ModifiedDate = x.ModifiedDate
                }).FirstOrDefault();
                if (usermasterviewmodel != null)
                {
                    result.Message = "Record get successfully";
                    result.StatusCode = 1;
                    result.Result = "success";
                    result.IsValid = true;
                    result.Data = usermasterviewmodel;
                }
                else
                {
                    result.Message = "Record does not exits";
                    result.StatusCode = 0;
                    result.Result = "error";
                    result.IsValid = false;
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.Result = "error";
                result.IsValid = false;
            }
            return result;
        }

        public ResponseviewModel DeleteUserById(UserMasterViewModel useru)
        {
            try
            {
                UserMaster usermasterviewmodel = new UserMaster();
                usermasterviewmodel = _Context.UserMaster.Where(d => d.Pk_UserId == useru.Pk_UserId && d.IsActive == true && d.IsDelete == false).FirstOrDefault();
                if (usermasterviewmodel != null)
                {
                    usermasterviewmodel.IsDelete = true;
                    usermasterviewmodel.ModifiedBy = useru.ModifiedBy;
                    _Context.UserMaster.Update(usermasterviewmodel);
                    _Context.SaveChanges();
                    result.Message = "Record deleted successfully";
                    result.StatusCode = 1;
                    result.Result = "success";
                    result.IsValid = true;
                    result.Data = usermasterviewmodel;
                }
                else
                {
                    result.Message = "Record does not exits";
                    result.StatusCode = 0;
                    result.Result = "error";
                    result.IsValid = false;
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.Result = "error";
                result.IsValid = false;
            }
            return result;
        }
        public ResponseviewModel GetDDLRoleType()
        {
            try
            {
                List<DropdownViewModel> listUserTypeDDL = new List<DropdownViewModel>();
                listUserTypeDDL = _Context.RoleMaster.Where(d => d.IsActive == true && d.IsDelete == false).Select(x => new DropdownViewModel
                {
                    ID = x.Pk_RoleMasterID,
                    Value = x.RoleName
                }).ToList();
                if (listUserTypeDDL.Count > 0)
                {
                    result.Message = "Record get successfully";
                    result.StatusCode = 1;
                    result.Result = "success";
                    result.IsValid = true;
                    result.Data = listUserTypeDDL;
                }
                else
                {
                    result.Message = "Record does not exits";
                    result.StatusCode = 0;
                    result.Result = "error";
                    result.IsValid = false;
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.Result = "error";
                result.IsValid = false;
            }
            return result;
        }

    }
}
