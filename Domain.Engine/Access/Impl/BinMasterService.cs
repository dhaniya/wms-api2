﻿using Domain.Engine.Access.Interfaces;
using Domain.Objects.Models;
using Infrastructure.Model.Main;
using Infrastructure.Model.Main.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Domain.Engine.Access.Impl
{
    public class BinMasterService : IBinMasterService
    {
        protected readonly MainDbContext _Context;
        ResponseviewModel result = new ResponseviewModel();
        protected MainDbContext Context
        {
            get
            {
                return (MainDbContext)_Context;
            }
        }

        public BinMasterService(IMainDbContext context)
        {
            _Context = (MainDbContext)context;
        }

        public ResponseviewModel InsertBinMaster(List<BinMasterModel> binmastermodel)
        {
            try
            {
                BinMaster bin = new BinMaster();
                bin = _Context.BinMaster.Where(d => d.RackName.ToLower().Trim() == binmastermodel.FirstOrDefault().RackName.ToLower().Trim() && d.ColumnNo == binmastermodel.FirstOrDefault().ColumnNo && d.RowNo == binmastermodel.FirstOrDefault().RowNo).FirstOrDefault();
                if (bin == null)
                {
                    foreach (var item in binmastermodel)
                    {
                        LocationMaster locationmaster = new LocationMaster();
                        string oldlocationcode = item.RackName + "-" + item.ColumnNo + "-" + item.RowNo;
                        locationmaster = _Context.LocationMaster.Where(d => d.LocationCode.Trim().ToLower() == oldlocationcode.Trim().ToLower() && d.ZoneType == 2).FirstOrDefault();
                        if (locationmaster != null)
                        {
                            BinMaster binmasters = new BinMaster();
                            binmasters.RackName = item.RackName;
                            binmasters.LocationCode = item.LocationCode;
                            binmasters.RowNo = item.RowNo;
                            binmasters.ColumnNo = item.ColumnNo;
                            binmasters.IsUsed = false;
                            binmasters.IsActive = true;
                            binmasters.IsDelete = false;
                            binmasters.CreatedBy = item.CreatedBy;
                            binmasters.CreatedDate = System.DateTime.Now;
                            binmasters.ModifiedDate = System.DateTime.Now;
                            _Context.Add(binmasters);
                        }
                        else
                        {
                            result.Message = "This Location in not available";
                            result.StatusCode = 0;
                            result.IsValid = false;
                            result.Result = "error";
                            result.pk_ID = null;
                            result.Data = null;
                            return result;
                        }
                    }
                    _Context.SaveChanges();
                    result.Message = "Data inserted successfully";
                    result.StatusCode = 1;
                    result.IsValid = true;
                    result.Result = "success";
                    result.Data = binmastermodel;
                }
                else
                {
                    result.Message = "Rack name already exists.!";
                    result.StatusCode = 0;
                    result.IsValid = false;
                    result.Result = "error";
                    result.pk_ID = null;
                    result.Data = null;
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.pk_ID = null;
                result.Data = null;

            }
            return result;
        }

        public ResponseviewModel GetAllBinMasterList()
        {
            try
            {
                List<BinMaster> listbin = new List<BinMaster>();
                listbin = _Context.BinMaster.Where(d => d.IsActive == true && d.IsDelete == false).OrderBy(x => x.LocationCode).ToList();
                if (listbin.Count > 0)
                {
                    result.Message = "Record Get Successfully";
                    result.StatusCode = 1;
                    result.IsValid = true;
                    result.Result = "success";
                    result.Data = listbin;
                }
                else
                {
                    result.Message = "Record does not exists.!";
                    result.StatusCode = 0;
                    result.IsValid = false;
                    result.Result = "error";
                    result.pk_ID = null;
                    result.Data = null;
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.pk_ID = null;
                result.Data = null;
            }
            return result;
        }

        public ResponseviewModel GetRackName()
        {
            try
            {
                var RackNameList = _Context.BinMaster.Where(d => d.IsActive == true && d.IsDelete == false).OrderBy(x => x.RackName).Select(x => new StringDropdownviewModel
                {
                    ID = x.RackName,
                    Value = x.RackName,
                }).Distinct().ToList();
                if (RackNameList.Count > 0)
                {
                    result.Message = "Record Get Successfully";
                    result.StatusCode = 1;
                    result.IsValid = true;
                    result.Result = "success";
                    result.Data = RackNameList;
                }
                else
                {
                    result.Message = "Record does not exists";
                    result.StatusCode = 0;
                    result.IsValid = false;
                    result.Result = "error";
                    result.pk_ID = null;
                    result.Data = null;

                }
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.pk_ID = null;
                result.Data = null;

            }
            return result;
        }

        public ResponseviewModel GetDLLOfBinLocationWithLocationTransfer()
        {
            try
            {
                List<StringDropdownviewModel> MasterBinLocationDropDownList = new List<StringDropdownviewModel>();
                List<LocationMaster> BinLocationMaster = new List<LocationMaster>();
                BinLocationMaster = _Context.LocationMaster.Where(d => d.ZoneType == 2 && d.IsActive == true && d.IsDelete == false).ToList();
                if (BinLocationMaster.Count > 0)
                {
                    foreach (var item in BinLocationMaster)
                    {
                        StringDropdownviewModel BinLocationDLL = new StringDropdownviewModel();
                        List<BinMaster> availablebinlocation = new List<BinMaster>();
                        availablebinlocation = _Context.BinMaster.Where(d => d.RackName.ToLower().Trim() == item.RackName.ToLower().Trim()).ToList();
                        if (availablebinlocation.Count > 0)
                        {
                            if (item.LocationCode.Trim() == (availablebinlocation.FirstOrDefault().RackName + "-" + availablebinlocation.FirstOrDefault().RowNo + "-" + availablebinlocation.FirstOrDefault().ColumnNo).Trim().ToString())
                            {
                                var BinLocationlist = availablebinlocation.Select(x => new StringDropdownviewModel
                                {
                                    ID = x.LocationCode,
                                    Value = x.LocationCode
                                }).ToList();
                                if (BinLocationlist.Count > 0)
                                    MasterBinLocationDropDownList.AddRange(BinLocationlist);
                            }
                            else
                            {
                                BinLocationDLL.ID = item.LocationCode;
                                BinLocationDLL.Value = item.LocationCode;
                                MasterBinLocationDropDownList.Add(BinLocationDLL);
                            }
                        }
                        else
                        {
                            BinLocationDLL.ID = item.LocationCode;
                            BinLocationDLL.Value = item.LocationCode;
                            MasterBinLocationDropDownList.Add(BinLocationDLL);
                        }
                    }
                    if (MasterBinLocationDropDownList.Count > 0)
                    {
                        result.Message = "Record get successfully.!";
                        result.StatusCode = 1;
                        result.IsValid = true;
                        result.Result = "success";
                        result.Data = MasterBinLocationDropDownList;
                    }
                    else
                    {
                        result.Message = "Record does not exits.!";
                        result.StatusCode = 0;
                        result.IsValid = false;
                        result.Result = "error";
                        result.pk_ID = null;
                        result.Data = null;
                    }
                }
                else
                {
                    result.Message = "Record does not exits.!";
                    result.StatusCode = 0;
                    result.IsValid = false;
                    result.Result = "error";
                    result.pk_ID = null;
                    result.Data = null;

                }
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.pk_ID = null;
                result.Data = null;
            }
            return result;
        }
        public ResponseviewModel Deleteitemtransfer(Guid id)
        {
            try
            {
                List<LocationMaster> BinLocationMaster = new List<LocationMaster>();
                var itemtransfer = _Context.BinLocationHistory.Where(a => a.PK_BinLocationHistoryId == id).FirstOrDefault();
                if (itemtransfer != null)
                {
                    itemtransfer.IsActive = false;
                    itemtransfer.IsDelete = true;
                    itemtransfer.ModifiedDate = System.DateTime.Now;
                    _Context.SaveChanges();
                    result.Message = "Record Deleted Successfully";
                        result.StatusCode = 1;
                        result.IsValid = true;
                        result.Result = "success";
                        result.Data = itemtransfer;
                    }
                    else
                    {
                        result.Message = "Record does not exits";
                        result.StatusCode = 0;
                        result.IsValid = false;
                        result.Result = "error";
                        result.pk_ID = null;
                        result.Data = null;
                    }
                
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
    }
}
