﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Domain.Engine.Access.Interfaces;
using Domain.Objects.Models;
using Infrastructure.Model.Main;
using Infrastructure.Model.Main.Entities;

namespace Domain.Engine.Access.Impl
{
    public class LocationTransferService : ILocationTransferService
    {
        protected readonly MainDbContext _Context;
        private readonly ILocationMasterService _locationmasterservice;

        ResponseviewModel result = new ResponseviewModel();
        protected MainDbContext Context
        {
            get
            {
                return (MainDbContext)_Context;
            }
        }

        public LocationTransferService(IMainDbContext context, ILocationMasterService locationmasterservice)
        {
            _Context = (MainDbContext)context;
            _locationmasterservice = locationmasterservice;
        }

        public ResponseviewModel GetLocationTransferlist(searchlocationtransfer searchlocationtransfer)
        {
            try
            {
                List<LocationTransferModel> locationtransfermodellist = new List<LocationTransferModel>();
                locationtransfermodellist = _Context.SortingDetail.Where(d => d.SortingMaster.IsPutaway == true && d.IsActive == true && d.IsDelete == false).OrderByDescending(k => k.ModifiedDate).Select(a => new LocationTransferModel
                {
                    LocationID = a.SortingMaster.LocationID,
                    RepakedCaseNo = a.SortingMaster.RepakedCaseNo,
                    Pk_SortingID = a.SortingMaster.Pk_SortingID,
                    Height = a.Height,
                    Weight = a.Weight,
                    Length = a.Length,
                    Breadth = a.Breadth
                }).ToList().GroupBy(k => k.RepakedCaseNo).Select(c => new LocationTransferModel
                {
                    LocationID = c.FirstOrDefault().LocationID,
                    Pk_SortingID = c.FirstOrDefault().Pk_SortingID,
                    Height = c.FirstOrDefault().Height,
                    Weight = c.FirstOrDefault().Weight,
                    Length = c.FirstOrDefault().Length,
                    Breadth = c.FirstOrDefault().Breadth,
                    RepakedCaseNo = c.Key
                }).ToList();

                if (searchlocationtransfer.RepakedCaseNo != null && searchlocationtransfer.LocationID != null)
                {
                    locationtransfermodellist = locationtransfermodellist.Where(d => d.RepakedCaseNo.ToLower().Trim() == searchlocationtransfer.RepakedCaseNo.ToLower().Trim() && d.LocationID.ToLower().Trim() == searchlocationtransfer.LocationID.ToLower().Trim()).ToList();
                }
                else if (searchlocationtransfer.RepakedCaseNo != null)
                {
                    locationtransfermodellist = locationtransfermodellist.Where(d => d.RepakedCaseNo.ToLower().Trim() == searchlocationtransfer.RepakedCaseNo.ToLower().Trim()).ToList();
                }
                else if (searchlocationtransfer.LocationID != null)
                {
                    locationtransfermodellist = locationtransfermodellist.Where(d => d.LocationID.ToLower().Trim() == searchlocationtransfer.LocationID.ToLower().Trim()).ToList();
                }

                if (locationtransfermodellist != null)
                {
                    result.Message = "Record Get Successfully";
                    result.StatusCode = 1;
                    result.IsValid = true;
                    result.Result = "success";
                    result.Data = locationtransfermodellist;
                }
                else
                {
                    result.Message = "Record does not exists";
                    result.StatusCode = 0;
                    result.IsValid = false;
                    result.Result = "error";
                    result.Data = null;
                }

            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.Data = null;
            }
            return result;
        }

        public ResponseviewModel GetDropdownofRepackedCaseNo()
        {
            try
            {
                var ddlrepackedcasenolist = _Context.SortingDetail.Where(d => d.SortingMaster.IsPutaway == true && d.IsActive == true && d.IsDelete == false).OrderBy(k => k.RepakedCaseNo).Select(a => new StringDropdownviewModel
                {
                    ID = a.SortingMaster.RepakedCaseNo,
                    Value = a.SortingMaster.RepakedCaseNo,
                }).Distinct().ToList();
                if (ddlrepackedcasenolist.Count > 0)
                {
                    result.Message = "Record Get Successfully";
                    result.StatusCode = 1;
                    result.IsValid = true;
                    result.Result = "success";
                    result.Data = ddlrepackedcasenolist;
                }
                else
                {
                    result.Message = "Record does not exists";
                    result.StatusCode = 0;
                    result.IsValid = false;
                    result.Result = "error";
                    result.Data = null;
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.Data = null;
            }
            return result;
        }

        public ResponseviewModel GetDropdownofLocationId()
        {
            try
            {
                var ddllocationidlist = _Context.SortingMaster.Where(d => d.IsPutaway == true && d.IsActive == true && d.IsDelete == false).OrderBy(k => k.LocationID).Select(a => new StringDropdownviewModel
                {
                    ID = a.LocationID,
                    Value = a.LocationID,
                }).Distinct().ToList();
                if (ddllocationidlist.Count > 0)
                {
                    result.Message = "Record Get Successfully";
                    result.StatusCode = 1;
                    result.IsValid = true;
                    result.Result = "success";
                    result.Data = ddllocationidlist;
                }
                else
                {
                    result.Message = "Record does not exists";
                    result.StatusCode = 0;
                    result.IsValid = false;
                    result.Result = "error";
                    result.Data = null;
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.Data = null;
            }
            return result;
        }

        public ResponseviewModel UpdateLocationBysortingid(UpdateLocationTransferModel updatelocationtransfermodel)
        {
            try
            {

                var sortingmaster = _Context.SortingMaster.Where(d => d.RepakedCaseNo.ToLower().Trim() == updatelocationtransfermodel.RepakedCaseNo.ToLower().Trim() && d.IsPutaway == true && d.IsActive == true && d.IsDelete == false && d.IsBin != true && d.Quantity > 0).ToList();

                foreach (var item in sortingmaster)

                {
                    bool UnUsedLocation = _locationmasterservice.ChangeUnusedByLocationCode(item.LocationID);
                    //var change1= _Context.LocationMaster.Where(d=>d.LocationCode.ToLower().Trim()==LocationCode.ToLower().Trim()).FirstOrDefault();

                    item.LocationID = updatelocationtransfermodel.NewLocationID;
                    item.ModifiedBy = updatelocationtransfermodel.ModifiedBy;
                    item.ModifiedDate = System.DateTime.Now;
                    _Context.Update(item);
                    _Context.SaveChanges();
                    //if(!_Context.StockTransaction.Where(k=>k.SortingMaster.LocationID== updatelocationtransfermodel.cu && k.SortingMaster.IsBin!=true && k.SortingMaster.IsDelete==false && k.QuantityRemaining>0 ).Any())

                    bool UsedLocation = _locationmasterservice.ChangeActiveByLocationCode(updatelocationtransfermodel.NewLocationID);

                    // }
                    result.Message = "Location updated successfully";
                    result.StatusCode = 1;
                    result.IsValid = true;
                    result.Result = "success";
                    result.Data = null;


                }
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.Data = null;
            }
            return result;
        }


    }
}
