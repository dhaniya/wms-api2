﻿using Domain.Engine.Access.Interfaces;
using Domain.Objects.Models;
using Infrastructure.Model.Main;
using Infrastructure.Model.Main.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Domain.Engine.Access.Impl
{
  public  class UserAccessService: IUserAccessService
    {
        protected readonly MainDbContext _Context;
        ResponseviewModel result = new ResponseviewModel();
        protected MainDbContext Context
        {
            get
            {
                return (MainDbContext)_Context;
            }
        }

        public UserAccessService(IMainDbContext context)
        {
            _Context = (MainDbContext)context;
        }

        public ResponseviewModel GetListRoutes()
        {
            result.Message = "Data Get Successfully";
            result.StatusCode = 1;
            result.IsValid = true;
            result.Result = "success";
            
            result.Data = new CommonViewModel().Routes().ToList();
            return result;
        }

        public ResponseviewModel AddUserAccess(List<UserAccessViewModel> model)
        {
            try
            {
               
                    var checkexist = _Context.UserAccess.Where(ua => ua.UserId == model.FirstOrDefault().UserId ).ToList();

                    if (checkexist.Count>0)
                    {
                        _Context.UserAccess.RemoveRange(checkexist);
                        _Context.SaveChanges();
                       
                       
                       

                    }
                foreach (var item in model)
                {
                    if (item.Add==true || item.Edit==true || item.Delete==true|| item.View==true)
                    {
                        UserAccess userAccess = new UserAccess();
                        userAccess.Add = item.Add;
                        userAccess.Delete = item.Delete;
                        userAccess.Edit = item.Edit;
                        userAccess.RoleName = item.RoleName;
                        userAccess.View = item.View;
                        userAccess.Pk_UserAccessId = new Guid();
                        userAccess.CreatedDate = System.DateTime.Now;
                        userAccess.IsDeleted = false;
                        userAccess.RouteUrl = item.RouteUrl;
                        userAccess.UserId = item.UserId;
                        _Context.UserAccess.Add(userAccess);
                    }
                 
                   
                }
                _Context.SaveChanges();
                result.Message = "Data Added Successfully";
                        result.StatusCode = 1;
                        result.IsValid = true;
                        result.Result = "success";
                       
                        result.Data = model;
                    

               

            }
            catch (Exception ex)
            {

                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.pk_ID = null;
                result.Data = null;
            }
            return result;
        }

        public ResponseviewModel GetUserAccessByid(Guid userId)
        {
            var getUserAccess = _Context.UserAccess.Where(ua => ua.UserId == userId).Select(u => new UserAccessViewModel
            {
                Add = u.Add,
                 Delete= u.Delete,
                  UserId= u.UserId.Value,
                   Edit= u.Edit,
                   View=u.View,
                    IsDeleted= u.IsDeleted,
                     Pk_UserAccessId= u.Pk_UserAccessId,
                      RoleName= u.RoleName,
                       RouteUrl= u.RouteUrl
                       

            

            }).ToList();
            result.Message = "Data Get Successfully";
            result.StatusCode = 1;
            result.IsValid = true;
            result.Result = "success";
          
            result.Data = getUserAccess;
            return result;
        }
    }
}
