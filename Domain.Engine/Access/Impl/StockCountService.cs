﻿using Domain.Engine.Access.Interfaces;
using Domain.Objects.Models;
using Infrastructure.Model.Main;
using Infrastructure.Model.Main.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Domain.Engine.Access.Impl
{

    public class StockCountService : IStockCountService
    {
        protected readonly MainDbContext _Context;
        ResponseviewModel result = new ResponseviewModel();
        private readonly IStockCountService _StockCountService;
        protected MainDbContext Context
        {
            get
            {
                return (MainDbContext)_Context;
            }
        }

        public StockCountService(IMainDbContext context)
        {

            _Context = (MainDbContext)context;
        }

        public ResponseviewModel GetStockCountList()
        {
            try
            {
                List<StockCountView> liststockcount = new List<StockCountView>();
                liststockcount = _Context.StockCountTab.Include(a => a.UserMaster.Pk_UserId).OrderByDescending(x => x.ModifiedDate).Select(a => new StockCountView
                {
                    Pk_StockCountID = a.Pk_StockCountID,
                    Location = a.Location,
                    ItemCode = a.ItemCode,
                    RunningNo = a.RunningNo,
                    Qty = a.Qty,
                    UserName = a.UserMaster.FirstName,
                    CreatedDate = a.CreatedDate

                }).ToList();
                if (liststockcount.Count > 0)
                {

                    result.Message = "Record Get Successfully";
                    result.StatusCode = 1;
                    result.IsValid = true;
                    result.Result = "success";
                    result.Data = liststockcount;
                }
                else
                {
                    result.Message = "Record does not exists";
                    result.StatusCode = 0;
                    result.IsValid = false;
                    result.Result = "error";
                    result.Data = null;
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.pk_ID = null;
                result.Data = null;
            }
            return result;
        }

        public ResponseviewModel InsertUpdateStockCount(StockCountModel stockcountmodel)
        {
            try
            {
                if (stockcountmodel.Pk_StockCountID == Guid.Empty)
                {
                    StockCount stockcountadd = new StockCount();
                    stockcountadd.Pk_StockCountID = Guid.NewGuid();
                    stockcountadd.Location = stockcountmodel.Location;
                    stockcountadd.ItemCode = stockcountmodel.ItemCode;
                    stockcountadd.RunningNo = stockcountmodel.RunningNo;
                    stockcountadd.Qty = stockcountmodel.Qty;
                    stockcountadd.CreatedBy = stockcountmodel.CreatedBy;
                    stockcountadd.CreatedDate = System.DateTime.Now;
                    stockcountadd.ModifiedBy = stockcountmodel.CreatedBy;
                    stockcountadd.ModifiedDate = System.DateTime.Now;
                    _Context.AddAsync(stockcountadd);
                    _Context.SaveChanges();

                    result.Message = "Data Inserted Successfully";
                    result.StatusCode = 1;
                    result.IsValid = true;
                    result.Result = "success";
                    result.pk_ID = stockcountadd.Pk_StockCountID;
                    result.Data = stockcountadd;
                }
                else
                {
                    StockCount sortincountupdate = _Context.StockCountTab.FirstOrDefault(d => d.Pk_StockCountID == stockcountmodel.Pk_StockCountID);
                    if (sortincountupdate != null)
                    {
                        sortincountupdate.Location = stockcountmodel.Location;
                        sortincountupdate.ItemCode = stockcountmodel.ItemCode;
                        sortincountupdate.RunningNo = stockcountmodel.RunningNo;
                        sortincountupdate.Qty = stockcountmodel.Qty;
                        sortincountupdate.ModifiedDate = System.DateTime.Now;
                        sortincountupdate.ModifiedBy = stockcountmodel.ModifiedBy;
                        _Context.Update(sortincountupdate);
                        _Context.SaveChanges();

                        result.Message = "Data Updated Successfully";
                        result.StatusCode = 1;
                        result.IsValid = true;
                        result.Result = "success";
                        result.pk_ID = sortincountupdate.Pk_StockCountID;
                        result.Data = sortincountupdate;


                    }
                    else
                    {
                        result.Message = "Record does not exists";
                        result.StatusCode = 0;
                        result.IsValid = false;
                        result.Result = "error";
                        result.pk_ID = sortincountupdate.Pk_StockCountID;
                        result.Data = null;
                    }
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.pk_ID = null;
                result.Data = null;
            }
            return result;
        }

        public ResponseviewModel DeleteStockCountbyID(StockCountModel stockd)
        {
            try
            {
                StockCount stock = _Context.StockCountTab.FirstOrDefault(d => d.Pk_StockCountID == stockd.Pk_StockCountID);
                if (stock != null)
                {
                    _Context.StockCountTab.Remove(stock);


                    _Context.SaveChanges();

                    result.Message = "Data Deleted Successfully";
                    result.StatusCode = 1;
                    result.IsValid = true;
                    result.Result = "success";
                    result.pk_ID = stock.Pk_StockCountID;
                }
                else
                {
                    result.Message = "Record does not exists";
                    result.StatusCode = 0;
                    result.IsValid = false;
                    result.Result = "error";
                    result.pk_ID = stock.Pk_StockCountID;
                    result.Data = null;
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.pk_ID = null;
                result.Data = null;
            }
            return result;
        }

        public ResponseviewModel GetRunningNumber()
        {
            try
            {
                int runningnumber = 1;
                if (_Context.StockCountTab.Any())
                {
                    runningnumber = _Context.StockCountTab.Max(d => d.RunningNo) + 1;
                }


                result.Message = " Running Number";
                result.StatusCode = 1;
                result.IsValid = true;
                result.Data = runningnumber;

            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "success";
                result.pk_ID = null;
                result.Data = null;
            }
            return result;
        }

        public ResponseviewModel GetStockDiscrepancyData(StockCountFilter filterdata)
        {
            try
            {
                List<StockCountQtyview> Data = (from st in _Context.StockCountTab
                                                join pm in _Context.PartsMaster on st.ItemCode equals pm.PartNumber
                                                where st.RunningNo == (filterdata.RunningNo > 0 ? filterdata.RunningNo : st.RunningNo)
                                                group new { st.ItemCode, st.RunningNo, st.Location, pm.PartUnitPrice, pm.PartDescription, st.Qty, st.CreatedDate } by new { st.ItemCode } into g
                                                let fd = g.FirstOrDefault()
                                                let ItemCode = g.FirstOrDefault().ItemCode
                                                let Location = g.FirstOrDefault().Location
                                                let Partdes = g.FirstOrDefault().PartDescription
                                                let qty = g.Sum(a => a.Qty)
                                                let Date = g.FirstOrDefault().CreatedDate
                                                let Rn = g.FirstOrDefault().RunningNo
                                                select new StockCountQtyview
                                                {
                                                    CountQuantityNumber = qty,
                                                    ItemCode = ItemCode,
                                                    Location = Location,
                                                    PartDescripation = Partdes,
                                                    Date = Date,
                                                    stockCount = Rn
                                                }).ToList();

                //List<StockCountQtyview> Data = _Context.StockCountTab.Where(a => a.RunningNo == (filterdata.RunningNo > 0 ? filterdata.RunningNo : a.RunningNo))
                //.GroupBy(p => new { p.Location, p.ItemCode })
                //.Select(g => new StockCountQtyview
                //{
                //    StockQty = g.Sum(a => a.Qty),
                //    ItemCode = g.Key.ItemCode,
                //    Location = g.Key.Location,
                //}).ToList();
                if (Data.Count > 0)
                {
                    foreach (var item in Data)
                    {
                        var qty = _Context.StockTransaction.Where(d => d.SortingMaster.ItemCode == item.ItemCode && d.SortingMaster.LocationID == item.Location && d.SortingMaster.IsDelete == false).Sum(d => d.QuantityRemaining);
                        var price = _Context.PartsMaster.Where(d => d.PartNumber == item.ItemCode).Select(d => d.PartUnitPrice).FirstOrDefault();
                        item.SystemQuantity = qty;
                        var EQty = item.CountQuantityNumber - qty;
                        if (EQty < 0)
                        {
                            item.ShortQty = EQty;
                            if (item.ShortQty < 0)
                            {
                                item.EXTcost = item.ShortQty * price;
                            }
                        }
                        else
                        {
                            item.ExcessQty = EQty;
                            item.EXTcost = item.ExcessQty * price;

                        }
                        item.cost = item.ExcessQty * price;
                    }

                    result.Message = "Record Get Successfully";
                    result.StatusCode = 1;
                    result.IsValid = true;
                    result.Data = Data;
                }

                else
                {
                    result.Message = "Record does not exists";
                    result.StatusCode = 0;
                    result.IsValid = false;
                    result.Data = null;
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.pk_ID = null;
                result.Data = null;
            }
            return result;
        }

        public ResponseviewModel GetAllRunningNumber()
        {
            try
            {
                List<StockCountFilter> Data = _Context.StockCountTab.GroupBy(a => new { a.RunningNo }).OrderBy(a => a.Key.RunningNo)
                 .Select(g => new StockCountFilter
                 { RunningNo = g.Key.RunningNo }).ToList();
                if (Data.Count > 0)
                {
                    result.Message = "Record Get Successfully";
                    result.StatusCode = 1;
                    result.IsValid = true;
                    result.Data = Data;
                }
                else
                {
                    result.Message = "Record does not exists";
                    result.StatusCode = 0;
                    result.IsValid = false;
                    result.Result = "error";
                    result.Data = null;
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.Data = null;
            }
            return result;
        }

    }
}
