using Domain.Engine.Access.Interfaces;
using Domain.Objects.Models;
using Infrastructure.Model.Main;
using Infrastructure.Model.Main.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static Domain.Objects.Models.ResponseviewModel;

namespace Domain.Engine.Access.Impl
{
    public class BinLocationService : IBinLocationService
    {
        protected readonly MainDbContext _Context;
        ResponseviewModel result = new ResponseviewModel();
        private readonly ISortingMasterService _SortingMasterService;
        private readonly IBarCodeService _BarCodeService;
        private readonly ISortingDetailService _SortingDetailService;
        private readonly IStockTransactionService _stockTransactionService;
        private readonly IPartsMasterService _partsMasterService;
        private Func<BinLocationModel, string, object> sortFunc;
        protected MainDbContext Context
        {
            get
            {
                return (MainDbContext)_Context;
            }
        }

        public BinLocationService(IMainDbContext context, ISortingMasterService sortingmasterservice, IBarCodeService barcodeservice, ISortingDetailService sortingdetailservide, IStockTransactionService stockTransactionService, IPartsMasterService partsMasterService)
        {
            _Context = (MainDbContext)context;
            _SortingMasterService = sortingmasterservice;
            _BarCodeService = barcodeservice;
            _SortingDetailService = sortingdetailservide;
            _stockTransactionService = stockTransactionService;
            _partsMasterService = partsMasterService;
        }
        public ResponseviewModel GetDDLRepackedCaseNo()
        {
            try
            {
                List<StringDropdownviewModel> DDLRepackCaseno = new List<StringDropdownviewModel>();
                DDLRepackCaseno = _Context.SortingMaster.Where(d => d.IsActive == true && d.IsDelete == false).OrderByDescending(k => k.ModifiedDate)
                .Select(x => new StringDropdownviewModel
                {
                    ID = x.RepakedCaseNo,
                    Value = x.RepakedCaseNo
                }).Distinct().ToList();
                if (DDLRepackCaseno.Count > 0)
                {
                    result.Message = "Record Get Successfully";
                    result.StatusCode = 1;
                    result.IsValid = true;
                    result.Result = "success";
                    result.Data = DDLRepackCaseno;
                }
                else
                {
                    result.Message = "Record does not exists";
                    result.StatusCode = 0;
                    result.IsValid = false;
                    result.Result = "error";
                    result.Data = null;
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.pk_ID = null;
                result.Data = null;
            }
            return result;
        }
        public ResponseviewModel GetDDLItemCodeByRepackedcaseNo(string repackedcaseno)
        {
            try
            {
                List<StringDropdownviewModel> DDLItemCode = new List<StringDropdownviewModel>();
                DDLItemCode = _Context.SortingMaster.Where(d => d.RepakedCaseNo.ToLower().Trim() == repackedcaseno.ToLower().Trim() && d.IsActive == true && d.IsDelete == false).OrderByDescending(k => k.ModifiedDate)
                .Select(x => new StringDropdownviewModel
                {
                    ID = x.ItemCode,
                    Value = x.ItemCode
                }).Distinct().ToList();
                if (DDLItemCode.Count > 0)
                {
                    result.Message = "Record Get Successfully";
                    result.StatusCode = 1;
                    result.IsValid = true;
                    result.Result = "success";
                    result.Data = DDLItemCode;
                }
                else
                {
                    result.Message = "Record does not exists";
                    result.StatusCode = 0;
                    result.IsValid = false;
                    result.Result = "error";
                    result.Data = null;
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.pk_ID = null;
                result.Data = null;
            }
            return result;
        }
        public ResponseviewModel GetNoofPiecesAndQtyByItemCode(string RepackedCaseNo, string ItemCode)
        {
            try
            {
                int AvailableQty = 0;
                int NoOfPieces = 0;
                var sortingdata = _Context.StockTransaction.Include(k => k.SortingMaster).Where(d => d.SortingMaster.RepakedCaseNo.ToLower().Trim() == RepackedCaseNo.ToLower().Trim() && d.SortingMaster.ItemCode.ToLower().Trim() == ItemCode.ToLower().Trim() && d.SortingMaster.IsActive == true && d.SortingMaster.IsDelete == false && d.SortingMaster.IsBin != true).ToList();
                AvailableQty = sortingdata.Sum(k => k.QuantityRemaining) ?? 0;
                NoOfPieces = _Context.SortingMaster.Where(d => d.RepakedCaseNo.ToLower().Trim() == RepackedCaseNo.ToLower().Trim() && d.ItemCode.ToLower().Trim() == ItemCode.ToLower().Trim() && d.IsActive == true && d.IsDelete == false).Sum(x => Convert.ToInt32(x.NoOfPieces));
                BinLocationDetailModel binlocationviewmodel = new BinLocationDetailModel();
                binlocationviewmodel.AvailableQty = AvailableQty;
                binlocationviewmodel.NoOfPieces = NoOfPieces;
                binlocationviewmodel.FK_SortingMaster_Id = sortingdata.FirstOrDefault().SortingMaster.Pk_SortingID;
                binlocationviewmodel.LocationId = sortingdata.FirstOrDefault().SortingMaster.LocationID;

                result.Message = "Record Get Successfully";
                result.StatusCode = 1;
                result.IsValid = true;
                result.Result = "success";
                result.Data = binlocationviewmodel;
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.pk_ID = null;
                result.Data = null;
            }
            return result;
        }

        public ResponseviewModel InsertBinLocation(BinLocationModel binlocationmodel)
        {
            try
            {
                var IsvalidLocation = false;
                if (!_Context.LocationMaster.Any(a => a.LocationCode == binlocationmodel.LocationCode.Trim() && a.IsDelete == false && a.LocationType == 2 && a.ZoneType == 2))
                {
                    IsvalidLocation = _Context.BinMaster.Any(a => a.LocationCode == binlocationmodel.LocationCode.Trim());
                }
                else
                {
                    IsvalidLocation = true;
                }


                if (IsvalidLocation)
                {
                    int AvailableQuantity = GetAvailableQuantity(binlocationmodel.ItemCode, binlocationmodel.RepackedCaseNo);
                    if (AvailableQuantity >= binlocationmodel.Quantity)
                    {
                        List<SortingMaster> sortingmaster = new List<SortingMaster>();
                        sortingmaster = _Context.SortingMaster.Where(d => d.RepakedCaseNo.ToLower().Trim() == binlocationmodel.RepackedCaseNo.ToLower().Trim() && d.ItemCode.ToLower().Trim() == binlocationmodel.ItemCode.ToLower().Trim() && d.Quantity != 0 && d.IsBin == false && d.IsDelete == false).OrderBy(k => k.CreatedDate).ToList();
                        if (sortingmaster.Count > 0)
                        {
                            int totalqty = binlocationmodel.Quantity;
                            bool checkqty = false;
                            foreach (var item in sortingmaster)
                            {
                                if (item.Quantity >= totalqty)
                                {
                                    item.Quantity = item.Quantity - totalqty;
                                    checkqty = true;
                                }
                                else
                                {
                                    totalqty = totalqty - item.Quantity;
                                    item.Quantity = 0;
                                }
                                item.IsBin = false;

                                _Context.Update(item);
                                _Context.SaveChanges();


                                //update for stock
                                StockTransactionModel stockTransactionModel = new StockTransactionModel();
                                stockTransactionModel.CreatedBy = item.CreatedBy;
                                stockTransactionModel.PK_SortingID = item.Pk_SortingID;
                                stockTransactionModel.QuantityReceived = item.Quantity;
                                // stockTransactionModel.QuantityRemaining = sortingmaster.Quantity - binlocationmodel.Quantity;
                                _stockTransactionService.InsertStockTransaction(stockTransactionModel);

                                BinItemHistory bindetail = new BinItemHistory();
                                bindetail.Pk_BinItemHistoryId = Guid.NewGuid();
                                bindetail.FK_SortingMasterID = item.Pk_SortingID;
                                bindetail.RepakedCaseNo = binlocationmodel.RepackedCaseNo;
                                bindetail.InvoiceNo = item.InvoiceNo;
                                bindetail.CaseNo = item.CaseNo;
                                bindetail.ItemCode = item.ItemCode.Trim();
                                bindetail.LocationID = binlocationmodel.LocationCode;
                                bindetail.Quantity = binlocationmodel.Quantity;
                                bindetail.IsActive = true;
                                bindetail.IsDelete = false;
                                bindetail.CreatedBy = binlocationmodel.CreatedBy;
                                bindetail.CreatedDate = System.DateTime.Now;
                                bindetail.ModifiedDate = System.DateTime.Now;
                                _Context.Add(bindetail);
                                _Context.SaveChanges();

                                if (checkqty == true)
                                {
                                    break;
                                }





                            }

                            SortingMaster binsortingmaster = new SortingMaster();
                            binsortingmaster = _Context.SortingMaster.Where(d => d.LocationID == binlocationmodel.LocationCode && d.ItemCode == binlocationmodel.ItemCode && d.IsBin == true && d.IsDelete == false && d.IsActive == true).FirstOrDefault();
                            if (binsortingmaster == null)
                            {
                                binsortingmaster = new SortingMaster();
                                binsortingmaster.Pk_SortingID = Guid.NewGuid();
                                binsortingmaster.NoOfPieces = binlocationmodel.NoOfPieces.ToString();
                                binsortingmaster.LocationID = binlocationmodel.LocationCode;
                                binsortingmaster.RepakedCaseNo = binlocationmodel.RepackedCaseNo;
                                binsortingmaster.Quantity = binlocationmodel.Quantity;
                                binsortingmaster.ItemCode = binlocationmodel.ItemCode;
                                binsortingmaster.IsPutaway = true;
                                binsortingmaster.IsBin = true;
                                binsortingmaster.SortingDescription = _partsMasterService.GetDetailsBynumber(binlocationmodel.ItemCode) == null ? "" : _partsMasterService.GetDetailsBynumber(binlocationmodel.ItemCode).PartDescription;
                                binsortingmaster.CaseNo = "";
                                binsortingmaster.InvoiceNo = "";
                                binsortingmaster.CustomerCode = "";
                                binsortingmaster.IsDelete = false;
                                binsortingmaster.IsActive = true;
                                binsortingmaster.CreatedBy = binlocationmodel.CreatedBy;
                                binsortingmaster.CreatedDate = System.DateTime.Now;
                                binsortingmaster.ModifiedBy = binlocationmodel.CreatedBy;
                                binsortingmaster.ModifiedDate = System.DateTime.Now;
                                _Context.AddAsync(binsortingmaster);
                                _Context.SaveChanges();

                            }
                            else
                            {

                                binsortingmaster.NoOfPieces = binlocationmodel.NoOfPieces.ToString();
                                // binsortingmaster.LocationID = binlocationmodel.LocationCode;

                                binsortingmaster.Quantity = binsortingmaster.Quantity + binlocationmodel.Quantity;
                                // binsortingmaster.ItemCode = binlocationmodel.ItemCode;

                                binsortingmaster.ModifiedBy = binlocationmodel.CreatedBy;
                                binsortingmaster.ModifiedDate = System.DateTime.Now;
                                _Context.Update(binsortingmaster);
                                _Context.SaveChanges();
                            }



                            //add new bin location
                            StockTransactionModel stockins = new StockTransactionModel();
                            stockins.CreatedBy = binsortingmaster.CreatedBy;
                            stockins.PK_SortingID = binsortingmaster.Pk_SortingID;
                            stockins.QuantityReceived = binsortingmaster.Quantity;
                            stockins.QuantityRemaining = binsortingmaster.Quantity;
                            _stockTransactionService.InsertStockTransaction(stockins);

                            //Insert Default data in sorting detail table
                            //SortingDetailModels sortingdetailmodel = new SortingDetailModels();
                            //sortingdetailmodel.FK_SortingMasterID = binsortingmaster.Pk_SortingID;
                            //sortingdetailmodel.RepakedCaseNo = sortingmaster.RepakedCaseNo;
                            //sortingdetailmodel.Height = 0;
                            //sortingdetailmodel.Weight = 0;
                            //sortingdetailmodel.Breadth = 0;
                            //sortingdetailmodel.Volume = 0;
                            //sortingdetailmodel.Weight = 0;
                            //_SortingDetailService.InsertUpdateSortingDetail(sortingdetailmodel);
                            //_Context.SaveChanges();

                            var location = _Context.LocationMaster.Where(a => a.LocationCode == binlocationmodel.LocationCode).FirstOrDefault();
                            if (location != null)
                            {
                                location.IsUsed = true;
                                location.ModifiedDate = System.DateTime.Now;
                                location.ModifiedBy = binlocationmodel.CreatedBy;
                                _Context.SaveChanges();
                            }
                            else
                            {
                                var binlocation = _Context.BinMaster.Where(a => a.LocationCode == binlocationmodel.LocationCode).FirstOrDefault();
                                if (binlocation != null)
                                {
                                    binlocation.IsUsed = true;
                                    binlocation.ModifiedDate = System.DateTime.Now;
                                    binlocation.ModifiedBy = binlocationmodel.CreatedBy;
                                    _Context.SaveChanges();
                                }
                            }

                            result.Message = "Data Inserted Successfully";
                            result.StatusCode = 1;
                            result.IsValid = true;
                            result.Result = "success";
                            result.pk_ID = Guid.Empty;
                            result.Data = sortingmaster;



                            result.Message = "Data Inserted Successfully";
                            result.StatusCode = 1;
                            result.IsValid = true;
                            result.Result = "success";
                            result.pk_ID = Guid.Empty;
                            result.Data = sortingmaster;


                        }



                        else
                        {
                            result.Message = "Record does not exits";
                            result.StatusCode = 0;
                            result.IsValid = false;
                            result.Result = "error";
                            result.pk_ID = null;
                            result.Data = null;
                        }
                    }
                    else
                    {
                        result.Message = "Quantity is not available";
                        result.StatusCode = 0;
                        result.IsValid = false;
                        result.Result = "error";
                        result.pk_ID = null;
                        result.Data = null;
                    }
                }
                else
                {
                    result.Message = "Location Invalid";
                    result.StatusCode = 0;
                    result.IsValid = false;
                    result.Result = "error";
                    result.pk_ID = null;
                    result.Data = null;
                }

            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.pk_ID = null;
                result.Data = null;
            }
            return result;
        }


        public ResponseviewModel GetSortedBinLocation(SortedBinLocationModel model)
        {
            try
            {
                PagedItemSet<BinLocationModel> pagedItems = new PagedItemSet<BinLocationModel>();

                List<BinLocationModel> finallistbinlocationmodel = new List<BinLocationModel>();
                IQueryable<BinLocationModel> listbinlocationmodel = _Context.StockTransaction.Include(k => k.SortingMaster).Where(d => ((d.QuantityRemaining > 0) || (d.QuantitySelected > 0)) && d.SortingMaster.IsPutaway == true && d.SortingMaster.IsBin == true && d.SortingMaster.IsDelete == false && d.SortingMaster.IsActive == true && (String.IsNullOrEmpty(model.search) ? true : (d.SortingMaster.RepakedCaseNo.Trim().ToLower() == model.search || d.SortingMaster.ItemCode.Trim().ToLower() == model.search || d.SortingMaster.LocationID.Trim().ToLower() == model.search))).OrderByDescending(k => k.CreatedDate).Select(x => new BinLocationModel
                {
                    ItemCode = x.SortingMaster.ItemCode,
                    Quantity = x.SortingMaster.Quantity,
                    LocationCode = x.SortingMaster.LocationID,
                    fk_sortingid = x.SortingMaster.Pk_SortingID,
                    UserName = x.SortingMaster.UserMaster.FirstName,
                    RepackedCaseNo = x.SortingMaster.RepakedCaseNo
                }).AsQueryable();
                //if (String.IsNullOrEmpty(model.SortColumnName) == false)
                //{
                //    listbinlocationmodel = (model.IsDescending.HasValue && model.IsDescending.Value) ?
                //        listbinlocationmodel.OrderByDescending(x => sortFunc(x, model.SortColumnName)) :
                //        listbinlocationmodel.OrderBy(x => sortFunc(x, model.SortColumnName));
                //}
                pagedItems.TotalResults = listbinlocationmodel.Count();
                if (model.Skip.HasValue)
                {
                    listbinlocationmodel = listbinlocationmodel.Skip(model.Skip.Value);
                }
                else
                {
                    listbinlocationmodel = listbinlocationmodel.Skip(0);
                }

                if (model.Take.HasValue)
                {
                    listbinlocationmodel = listbinlocationmodel.Take(model.Take.Value);
                }
                else
                {
                    listbinlocationmodel = listbinlocationmodel.Take(10);
                }
                finallistbinlocationmodel = listbinlocationmodel.ToList();
                //foreach (var item in listbinlocationmodel.ToList())
                //{
                //  int availableqty = _Context.StockTransaction.FirstOrDefault(k => k.PK_SortingID == item.fk_sortingid) != null ? _Context.StockTransaction.FirstOrDefault(k => k.PK_SortingID == item.fk_sortingid).QuantityRemaining.Value : 0;// GetBinAvailableQuantityByItemCode(item.ItemCode, item.LocationCode);
                //  if (availableqty > 0)
                //  {
                //    item.Quantity = availableqty;
                //    finallistbinlocationmodel.Add(item);
                //  }
                //}
                if (finallistbinlocationmodel.Count > 0)
                {
                    pagedItems.Items = finallistbinlocationmodel;
                    result.Message = "Record get successfully";
                    result.StatusCode = 1;
                    result.IsValid = true;
                    result.Result = "success";
                    result.Data = pagedItems;
                }
                else
                {
                    result.Message = "Record does not exits";
                    result.StatusCode = 0;
                    result.IsValid = false;
                    result.Result = "error";
                    result.pk_ID = null;
                    result.Data = null;
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.pk_ID = null;
                result.Data = null;
            }
            return result;
        }
        public ResponseviewModel GetSortedBinLocationExcel()
        {
            try
            {
                List<BinLocationModel> finallistbinlocationmodel = new List<BinLocationModel>();


                var InventorySearch = _Context.StockTransaction.Include(k => k.SortingMaster).Where(d => ((d.QuantityRemaining > 0) || (d.QuantitySelected > 0)) && d.SortingMaster.IsPutaway == true && d.SortingMaster.IsBin == true && d.SortingMaster.IsDelete == false && d.SortingMaster.IsActive == true).OrderByDescending(k => k.CreatedDate).Select(b => new BinLocationModelExcel
                {
                    ItemCode = b.SortingMaster.ItemCode,
                    Quantity = b.SortingMaster.Quantity,
                    LocationCode = b.SortingMaster.LocationID,
                    RepackedCaseNo = b.SortingMaster.RepakedCaseNo,
                  UserName=b.SortingMaster.UserMaster.FirstName
                    //Description = _partsMasterService.GetDetailsBynumber(b.SortingMaster.ItemCode).PartDescription,
                    // Dimention = b.SortingMaster.Length + " X " + b.Breadth + " X " + b.Height,
                    //CustomerCode = b.SortingMaster.CustomerCode,
                    //weight = _Context.SortingDetail.Where(p => p.FK_SortingMasterID == b.PK_SortingID).FirstOrDefault().Weight
                    // weight = b.Weight
                }).ToList();
               
                if (InventorySearch.Count > 0)
                {
                    result.Message = "Record Get Successfully";
                    result.StatusCode = 1;
                    result.IsValid = true;
                    result.Result = "success";
                    result.Data = InventorySearch;
                }
                else
                {
                    result.Message = "Record does not exists";
                    result.StatusCode = 0;
                    result.IsValid = false;
                    result.Result = "error";
                    result.Data = null;
                }
            }

            //    IQueryable<BinLocationModel> listbinlocationmodel = _Context.StockTransaction.Include(k => k.SortingMaster).Where(d => d.QuantityRemaining > 0 && d.SortingMaster.IsPutaway == true && d.SortingMaster.IsBin == true && d.SortingMaster.IsDelete == false && d.SortingMaster.IsActive == true).OrderByDescending(k => k.CreatedDate).Select(x => new BinLocationModel
            //    {
            //        ItemCode = x.SortingMaster.ItemCode,
            //        Quantity = x.SortingMaster.Quantity,
            //        LocationCode = x.SortingMaster.LocationID,
            //        fk_sortingid = x.SortingMaster.Pk_SortingID,
            //        UserName = x.SortingMaster.UserMaster.FirstName,
            //        RepackedCaseNo = x.SortingMaster.RepakedCaseNo
            //    }).ToList();
            //}
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.pk_ID = null;
                result.Data = null;
            }
            return result;
        }

        public ResponseviewModel GetSortedBinLocationItemCode(string itemcode)
        {
            try
            {
                List<BinLocationModel> listbinlocationmodel = new List<BinLocationModel>();
                List<BinLocationModel> finallistbinlocationmodel = new List<BinLocationModel>();
                listbinlocationmodel = _Context.SortingMaster.Where(d => d.IsPutaway == true && d.IsBin == true && d.IsDelete == false && d.IsActive == true && d.ItemCode.ToLower().Trim() == itemcode.ToLower().Trim()).OrderByDescending(k => k.CreatedDate).Select(x => new BinLocationModel
                {
                    ItemCode = x.ItemCode,
                    Quantity = x.Quantity,
                    LocationCode = x.LocationID,
                    fk_sortingid = x.Pk_SortingID
                }).Distinct().ToList();

                foreach (var item in listbinlocationmodel)
                {
                    int availableqty = _Context.StockTransaction.FirstOrDefault(k => k.PK_SortingID == item.fk_sortingid) != null ? _Context.StockTransaction.FirstOrDefault(k => k.PK_SortingID == item.fk_sortingid).QuantityRemaining.Value : 0;// GetBinAvailableQuantityByItemCode(item.ItemCode, item.LocationCode);
                    if (availableqty > 0)
                    {
                        item.Quantity = availableqty;
                        finallistbinlocationmodel.Add(item);
                    }
                }
                if (finallistbinlocationmodel.Count > 0)
                {
                    result.Message = "Record get successfully";
                    result.StatusCode = 1;
                    result.IsValid = true;
                    result.Result = "success";
                    result.Data = finallistbinlocationmodel;
                }
                else
                {
                    result.Message = "Record does not exits";
                    result.StatusCode = 0;
                    result.IsValid = false;
                    result.Result = "error";
                    result.pk_ID = null;
                    result.Data = null;
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.pk_ID = null;
                result.Data = null;
            }
            return result;
        }


        public ResponseviewModel GetBinAvailableQTYByItemCode(string itemcode, string locationid)
        {
            try
            {
                AvailableQuantityModel availableqtymodel = new AvailableQuantityModel();
                 //availableqtymodel.AvailableQty = GetBinAvailableQuantityByItemCode(itemcode, locationid);
                availableqtymodel.AvailableQty = _Context.StockTransaction.Where(k => k.SortingMaster.ItemCode == itemcode && k.SortingMaster.LocationID == locationid && k.SortingMaster.IsBin == true && k.SortingMaster.IsDelete == false).FirstOrDefault().QuantityRemaining;
                availableqtymodel.fk_sortingmasterid = _Context.SortingMaster.Where(k => k.ItemCode == itemcode && k.LocationID == locationid && k.IsBin == true && k.IsDelete == false).FirstOrDefault().Pk_SortingID;
                result.Message = "Record Get Sutrccessfully";
                result.StatusCode = 1;
                result.IsValid = true;
                result.Result = "success";
                result.Data = availableqtymodel;
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.pk_ID = null;
                result.Data = null;
            }
            return result;
        }

        public ResponseviewModel GetBinLocationByItemCode(string itemcode)
        {
            try
            {
                List<StringDropdownviewModel> stringddlmodel = new List<StringDropdownviewModel>();
                stringddlmodel = _Context.SortingMaster.Where(d => d.IsActive == true && d.IsDelete == false && d.IsPutaway == true && d.IsBin == true && d.ItemCode == itemcode).Select(x => new StringDropdownviewModel
                {
                    ID = x.LocationID,
                    Value = x.LocationID
                }).ToList();
                result.Message = "Record Get Sutrccessfully";
                result.StatusCode = 1;
                result.IsValid = true;
                result.Result = "success";
                result.Data = stringddlmodel;
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.pk_ID = null;
                result.Data = null;
            }
            return result;
        }

        public int GetBinAvailableQuantityByItemCode(string itemcode, string locationid)
        {
            int availablequantity = 0;
            try
            {
                int totalquantity = 0;
                int pickedbinquantity = 0;
                totalquantity = _Context.SortingMaster.Where(d => d.ItemCode.ToLower().Trim() == itemcode.ToLower().Trim() && d.LocationID.ToLower().Trim() == locationid.ToLower().Trim() && d.IsActive == true && d.IsBin == true && d.IsDelete == false).Sum(x => x.Quantity);
                pickedbinquantity = _Context.PickingDetail.Where(d => d.ItemCode.ToLower().Trim() == itemcode.ToLower().Trim() && d.LocationId.ToLower().Trim() == locationid.ToLower().Trim() && d.PickingType == 3 && d.IsActive == true && d.IsDelete == false).Sum(x => x.Quantity);
                //Remain to count sorted quantity
                availablequantity = totalquantity - pickedbinquantity;
                if (availablequantity <= 0)
                {
                    availablequantity = 0;
                }
            }
            catch (Exception)
            {
                availablequantity = 0;
            }
            return availablequantity;
        }


        public int GetAvailableQuantity(string itemcode, string caseno)
        {
            int availablequantity = 0;
            try
            {
                int totalquantity = 0;
                int binquantity = 0;
                totalquantity = _Context.StockTransaction.Where(st => st.SortingMaster.RepakedCaseNo.ToLower().Trim() == caseno.ToLower().Trim() && st.SortingMaster.ItemCode.ToLower().Trim() == itemcode.ToLower().Trim() && st.SortingMaster.IsDelete == false && st.SortingMaster.IsActive == true && st.SortingMaster.IsBin == false).Sum(a => a.QuantityRemaining).Value; // _Context.SortingMaster.Where(d => d.RepakedCaseNo.ToLower().Trim() == caseno.ToLower().Trim() && d.ItemCode.ToLower().Trim() == itemcode.ToLower().Trim() && d.IsActive == true && d.IsDelete == false).FirstOrDefault().Pk_SortingID;
                binquantity = 0;// _Context.SortingMaster.Where(d => d.RepakedCaseNo.ToLower().Trim() == caseno.ToLower().Trim() && d.ItemCode.ToLower().Trim() == itemcode.ToLower().Trim() && d.IsPutaway == true && d.IsBin == true && d.IsActive == true && d.IsDelete == false).Sum(x => x.Quantity);

                //Remain to count sorted quantity
                availablequantity = totalquantity - binquantity;
                if (availablequantity <= 0)
                {
                    availablequantity = 0;
                }
            }
            catch (Exception ex)
            {
                availablequantity = 0;
                throw ex;

            }
            return availablequantity;
        }

        public ResponseviewModel InsertBinLocationHistory(BinLocationHistoryModel binlocationhistorymodel)
        {
            try
            {


                if (binlocationhistorymodel.PK_BinLocationHistoryId == null)
                {
                    var already = _Context.BinLocationHistory.Where(bl => bl.FK_SortingMasterID == binlocationhistorymodel.FK_SortingMasterID && bl.Status == 0 && bl.IsDelete == false).FirstOrDefault();
                    if (already != null)
                    {
                        already.Quantity = already.Quantity + binlocationhistorymodel.Quantity;
                        _Context.BinLocationHistory.Update(already);
                        _Context.SaveChanges();
                        LocationTransferLog locationTransferLog = new LocationTransferLog();
                        locationTransferLog.LocationTransferLogID = new Guid();
                        locationTransferLog.CreatedDate = System.DateTime.Now;
                        locationTransferLog.FK_BinLocationHistoryId = already.PK_BinLocationHistoryId;
                        locationTransferLog.Fk_SortingMasterID = already.FK_SortingMasterID;
                        locationTransferLog.Location = binlocationhistorymodel.CurrentLocation;
                        locationTransferLog.Quantity = binlocationhistorymodel.Quantity;
                        locationTransferLog.Type = 0;
                        _Context.LocationTransferLog.Add(locationTransferLog);
                        _Context.SaveChanges();
                    }
                    else
                    {
                        BinLocationHistory binlocationhistory = new BinLocationHistory();

                        binlocationhistory.PK_BinLocationHistoryId = new Guid();
                        binlocationhistory.Quantity = binlocationhistorymodel.Quantity;
                        binlocationhistory.FK_SortingMasterID = binlocationhistorymodel.FK_SortingMasterID;
                        binlocationhistory.CurrentLocation = binlocationhistorymodel.CurrentLocation;
                        binlocationhistory.Status = 0;
                        binlocationhistory.Type = binlocationhistorymodel.Type;
                        binlocationhistory.IsDelete = false;
                        binlocationhistory.IsActive = true;
                        binlocationhistory.CreatedBy = binlocationhistorymodel.CreatedBy;
                        binlocationhistory.CreatedDate = System.DateTime.Now;
                        binlocationhistory.ModifiedBy = binlocationhistorymodel.CreatedBy;
                        binlocationhistory.ModifiedDate = System.DateTime.Now;
                        _Context.AddAsync(binlocationhistory);
                        _Context.SaveChanges();
                        result.Message = "Data Inserted Successfully";
                        result.StatusCode = 1;
                        result.IsValid = true;
                        result.Result = "success";
                        result.pk_ID = binlocationhistory.PK_BinLocationHistoryId;
                        result.Data = binlocationhistory;
                        LocationTransferLog locationTransferLog = new LocationTransferLog();
                        locationTransferLog.LocationTransferLogID = new Guid();
                        locationTransferLog.CreatedDate = System.DateTime.Now;
                        locationTransferLog.FK_BinLocationHistoryId = binlocationhistory.PK_BinLocationHistoryId;
                        locationTransferLog.Fk_SortingMasterID = binlocationhistory.FK_SortingMasterID;
                        locationTransferLog.Location = binlocationhistorymodel.CurrentLocation;
                        locationTransferLog.Quantity = binlocationhistorymodel.Quantity;
                        locationTransferLog.Type = 0;
                        _Context.LocationTransferLog.Add(locationTransferLog);
                        _Context.SaveChanges();
                    }
                }
                else
                {
                    var IsvalidLocation = false;
                    if (!_Context.LocationMaster.Any(a => a.LocationCode == binlocationhistorymodel.NewLocation.Trim() && a.IsDelete == false && a.LocationType == 2 && a.ZoneType == 2))
                    {
                        IsvalidLocation = _Context.BinMaster.Any(a => a.LocationCode == binlocationhistorymodel.NewLocation.Trim());
                    }
                    else
                    {
                        IsvalidLocation = true;
                    }
                    if (IsvalidLocation)
                    {
                        BinLocationHistory binlocationh = _Context.BinLocationHistory.FirstOrDefault(d => d.PK_BinLocationHistoryId == binlocationhistorymodel.PK_BinLocationHistoryId && d.IsActive == true && d.IsDelete == false);
                        if (binlocationh != null)
                        {
                            if (binlocationh.Quantity == binlocationhistorymodel.Quantity)
                            {
                                binlocationh.NewLocation = binlocationhistorymodel.NewLocation;
                                binlocationh.Status = 1;

                                binlocationh.ModifiedDate = System.DateTime.Now;
                                binlocationh.ModifiedBy = binlocationhistorymodel.ModifiedBy;
                                _Context.BinLocationHistory.Update(binlocationh);
                                _Context.SaveChanges();
                            }
                            if (binlocationh.Quantity > binlocationhistorymodel.Quantity)
                            {
                                binlocationh.Quantity = binlocationh.Quantity - binlocationhistorymodel.Quantity;


                                binlocationh.ModifiedDate = System.DateTime.Now;
                                binlocationh.ModifiedBy = binlocationhistorymodel.ModifiedBy;
                                _Context.BinLocationHistory.Update(binlocationh);
                                _Context.SaveChanges();
                            }
                            binlocationh = _Context.BinLocationHistory.AsNoTracking().FirstOrDefault(d => d.PK_BinLocationHistoryId == binlocationhistorymodel.PK_BinLocationHistoryId && d.IsActive == true && d.IsDelete == false);



                            LocationTransferLog locationTransferLog = new LocationTransferLog();
                            locationTransferLog.LocationTransferLogID = new Guid();
                            locationTransferLog.CreatedDate = System.DateTime.Now;
                            locationTransferLog.FK_BinLocationHistoryId = binlocationh.PK_BinLocationHistoryId;
                            locationTransferLog.Fk_SortingMasterID = binlocationh.FK_SortingMasterID;
                            locationTransferLog.Location = binlocationhistorymodel.NewLocation;
                            locationTransferLog.Quantity = binlocationhistorymodel.Quantity;
                            locationTransferLog.Type = 1;
                            _Context.LocationTransferLog.Add(locationTransferLog);
                            _Context.SaveChanges();


                            var sortingtab = _Context.SortingMaster.Where(a => a.Pk_SortingID == binlocationh.FK_SortingMasterID).FirstOrDefault();
                            if (sortingtab.Quantity == binlocationh.Quantity)
                            {
                                sortingtab.LocationID = binlocationhistorymodel.NewLocation;
                                sortingtab.ModifiedDate = System.DateTime.Now;
                                sortingtab.ModifiedBy = binlocationhistorymodel.ModifiedBy;
                                _Context.SortingMaster.Update(sortingtab);
                                _Context.SaveChanges();
                            }
                            else
                            {
                                sortingtab.Quantity = sortingtab.Quantity - binlocationhistorymodel.Quantity;
                                sortingtab.ModifiedDate = System.DateTime.Now;
                                sortingtab.ModifiedBy = binlocationhistorymodel.ModifiedBy;
                                //update for stock
                                StockTransactionModel stockTransactionModel = new StockTransactionModel();
                                stockTransactionModel.CreatedBy = sortingtab.CreatedBy;
                                stockTransactionModel.PK_SortingID = sortingtab.Pk_SortingID;
                                stockTransactionModel.QuantityReceived = sortingtab.Quantity;
                                // stockTransactionModel.QuantityRemaining = sortingmaster.Quantity - binlocationmodel.Quantity;
                                _stockTransactionService.InsertStockTransaction(stockTransactionModel);
                                _Context.SortingMaster.Update(sortingtab);

                                _Context.SaveChanges();

                                var sameavailable = _Context.SortingMaster.Where(d => d.LocationID == binlocationhistorymodel.NewLocation && d.ItemCode == sortingtab.ItemCode && d.IsBin == true && d.IsDelete == false && d.IsActive == true).FirstOrDefault();
                                if (sameavailable != null)
                                {
                                    sameavailable.Quantity = sameavailable.Quantity + binlocationhistorymodel.Quantity;

                                    StockTransactionModel stockTransactionModelnew = new StockTransactionModel();
                                    stockTransactionModelnew.CreatedBy = sortingtab.CreatedBy;
                                    stockTransactionModelnew.PK_SortingID = sameavailable.Pk_SortingID;
                                    stockTransactionModelnew.QuantityReceived = sameavailable.Quantity;
                                    // stockTransactionModel.QuantityRemaining = sortingmaster.Quantity - binlocationmodel.Quantity;
                                    _stockTransactionService.InsertStockTransaction(stockTransactionModelnew);
                                    _Context.SortingMaster.Update(sameavailable);
                                    _Context.SaveChanges();
                                }
                                else
                                {
                                    SortingMaster newSorting = new SortingMaster();
                                    newSorting.Pk_SortingID = new Guid();
                                    newSorting.Quantity = binlocationhistorymodel.Quantity;
                                    newSorting.CaseNo = sortingtab.CaseNo;
                                    newSorting.CustomerCode = sortingtab.CustomerCode;
                                    newSorting.Description = sortingtab.Description;
                                    newSorting.InvoiceNo = sortingtab.InvoiceNo;
                                    newSorting.IsActive = sortingtab.IsActive;
                                    newSorting.IsBin = sortingtab.IsBin;
                                    newSorting.IsDelete = sortingtab.IsDelete;
                                    newSorting.IsPutaway = sortingtab.IsPutaway;
                                    newSorting.ItemCode = sortingtab.ItemCode;
                                    newSorting.NoOfPieces = sortingtab.NoOfPieces;
                                    newSorting.RepakedCaseNo = sortingtab.RepakedCaseNo;
                                    newSorting.SortingDescription = sortingtab.SortingDescription;

                                    newSorting.LocationID = binlocationhistorymodel.NewLocation;
                                    newSorting.CreatedDate = System.DateTime.Now;
                                    newSorting.CreatedBy = binlocationhistorymodel.ModifiedBy;
                                    _Context.SortingMaster.Add(newSorting);
                                    _Context.SaveChanges();
                                    //add new row
                                    StockTransactionModel stockins = new StockTransactionModel();
                                    stockins.CreatedBy = sortingtab.CreatedBy;
                                    stockins.PK_SortingID = newSorting.Pk_SortingID;
                                    stockins.QuantityReceived = newSorting.Quantity;
                                    stockins.QuantityRemaining = newSorting.Quantity;
                                    _stockTransactionService.InsertStockTransaction(stockins);
                                }

                            }
                            var location = _Context.LocationMaster.Where(a => a.LocationCode.ToLower().Trim() == binlocationhistorymodel.NewLocation.ToLower().Trim()).FirstOrDefault();
                            if (location != null)
                            {
                                location.IsUsed = true;
                                location.ModifiedDate = System.DateTime.Now;
                                location.ModifiedBy = binlocationhistorymodel.ModifiedBy;
                                _Context.SaveChanges();
                            }
                            else
                            {
                                var binlocation = _Context.BinMaster.Where(a => a.LocationCode.ToLower().Trim() == binlocationhistorymodel.NewLocation.ToLower().Trim()).FirstOrDefault();
                                if (binlocation != null)
                                {
                                    binlocation.IsUsed = true;
                                    binlocation.ModifiedDate = System.DateTime.Now;
                                    binlocation.ModifiedBy = binlocationhistorymodel.ModifiedBy;
                                    _Context.SaveChanges();
                                }
                            }
                            if (!(_Context.StockTransaction.Any(st => st.QuantityRemaining > 0 && st.SortingMaster.LocationID == binlocationhistorymodel.CurrentLocation && st.SortingMaster.IsBin == true && st.SortingMaster.IsDelete == false && st.SortingMaster.IsActive == true)))
                            {
                                var oldlocation = _Context.LocationMaster.Where(a => a.LocationCode == binlocationhistorymodel.CurrentLocation).FirstOrDefault();
                                if (oldlocation != null)
                                {
                                    oldlocation.IsUsed = false;
                                    oldlocation.ModifiedDate = System.DateTime.Now;
                                    oldlocation.ModifiedBy = binlocationhistorymodel.ModifiedBy;
                                    _Context.SaveChanges();
                                }
                                else
                                {
                                    var oldbinlocation = _Context.BinMaster.Where(a => a.LocationCode == binlocationhistorymodel.CurrentLocation).FirstOrDefault();
                                    if (oldbinlocation != null)
                                    {
                                        oldbinlocation.IsUsed = false;
                                        oldbinlocation.ModifiedDate = System.DateTime.Now;
                                        oldbinlocation.ModifiedBy = binlocationhistorymodel.ModifiedBy;
                                        _Context.SaveChanges();
                                    }
                                }
                            }
                            //  _Context.SaveChanges();
                            result.Message = "Data Updated Successfully";
                            result.StatusCode = 1;
                            result.IsValid = true;
                            result.Result = "success";
                            result.pk_ID = binlocationh.PK_BinLocationHistoryId;
                            result.Data = binlocationh;
                        }
                        else
                        {
                            result.Message = "Record does not exists";
                            result.StatusCode = 0;
                            result.IsValid = false;
                            result.Result = "error";
                            result.pk_ID = binlocationhistorymodel.PK_BinLocationHistoryId;
                            result.Data = null;
                        }
                    }
                    else
                    {
                        result.Message = "Location Invalid";
                        result.StatusCode = 0;
                        result.IsValid = false;
                        result.Result = "error";
                        result.pk_ID = null;
                        result.Data = null;
                    }
                }



            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.pk_ID = null;
                result.Data = null;
            }
            return result;
        }
        public ResponseviewModel InsertBinLocationHistoryLooseItem(BinLocationHistoryModel binlocationhistorymodel)
        {
            try
            {

                if (binlocationhistorymodel.PK_BinLocationHistoryId == null)
                {
                    var already = _Context.BinLocationHistory.Where(bl => bl.FK_SortingMasterID == binlocationhistorymodel.FK_SortingMasterID && bl.Status == 0 && bl.IsDelete == false).FirstOrDefault();
                    if (already != null)
                    {
                        already.Quantity = already.Quantity + binlocationhistorymodel.Quantity;
                        _Context.BinLocationHistory.Update(already);
                        _Context.SaveChanges();
                        LooseItemTransferLog locationTransferLog = new LooseItemTransferLog();
                        locationTransferLog.LooseItemTransferLogID = new Guid();
                        locationTransferLog.CreatedDate = System.DateTime.Now;
                        locationTransferLog.FK_BinLocationHistoryId = already.PK_BinLocationHistoryId;
                        locationTransferLog.Fk_SortingMasterID = already.FK_SortingMasterID;
                        locationTransferLog.RePackCase = binlocationhistorymodel.CurrentLocation;
                        locationTransferLog.Quantity = binlocationhistorymodel.Quantity;
                        locationTransferLog.Type = 1;
                        _Context.LooseItemTransferLog.Add(locationTransferLog);
                        _Context.SaveChanges();
                    }
                    else
                    {
                        BinLocationHistory binlocationhistory = new BinLocationHistory();

                        binlocationhistory.PK_BinLocationHistoryId = new Guid();
                        binlocationhistory.Quantity = binlocationhistorymodel.Quantity;
                        binlocationhistory.FK_SortingMasterID = binlocationhistorymodel.FK_SortingMasterID;
                        binlocationhistory.CurrentLocation = binlocationhistorymodel.CurrentLocation;
                        binlocationhistory.Status = 0;
                        binlocationhistory.Type = 1;
                        binlocationhistory.IsDelete = false;
                        binlocationhistory.IsActive = true;
                        binlocationhistory.CreatedBy = binlocationhistorymodel.CreatedBy;
                        binlocationhistory.CreatedDate = System.DateTime.Now;
                        binlocationhistory.ModifiedBy = binlocationhistorymodel.CreatedBy;
                        binlocationhistory.ModifiedDate = System.DateTime.Now;
                        _Context.AddAsync(binlocationhistory);
                        _Context.SaveChanges();
                        result.Message = "Data Inserted Successfully";
                        result.StatusCode = 1;
                        result.IsValid = true;
                        result.Result = "success";
                        result.pk_ID = binlocationhistory.PK_BinLocationHistoryId;
                        result.Data = binlocationhistory;
                        LooseItemTransferLog locationTransferLog = new LooseItemTransferLog();
                        locationTransferLog.LooseItemTransferLogID = new Guid();
                        locationTransferLog.CreatedDate = System.DateTime.Now;
                        locationTransferLog.FK_BinLocationHistoryId = binlocationhistory.PK_BinLocationHistoryId;
                        locationTransferLog.Fk_SortingMasterID = binlocationhistory.FK_SortingMasterID;
                        locationTransferLog.RePackCase = binlocationhistorymodel.RePackCase;
                        locationTransferLog.Quantity = binlocationhistorymodel.Quantity;
                        locationTransferLog.Type = 0;
                        _Context.LooseItemTransferLog.Add(locationTransferLog);
                        _Context.SaveChanges();
                    }






                }
                else
                {
                    BinLocationHistory binlocationh = _Context.BinLocationHistory.FirstOrDefault(d => d.PK_BinLocationHistoryId == binlocationhistorymodel.PK_BinLocationHistoryId && d.IsActive == true && d.IsDelete == false);
                    if (binlocationh != null)
                    {
                        if (binlocationh.Quantity == binlocationhistorymodel.Quantity)
                        {
                            binlocationh.NewLocation = binlocationhistorymodel.RePackCase;
                            binlocationh.Status = 1;

                            binlocationh.ModifiedDate = System.DateTime.Now;
                            binlocationh.ModifiedBy = binlocationhistorymodel.ModifiedBy;
                            _Context.BinLocationHistory.Update(binlocationh);
                            _Context.SaveChanges();
                        }
                        if (binlocationh.Quantity > binlocationhistorymodel.Quantity)
                        {
                            binlocationh.Quantity = binlocationh.Quantity - binlocationhistorymodel.Quantity;


                            binlocationh.ModifiedDate = System.DateTime.Now;
                            binlocationh.ModifiedBy = binlocationhistorymodel.ModifiedBy;
                            _Context.BinLocationHistory.Update(binlocationh);
                            _Context.SaveChanges();
                        }
                        binlocationh = _Context.BinLocationHistory.AsNoTracking().FirstOrDefault(d => d.PK_BinLocationHistoryId == binlocationhistorymodel.PK_BinLocationHistoryId && d.IsActive == true && d.IsDelete == false);

                        LooseItemTransferLog locationTransferLog = new LooseItemTransferLog();
                        locationTransferLog.LooseItemTransferLogID = new Guid();
                        locationTransferLog.CreatedDate = System.DateTime.Now;
                        locationTransferLog.FK_BinLocationHistoryId = binlocationh.PK_BinLocationHistoryId;
                        locationTransferLog.Fk_SortingMasterID = binlocationh.FK_SortingMasterID;
                        locationTransferLog.RePackCase = binlocationhistorymodel.RePackCase;
                        locationTransferLog.Quantity = binlocationhistorymodel.Quantity;
                        locationTransferLog.Type = 1;
                        _Context.LooseItemTransferLog.Add(locationTransferLog);
                        _Context.SaveChanges();

                        var loc = _Context.StockTransaction.Include(k => k.SortingMaster).Where(a => a.SortingMaster.RepakedCaseNo == binlocationhistorymodel.RePackCase && a.QuantityRemaining > 0 && a.SortingMaster.IsBin != true && a.SortingMaster.IsDelete == false && a.SortingMaster.IsActive == true).FirstOrDefault();

                        var sortingtab = _Context.SortingMaster.Where(a => a.Pk_SortingID == binlocationh.FK_SortingMasterID).FirstOrDefault();

                        if (sortingtab.Quantity == binlocationh.Quantity)
                        {
                            sortingtab.RepakedCaseNo = binlocationhistorymodel.RePackCase;
                            sortingtab.LocationID = loc != null ? loc.SortingMaster.LocationID : sortingtab.LocationID;
                            sortingtab.ModifiedDate = System.DateTime.Now;
                            sortingtab.ModifiedBy = binlocationhistorymodel.ModifiedBy;
                            _Context.SortingMaster.Update(sortingtab);
                            _Context.SaveChanges();
                        }
                        else
                        {
                            sortingtab.Quantity = sortingtab.Quantity - binlocationhistorymodel.Quantity;
                            sortingtab.ModifiedDate = System.DateTime.Now;
                            sortingtab.ModifiedBy = binlocationhistorymodel.ModifiedBy;
                            //update for stock
                            StockTransactionModel stockTransactionModel = new StockTransactionModel();
                            stockTransactionModel.CreatedBy = sortingtab.CreatedBy;
                            stockTransactionModel.PK_SortingID = sortingtab.Pk_SortingID;
                            stockTransactionModel.QuantityReceived = sortingtab.Quantity;
                            // stockTransactionModel.QuantityRemaining = sortingmaster.Quantity - binlocationmodel.Quantity;
                            _stockTransactionService.InsertStockTransaction(stockTransactionModel);
                            _Context.SortingMaster.Update(sortingtab);

                            _Context.SaveChanges();

                            var sameavailable = _Context.SortingMaster.Where(d => d.RepakedCaseNo == binlocationhistorymodel.RePackCase && d.ItemCode == sortingtab.ItemCode && d.IsBin != true && d.IsDelete == false && d.IsActive == true).FirstOrDefault();
                            var all = _Context.SortingMaster.Where(d => d.RepakedCaseNo == binlocationhistorymodel.RePackCase && d.IsBin != true && d.IsDelete == false && d.IsActive == true).FirstOrDefault();
                            if (sameavailable != null)
                            {
                                sameavailable.Quantity = sameavailable.Quantity + binlocationhistorymodel.Quantity;

                                StockTransactionModel stockTransactionModelnew = new StockTransactionModel();
                                stockTransactionModelnew.CreatedBy = sortingtab.CreatedBy;
                                stockTransactionModelnew.PK_SortingID = sameavailable.Pk_SortingID;
                                stockTransactionModelnew.QuantityReceived = sameavailable.Quantity;
                                // stockTransactionModel.QuantityRemaining = sortingmaster.Quantity - binlocationmodel.Quantity;
                                _stockTransactionService.InsertStockTransaction(stockTransactionModelnew);
                                _Context.SortingMaster.Update(sameavailable);
                                _Context.SaveChanges();
                            }
                            else
                            {
                                SortingMaster newSorting = new SortingMaster();
                                newSorting.Pk_SortingID = new Guid();
                                newSorting.Quantity = binlocationhistorymodel.Quantity;
                                newSorting.CaseNo = sortingtab.CaseNo;
                                newSorting.CustomerCode = all.CustomerCode;
                                newSorting.Description = all.Description;
                                newSorting.InvoiceNo = all.InvoiceNo;
                                newSorting.IsActive = all.IsActive;
                                newSorting.IsBin = all.IsBin;
                                newSorting.IsDelete = all.IsDelete;
                                newSorting.IsPutaway = all.IsPutaway;
                                newSorting.ItemCode = sortingtab.ItemCode;
                                newSorting.NoOfPieces = all.NoOfPieces;
                                newSorting.LocationID = loc != null ? loc.SortingMaster.LocationID : "";
                                //if (loc != null)
                                //{
                                //    newSorting.IsPutaway = false;
                                //}
                                newSorting.LocationID = loc != null ? loc.SortingMaster.LocationID : "";
                                newSorting.SortingDescription = sortingtab.SortingDescription;
                                //newSorting.IsPutaway = false;


                                newSorting.RepakedCaseNo = binlocationhistorymodel.RePackCase;
                                newSorting.CreatedDate = System.DateTime.Now;
                                newSorting.CreatedBy = binlocationhistorymodel.ModifiedBy;
                                _Context.SortingMaster.Add(newSorting);
                                _Context.SaveChanges();


                                SortingDetail sd = new SortingDetail();
                                sd.Pk_SortingDetailID = new Guid();
                                sd.FK_SortingMasterID = newSorting.Pk_SortingID;
                                sd.Volume = 0;
                                sd.Height = 0;
                                sd.Weight = 0;
                                sd.RepakedCaseNo = sortingtab.RepakedCaseNo;
                                sd.IsActive = true;
                                sd.IsDelete = false;
                                sd.CreatedDate = System.DateTime.Now;
                                sd.CreatedBy = binlocationhistorymodel.ModifiedBy;
                                sd.ModifiedBy = binlocationhistorymodel.ModifiedBy;
                                sd.ModifiedDate = System.DateTime.Now;
                                _Context.SortingDetail.Add(sd);
                                _Context.SaveChanges();

                                //add new row
                                StockTransactionModel stockins = new StockTransactionModel();
                                stockins.CreatedBy = sortingtab.CreatedBy;
                                stockins.PK_SortingID = newSorting.Pk_SortingID;
                                stockins.QuantityReceived = newSorting.Quantity;
                                stockins.QuantityRemaining = newSorting.Quantity;
                                _stockTransactionService.InsertStockTransaction(stockins);
                            }
                        }





                        //  _Context.SaveChanges();
                        result.Message = "Data Updated Successfully";
                        result.StatusCode = 1;
                        result.IsValid = true;
                        result.Result = "success";
                        result.pk_ID = binlocationh.PK_BinLocationHistoryId;
                        result.Data = binlocationh;
                    }
                    else
                    {
                        result.Message = "Record does not exists";
                        result.StatusCode = 0;
                        result.IsValid = false;
                        result.Result = "error";
                        result.pk_ID = binlocationhistorymodel.PK_BinLocationHistoryId;
                        result.Data = null;
                    }
                }

            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.pk_ID = null;
                result.Data = null;
            }
            return result;
        }
        public ResponseviewModel getRePackbyCode(string customercode)
        {
            var already = _Context.StockTransaction.Where(st => st.SortingMaster.CustomerCode == customercode && st.SortingMaster.IsPutaway == true && st.SortingMaster.IsBin != true && st.QuantityRemaining > 0).Select(s => new
            {
                CaseNo = s.SortingMaster.RepakedCaseNo
            }).Distinct().ToList();
            result.Message = "Data get Successfully";
            result.StatusCode = 1;
            result.IsValid = true;
            result.Result = "success";

            result.Data = already;
            return result;
        }
        public ResponseviewModel GetBinPickedItem()
        {
            try
            {
                var GetbinPicking = _Context.BinLocationHistory.Where(k => k.Type == 0 && k.Status == 0 && k.IsActive == true && k.IsDelete == false).Select(b => new BinLocationHistoryModel
                {
                    FK_SortingMasterID = b.FK_SortingMasterID,
                    CurrentLocation = b.CurrentLocation,
                    itemcode = b.SortingMaster.ItemCode,
                    RePackCase = b.SortingMaster.RepakedCaseNo,
                    PK_BinLocationHistoryId = b.PK_BinLocationHistoryId,
                    Quantity = b.Quantity,
                    UserName = b.UserMaster.FirstName,
                    Type = b.Type

                }).OrderByDescending(a=>a.CreatedDate).ToList();

                result.Message = "Data Get Successfully";
                result.StatusCode = 1;
                result.IsValid = true;
                result.Result = "success";

                result.Data = GetbinPicking;


            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.pk_ID = null;
                result.Data = null;
            }
            return result;
        }
        public ResponseviewModel GetLoosePickedItem()
        {
            try
            {
                var GetbinPicking = _Context.BinLocationHistory.Where(k => k.Type == 1 && k.Status == 0 && k.IsActive == true && k.IsDelete == false).Select(b => new BinLocationHistoryModel
                {
                    FK_SortingMasterID = b.FK_SortingMasterID,
                    CurrentLocation = b.CurrentLocation,
                    itemcode = b.SortingMaster.ItemCode,
                    RePackCase = b.SortingMaster.RepakedCaseNo,
                    PK_BinLocationHistoryId = b.PK_BinLocationHistoryId,
                    Quantity = b.Quantity,
                    Type = b.Type,
                    //  CreatedBy = b.CreatedBy.HasValue ? b.CreatedBy.Value : Guid.Empty,
                    UserName = b.UserMaster.FirstName

                }).ToList();

                foreach (var item in GetbinPicking)
                {
                    var sortinm = _Context.SortingMaster.Where(sm => sm.Pk_SortingID == item.FK_SortingMasterID).FirstOrDefault();
                    item.customerCode = sortinm.CustomerCode;
                    item.CurrentLocation = sortinm.LocationID;

                }
                result.Message = "Data Get Successfully";
                result.StatusCode = 1;
                result.IsValid = true;
                result.Result = "success";

                result.Data = GetbinPicking;


            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.pk_ID = null;
                result.Data = null;
            }
            return result;
        }


        public ResponseviewModel GetBinItemLocationHistory(string locationId, string itemcode)
        {
            try
            {
                var Data = _Context.BinItemHistory.Where(st => st.LocationID == locationId && st.ItemCode == itemcode && st.IsActive == true && st.IsDelete == false).Select(s => new BinItemHistoryView
                {
                    RepakedCaseNo = s.RepakedCaseNo,
                    InvoiceNo = s.InvoiceNo,
                    CaseNo = s.CaseNo,
                    ItemCode = itemcode,
                    LocationID = s.LocationID,
                    Quantity = s.Quantity,
                    CreatedDate = s.CreatedDate
                }).ToList();

                if (Data.Count > 0)
                {
                    result.Message = "Data get Successfully";
                    result.StatusCode = 1;
                    result.IsValid = true;
                    result.Result = "success";
                    result.Data = Data;
                    return result;
                }
                else
                {
                    result.Message = "No record Found";
                    result.StatusCode = 0;
                    result.IsValid = false;
                    result.Result = "error";
                    result.Data = null;
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public ResponseviewModel DeleteBinItemLocationbyID(BinLocationHistoryModel binLocationHistory)
        {
            try
            {
                BinLocationHistory binlocation = _Context.BinLocationHistory.FirstOrDefault(d => d.PK_BinLocationHistoryId == binLocationHistory.PK_BinLocationHistoryId && d.IsActive == true && d.IsDelete == false);
                if (binlocation != null)
                {
                    binlocation.IsActive = false;
                    binlocation.IsDelete = true;
                    binlocation.ModifiedBy = binLocationHistory.ModifiedBy;
                    binlocation.ModifiedDate = System.DateTime.Now;
                    _Context.Update(binlocation);
                    _Context.SaveChanges();

                    result.Message = "Record Delete Successfully";
                    result.StatusCode = 1;
                    result.IsValid = true;
                    result.Result = "success";
                    result.Data = binlocation;
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.pk_ID = null;
                result.Data = null;
            }
            return result;
        }

        public ResponseviewModel DeleteBinInLocationbyID(SortingMasterModel sortingmaster)
        {
            try
            {
                SortingMaster sortingmasterdata = _Context.SortingMaster.FirstOrDefault(d => d.Pk_SortingID == sortingmaster.Pk_SortingID && d.IsActive == true && d.IsDelete == false);
                if (sortingmasterdata != null)
                {
                    sortingmasterdata.IsActive = false;
                    sortingmasterdata.IsDelete = true;
                    sortingmasterdata.ModifiedBy = sortingmaster.ModifiedBy;
                    sortingmasterdata.ModifiedDate = System.DateTime.Now;
                    _Context.Update(sortingmasterdata);
                    _Context.SaveChanges();

                    result.Message = "Record Delete Successfully";
                    result.StatusCode = 1;
                    result.IsValid = true;
                    result.Result = "success";
                    result.Data = sortingmasterdata;
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.pk_ID = null;
                result.Data = null;
            }
            return result;
        }
    }
}
