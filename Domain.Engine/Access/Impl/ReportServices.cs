using Domain.Engine.Access.Interfaces;
using Domain.Objects.Models;
using Infrastructure.Model.Main;
using Infrastructure.Model.Main.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore.Metadata;
using System.Data.SqlClient;
using Microsoft.Extensions.Options;

namespace Domain.Engine.Access.Impl
{
    public class ReportServices : IReportService
    {
        protected readonly MainDbContext _Context;
        protected readonly IPartsMasterService _partsMasterService;
        private readonly ConnectionStrings _connectionStrings;
        ResponseviewModel result = new ResponseviewModel();
        protected MainDbContext Context
        {
            get
            {
                return (MainDbContext)_Context;
            }
        }
        public ReportServices(IMainDbContext context, IPartsMasterService partsMasterService, IOptions<ConnectionStrings> connectionStrings)
        {
            _Context = (MainDbContext)context;
            _partsMasterService = partsMasterService;
            _connectionStrings = connectionStrings.Value;

        }

        public ResponseviewModel PartReportDetails(PartReportDetailsModel searchmodel)
        {
            try
            {
                PagedItemSet<PartReportDetailsModel> pagedItems = new PagedItemSet<PartReportDetailsModel>();
                var partReports = _Context.StockTransaction.Where(k => k.SortingMaster.IsDelete == false && k.SortingMaster.ItemCode == (searchmodel.itemcode != "" ? searchmodel.itemcode : k.SortingMaster.ItemCode) && k.QuantityRemaining == (searchmodel.qtyavailable > 0 ? searchmodel.qtyavailable : k.QuantityRemaining) && (k.QuantitySelected > 0 || k.QuantityRemaining > 0)).GroupBy(c => new { c.SortingMaster.ItemCode, c.SortingMaster.LocationID }).Select(b => new PartReportDetailsModel
                {
                    itemcode = b.Key.ItemCode,
                    qtyavailable = b.Sum(q => q.QuantityRemaining).HasValue ? b.Sum(q => q.QuantityRemaining).Value : 0,
                    qtyonhand = (b.Sum(q => q.QuantityReceived).HasValue ? b.Sum(q => q.QuantityReceived).Value : 0) - (b.Sum(q => q.QuantityDelivered).HasValue ? b.Sum(q => q.QuantityDelivered).Value : 0),
                    Netprice = _Context.PartsMaster.Where(a => a.PartNumber == b.Key.ItemCode).Select(a => a.PartUnitPrice).FirstOrDefault(),
                    qtyselected = b.Sum(q => q.QuantitySelected).HasValue ? b.Sum(q => q.QuantitySelected).Value : 0,
                    totaldeliver = (b.Sum(q => q.QuantityDelivered).HasValue ? b.Sum(q => q.QuantityDelivered).Value : 0),
                    totalrecived = (b.Sum(q => q.QuantityReceived).HasValue ? b.Sum(q => q.QuantityReceived).Value : 0),
                    itemdesc = b.FirstOrDefault().SortingMaster.SortingDescription,
                    location = b.Key.LocationID
                }).AsQueryable();
                pagedItems.TotalResults = partReports.Count();
                if (searchmodel.Skip.HasValue)
                {
                    partReports = partReports.Skip(searchmodel.Skip.Value);
                }
                else
                {
                    partReports = partReports.Skip(0);
                }

                if (searchmodel.Take.HasValue)
                {
                    partReports = partReports.Take(searchmodel.Take.Value);
                }
                else
                {
                    partReports = partReports.Take(10);
                }
                pagedItems.Items = partReports
                   .ToList();
                // partReports.ForEach(k => k.itemdesc = _partsMasterService.GetDetailsBynumber(k.itemcode) !=null ? _partsMasterService.GetDetailsBynumber(k.itemcode).PartDescription: "");
                if (pagedItems.TotalResults > 0)
                {
                    result.Message = "Record Get Successfully";
                    result.StatusCode = 1;
                    result.IsValid = true;
                    result.Result = "success";
                    result.Data = pagedItems;
                }
                else
                {
                    result.Message = "Record does not exists";
                    result.StatusCode = 0;
                    result.IsValid = false;
                    result.Result = "error";
                    result.Data = null;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        // Part Report Excel ExportPartReportDetailsModel
        public ResponseviewModel GetPartReportExcelExport(PartReportDetailsModelExcelExport searchmodel)
        {


            var partReports = _Context.StockTransaction.Where(k => k.SortingMaster.IsDelete == false && k.SortingMaster.ItemCode == (searchmodel.partnumber != null ? searchmodel.partnumber : k.SortingMaster.ItemCode) && k.QuantityRemaining == (searchmodel.qtyavailable > 0 ? searchmodel.qtyavailable : k.QuantityRemaining) && (k.QuantitySelected > 0 || k.QuantityRemaining > 0)).GroupBy(k => k.SortingMaster.ItemCode).
            Select(b => new PartReportDetailsModelExcelExport
            {
                partnumber = b.Key,
                qtyavailable = (b.Sum(q => q.QuantityRemaining).HasValue ? b.Sum(q => q.QuantityRemaining).Value : 0),
                qtyonhand = (b.Sum(q => q.QuantityReceived).HasValue ? b.Sum(q => q.QuantityReceived).Value : 0) - (b.Sum(q => q.QuantityDelivered).HasValue ? b.Sum(q => q.QuantityDelivered).Value : 0),
                Netprice = _Context.PartsMaster.Where(a => a.PartNumber == b.Key).Select(a => a.PartUnitPrice).FirstOrDefault(),
                qtyselected = b.Sum(q => q.QuantitySelected).HasValue ? b.Sum(q => q.QuantitySelected).Value : 0,
                totaldeliver = (b.Sum(q => q.QuantityDelivered).HasValue ? b.Sum(q => q.QuantityDelivered).Value : 0),
                totalrecived = (b.Sum(q => q.QuantityReceived).HasValue ? b.Sum(q => q.QuantityReceived).Value : 0),
                itemdesc = b.FirstOrDefault().SortingMaster.SortingDescription
            }).ToList();

            if (partReports.Count > 0)
            {
                result.Message = "Record Get Successfully";
                result.StatusCode = 1;
                result.IsValid = true;
                result.Result = "success";
                result.Data = partReports;
            }
            else
            {
                result.Message = "Record does not exists";
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.Data = null;
            }
            return result;

        }

        public ResponseviewModel Discrepancy(DiscrepancyFilterModel searchmodel)
        {
            PagedItemSet<DiscrepancyModel> pagedItems = new PagedItemSet<DiscrepancyModel>();

            var partReports = _Context.ASNItemDetails.Where(k => k.ASNMaster.InvoiceNo == (searchmodel.InvoiceNo != null ? searchmodel.InvoiceNo : k.ASNMaster.InvoiceNo) && k.IsDelete == false && k.ASNMaster.IsDelete==false).OrderBy(a => a.ASNMaster.InvoiceNo)
                .GroupBy(g => new { g.PalletNo, g.SuppliedPartNo, g.Customer, g.ASNMaster.InvoiceNo })
                .Select(s => new DiscrepancyModel
                {
                    PartNo = s.Key.SuppliedPartNo,
                    PartDescription = _partsMasterService.GetDetailsBynumber(s.Key.SuppliedPartNo.Trim()) != null ? _partsMasterService.GetDetailsBynumber(s.Key.SuppliedPartNo.Trim()).PartDescription : "",
                    customerCode = s.Key.Customer,
                    InvoiceNo = s.Key.InvoiceNo,
                    PalletNo = s.Key.PalletNo,
                    ReceivedQty = s.Sum(a => a.ReceivedQty),
                    ASNQty = s.Sum(a => a.Quantity),
                    Sortingqty = s.Sum(a => a.ReceivedQty)
                }).OrderByDescending(a => a.PalletNo).AsQueryable();
            pagedItems.TotalResults = partReports.Count();//
            if (searchmodel.Skip.HasValue)
            {
                partReports = partReports.Skip(searchmodel.Skip.Value);
            }
            else
            {
                partReports = partReports.Skip(0);
            }

            if (searchmodel.Take.HasValue)
            {
                partReports = partReports.Take(searchmodel.Take.Value);


            }
            else
            {
                partReports = partReports.Take(10);
            }
            pagedItems.Items = partReports
               .ToList();

            foreach (var item in pagedItems.Items)
            {
                item.DiscrepancyQty = item.ASNQty - item.Sortingqty;
            }

            // partReports.ForEach(k => k.itemdesc = _partsMasterService.GetDetailsBynumber(k.itemcode) !=null ? _partsMasterService.GetDetailsBynumber(k.itemcode).PartDescription: "");
            if (pagedItems.TotalResults > 0)
            {
                result.Message = "Record Get Successfully";
                result.StatusCode = 1;
                result.IsValid = true;
                result.Result = "success";
                result.Data = pagedItems;
            }
            else
            {
                result.Message = "Record does not exists";
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.Data = null;
            }
            return result;

        }

        // Asn Discrepancy Report Excel Export

        public ResponseviewModel DiscrepancyExcelExport(DiscrepancyFilterModel searchmodel)
        {
            var partReports = _Context.ASNItemDetails.Where(k => k.ASNMaster.InvoiceNo == (searchmodel.InvoiceNo != null ? searchmodel.InvoiceNo : k.ASNMaster.InvoiceNo)).OrderBy(a => a.ASNMaster.InvoiceNo)
               .GroupBy(g => new { g.PalletNo, g.SuppliedPartNo, g.Customer, g.ASNMaster.InvoiceNo })
               .Select(s => new DiscrepancyModel
               {
                   PartNo = s.Key.SuppliedPartNo,
                   PartDescription = _partsMasterService.GetDetailsBynumber(s.Key.SuppliedPartNo.Trim()) != null ? _partsMasterService.GetDetailsBynumber(s.Key.SuppliedPartNo.Trim()).PartDescription : "",
                   customerCode = s.Key.Customer,
                   InvoiceNo = s.Key.InvoiceNo,
                   PalletNo = s.Key.PalletNo,
                   ReceivedQty = s.Sum(a => a.ReceivedQty),
                   ASNQty = s.Sum(a => a.Quantity),
                   Sortingqty = s.Sum(a => a.ReceivedQty)
               }).ToList();

            foreach (var item in partReports)
            {
                item.DiscrepancyQty = item.ASNQty - item.Sortingqty;
            }

            // partReports.ForEach(k => k.itemdesc = _partsMasterService.GetDetailsBynumber(k.itemcode) !=null ? _partsMasterService.GetDetailsBynumber(k.itemcode).PartDescription: "");
            if (partReports.Count > 0)
            {
                result.Message = "Record Get Successfully";
                result.StatusCode = 1;
                result.IsValid = true;
                result.Result = "success";
                result.Data = partReports;
            }
            else
            {
                result.Message = "Record does not exists";
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.Data = null;
            }
            return result;

        }

        //Get Incoming Shipment Summary Report
        public ResponseviewModel GetIncomingShipmentSummaryReport(IncomingShipmentSummaryReportFilterModel searchmodel)
        {
            try
            {
                PagedItemSet<IncomingShipmentSummaryReportDetailsModel> pagedItems = new PagedItemSet<IncomingShipmentSummaryReportDetailsModel>();
                var SummaryReport = _Context.ASNItemDetails.Where(k => k.ContainerNo == (searchmodel.ContainerNo != "" ? searchmodel.ContainerNo : k.ContainerNo) && (k.ASNMaster.DateOfArrival.Date >= (searchmodel.FromDate != null ? searchmodel.FromDate : k.ASNMaster.DateOfArrival.Date) &&
                                                                  k.ASNMaster.DateOfArrival.Date <= (searchmodel.ToDate != null ? searchmodel.ToDate : k.ASNMaster.DateOfArrival.Date)) && k.ASNMaster.InvoiceNo == (searchmodel.InvoiceNo != "" ? searchmodel.InvoiceNo : k.ASNMaster.InvoiceNo) && k.ASNMaster.Remark == (searchmodel.ShippingRefNumber != null ? searchmodel.ShippingRefNumber : k.ASNMaster.Remark)).Select(b => new IncomingShipmentSummaryReportDetailsModel
                                                                  {
                                                                      InvoiceNo = b.ASNMaster.InvoiceNo,
                                                                      DateOfArrival = b.ASNMaster.DateOfArrival,
                                                                      VesselName = b.ASNMaster.VesselName,
                                                                      VesselVoyageNo = b.ASNMaster.VesselVoyageNo,
                                                                      ShippingRefNo = b.ASNMaster.Remark,
                                                                      ContainerNo = b.ContainerNo,
                                                                      PalletNo = b.PalletNo,
                                                                      PartNo = b.SuppliedPartNo,
                                                                      Description = _Context.PartsMaster.Where(a => a.PartNumber == b.SuppliedPartNo).Select(a => a.PartDescription).FirstOrDefault(),
                                                                      Quantity = b.Quantity,
                                                                      UnitPrice = b.UnitPrice,
                                                                      Customer = b.Customer,
                                                                      CustomerPO = b.PurchaseOrderNo,
                                                                      OrderNo = b.OrderNo,
                                                                      Length = b.Length,
                                                                      Breadth = b.Breadth,
                                                                      Height = b.Height,
                                                                      Weight = b.Weight,
                                                                      Volume = b.Breadth * b.Height * b.Height,
                                                                      CompanyName = _Context.CompanyMaster.Where(a => a.PK_CompanyID == b.ASNMaster.FK_CompanyID).Select(a => a.CompanyName).FirstOrDefault(),
                                                                      SupplierName = _Context.SupplierMaster.Where(a => a.Pk_SupplierID == b.ASNMaster.FK_SupplierID).Select(a => a.SupplierName).FirstOrDefault(),
                                                                      ShippingRefNumber = b.ASNMaster.Remark,
                                                                      RepackCaseNumber = _Context.SortingDetail.Where(a => a.SortingMaster.CaseNo == b.PalletNo && a.SortingMaster.InvoiceNo == b.ASNMaster.InvoiceNo && a.SortingMaster.CustomerCode == b.Customer).Select(a => a.RepakedCaseNo).FirstOrDefault(),
                                                                      Remark1 = b.Remark1,
                                                                      Remark2 = b.Remark2,
                                                                      Remark3 = b.Remark3
                                                                  }).OrderByDescending(a => a.RepackCaseNumber).AsQueryable();
                pagedItems.TotalResults = SummaryReport.Count();// _Context.ASNItemDetails.Where(k => k.ContainerNo == (searchmodel.ContainerNo != "" ? searchmodel.ContainerNo : k.ContainerNo) && k.ASNMaster.DateOfArrival.Date == (searchmodel.DateOfArrival != null ? searchmodel.DateOfArrival : k.ASNMaster.DateOfArrival.Date) && k.ASNMaster.InvoiceNo == (searchmodel.InvoiceNo != "" ? searchmodel.InvoiceNo : k.ASNMaster.InvoiceNo) && k.ASNMaster.Remark == (searchmodel.ShippingRefNumber != "" ? searchmodel.ShippingRefNumber : k.ASNMaster.Remark)).Count();
                if (searchmodel.Skip.HasValue)
                {
                    SummaryReport = SummaryReport.Skip(searchmodel.Skip.Value);
                }
                else
                {
                    SummaryReport = SummaryReport.Skip(0);
                }

                if (searchmodel.Take.HasValue)
                {
                    SummaryReport = SummaryReport.Take(searchmodel.Take.Value);
                }
                else
                {
                    SummaryReport = SummaryReport.Take(10);
                }
                pagedItems.Items = SummaryReport
                   .ToList();
                foreach (var item in pagedItems.Items)
                {
                    var h = item.Height.ToString();
                    var l = item.Length.ToString();
                    var b = item.Breadth.ToString();
                    if (h.Length > 2 || l.Length > 2 || b.Length > 2)
                    {
                        item.Height = item.Height / 100;
                        item.Length = item.Length / 100;
                        item.Breadth = item.Breadth / 100;
                        item.Volume = item.Height * item.Length * item.Breadth;
                    }
                }
                if (pagedItems.TotalResults > 0)
                {
                    result.Message = "Record Get Successfully";
                    result.StatusCode = 1;
                    result.IsValid = true;
                    result.Result = "success";
                    result.Data = pagedItems;
                }
                else
                {
                    result.Message = "Record does not exists";
                    result.StatusCode = 0;
                    result.IsValid = false;
                    result.Result = "error";
                    result.Data = null;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return result;

        }

        //Get Incoming Shipment Summary Report Excel Expoet
        public ResponseviewModel GetIncomingShipmentSummaryReportExcelExport(IncomingShipmentSummaryReportFilterExcelExportModel searchmodel)
        {
            try
            {
                var SummaryReport = _Context.ASNItemDetails.Where(k => k.ContainerNo == (searchmodel.ContainerNo != "" ? searchmodel.ContainerNo : k.ContainerNo) &&
                                                                (k.ASNMaster.DateOfArrival.Date >= (searchmodel.FromDate != null ? searchmodel.FromDate : k.ASNMaster.DateOfArrival.Date) &&
                                                                  k.ASNMaster.DateOfArrival.Date <= (searchmodel.ToDate != null ? searchmodel.ToDate : k.ASNMaster.DateOfArrival.Date)) &&
                                                                  k.ASNMaster.InvoiceNo == (searchmodel.InvoiceNo != "" ? searchmodel.InvoiceNo : k.ASNMaster.InvoiceNo) &&
                                                                  k.ASNMaster.Remark == (searchmodel.ShippingRefNumber != null ? searchmodel.ShippingRefNumber : k.ASNMaster.Remark)).Select(b => new IncomingShipmentReportExcelExportModel
                                                                  {
                                                                      InvoiceNo = b.ASNMaster.InvoiceNo,
                                                                      DateOfArrival = b.ASNMaster.DateOfArrival,
                                                                      VesselName = b.ASNMaster.VesselName,
                                                                      VesselVoyageNo = b.ASNMaster.VesselVoyageNo,
                                                                      ShippingRefNo = b.ASNMaster.Remark,
                                                                      ContainerNo = b.ContainerNo,
                                                                      PalletNo = b.PalletNo,
                                                                      PartNo = b.SuppliedPartNo,
                                                                      Description = _Context.PartsMaster.Where(a => a.PartNumber == b.SuppliedPartNo).Select(a => a.PartDescription).FirstOrDefault(),
                                                                      Quantity = b.Quantity,
                                                                      UnitPrice = b.UnitPrice,
                                                                      Customer = b.Customer,
                                                                      CustomerPO = b.PurchaseOrderNo,
                                                                      OrderNo = b.OrderNo,
                                                                      Length = b.Length,
                                                                      Breadth = b.Breadth,
                                                                      Height = b.Height,
                                                                      Weight = b.Weight,
                                                                      //Volume = b.Breadth * b.Height * b.Height,
                                                                      CompanyName = _Context.CompanyMaster.Where(a => a.PK_CompanyID == b.ASNMaster.FK_CompanyID).Select(a => a.CompanyName).FirstOrDefault(),
                                                                      SupplierName = _Context.SupplierMaster.Where(a => a.Pk_SupplierID == b.ASNMaster.FK_SupplierID).Select(a => a.SupplierName).FirstOrDefault(),
                                                                      ShippingRefNumber = b.ASNMaster.Remark,
                                                                      RepackCaseNumber = _Context.SortingDetail.Where(a => a.SortingMaster.CaseNo == b.PalletNo && a.SortingMaster.InvoiceNo == b.ASNMaster.InvoiceNo && a.SortingMaster.CustomerCode == b.Customer).Select(a => a.RepakedCaseNo).FirstOrDefault(),
                                                                      Remark1 = b.Remark1,
                                                                      Remark2 = b.Remark2,
                                                                      Remark3 = b.Remark3
                                                                  }).ToList();
                foreach (var item in SummaryReport)
                {
                    var h = item.Height.ToString();
                    var l = item.Length.ToString();
                    var b = item.Breadth.ToString();
                    if (h.Length > 2 || l.Length > 2 || b.Length > 2)
                    {
                        item.Height = item.Height / 100;
                        item.Length = item.Length / 100;
                        item.Breadth = item.Breadth / 100;
                    }
                }
                if (SummaryReport.Count > 0)
                {
                    result.Message = "Record Get Successfully";
                    result.StatusCode = 1;
                    result.IsValid = true;
                    result.Result = "success";
                    result.Data = SummaryReport;
                }
                else
                {
                    result.Message = "Record does not exists";
                    result.StatusCode = 0;
                    result.IsValid = false;
                    result.Result = "error";
                    result.Data = null;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return result;

        }

        //Get Shipment Report
        public ResponseviewModel GetShipmentReport(ShipmentReportFilterView searchmodel)
        {
            try
            {
                PagedItemSet<ShipmentReportView> pagedItems = new PagedItemSet<ShipmentReportView>();
                var ShipmentReport = _Context.ASNItemDetails.Where(k => k.ContainerNo == (searchmodel.ContainerNo != "" ? searchmodel.ContainerNo : k.ContainerNo) &&
                                                                  (k.ASNMaster.DateOfArrival.Date >= (searchmodel.FromDate != null ? searchmodel.FromDate : k.ASNMaster.DateOfArrival.Date) &&
                                                                  k.ASNMaster.DateOfArrival.Date <= (searchmodel.ToDate != null ? searchmodel.ToDate : k.ASNMaster.DateOfArrival.Date)) &&
                                                                  k.ASNMaster.InvoiceNo == (searchmodel.InvoiceNo != "" ? searchmodel.InvoiceNo : k.ASNMaster.InvoiceNo) &&
                                                                  k.SuppliedPartNo == (searchmodel.Partno != "" ? searchmodel.Partno : k.SuppliedPartNo) &&
                                                                  k.PalletNo == (searchmodel.Caseno != "" ? searchmodel.Caseno : k.PalletNo) &&
                                                                  k.ASNMaster.Remark == (searchmodel.ShippingRefNo != "" ? searchmodel.ShippingRefNo : k.ASNMaster.Remark)).Select(b => new ShipmentReportView
                                                                  {
                                                                      InvoiceNo = b.ASNMaster.InvoiceNo,
                                                                      DateOfArrival = b.ASNMaster.DateOfArrival,
                                                                      VesselName = b.ASNMaster.VesselName,
                                                                      VesselVoyageNo = b.ASNMaster.VesselVoyageNo,
                                                                      ShippingRefNumber = b.ASNMaster.Remark,
                                                                      ContainerNo = b.ContainerNo,
                                                                      PalletNo = b.PalletNo,
                                                                      PartNo = b.SuppliedPartNo,
                                                                      Description = _Context.PartsMaster.Where(a => a.PartNumber == b.SuppliedPartNo).Select(a => a.PartDescription).FirstOrDefault(),
                                                                      Quantity = b.Quantity,
                                                                      UnitPrice = b.UnitPrice,
                                                                      Customer = b.Customer,
                                                                      CustomerPO = b.PurchaseOrderNo,
                                                                      OrderNo = b.OrderNo,
                                                                      Length = b.Length,
                                                                      Breadth = b.Breadth,
                                                                      Height = Convert.ToDecimal(b.Height),
                                                                      Volume = b.Length * b.Height * b.Breadth,
                                                                      Weight = Convert.ToDecimal(b.Weight),
                                                                      Remark1 = b.Remark1,
                                                                      Remark2 = b.Remark2,
                                                                      RepackCaseNumber = _Context.SortingMaster.Where(a => a.InvoiceNo == b.ASNMaster.InvoiceNo && a.ItemCode == b.SuppliedPartNo && a.CustomerCode == b.Customer).FirstOrDefault().RepakedCaseNo
                                                                  }).OrderByDescending(a => a.RepackCaseNumber).AsQueryable();

                pagedItems.TotalResults = ShipmentReport.Count();// _Context.ASNItemDetails.Where(k => k.ContainerNo == (searchmodel.ContainerNo != "" ? searchmodel.ContainerNo : k.ContainerNo) && k.ASNMaster.DateOfArrival.Date == (searchmodel.DateOfArrival != null ? searchmodel.DateOfArrival : k.ASNMaster.DateOfArrival.Date) && k.ASNMaster.InvoiceNo == (searchmodel.InvoiceNo != "" ? searchmodel.InvoiceNo : k.ASNMaster.InvoiceNo) && k.ASNMaster.Remark == (searchmodel.ShippingRefNumber != "" ? searchmodel.ShippingRefNumber : k.ASNMaster.Remark)).Count();
                if (searchmodel.Skip.HasValue)
                {
                    ShipmentReport = ShipmentReport.Skip(searchmodel.Skip.Value);
                }
                else
                {
                    ShipmentReport = ShipmentReport.Skip(0);
                }

                if (searchmodel.Take.HasValue)
                {
                    ShipmentReport = ShipmentReport.Take(searchmodel.Take.Value);
                }
                else
                {
                    ShipmentReport = ShipmentReport.Take(10);
                }
                pagedItems.Items = ShipmentReport
                   .ToList();
                foreach (var item in pagedItems.Items)
                {
                    var h = item.Height.ToString();
                    var l = item.Length.ToString();
                    var b = item.Breadth.ToString();
                    if (h.Length > 2 || l.Length > 2 || b.Length > 2)
                    {
                        item.Height = item.Height / 100;
                        item.Length = item.Length / 100;
                        item.Breadth = item.Breadth / 100;
                        item.Volume = item.Height * item.Length * item.Breadth;
                    }
                }
                if (pagedItems.TotalResults > 0)
                {
                    result.Message = "Record Get Successfully";
                    result.StatusCode = 1;
                    result.IsValid = true;
                    result.Result = "success";
                    result.Data = pagedItems;
                }
                else
                {
                    result.Message = "Record does not exists";
                    result.StatusCode = 0;
                    result.IsValid = false;
                    result.Result = "error";
                    result.Data = null;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return result;

        }

        //Get Shipment Report Excel Export
        public ResponseviewModel GetShipmentReportExcelExport(ShipmentReportFilterExcelView searchmodel)
        {
            try
            {
                PagedItemSet<ShipmentReportExcelExportView> pagedItems = new PagedItemSet<ShipmentReportExcelExportView>();
                var ShipmentReport = _Context.ASNItemDetails.Where(k => k.ContainerNo == (searchmodel.ContainerNo != "" ? searchmodel.ContainerNo : k.ContainerNo) &&
                                                                  (k.ASNMaster.DateOfArrival.Date >= (searchmodel.FromDate != null ? searchmodel.FromDate : k.ASNMaster.DateOfArrival.Date) &&
                                                                  k.ASNMaster.DateOfArrival.Date <= (searchmodel.ToDate != null ? searchmodel.ToDate : k.ASNMaster.DateOfArrival.Date)) &&
                                                                  k.ASNMaster.InvoiceNo == (searchmodel.InvoiceNo != "" ? searchmodel.InvoiceNo : k.ASNMaster.InvoiceNo) &&
                                                                   k.SuppliedPartNo == (searchmodel.Partno != "" ? searchmodel.Partno : k.SuppliedPartNo) &&
                                                                  k.PalletNo == (searchmodel.Caseno != "" ? searchmodel.Caseno : k.PalletNo) &&
                                                                  k.ASNMaster.Remark == (searchmodel.ShippingRefNo != "" ? searchmodel.ShippingRefNo : k.ASNMaster.Remark)).Select(b => new ShipmentReportExcelExportView
                                                                  {
                                                                      InvoiceNo = b.ASNMaster.InvoiceNo,
                                                                      DateOfArrival = b.ASNMaster.DateOfArrival,
                                                                      //VesselName = b.ASNMaster.VesselName,
                                                                      VesselVoyageNo = b.ASNMaster.VesselVoyageNo,
                                                                      //ShippingRefNumber = b.ASNMaster.Remark,
                                                                      ContainerNo = b.ContainerNo,
                                                                      PalletNo = b.PalletNo,
                                                                      PartNo = b.SuppliedPartNo,
                                                                      Description = _Context.PartsMaster.Where(a => a.PartNumber == b.SuppliedPartNo).Select(a => a.PartDescription).FirstOrDefault(),
                                                                      Quantity = b.Quantity,
                                                                      UnitPrice = b.UnitPrice,
                                                                      Customercode = b.Customer,
                                                                      CustomerPO = b.PurchaseOrderNo,
                                                                      OrderNo = b.OrderNo,
                                                                      Length = b.Length,
                                                                      Breadth = b.Breadth,
                                                                      Height = Convert.ToDecimal(b.Height),
                                                                      Volume = b.Length * b.Height * b.Breadth,
                                                                      Remark1 = b.Remark1,
                                                                      Remark2 = b.Remark2,
                                                                      Remark3 = b.Remark3
                                                                      // RepackCaseNumber = null,
                                                                  }).ToList();
                foreach (var item in ShipmentReport)
                {
                    var h = item.Height.ToString();
                    var l = item.Length.ToString();
                    var b = item.Breadth.ToString();
                    if (h.Length > 2 || l.Length > 2 || b.Length > 2)
                    {
                        item.Height = item.Height / 100;
                        item.Length = item.Length / 100;
                        item.Breadth = item.Breadth / 100;
                        item.Volume = item.Height * item.Length * item.Breadth;
                    }
                }

                if (ShipmentReport.Count > 0)
                {
                    result.Message = "Record Get Successfully";
                    result.StatusCode = 1;
                    result.IsValid = true;
                    result.Result = "success";
                    result.Data = ShipmentReport;
                }
                else
                {
                    result.Message = "Record does not exists";
                    result.StatusCode = 0;
                    result.IsValid = false;
                    result.Result = "error";
                    result.Data = null;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return result;

        }

        // Delivery Listing Report
        public ResponseviewModel GetDeliveryListingReport(DeliveryListingReportFilterView searchmodel)
        {
            try
            {
                PagedItemSet<DeliveryListingReportView> pagedItems = new PagedItemSet<DeliveryListingReportView>();
                var DeliveryListReport = _Context.DeliveryOrderDetail.Where(k => k.DeliveryOrder.CustomerName == (searchmodel.CustomerName != "" ? searchmodel.CustomerName : k.DeliveryOrder.CustomerName) &&
                                                                  k.DeliveryOrder.DONumber == (searchmodel.DoNo != "" ? searchmodel.DoNo : k.DeliveryOrder.DONumber) &&
                                                                  k.DeliveryOrder.CustomerCode == (searchmodel.CustomerCode != "" ? searchmodel.CustomerCode : k.DeliveryOrder.CustomerCode) &&
                                                                  k.DeliveryOrder.DateOfDeliver == (searchmodel.Date != null ? searchmodel.Date : k.DeliveryOrder.DateOfDeliver.Date) &&
                                                                  (k.DeliveryOrder.DateOfDeliver >= (searchmodel.FromDate != null ? searchmodel.FromDate : k.DeliveryOrder.DateOfDeliver) &&
                                                                  k.DeliveryOrder.DateOfDeliver <= (searchmodel.ToDate != null ? searchmodel.ToDate : k.DeliveryOrder.DateOfDeliver)))
                                                                  .Select(b => new DeliveryListingReportView
                                                                  {
                                                                      DONumber = b.DeliveryOrder.DONumber,
                                                                      DateOfDeliver = b.DeliveryOrder.DateOfDeliver,
                                                                      From = _Context.CompanyMaster.Where(a => a.PK_CompanyID == b.DeliveryOrder.Fk_CompanyID).Select(a => a.CompanyName).FirstOrDefault(),
                                                                      To = b.DeliveryOrder.CustomerName,
                                                                      //To = _Context.CustomerMaster.Where(a => a.Code == b.DeliveryOrder.CustomerCode).Select(a => a.Name).FirstOrDefault(),
                                                                      CustomerCode = b.DeliveryOrder.CustomerCode,
                                                                      CustomerPo = _Context.ASNItemDetails.Where(a => a.PalletNo == b.SortingMaster.CaseNo && a.SuppliedPartNo == b.SortingMaster.ItemCode).Select(a => a.PurchaseOrderNo).FirstOrDefault(),
                                                                      RepackedCaseNo = b.RepackedCaseNo,
                                                                      PartNo = b.ItemCode,
                                                                      Description = b.SortingMaster.SortingDescription,
                                                                      ShippedQuantity = _Context.StockTransaction.Where(a => a.PK_SortingID == b.Fk_SortingMasterID).Select(a => a.QuantityReceived).FirstOrDefault(),
                                                                      NoOfCase = b.DeliveryOrder.NoOfBox,
                                                                      Weight = _Context.SortingDetail.Where(a => a.FK_SortingMasterID == b.Fk_SortingMasterID).Select(a => a.Weight).FirstOrDefault(),
                                                                      Length = _Context.SortingDetail.Where(a => a.FK_SortingMasterID == b.Fk_SortingMasterID).Select(a => a.Length).FirstOrDefault(),
                                                                      Breadth = _Context.SortingDetail.Where(a => a.FK_SortingMasterID == b.Fk_SortingMasterID).Select(a => a.Breadth).FirstOrDefault(),
                                                                      Height = _Context.SortingDetail.Where(a => a.FK_SortingMasterID == b.Fk_SortingMasterID).Select(a => a.Height).FirstOrDefault(),
                                                                      Dimension = _Context.SortingDetail.Where(a => a.FK_SortingMasterID == b.Fk_SortingMasterID).Select(a => a.Length).FirstOrDefault() + "X" + _Context.SortingDetail.Where(a => a.FK_SortingMasterID == b.Fk_SortingMasterID).Select(a => a.Breadth).FirstOrDefault() + "X" + _Context.SortingDetail.Where(a => a.FK_SortingMasterID == b.Fk_SortingMasterID).Select(a => a.Height).FirstOrDefault(),
                                                                      // Volume= 10,
                                                                      Volume = _Context.SortingDetail.Where(a => a.FK_SortingMasterID == b.Fk_SortingMasterID).Select(a => a.Length).FirstOrDefault() * _Context.SortingDetail.Where(a => a.FK_SortingMasterID == b.Fk_SortingMasterID).Select(a => a.Breadth).FirstOrDefault() * _Context.SortingDetail.Where(a => a.FK_SortingMasterID == b.Fk_SortingMasterID).Select(a => a.Height).FirstOrDefault(),
                                                                      Remark1 = _Context.ASNItemDetails.Where(a => a.PalletNo == b.SortingMaster.CaseNo && a.SuppliedPartNo == b.SortingMaster.ItemCode).Select(a => a.Remark1).FirstOrDefault(),
                                                                      Remark2 = _Context.ASNItemDetails.Where(a => a.PalletNo == b.SortingMaster.CaseNo && a.SuppliedPartNo == b.SortingMaster.ItemCode).Select(a => a.Remark2).FirstOrDefault(),
                                                                      Remark3 = _Context.ASNItemDetails.Where(a => a.PalletNo == b.SortingMaster.CaseNo && a.SuppliedPartNo == b.SortingMaster.ItemCode).Select(a => a.Remark3).FirstOrDefault()
                                                                  }).OrderByDescending(a => a.DONumber).AsQueryable();
                pagedItems.TotalResults = DeliveryListReport.Count();
                if (searchmodel.Skip.HasValue)
                {
                    DeliveryListReport = DeliveryListReport.Skip(searchmodel.Skip.Value);
                }
                else
                {
                    DeliveryListReport = DeliveryListReport.Skip(0);
                }

                if (searchmodel.Take.HasValue)
                {
                    DeliveryListReport = DeliveryListReport.Take(searchmodel.Take.Value);
                }
                else
                {
                    DeliveryListReport = DeliveryListReport.Take(10);
                }
                pagedItems.Items = DeliveryListReport
                   .ToList();

                if (pagedItems.TotalResults > 0)
                {
                    result.Message = "Record Get Successfully";
                    result.StatusCode = 1;
                    result.IsValid = true;
                    result.Result = "success";
                    result.Data = pagedItems;
                }
                else
                {
                    result.Message = "Record does not exists";
                    result.StatusCode = 0;
                    result.IsValid = false;
                    result.Result = "error";
                    result.Data = null;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return result;

        }

        // Delivery Listing Report Excel Export
        public ResponseviewModel GetDeliveryListingReportExcelExport(DeliveryListingReportFilterExcelExportView searchmodel)
        {
            try
            {
                var DeliveryListReport = _Context.DeliveryOrderDetail.Where(k => k.DeliveryOrder.CustomerName == (searchmodel.CustomerName != "" ? searchmodel.CustomerName : k.DeliveryOrder.CustomerName) &&
                                                                  k.DeliveryOrder.DONumber == (searchmodel.DoNo != "" ? searchmodel.DoNo : k.DeliveryOrder.DONumber) &&
                                                                  k.DeliveryOrder.CustomerCode == (searchmodel.CustomerCode != "" ? searchmodel.CustomerCode : k.DeliveryOrder.CustomerCode) &&
                                                                  k.DeliveryOrder.DateOfDeliver.Date == (searchmodel.Date != null ? searchmodel.Date : k.DeliveryOrder.DateOfDeliver.Date))
                                                                  .Select(b => new DeliveryListingReportExcelExportView
                                                                  {
                                                                      DONumber = b.DeliveryOrder.DONumber,
                                                                      DateOfDeliver = b.DeliveryOrder.DateOfDeliver,
                                                                      From = _Context.CompanyMaster.Where(a => a.PK_CompanyID == b.DeliveryOrder.Fk_CompanyID).Select(a => a.CompanyName).FirstOrDefault(),
                                                                      To = b.DeliveryOrder.CustomerName,
                                                                      //To = _Context.CustomerMaster.Where(a => a.Code == b.DeliveryOrder.CustomerCode).Select(a => a.Name).FirstOrDefault(),
                                                                      CustomerCode = b.DeliveryOrder.CustomerCode,
                                                                      CustomerPo = _Context.ASNItemDetails.Where(a => a.PalletNo == b.SortingMaster.CaseNo && a.SuppliedPartNo == b.SortingMaster.ItemCode).Select(a => a.PurchaseOrderNo).FirstOrDefault(),
                                                                      //Dimension = b.Length + "X" + b.Height + "X" + b.Breadth,
                                                                      //Volume=b.Length*b.Breadth*b.Height,
                                                                      RepackedCaseNo = b.RepackedCaseNo,
                                                                      PartNo = b.ItemCode,
                                                                      Weight = _Context.SortingDetail.Where(a => a.FK_SortingMasterID == b.Fk_SortingMasterID).Select(a => a.Weight).FirstOrDefault(),
                                                                      Length = _Context.SortingDetail.Where(a => a.FK_SortingMasterID == b.Fk_SortingMasterID).Select(a => a.Length).FirstOrDefault(),
                                                                      Breadth = _Context.SortingDetail.Where(a => a.FK_SortingMasterID == b.Fk_SortingMasterID).Select(a => a.Breadth).FirstOrDefault(),
                                                                      Height = _Context.SortingDetail.Where(a => a.FK_SortingMasterID == b.Fk_SortingMasterID).Select(a => a.Height).FirstOrDefault(),
                                                                      Dimension = _Context.SortingDetail.Where(a => a.FK_SortingMasterID == b.Fk_SortingMasterID).Select(a => a.Length).FirstOrDefault() + "X" + _Context.SortingDetail.Where(a => a.FK_SortingMasterID == b.Fk_SortingMasterID).Select(a => a.Breadth).FirstOrDefault() + "X" + _Context.SortingDetail.Where(a => a.FK_SortingMasterID == b.Fk_SortingMasterID).Select(a => a.Height).FirstOrDefault(),
                                                                      Volume = _Context.SortingDetail.Where(a => a.FK_SortingMasterID == b.Fk_SortingMasterID).Select(a => a.Length).FirstOrDefault() * _Context.SortingDetail.Where(a => a.FK_SortingMasterID == b.Fk_SortingMasterID).Select(a => a.Breadth).FirstOrDefault() * _Context.SortingDetail.Where(a => a.FK_SortingMasterID == b.Fk_SortingMasterID).Select(a => a.Height).FirstOrDefault(),
                                                                      Description = b.SortingMaster.SortingDescription,
                                                                      ShippedQuantity = _Context.StockTransaction.Where(a => a.PK_SortingID == b.Fk_SortingMasterID).Select(a => a.QuantityReceived).FirstOrDefault(),
                                                                      NoOfCase = b.DeliveryOrder.NoOfBox,
                                                                      Remark1 = _Context.ASNItemDetails.Where(a => a.PalletNo == b.SortingMaster.CaseNo && a.SuppliedPartNo == b.SortingMaster.ItemCode).Select(a => a.Remark1).FirstOrDefault(),
                                                                      Remark2 = _Context.ASNItemDetails.Where(a => a.PalletNo == b.SortingMaster.CaseNo && a.SuppliedPartNo == b.SortingMaster.ItemCode).Select(a => a.Remark2).FirstOrDefault(),
                                                                      Remark3 = _Context.ASNItemDetails.Where(a => a.PalletNo == b.SortingMaster.CaseNo && a.SuppliedPartNo == b.SortingMaster.ItemCode).Select(a => a.Remark3).FirstOrDefault()

                                                                  }).ToList();

                if (DeliveryListReport.Count > 0)
                {
                    result.Message = "Record Get Successfully";
                    result.StatusCode = 1;
                    result.IsValid = true;
                    result.Result = "success";
                    result.Data = DeliveryListReport;
                }
                else
                {
                    result.Message = "Record does not exists";
                    result.StatusCode = 0;
                    result.IsValid = false;
                    result.Result = "error";
                    result.Data = null;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return result;

        }

        // Delivery Listing Summary Report
        public ResponseviewModel GetDeliveryListingSummaryReport(DeliveryListingReportFilterView searchmodel)
        {
            try
            {
                PagedItemSet<DeliveryListingSummuryReportView> pagedItems = new PagedItemSet<DeliveryListingSummuryReportView>();
                var DeliveryListReport = _Context.DeliveryOrderDetail.Where(k => k.DeliveryOrder.CustomerName == (searchmodel.CustomerName != "" ? searchmodel.CustomerName : k.DeliveryOrder.CustomerName) &&
                                                                  k.DeliveryOrder.DONumber == (searchmodel.DoNo != "" ? searchmodel.DoNo : k.DeliveryOrder.DONumber) &&
                                                                  k.DeliveryOrder.CustomerCode == (searchmodel.CustomerCode != "" ? searchmodel.CustomerCode : k.DeliveryOrder.CustomerCode) &&
                                                                  k.DeliveryOrder.DateOfDeliver.Date == (searchmodel.Date != null ? searchmodel.Date : k.DeliveryOrder.DateOfDeliver.Date))
                                                                   .Select(b => new DeliveryListingSummuryReportView
                                                                   {
                                                                       DONumber = b.DeliveryOrder.DONumber,
                                                                       DateOfDeliver = b.DeliveryOrder.DateOfDeliver,
                                                                       From = _Context.CompanyMaster.Where(a => a.PK_CompanyID == b.DeliveryOrder.Fk_CompanyID).Select(a => a.CompanyName).FirstOrDefault(),
                                                                       To = b.DeliveryOrder.CustomerName,
                                                                       CustomerCode = b.DeliveryOrder.CustomerCode,
                                                                       RepackedCaseNo = b.RepackedCaseNo,
                                                                       NoOfCase = b.DeliveryOrder.NoOfBox,
                                                                       Weight = _Context.SortingDetail.Where(a => a.FK_SortingMasterID == b.Fk_SortingMasterID).Select(a => a.Weight).FirstOrDefault(),
                                                                       Volume = _Context.SortingDetail.Where(a => a.FK_SortingMasterID == b.Fk_SortingMasterID).Select(a => a.Length).FirstOrDefault() * _Context.SortingDetail.Where(a => a.FK_SortingMasterID == b.Fk_SortingMasterID).Select(a => a.Breadth).FirstOrDefault() * _Context.SortingDetail.Where(a => a.FK_SortingMasterID == b.Fk_SortingMasterID).Select(a => a.Height).FirstOrDefault(),
                                                                   }).OrderByDescending(a => a.DONumber).AsQueryable();
                pagedItems.TotalResults = DeliveryListReport.Count();
                if (searchmodel.Skip.HasValue)
                {
                    DeliveryListReport = DeliveryListReport.Skip(searchmodel.Skip.Value);
                }
                else
                {
                    DeliveryListReport = DeliveryListReport.Skip(0);
                }

                if (searchmodel.Take.HasValue)
                {
                    DeliveryListReport = DeliveryListReport.Take(searchmodel.Take.Value);
                }
                else
                {
                    DeliveryListReport = DeliveryListReport.Take(10);
                }
                pagedItems.Items = DeliveryListReport
                   .ToList();

                if (pagedItems.TotalResults > 0)
                {
                    result.Message = "Record Get Successfully";
                    result.StatusCode = 1;
                    result.IsValid = true;
                    result.Result = "success";
                    result.Data = DeliveryListReport;
                }
                else
                {
                    result.Message = "Record does not exists";
                    result.StatusCode = 0;
                    result.IsValid = false;
                    result.Result = "error";
                    result.Data = null;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return result;

        }

        // Delivery Listing Summary Report Excel Export
        public ResponseviewModel GetDeliveryListingSummaryReportExcelExport(DeliveryListingReportFilterExcelExportView searchmodel)
        {
            try
            {

                var DeliveryListReport = _Context.DeliveryOrderDetail.Where(k => k.DeliveryOrder.DONumber == (searchmodel.DoNo != "" ? searchmodel.DoNo : k.DeliveryOrder.DONumber) &&
                                                                    k.DeliveryOrder.CustomerName == (searchmodel.CustomerName != "" ? searchmodel.CustomerName : k.DeliveryOrder.CustomerName) &&
                                                                  k.DeliveryOrder.CustomerCode == (searchmodel.CustomerCode != "" ? searchmodel.CustomerCode : k.DeliveryOrder.CustomerCode) &&
                                                                  k.DeliveryOrder.DateOfDeliver.Date == (searchmodel.Date != null ? searchmodel.Date : k.DeliveryOrder.DateOfDeliver.Date)
                                                                  )
                                                                  .Select(b => new DeliveryListingSummaryReportExcelExportView
                                                                  {
                                                                      DONumber = b.DeliveryOrder.DONumber,
                                                                      DateOfDeliver = b.DeliveryOrder.DateOfDeliver,
                                                                      //From = "Company",
                                                                      //To = _Context.CompanyMaster.Where(a => a.PK_CompanyID == b.DeliveryOrder.Fk_CompanyID).Select(a => a.CompanyName).FirstOrDefault(),
                                                                      From = _Context.CompanyMaster.Where(a => a.PK_CompanyID == b.DeliveryOrder.Fk_CompanyID).Select(a => a.CompanyName).FirstOrDefault(),
                                                                      To = b.DeliveryOrder.CustomerName,

                                                                      //To = _Context.CustomerMaster.Where(a => a.Code == b.DeliveryOrder.CustomerCode).Select(a => a.Name).FirstOrDefault(),
                                                                      CustomerCode = b.DeliveryOrder.CustomerCode,
                                                                      RepackedCaseNo = b.RepackedCaseNo,
                                                                      NoOfCase = b.DeliveryOrder.NoOfBox,
                                                                      Weight = _Context.SortingDetail.Where(a => a.FK_SortingMasterID == b.Fk_SortingMasterID).Select(a => a.Weight).FirstOrDefault(),
                                                                      Volume = b.Length * b.Height * b.Breadth,
                                                                  }).ToList();

                if (DeliveryListReport.Count > 0)
                {
                    result.Message = "Record Get Successfully";
                    result.StatusCode = 1;
                    result.IsValid = true;
                    result.Result = "success";
                    result.Data = DeliveryListReport;
                }
                else
                {
                    result.Message = "Record does not exists";
                    result.StatusCode = 0;
                    result.IsValid = false;
                    result.Result = "error";
                    result.Data = null;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return result;

        }
        public ResponseviewModel GetPartsTransactionsList(GetPartsTransactionsListView filterdata)
        {

            try
            {

                PagedItemSet<GetPartsTransactionsListView> pagedItems = new PagedItemSet<GetPartsTransactionsListView>();
                var PartsTransaction = _Context.DeliveryOrderDetail.Where(k => k.InvoiceNo == (filterdata.InvoiceNo != "" ? filterdata.InvoiceNo : k.InvoiceNo) &&
                                            k.ItemCode == (filterdata.PartNo != null ? filterdata.PartNo : k.ItemCode)
                                            && k.IsActive == true && k.IsDelete == false && k.SortingMaster.IsDelete == false && k.SortingMaster.IsBin == false && (String.IsNullOrEmpty(filterdata.search) ? true : (k.InvoiceNo.Trim().ToLower() == filterdata.search || k.SortingMaster.CaseNo.Trim().ToLower() == filterdata.search || k.DeliveryOrder.DONumber.Trim().ToLower() == filterdata.search || k.ItemCode.Trim().ToLower() == filterdata.search || k.RepackedCaseNo.Trim().ToLower() == filterdata.search))).Select(b => new GetPartsTransactionsListView
                                            {
                                                InvoiceNo = b.InvoiceNo,
                                                Caseno = b.SortingMaster.CaseNo,
                                                DONumber = b.DeliveryOrder.DONumber,
                                                Quantity = _Context.ASNItemDetails.Where(a => a.SuppliedPartNo == b.ItemCode && a.ASNMaster.InvoiceNo == b.InvoiceNo && a.PalletNo == b.SortingMaster.CaseNo && a.Customer == b.SortingMaster.CustomerCode && a.IsDelete == false).Sum(a => a.Quantity),
                                                PartNo = b.ItemCode,
                                                PartDescription = _Context.PartsMaster.Where(a => a.PartNumber == b.ItemCode).FirstOrDefault().PartDescription,
                                                Repackcaseno = b.RepackedCaseNo,
                                                DoNumberDate = b.DeliveryOrder.DateOfDeliver,
                                                sortedQty = b.SortingMaster.Quantity,
                                                issuedQty = b.Quantity,
                                                Balance = _Context.StockTransaction.Where(a => a.PK_SortingID == b.SortingMaster.Pk_SortingID && b.SortingMaster.IsDelete == false).FirstOrDefault().QuantityRemaining
                                            }).OrderByDescending(a => a.InvoiceNo).OrderBy(a => a.Repackcaseno).ThenBy(a => a.DONumber).AsQueryable();

                pagedItems.TotalResults = PartsTransaction.Count();
                if (filterdata.Skip.HasValue)
                {
                    PartsTransaction = PartsTransaction.Skip(filterdata.Skip.Value);
                }
                else
                {
                    PartsTransaction = PartsTransaction.Skip(0);
                }

                if (filterdata.Take.HasValue)
                {
                    PartsTransaction = PartsTransaction.Take(filterdata.Take.Value);
                }
                else
                {
                    PartsTransaction = PartsTransaction.Take(10);
                }
                pagedItems.Items = PartsTransaction
                   .ToList();

                if (pagedItems.TotalResults > 0)
                {
                    result.Message = "Record Get Successfully";
                    result.StatusCode = 1;
                    result.IsValid = true;
                    result.Result = "success";
                    result.Data = pagedItems;
                }
                else
                {
                    result.Message = "Record does not exists";
                    result.StatusCode = 0;
                    result.IsValid = false;
                    result.Result = "error";
                    result.Data = null;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
        public ResponseviewModel GetPartsTransactionsListExcelExport(GetPartsTransactionsListView filterdata)
        {
            try
            {

                PagedItemSet<GetPartsTransactionsListView> pagedItems = new PagedItemSet<GetPartsTransactionsListView>();
                var PartsTransaction = _Context.DeliveryOrderDetail.Where(k => k.InvoiceNo == (filterdata.InvoiceNo != "" ? filterdata.InvoiceNo : k.InvoiceNo) && k.IsActive == true && k.IsDelete == false && k.SortingMaster.IsDelete == false && k.SortingMaster.IsBin == false && (String.IsNullOrEmpty(filterdata.search) ? true : (k.InvoiceNo.Trim().ToLower() == filterdata.search || k.SortingMaster.CaseNo.Trim().ToLower() == filterdata.search || k.DeliveryOrder.DONumber.Trim().ToLower() == filterdata.search || k.ItemCode.Trim().ToLower() == filterdata.search || k.RepackedCaseNo.Trim().ToLower() == filterdata.search))).Select(b => new GetPartsTransactionsListView
                {
                    InvoiceNo = b.InvoiceNo,
                    Caseno = b.SortingMaster.CaseNo,
                    DONumber = b.DeliveryOrder.DONumber,
                    Quantity = _Context.ASNItemDetails.Where(a => a.SuppliedPartNo == b.ItemCode && a.ASNMaster.InvoiceNo == b.InvoiceNo && a.PalletNo == b.SortingMaster.CaseNo && a.Customer == b.SortingMaster.CustomerCode && a.IsDelete == false).Sum(a => a.Quantity),
                    PartNo = b.ItemCode,
                    PartDescription = _Context.PartsMaster.Where(a => a.PartNumber == b.ItemCode).FirstOrDefault().PartDescription,
                    Repackcaseno = b.RepackedCaseNo,
                    DoNumberDate = b.DeliveryOrder.DateOfDeliver,
                    sortedQty = b.SortingMaster.Quantity,
                    issuedQty = b.Quantity,
                    Balance = _Context.StockTransaction.Where(a => a.PK_SortingID == b.SortingMaster.Pk_SortingID && b.SortingMaster.IsDelete == false).FirstOrDefault().QuantityRemaining
                }).OrderByDescending(a => a.InvoiceNo).OrderBy(a => a.Repackcaseno).ThenBy(a => a.DONumber).ToList();



                if (PartsTransaction.Count > 0)
                {
                    result.Message = "Record Get Successfully";
                    result.StatusCode = 1;
                    result.IsValid = true;
                    result.Result = "success";
                    result.Data = PartsTransaction;
                }
                else
                {
                    result.Message = "Record does not exists";
                    result.StatusCode = 0;
                    result.IsValid = false;
                    result.Result = "error";
                    result.Data = null;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }


        public ResponseviewModel GetPartsTransactionsBinList(GetPartsTransactionsBinList filterdata)
        {

            try
            {

                PagedItemSet<GetPartsTransactionsBinList> pagedItems = new PagedItemSet<GetPartsTransactionsBinList>();
                var PartsTransactionBin = _Context.SortingMaster.Where(k => k.ItemCode == (filterdata.partnumber != null ? filterdata.partnumber : k.ItemCode) && k.LocationID == (filterdata.location != null ? filterdata.location : k.LocationID) && k.IsActive == true && k.IsBin == true && k.IsDelete == false).Select(b => new GetPartsTransactionsBinList
                {
                    InvoiceNo = b.InvoiceNo,
                    Caseno = b.CaseNo,
                    Quantity = _Context.StockTransaction.Where(a => a.PK_SortingID == b.Pk_SortingID).FirstOrDefault().QuantityRemaining,
                    PartNo = b.ItemCode,
                    PartDescription = _Context.PartsMaster.Where(a => a.PartNumber == b.ItemCode).FirstOrDefault().PartDescription,
                    BinLocation = b.LocationID,
                    sortedQty = b.Quantity,

                }).AsQueryable();
                pagedItems.TotalResults = PartsTransactionBin.Count();
                if (filterdata.Skip.HasValue)
                {
                    PartsTransactionBin = PartsTransactionBin.Skip(filterdata.Skip.Value);
                }
                else
                {
                    PartsTransactionBin = PartsTransactionBin.Skip(0);
                }

                if (filterdata.Take.HasValue)
                {
                    PartsTransactionBin = PartsTransactionBin.Take(filterdata.Take.Value);
                }
                else
                {
                    PartsTransactionBin = PartsTransactionBin.Take(10);
                }
                pagedItems.Items = PartsTransactionBin
                   .ToList();

                if (pagedItems.TotalResults > 0)
                {
                    result.Message = "Record Get Successfully";
                    result.StatusCode = 1;
                    result.IsValid = true;
                    result.Result = "success";
                    result.Data = pagedItems;
                }
                else
                {
                    result.Message = "Record does not exists";
                    result.StatusCode = 0;
                    result.IsValid = false;
                    result.Result = "error";
                    result.Data = null;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
        public ResponseviewModel GetPartsTransactionsBinDoList(GetPartsTransactionsBinDoList filterdata)
        {
            PagedItemSet<GetPartsTransactionsBinDoList> pagedItems = new PagedItemSet<GetPartsTransactionsBinDoList>();

            try
            {
                var PartsTransactionBin = _Context.DeliveryOrderDetail.Where(k => k.ItemCode == (filterdata.partnumber != null ? filterdata.partnumber : k.ItemCode) && k.LocationId == (filterdata.location != null ? filterdata.location : k.LocationId) && k.IsActive == true && k.IsDelete == false && k.SortingMaster.IsBin == true).Select(b => new GetPartsTransactionsBinDoList
                {
                    DONumber = b.DeliveryOrder.DONumber,
                    PartNo = b.ItemCode,
                    DoNumberDate = b.DeliveryOrder.DateOfDeliver,
                    issuedQty = b.Quantity,
                    BinLocation = b.LocationId

                }).OrderByDescending(a => a.DoNumberDate).AsQueryable();
                pagedItems.TotalResults = PartsTransactionBin.Count();
                if (filterdata.Skip.HasValue)
                {
                    PartsTransactionBin = PartsTransactionBin.Skip(filterdata.Skip.Value);
                }
                else
                {
                    PartsTransactionBin = PartsTransactionBin.Skip(0);
                }

                if (filterdata.Take.HasValue)
                {
                    PartsTransactionBin = PartsTransactionBin.Take(filterdata.Take.Value);
                }
                else
                {
                    PartsTransactionBin = PartsTransactionBin.Take(10);
                }
                pagedItems.Items = PartsTransactionBin
                   .ToList();

                if (pagedItems.TotalResults > 0)
                {
                    result.Message = "Record Get Successfully";
                    result.StatusCode = 1;
                    result.IsValid = true;
                    result.Result = "success";
                    result.Data = pagedItems;
                }
                else
                {
                    result.Message = "Record does not exists";
                    result.StatusCode = 0;
                    result.IsValid = false;
                    result.Result = "error";
                    result.Data = null;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
        public ResponseviewModel GetReallocationList(GetRealocationsListView filterdata)
        {
            try
            {

                PagedItemSet<GetRealocationsListView> pagedItems = new PagedItemSet<GetRealocationsListView>();
                //var Realocation = _Context.LooseItemTransferLog.Select(g => new



                //{
                //    PartNo = _Context.SortingMaster.Where(a => a.Pk_SortingID == g.Fk_SortingMasterID).FirstOrDefault().ItemCode,
                //    // PartNo=b.ItemCode,
                //    Quantity = g.Quantity,
                //    CustomerCode = _Context.SortingMaster.Where(a => a.Pk_SortingID == g.Fk_SortingMasterID).FirstOrDefault().CustomerCode,
                //    RepackedCaseNo = g.RePackCase,
                //    NewLocation = _Context.BinLocationHistory.Where(a => a.FK_SortingMasterID == g.Fk_SortingMasterID).FirstOrDefault().NewLocation,
                //    Date = g.CreatedDate,
                //    User = _Context.SortingMaster.Where(a => a.Pk_SortingID == g.Fk_SortingMasterID).FirstOrDefault().UserName
                //}).GroupBy(g => g.PartNo).AsQueryable();

                var Realocation = (from lit in _Context.LooseItemTransferLog
                                   join sm in _Context.SortingMaster on lit.Fk_SortingMasterID equals sm.Pk_SortingID
                                   group new { sm.ItemCode, lit.Quantity, sm.CustomerCode, lit.FK_BinLocationHistoryId, lit.CreatedDate, sm.CreatedBy, sm.RepakedCaseNo } by new { sm.ItemCode } into pg
                                   let fd = pg.FirstOrDefault()
                                   let PartNo = pg.FirstOrDefault().ItemCode
                                   let Quantity = pg.FirstOrDefault().Quantity
                                   let CustomerCode = pg.FirstOrDefault().CustomerCode
                                   let RepackedCaseNo = pg.FirstOrDefault().RepakedCaseNo
                                   let NewLocation = _Context.BinLocationHistory.Where(a => a.PK_BinLocationHistoryId == pg.FirstOrDefault().FK_BinLocationHistoryId).FirstOrDefault().NewLocation
                                   let Date = pg.FirstOrDefault().CreatedDate
                                   let User = _Context.UserMaster.Where(a => a.Pk_UserId == pg.FirstOrDefault().CreatedBy).FirstOrDefault().FirstName + ' ' + _Context.UserMaster.Where(a => a.Pk_UserId == pg.FirstOrDefault().CreatedBy).FirstOrDefault().LastName
                                   select new GetRealocationsListView
                                   {
                                       PartNo = PartNo,
                                       Quantity = Quantity,
                                       CustomerCode = CustomerCode,
                                       RepackedCaseNo = RepackedCaseNo,
                                       NewLocation = NewLocation,
                                       Date = Date,
                                       User = User
                                   }).OrderByDescending(a => a.Date).AsQueryable();


                pagedItems.TotalResults = Realocation.Count();
                if (filterdata.Skip.HasValue)
                {
                    Realocation = Realocation.Skip(filterdata.Skip.Value);
                }
                else
                {
                    Realocation = Realocation.Skip(0);
                }

                if (filterdata.Take.HasValue)
                {
                    Realocation = Realocation.Take(filterdata.Take.Value);
                }
                else
                {
                    Realocation = Realocation.Take(10);
                }
                pagedItems.Items = Realocation.ToList();

                if (pagedItems.TotalResults > 0)
                {
                    result.Message = "Record Get Successfully";
                    result.StatusCode = 1;
                    result.IsValid = true;
                    result.Result = "success";
                    result.Data = pagedItems;
                }
                else
                {
                    result.Message = "Record does not exists";
                    result.StatusCode = 0;
                    result.IsValid = false;
                    result.Result = "error";
                    result.Data = null;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
        public ResponseviewModel GetRealocationsListExcelExport()
        {
            try
            {
                var Realocation = (from lit in _Context.LooseItemTransferLog
                                   join sm in _Context.SortingMaster on lit.Fk_SortingMasterID equals sm.Pk_SortingID
                                   group new { sm.ItemCode, lit.Quantity, sm.CustomerCode, lit.FK_BinLocationHistoryId, lit.CreatedDate, sm.CreatedBy, sm.RepakedCaseNo } by new { sm.ItemCode } into pg
                                   let fd = pg.FirstOrDefault()
                                   let PartNo = pg.FirstOrDefault().ItemCode
                                   let Quantity = pg.FirstOrDefault().Quantity
                                   let CustomerCode = pg.FirstOrDefault().CustomerCode
                                   let RepackedCaseNo = pg.FirstOrDefault().RepakedCaseNo
                                   let NewLocation = _Context.BinLocationHistory.Where(a => a.PK_BinLocationHistoryId == pg.FirstOrDefault().FK_BinLocationHistoryId).FirstOrDefault().NewLocation
                                   let Date = pg.FirstOrDefault().CreatedDate
                                   let User = _Context.UserMaster.Where(a => a.Pk_UserId == pg.FirstOrDefault().CreatedBy).FirstOrDefault().FirstName + ' ' + _Context.UserMaster.Where(a => a.Pk_UserId == pg.FirstOrDefault().CreatedBy).FirstOrDefault().LastName
                                   select new GetRealocationsListView
                                   {
                                       PartNo = PartNo,
                                       //    // PartNo=b.ItemCode,
                                       Quantity = Quantity,
                                       CustomerCode = CustomerCode,
                                       RepackedCaseNo = RepackedCaseNo,
                                       NewLocation = NewLocation,
                                       Date = Date,
                                       User = User
                                   }).ToList();
                if (Realocation.Count > 0)
                {
                    result.Message = "Record Get Successfully";
                    result.StatusCode = 1;
                    result.IsValid = true;
                    result.Result = "success";
                    result.Data = Realocation;
                }
                else
                {
                    result.Message = "Record does not exists";
                    result.StatusCode = 0;
                    result.IsValid = false;
                    result.Result = "error";
                    result.Data = null;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
        public ResponseviewModel GetStockAgingReports(GetStockAgingReportsListView filterdata)
        {
            try
            {
                PagedItemSet<GetStockAgingReportsListView> pagedItems = new PagedItemSet<GetStockAgingReportsListView>();


                this._Context.Database.SetCommandTimeout(1000);
                //List<SortAging> data = new List<SortAging>();
                var data = _Context.SortAging.Select(a => new GetStockAgingReportsListView
                {
                    PickingType = a.PickingType,
                    LocationNumber = a.LocationNumber,
                    LocationId = a.LocationId,
                    PartCode = a.PartCode,
                    PartDescription = a.PartDescription,
                    EarlyGoodsreceivedDate = a.EarlyGoodsreceivedDate,
                    ReportRunnungDate = a.ReportRunnungDate,
                    Aging = a.Aging,
                    OnhandQty = a.OnhandQty,
                    UnitCost = a.UnitCost,
                    QTY0_5 = a.QTY0_5,
                    Totalcost0_5 = a.Totalcost0_5,
                    QTY1 = a.QTY1,
                    Totalcost1 = a.Totalcost1,
                    QTY1_5 = a.QTY1_5,
                    Totalcost1_5 = a.Totalcost1_5,
                    QTY2 = a.QTY2,
                    Totalcost2 = a.Totalcost2,
                    QTY2_5 = a.QTY2_5,
                    Totalcost2_5 = a.Totalcost2_5,
                    QTY3 = a.QTY3,
                    Totalcost3 = a.Totalcost3,
                    QTY3_5 = a.QTY3_5,
                    Totalcost3_5 = a.Totalcost3_5,
                    QTY4 = a.QTY4,
                    Totalcost4 = a.Totalcost4,
                    QTY4_5 = a.QTY4_5,
                    Totalcost4_5 = a.Totalcost4_5,
                    QTY5 = a.QTY5,
                    Totalcost5 = a.Totalcost5,
                    QTY5_5 = a.QTY5_5,
                    Totalcost5_5 = a.Totalcost5_5,

                }).OrderByDescending(a => a.EarlyGoodsreceivedDate)
               .AsQueryable();
                pagedItems.TotalResults = data.Count();
                if (filterdata.Skip.HasValue)
                {
                    data = data.Skip(filterdata.Skip.Value);
                }
                else
                {
                    data = data.Skip(0);
                }

                if (filterdata.Take.HasValue)
                {
                    data = data.Take(filterdata.Take.Value);
                }
                else
                {
                    data = data.Take(10);
                }
                pagedItems.Items = data.ToList();

                if (data != null)
                {
                    result.Message = "Record Get Successfully";
                    result.StatusCode = 1;
                    result.IsValid = true;
                    result.Result = "success";
                    result.Data = pagedItems;
                }
                else
                {
                    result.Message = "Record does not exists";
                    result.StatusCode = 0;
                    result.IsValid = false;
                    result.Result = "error";
                    result.Data = null;
                }
                return result;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ResponseviewModel GetStockAgingReportsExcelExport()
        {
            try
            {
                this._Context.Database.SetCommandTimeout(1000);
                var data = _Context.SortAging.Select(a => new GetStockAgingReportsListView
                {
                    PickingType = a.PickingType,
                    LocationNumber = a.LocationNumber,
                    LocationId = a.LocationId,
                    PartCode = a.PartCode,
                    PartDescription = a.PartDescription,
                    EarlyGoodsreceivedDate = a.EarlyGoodsreceivedDate,
                    ReportRunnungDate = a.ReportRunnungDate,
                    Aging = a.Aging,
                    OnhandQty = a.OnhandQty,
                    UnitCost = a.UnitCost,
                    QTY0_5 = a.QTY0_5,
                    Totalcost0_5 = a.Totalcost0_5,
                    QTY1 = a.QTY1,
                    Totalcost1 = a.Totalcost1,
                    QTY1_5 = a.QTY1_5,
                    Totalcost1_5 = a.Totalcost1_5,
                    QTY2 = a.QTY2,
                    Totalcost2 = a.Totalcost2,
                    QTY2_5 = a.QTY2_5,
                    Totalcost2_5 = a.Totalcost2_5,
                    QTY3 = a.QTY3,
                    Totalcost3 = a.Totalcost3,
                    QTY3_5 = a.QTY3_5,
                    Totalcost3_5 = a.Totalcost3_5,
                    QTY4 = a.QTY4,
                    Totalcost4 = a.Totalcost4,
                    QTY4_5 = a.QTY4_5,
                    Totalcost4_5 = a.Totalcost4_5,
                    QTY5 = a.QTY5,
                    Totalcost5 = a.Totalcost5,
                    QTY5_5 = a.QTY5_5,
                    Totalcost5_5 = a.Totalcost5_5,
                })
               .ToList();
                if (data.Count > 0)
                {
                    result.Message = "Record Get Successfully";
                    result.StatusCode = 1;
                    result.IsValid = true;
                    result.Result = "success";
                    result.Data = data;
                }
                else
                {
                    result.Message = "Record does not exists";
                    result.StatusCode = 0;
                    result.IsValid = false;
                    result.Result = "error";
                    result.Data = null;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
        public ResponseviewModel GetStockAgingFeatchSP()
        {
            try
            {

                this._Context.Database.SetCommandTimeout(1000);
                var conn = _connectionStrings.wms; ;
                using (SqlConnection con = new SqlConnection(conn))
                {
                    using (SqlCommand cmd = new SqlCommand("wms.StockAgingRepack"))
                    {
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.Connection = con;
                        con.Open();


                        SqlDataReader da = cmd.ExecuteReader();
                        if (da.HasRows)
                        {
                            var data = da;
                        }
                    }
                }


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
    }
}
