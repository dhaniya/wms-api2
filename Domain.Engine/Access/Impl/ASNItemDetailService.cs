﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Domain.Engine.Access.Interfaces;
using Domain.Objects.Models;
using Infrastructure.Model.Main;
using Infrastructure.Model.Main.Entities;
namespace Domain.Engine.Access.Impl
{
    public class ASNItemDetailService:IASNItemDetailService
    {
        protected readonly MainDbContext _Context;
        protected MainDbContext Context
        {
            get
            {
                return (MainDbContext)_Context;
            }
        }

        public ASNItemDetailService(IMainDbContext context)
        {
            _Context = (MainDbContext)context;
        }
        #region AsnItemDetail
        public ResponseviewModel InsertUpdateAsnItemDetail(ASNItemDetailModels asnitemdetailmodels)
        {
            ResponseviewModel result = new ResponseviewModel();
            try
            {
                if (asnitemdetailmodels.Pk_AsnItemDetailsID == Guid.Empty)
                {
                    ASNItemDetails asnitemdetails = new ASNItemDetails();
                    asnitemdetails.Pk_AsnItemDetailsID = Guid.NewGuid();
                    asnitemdetails.FK_ASNMasterID= asnitemdetailmodels.Fk_AsnMasterID;
                    asnitemdetails.ContainerNo = asnitemdetailmodels.ContainerNo;
                    asnitemdetails.PalletNo = asnitemdetailmodels.PalletNo;
                    asnitemdetails.OrderNo = asnitemdetailmodels.OrderNo;
                    asnitemdetails.SuppliedPartNo = asnitemdetailmodels.SuppliedPartNo;
                    asnitemdetails.Quantity = asnitemdetailmodels.Quantity;
                    asnitemdetails.UnitPrice = asnitemdetailmodels.UnitPrice;
                    asnitemdetails.Length = asnitemdetailmodels.Length;
                    asnitemdetails.Breadth = asnitemdetailmodels.Breadth;
                    asnitemdetails.Height = asnitemdetailmodels.Height;
                    asnitemdetails.Weight = asnitemdetailmodels.Weight;
                    asnitemdetails.Customer = asnitemdetailmodels.Customer;
                    asnitemdetails.PurchaseOrderNo = asnitemdetailmodels.PurchaseOrderNo;
                    asnitemdetails.Remark1 = asnitemdetailmodels.Remark1;
                    asnitemdetails.Remark2 = asnitemdetailmodels.Remark2;
                    asnitemdetails.Remark3 = asnitemdetailmodels.Remark3;
                    asnitemdetails.Description = asnitemdetailmodels.Description;
                    asnitemdetails.IsDelete = false;
                    asnitemdetails.IsActive = true;
                    asnitemdetails.IsRecieved = false;
                    asnitemdetails.IsSorted = false;
                    asnitemdetails.CreatedBy = asnitemdetailmodels.CreatedBy;
                    asnitemdetails.CreatedDate = System.DateTime.Now;
                    asnitemdetails.ModifiedBy = asnitemdetailmodels.CreatedBy;
                    asnitemdetails.ModifiedDate = System.DateTime.Now;
                    _Context.AddAsync(asnitemdetails);
                    _Context.SaveChanges();
                    result.Message = "Data Inserted Successfully";
                    result.StatusCode = 1;
                    result.IsValid = true;
                    result.Result = "success";
                    result.pk_ID = asnitemdetails.Pk_AsnItemDetailsID;
                    result.Data = asnitemdetails;
                }
                else
                {
                    ASNItemDetails asnitemdetails = _Context.ASNItemDetails.FirstOrDefault(d => d.Pk_AsnItemDetailsID == asnitemdetailmodels.Pk_AsnItemDetailsID && d.IsActive == true && d.IsDelete == false);
                    if (asnitemdetails != null)
                    {
                        asnitemdetails.FK_ASNMasterID = asnitemdetailmodels.Fk_AsnMasterID;
                        asnitemdetails.ContainerNo = asnitemdetailmodels.ContainerNo;
                        asnitemdetails.PalletNo = asnitemdetailmodels.PalletNo;
                        asnitemdetails.OrderNo = asnitemdetailmodels.OrderNo;
                        asnitemdetails.SuppliedPartNo = asnitemdetailmodels.SuppliedPartNo;
                        asnitemdetails.Quantity = asnitemdetailmodels.Quantity;
                        asnitemdetails.UnitPrice = asnitemdetailmodels.UnitPrice;
                        asnitemdetails.Length = asnitemdetailmodels.Length;
                        asnitemdetails.Breadth = asnitemdetailmodels.Breadth;
                        asnitemdetails.Height = asnitemdetailmodels.Height;
                        asnitemdetails.Weight = asnitemdetailmodels.Weight;
                        asnitemdetails.Customer = asnitemdetailmodels.Customer;
                        asnitemdetails.PurchaseOrderNo = asnitemdetailmodels.PurchaseOrderNo;
                        asnitemdetails.Remark1 = asnitemdetailmodels.Remark1;
                        asnitemdetails.Remark2 = asnitemdetailmodels.Remark2;
                        asnitemdetails.Remark3 = asnitemdetailmodels.Remark3;
                        asnitemdetails.Description = asnitemdetailmodels.Description;
                        asnitemdetails.IsDelete = false;
                        asnitemdetails.IsActive = true;
                        asnitemdetails.ModifiedDate = System.DateTime.Now;
                        asnitemdetails.ModifiedBy = asnitemdetailmodels.ModifiedBy;
                        _Context.Update(asnitemdetails);
                        _Context.SaveChanges();
                        result.Message = "Data Updated Successfully";
                        result.StatusCode = 1;
                        result.IsValid = true;
                        result.Result = "success";
                        result.pk_ID = asnitemdetails.Pk_AsnItemDetailsID;
                        result.Data = asnitemdetails;
                    }
                    else
                    {
                        result.Message = "Record does not exists";
                        result.StatusCode = 0;
                        result.IsValid = false;
                        result.Result = "error";
                        result.pk_ID = asnitemdetails.Pk_AsnItemDetailsID;
                        result.Data = null;
                    }
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                result.StatusCode = 0;
                result.IsValid = false;
                result.Result = "error";
                result.pk_ID = null;
                result.Data = null;
            }
            return result;
        }

        public List<ASNItemDetailModels> GetAsnItemsDetails(Guid? Pk_ASNMasterID)
        {
            List<ASNItemDetailModels> ListAsnItemsDetails = new List<ASNItemDetailModels>();
            try
            {
                ListAsnItemsDetails = _Context.ASNItemDetails.Where(d => d.FK_ASNMasterID == Pk_ASNMasterID).Select(x => new ASNItemDetailModels
                {
                    Pk_AsnItemDetailsID = x.Pk_AsnItemDetailsID,
                    Fk_AsnMasterID = x.FK_ASNMasterID,
                    ContainerNo= x.ContainerNo,
                    PalletNo = x.PalletNo,
                    OrderNo = x.OrderNo,
                    SuppliedPartNo = x.SuppliedPartNo,
                    Quantity = x.Quantity,
                    UnitPrice = x.UnitPrice,
                    Length = x.Length,
                    Breadth = x.Breadth,
                    Height = x.Height,
                    Weight = x.Weight,
                    Customer = x.Customer,
                    PurchaseOrderNo = x.PurchaseOrderNo,
                    Remark1 = x.Remark1,
                    Remark2 = x.Remark2,
                    Remark3 = x.Remark3,
                    IsSorted = x.IsSorted,
                    IsRecieved = x.IsRecieved,
                    IsActive = x.IsActive,
                    CreatedDate=x.CreatedDate,
                    IsDelete = x.IsDelete
                }).OrderByDescending(a=>a.CreatedDate).ToList();
                
            }
            catch (Exception ex)
            {
                ListAsnItemsDetails = new List<ASNItemDetailModels>();
                throw ex;
            }
            return ListAsnItemsDetails;
        }

        #endregion
    }
}
