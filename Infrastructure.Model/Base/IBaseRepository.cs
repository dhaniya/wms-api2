﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Infrastructure.Model.Base
{
    interface IBaseRepository<C, T> : IDisposable
        where T : class
       where C : DbContext
    {
        C Context { get; set; }
        IQueryable<T> GetAll();
        IQueryable<T> FindBy(Expression<Func<T, bool>> predicate);
        Task AddAsync(T entity);
        Task AddRangeAsync(IEnumerable<T> entityList);
        void Delete(T entity);
        void DeleteRange(IEnumerable<T> entityList);
        void Edit(T entity);
        Task SaveAsync();

        //IQueryable<S> Sql<S>(string sql, params object[] parameters) where S : class;
        //IQueryable<T> Sql(string sql, params object[] parameters);

    }
}
