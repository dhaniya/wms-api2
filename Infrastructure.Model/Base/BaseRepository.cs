﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;


namespace Infrastructure.Model.Base
{
    public abstract class BaseRepository<C, T> :
    IDisposable,
    IBaseRepository<C, T>
       where C : DbContext
       where T : class
    {
        protected BaseRepository(C context)
        {
            Context = context;
        }

        public C Context { get; set; }

        public virtual IQueryable<T> GetAll()
        {
            return Context.Set<T>();
        }

        #region Sql
        /// <summary>
        /// Execute sql statement
        /// </summary>
        /// <typeparam name="S"></typeparam>
        /// <param name="sql"></param>
        /// <param name="parameters"></param>
        /// <returns>IQueryable<Set Type>"/></returns>
        //public virtual IQueryable<S> Sql<S>(string sql, params object[] parameters) where S : class
        //{
        //    return Context.Set<S>().FromSql(sql, parameters);
        //}
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="parameters"></param>
        /// <returns>IQueryable<T></returns>
        //public virtual IQueryable<T> Sql(string sql, params object[] parameters)
        //{
        //    return Context.Set<T>().FromSql(sql, parameters).AsQueryable<T>();
        //}
        #endregion Sql


        public virtual IQueryable<T> FindBy(System.Linq.Expressions.Expression<Func<T, bool>> predicate)
        {
            IQueryable<T> query = Context.Set<T>().Where(predicate);
            return query;
        }

        public virtual Task AddAsync(T entity)
        {
            return Context.Set<T>().AddAsync(entity);
        }
        public virtual Task AddRangeAsync(IEnumerable<T> entityList)
        {
            return Context.Set<T>().AddRangeAsync(entityList);
        }

        public virtual void Delete(T entity)
        {
            Context.Set<T>().Remove(entity);
        }
        public virtual void DeleteRange(IEnumerable<T> entityList)
        {
            Context.Set<T>().RemoveRange(entityList);
        }

        public virtual void Edit(T entity)
        {
            Context.Entry(entity).State = EntityState.Modified;
        }

        public virtual Task SaveAsync()
        {
            return Context.SaveChangesAsync();
        }

        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {

            if (!this.disposed && disposing)
            {
                Context.Dispose();
            }

            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
