﻿using Infrastructure.Model.Main.Entities;
using Microsoft.EntityFrameworkCore;



namespace Infrastructure.Model.Main
{
    public class MainDbContext : DbContext, IMainDbContext
    {
        public MainDbContext() : base()
        {

        }

        public MainDbContext(DbContextOptions<MainDbContext> options)
           : base(options)
        {

        }
        public virtual DbSet<CompanyMaster> CompanyMaster { get; set; }
        public virtual DbSet<SupplierMaster> SupplierMaster { get; set; }
        public virtual DbSet<ASNMaster> ASNMaster { get; set; }
        public virtual DbSet<ASNItemDetails> ASNItemDetails { get; set; }
        public virtual DbSet<SortingMaster> SortingMaster { get; set; }
        public virtual DbSet<SortingDetail> SortingDetail { get; set; }
        public virtual DbSet<GoodsReceiptMaster> GoodsReceiptMaster { get; set; }
        public virtual DbSet<CustomerMaster> CustomerMaster { get; set; }
        public virtual DbSet<PickingDetail> PickingDetail { get; set; }
        public virtual DbSet<PickingMaster> PickingMaster { get; set; }
        public virtual DbSet<DeliveryOrder> DeliveryOrder { get; set; }
        public virtual DbSet<DeliveryOrderDetail> DeliveryOrderDetail { get; set; }
        public virtual DbSet<BarcodeGenerate> BarcodeGenerate { get; set; }
        public virtual DbSet<LocationMaster> LocationMaster { get; set; }
        public virtual DbSet<PartsMaster> PartsMaster { get; set; }
        public virtual DbSet<BinMaster> BinMaster { get; set; }
        public virtual DbSet<UserMaster> UserMaster { get; set; }
        public virtual DbSet<RoleMaster> RoleMaster { get; set; }
        public virtual DbSet<StockTransaction> StockTransaction { get; set; }
        public virtual DbSet<BinLocationHistory> BinLocationHistory { get; set; }
        public virtual DbSet<LocationTransferLog> LocationTransferLog { get; set; }
        public virtual DbSet<LooseItemTransferLog> LooseItemTransferLog { get; set; }
        public virtual DbSet<UserAccess> UserAccess { get; set; }
        public virtual DbSet<StockCount> StockCountTab { get; set; }
        public virtual DbSet<BinItemHistory> BinItemHistory  { get; set; }

        public virtual DbSet<SortAging> SortAging { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("wms");
        }
        
    }
}
