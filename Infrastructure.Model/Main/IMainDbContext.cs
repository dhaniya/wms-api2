﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Model.Main
{
    public interface IMainDbContext
    {
        int SaveChanges();
    }
}
