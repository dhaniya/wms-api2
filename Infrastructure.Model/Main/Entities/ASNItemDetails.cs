﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;


namespace Infrastructure.Model.Main.Entities
{
    public class ASNItemDetails
    {  
        [Key]
        public Guid Pk_AsnItemDetailsID { get; set; }
        [ForeignKey("ASNMaster")]
        public Guid FK_ASNMasterID { get; set; }
        public virtual ASNMaster ASNMaster { get; set; }
        public string ContainerNo { get; set; }
        public string PalletNo { get; set; }//Pallet No = Case No
        public string OrderNo { get; set; }
        public string SuppliedPartNo { get; set; }
        public int Quantity{ get; set; }
        public decimal UnitPrice { get; set; }
        public decimal Length { get; set; }
        public decimal Breadth { get; set; }
        public decimal Height { get; set; }
        public decimal Weight { get; set; }
        public string Customer { get; set; }
        public string PurchaseOrderNo { get; set; }
        public string Remark1 { get; set; }
        public string Remark2 { get; set; }
        public string Remark3 { get; set; }
        public bool IsSorted { get; set; }
        public bool IsRecieved { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public bool IsDelete { get; set; }
        [ForeignKey("UserMaster")]
        public Guid? CreatedBy { get; set; }
        public virtual UserMaster UserMaster { get; set; }
        public Guid? ModifiedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string LocationId{ get; set; }
        public int? ReceivedQty { get; set; }

    }
}
