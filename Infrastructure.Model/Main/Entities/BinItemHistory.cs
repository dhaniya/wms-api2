﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Infrastructure.Model.Main.Entities
{
   public class BinItemHistory
    {

        [Key]
        public Guid Pk_BinItemHistoryId { get; set; }
        [ForeignKey("SortingMaster")]
        public Guid FK_SortingMasterID { get; set; }
        public string RepakedCaseNo { get; set; }
        public string InvoiceNo { get; set; }
        public string CaseNo { get; set; }
        public string ItemCode { get; set; } //Part No
        public string LocationID { get; set; }
        public int Quantity { get; set; }
        public bool IsActive { get; set; }
        public bool IsDelete { get; set; }
        [ForeignKey("UserMaster")]
        public Guid? CreatedBy { get; set; }
        public virtual UserMaster UserMaster { get; set; }
        public Guid? ModifiedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
    }
}
