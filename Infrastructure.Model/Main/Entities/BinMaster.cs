﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Infrastructure.Model.Main.Entities
{
    public class BinMaster
    {
        [Key]
        public Guid Pk_BinMasterId { get; set; }
        public string RackName { get; set; }
        public string LocationCode { get; set; }
        public string RowNo { get; set; }
        public string ColumnNo { get; set; }
        public bool IsUsed { get; set; }
        public bool IsActive { get; set; }
        public bool IsDelete { get; set; }
        [ForeignKey("UserMaster")]
        public Guid? CreatedBy { get; set; }
        public virtual UserMaster UserMaster { get; set; }
        public Guid? ModifiedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
    }
}
