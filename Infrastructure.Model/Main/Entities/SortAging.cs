﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Infrastructure.Model.Main.Entities
{
    public class SortAging
    {
        [Key]
        public int id { get; set; }
        public string PickingType { get; set; }
        public string LocationNumber { get; set; }
        public string LocationId { get; set; }
        public string PartCode { get; set; }
        public string PartDescription { get; set; }
        public DateTime EarlyGoodsreceivedDate { get; set; }//1
        public DateTime ReportRunnungDate { get; set; }//2
        public int Aging { get; set; }//count date 1-2
        public int? OnhandQty { get; set; }//AvalibelQty
        public decimal? UnitCost { get; set; }//price unite cost
        public int? QTY0_5 { get; set; }
        public Decimal? Totalcost0_5 { get; set; }
        public int? QTY1 { get; set; }
        public Decimal? Totalcost1 { get; set; }
        public int? QTY1_5 { get; set; }
        public Decimal? Totalcost1_5 { get; set; }
        public int? QTY2 { get; set; }
        public Decimal? Totalcost2 { get; set; }
        public int? QTY2_5 { get; set; }
        public Decimal? Totalcost2_5 { get; set; }
        public int? QTY3 { get; set; }
        public Decimal? Totalcost3 { get; set; }
        public int? QTY3_5 { get; set; }
        public Decimal? Totalcost3_5 { get; set; }
        public int? QTY4 { get; set; }
        public Decimal? Totalcost4 { get; set; }
        public int? QTY4_5 { get; set; }
        public Decimal? Totalcost4_5 { get; set; }
        public int? QTY5 { get; set; }
        public Decimal? Totalcost5 { get; set; }
        public int? QTY5_5 { get; set; }
        public Decimal? Totalcost5_5 { get; set; }

    }
}
