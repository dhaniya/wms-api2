﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Infrastructure.Model.Main.Entities
{
  public  class UserAccess
    {
        [Key]
        public Guid Pk_UserAccessId { get; set; }
        public string RoleName { get; set; }
        public string RouteUrl { get; set; }
        public bool Add { get; set; }
        public bool Edit { get; set; }
        public bool Delete { get; set; }
        public bool View { get; set; }
        public bool IsDeleted { get; set; }
        [ForeignKey("User")]
        [Column(Order =1)]
        public Guid? UserId { get; set; }
        [InverseProperty("UserAccesses")]
        public virtual UserMaster User { get; set; }
        [ForeignKey("UserMaster")]
        [Column(Order = 1)]
        public Guid? CreatedBy { get; set; }
        public virtual UserMaster UserMaster { get; set; }
        public Guid? ModifiedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
    }
}
