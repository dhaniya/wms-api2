﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;


namespace Infrastructure.Model.Main.Entities
{
    public class UserMaster
    {   [Key]
        public Guid Pk_UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string EmailID { get; set; }
        public string ContactNumber { get; set; }
        public string Token { get; set; }
        [ForeignKey("RoleMaster")]
        public Guid FK_RoleID { get; set; }
        public virtual RoleMaster RoleMaster { get; set; }
        public string RoleName { get; set; }
        public bool IsActive { get; set; }
        public bool IsDelete { get; set; }
        public Guid? CreatedBy { get; set; }
        public Guid? ModifiedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public virtual ICollection<UserAccess> UserAccesses { get; set; }
    }
}
