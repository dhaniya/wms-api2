﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Infrastructure.Model.Main.Entities
{
   public class LocationTransferLog
    {
        [Key]
        public Guid LocationTransferLogID { get; set; }
        public Guid Fk_SortingMasterID { get; set; }
        public int Quantity { get; set; }
        //0 for pick 1 for drop
        public int Type { get; set; }
        public DateTime? CreatedDate { get; set; }
        public Guid FK_BinLocationHistoryId { get; set; }
        public string Location { get; set; }
    }
}
