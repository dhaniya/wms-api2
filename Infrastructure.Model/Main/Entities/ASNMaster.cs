﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;


namespace Infrastructure.Model.Main.Entities
{
    public class ASNMaster
    {
        [Key]
        public Guid PK_AsnMasterID { get; set; }
        [ForeignKey("CompanyMaster")]
        public Guid FK_CompanyID { get; set; }
        public virtual CompanyMaster CompanyMaster { get; set; }
        [ForeignKey("SupplierMaster")]
        public Guid FK_SupplierID { get; set; }
        public virtual SupplierMaster SupplierMaster { get; set; }
        public DateTime DateOfArrival { get; set; }
        public string InvoiceNo { get; set; }
        public string origin { get; set; }
        public string Remark { get; set; }
        public int shippingmodeID { get; set; }
        public string ShippingmodeName { get; set; }
        public string VesselName { get; set; }
        public string VesselVoyageNo { get; set; }
        public string BillNumber { get; set; }
        public string FlightName { get; set; }
        public string FlightNumber { get; set; }
        public string AWLNumber { get; set; }
        public string TruckNumber { get; set; }
        public string DeliveryNumber { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public bool IsDelete { get; set; }
        [ForeignKey("UserMaster")]
        public Guid? CreatedBy { get; set; }
        public virtual UserMaster UserMaster { get; set; }
        public Guid? ModifiedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
    }
}
