﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Infrastructure.Model.Main.Entities
{
  public class PickingDetail
    {
        [Key]
        public Guid Pk_PickingDetailId { get; set; }
        [ForeignKey("PickingMaster")]
        public Guid Fk_PickingID { get; set; }
        public virtual PickingMaster PickingMaster { get; set; }
        public int? PickingType { get; set; }
        public string InvoiceNo { get; set; }
        public string RepackedCaseNo { get; set; }
        public string ItemCode { get; set; }
        public int Quantity { get; set; }
        public string LocationId { get; set; }
        public decimal Height { get; set; }
        public decimal Length { get; set; }
        public decimal Breadth { get; set; }
        public decimal Weight { get; set; }
        public bool IsPicked { get; set; }
        public bool IsActive { get; set; }
        public bool IsDelete { get; set; }
        [ForeignKey("SortingMaster")]
        public Guid? Fk_SortingMasterID { get; set; }
        public virtual SortingMaster SortingMaster { get; set; }
        [ForeignKey("UserMaster")]
        public Guid? CreatedBy { get; set; }
        public virtual UserMaster UserMaster { get; set; }
        public Guid? ModifiedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
       
    }
}
