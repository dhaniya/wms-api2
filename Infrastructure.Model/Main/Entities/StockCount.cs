﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Infrastructure.Model.Main.Entities
{
  public  class StockCount
    {
        [Key]
        public Guid Pk_StockCountID { get; set; }
        public string Location { get; set; }
        public string ItemCode { get; set; }
        public int RunningNo { get; set; }
        public int Qty { get; set; }
        [ForeignKey("UserMaster")]
        [Column(Order = 1)]
        public Guid? CreatedBy { get; set; }
        public virtual UserMaster UserMaster { get; set; }
        public Guid? ModifiedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
    }

    //public class StockCountView
    //{
    //    public Guid Pk_StockCountID { get; set; }
    //    public string Location { get; set; }
    //    public string ItemCode { get; set; }
    //    public int RunningNo { get; set; }
    //    public int Qty { get; set; }
    //    public Guid? CreatedBy { get; set; }
    //    public Guid? ModifiedBy { get; set; }
    //    public DateTime CreatedDate { get; set; }
    //    public DateTime ModifiedDate { get; set; }

    //}
}
