﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Infrastructure.Model.Main.Entities
{
    public class PickingMaster
    {
        [Key]
        public Guid Pk_PickingMasterID { get; set; }
        public string OrderNumber { get; set; }
        [ForeignKey("CustomerMaster")]
        public Guid? Fk_CustomerMasterId { get; set; }

        public virtual CustomerMaster CustomerMaster { get; set; }
        
        public string CustomerName { get; set; }
        public string CustomerCode { get; set; }
        public DateTime DateOfDeliver { get; set; }
        public string RefNo { get; set; }
        public int Priority { get; set; }
        public int PickingStatus { get; set; }
        public bool IsDeliverOrder { get; set; }
        public string LocationId { get; set; }
        public bool IsActive { get; set; }
        public bool IsDelete { get; set; }
        [ForeignKey("UserMaster")]
        public Guid? CreatedBy { get; set; }
        public virtual UserMaster UserMaster { get; set; }
        public Guid? ModifiedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
    }
}
