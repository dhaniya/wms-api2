using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Infrastructure.Model.Main.Entities
{
    public class StockTransaction
    {
        [Key]
        public Guid StockTransactionID { get; set; }
        [ForeignKey("SortingMaster")]
        public Guid? PK_SortingID { get; set; }
        public virtual SortingMaster SortingMaster { get; set; }
        public int? QuantityReceived { get; set; }
        public int? QuantityDelivered { get; set; }
        public int? QuantitySelected { get; set; }
        public int? QuantityRemaining { get; set; }
        public Guid? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public Guid? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }
}
