﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Infrastructure.Model.Main.Entities
{
    public class DeliveryOrder
    {
        [Key]
        public Guid Pk_DeliveryOrderID { get; set; }
        [ForeignKey("CompanyMaster")]
        public Guid? Fk_CompanyID { get; set; }
        public virtual CompanyMaster CompanyMaster { get; set; }
        public string DONumber { get; set; }
        public string DriverName { get; set; }
        public string VehicleNumber { get; set; }
        public int shippingmodeID { get; set; }
        public DateTime ActualDateOfDeliver { get; set; }

        [ForeignKey("PickingMaster")]
        public Guid Fk_PickingMasterID { get; set; }
        public virtual PickingMaster PickingMaster { get; set; }
        public string OrderNumber { get; set; }
        [ForeignKey("CustomerMaster")]
        public Guid? Fk_CustomerMasterId { get; set; }
        public virtual CustomerMaster CustomerMaster { get; set; }
        public string CustomerName { get; set; }
        public string CustomerCode { get; set; }
        public DateTime DateOfDeliver { get; set; }
        public string RefNo { get; set; }
        public int Priority { get; set; }
        public string Address { get; set; }
        public bool IsActive { get; set; }
        public bool IsDelete { get; set; }
        public bool IsConfirm { get; set; }

        [ForeignKey("UserMaster")]
        public Guid? CreatedBy { get; set; }
        public virtual UserMaster UserMaster { get; set; }
        public Guid? ModifiedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public int? NoOfBox { get; set; }
        public string Subject { get; set; }
        public string Remarks { get; set; }
    }
}
