﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Infrastructure.Model.Main.Entities
{
    public class BinLocationHistory
    {
        [Key]
        public Guid PK_BinLocationHistoryId { get; set; }
        public int Quantity { get; set; }
        [ForeignKey("SortingMaster")]
        public Guid FK_SortingMasterID { get; set; }
        public virtual SortingMaster SortingMaster { get; set; }
        public string CurrentLocation { get; set; }
        public string NewLocation { get; set; }
        public int Status { get; set; }
        public int Type { get; set; } //0 for bin 1 for loose
        public bool IsActive { get; set; }
        public bool IsDelete { get; set; }
        [ForeignKey("UserMaster")]
        public Guid? CreatedBy { get; set; }
        public virtual UserMaster UserMaster { get; set; }
        public Guid? ModifiedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
       
    }
}
