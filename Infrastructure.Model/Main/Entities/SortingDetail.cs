﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;


namespace Infrastructure.Model.Main.Entities
{
   public class SortingDetail
    {

        [Key]
        public Guid Pk_SortingDetailID { get; set; }
        [ForeignKey("SortingMaster")]
        public Guid FK_SortingMasterID { get; set; }
        public virtual SortingMaster SortingMaster { get; set; }
        public string RepakedCaseNo { get; set; }
        public decimal Height { get; set; }
        public decimal Length { get; set; }
        public decimal Breadth { get; set; }
        public decimal Volume { get; set; }
        public decimal Weight { get; set; }
        public bool IsActive { get; set; }
        public bool IsDelete { get; set; }
        [ForeignKey("UserMaster")]
        public Guid? CreatedBy { get; set; }
        public virtual UserMaster UserMaster { get; set; }
        public Guid? ModifiedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
    }
}
