﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Infrastructure.Model.Migrations
{
    public partial class ChangecolumnnameDeliverNametoDriverName : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "DeliverName",
                table: "DeliveryOrder",
                newName: "DriverName");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "DriverName",
                table: "DeliveryOrder",
                newName: "DeliverName");
        }
    }
}
