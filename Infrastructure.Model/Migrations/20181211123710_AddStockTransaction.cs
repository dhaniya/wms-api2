﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Infrastructure.Model.Migrations
{
    public partial class AddStockTransaction : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
           

            migrationBuilder.CreateTable(
                name: "StockTransaction",
                columns: table => new
                {
                    StockTransactionID = table.Column<Guid>(nullable: false),
                    PK_SortingID = table.Column<Guid>(nullable: true),
                    QuantityReceived = table.Column<int>(nullable: true),
                    QuantityDelivered = table.Column<int>(nullable: true),
                    QuantitySelected = table.Column<int>(nullable: true),
                    QuantityRemaining = table.Column<int>(nullable: true),
                    CreatedBy = table.Column<Guid>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: true),
                    ModifiedBy = table.Column<Guid>(nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StockTransaction", x => x.StockTransactionID);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "StockTransaction");

            
           
           
        }
    }
}
