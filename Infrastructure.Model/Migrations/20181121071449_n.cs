﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Infrastructure.Model.Migrations
{
    public partial class n : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "BinMaster",
                columns: table => new
                {
                    Pk_BinMasterId = table.Column<Guid>(nullable: false),
                    RackName = table.Column<string>(nullable: true),
                    LocationCode = table.Column<string>(nullable: true),
                    RowNo = table.Column<string>(nullable: true),
                    ColumnNo = table.Column<string>(nullable: true),
                    IsUsed = table.Column<bool>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    IsDelete = table.Column<bool>(nullable: false),
                    CreatedBy = table.Column<Guid>(nullable: true),
                    ModifiedBy = table.Column<Guid>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BinMaster", x => x.Pk_BinMasterId);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BinMaster");
        }
    }
}
