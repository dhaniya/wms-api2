﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Infrastructure.Model.Migrations
{
    public partial class AddBinLocationHistory : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
           migrationBuilder.CreateTable(
                name: "BinLocationHistory",
                columns: table => new
                {
                    PK_BinLocationHistoryId = table.Column<Guid>(nullable: false),
                    Quantity = table.Column<int>(nullable: false),
                    FK_SortingMasterID = table.Column<Guid>(nullable: false),
                    CurrentLocation = table.Column<string>(nullable: true),
                    NewLocation = table.Column<string>(nullable: true),
                    IsActive = table.Column<bool>(nullable: false),
                    IsDelete = table.Column<bool>(nullable: false),
                    CreatedBy = table.Column<Guid>(nullable: true),
                    ModifiedBy = table.Column<Guid>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BinLocationHistory", x => x.PK_BinLocationHistoryId);
                    table.ForeignKey(
                        name: "FK_BinLocationHistory_SortingMaster_FK_SortingMasterID",
                        column: x => x.FK_SortingMasterID,
                        principalTable: "SortingMaster",
                        principalColumn: "Pk_SortingID",
                        onDelete: ReferentialAction.Cascade);
                });

            

            migrationBuilder.CreateIndex(
                name: "IX_BinLocationHistory_FK_SortingMasterID",
                table: "BinLocationHistory",
                column: "FK_SortingMasterID");

          

           
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
           

            migrationBuilder.DropTable(
                name: "BinLocationHistory");

           

        }
    }
}
