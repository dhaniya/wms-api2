﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Infrastructure.Model.Migrations
{
    public partial class ASNMaster : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ReceivedQty",
                table: "ASNMaster",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ReceivedQty",
                table: "ASNMaster");
        }
    }
}
