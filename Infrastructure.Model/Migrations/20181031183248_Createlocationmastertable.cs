﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Infrastructure.Model.Migrations
{
    public partial class Createlocationmastertable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "LocationMaster",
                columns: table => new
                {
                    Pk_LocationMasterId = table.Column<Guid>(nullable: false),
                    RackName = table.Column<string>(nullable: true),
                    LocationCode = table.Column<string>(nullable: true),
                    LocationType = table.Column<int>(nullable: false),
                    ZoneType = table.Column<int>(nullable: false),
                    MyProperty = table.Column<int>(nullable: false),
                    IsUsed = table.Column<bool>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    IsDelete = table.Column<bool>(nullable: false),
                    CreatedBy = table.Column<Guid>(nullable: true),
                    ModifiedBy = table.Column<Guid>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LocationMaster", x => x.Pk_LocationMasterId);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "LocationMaster");
        }
    }
}
