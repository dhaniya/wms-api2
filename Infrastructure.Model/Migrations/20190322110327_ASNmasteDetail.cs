﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Infrastructure.Model.Migrations
{
    public partial class ASNmasteDetail : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ReceivedQty",
                table: "ASNMaster");

            migrationBuilder.AddColumn<int>(
                name: "ReceivedQty",
                table: "ASNItemDetails",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ReceivedQty",
                table: "ASNItemDetails");

            migrationBuilder.AddColumn<int>(
                name: "ReceivedQty",
                table: "ASNMaster",
                nullable: true);
        }
    }
}
