﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Infrastructure.Model.Migrations
{
    public partial class createpartsmaster : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "PartsMaster",
                columns: table => new
                {
                    Pk_PartsMasterID = table.Column<Guid>(nullable: false),
                    PartNumber = table.Column<string>(nullable: true),
                    PartDescription = table.Column<string>(nullable: true),
                    PartUnitPrice = table.Column<decimal>(nullable: false),
                    length = table.Column<decimal>(nullable: false),
                    Breadth = table.Column<decimal>(nullable: false),
                    Height = table.Column<decimal>(nullable: false),
                    Weight = table.Column<decimal>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    IsDelete = table.Column<bool>(nullable: false),
                    CreatedBy = table.Column<Guid>(nullable: true),
                    ModifiedBy = table.Column<Guid>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PartsMaster", x => x.Pk_PartsMasterID);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PartsMaster");
        }
    }
}
