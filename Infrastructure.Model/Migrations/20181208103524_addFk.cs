﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Infrastructure.Model.Migrations
{
    public partial class addFk : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "Fk_SortingID",
                table: "PickingDetail",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "Fk_SortingID",
                table: "DeliveryOrderDetail",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.CreateIndex(
                name: "IX_PickingDetail_Fk_SortingID",
                table: "PickingDetail",
                column: "Fk_SortingID");

            migrationBuilder.CreateIndex(
                name: "IX_DeliveryOrderDetail_Fk_SortingID",
                table: "DeliveryOrderDetail",
                column: "Fk_SortingID");

            migrationBuilder.AddForeignKey(
                name: "FK_DeliveryOrderDetail_SortingMaster_Fk_SortingID",
                table: "DeliveryOrderDetail",
                column: "Fk_SortingID",
                principalTable: "SortingMaster",
                principalColumn: "Pk_SortingID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_PickingDetail_SortingMaster_Fk_SortingID",
                table: "PickingDetail",
                column: "Fk_SortingID",
                principalTable: "SortingMaster",
                principalColumn: "Pk_SortingID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DeliveryOrderDetail_SortingMaster_Fk_SortingID",
                table: "DeliveryOrderDetail");

            migrationBuilder.DropForeignKey(
                name: "FK_PickingDetail_SortingMaster_Fk_SortingID",
                table: "PickingDetail");

            migrationBuilder.DropIndex(
                name: "IX_PickingDetail_Fk_SortingID",
                table: "PickingDetail");

            migrationBuilder.DropIndex(
                name: "IX_DeliveryOrderDetail_Fk_SortingID",
                table: "DeliveryOrderDetail");

            migrationBuilder.DropColumn(
                name: "Fk_SortingID",
                table: "PickingDetail");

            migrationBuilder.DropColumn(
                name: "Fk_SortingID",
                table: "DeliveryOrderDetail");
        }
    }
}
