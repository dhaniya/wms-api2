﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Infrastructure.Model.Migrations
{
    public partial class DeiveryOrdernewcolum : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Address",
                table: "PickingMaster");

            migrationBuilder.AddColumn<string>(
                name: "Address",
                table: "DeliveryOrder",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Address",
                table: "DeliveryOrder");

            migrationBuilder.AddColumn<string>(
                name: "Address",
                table: "PickingMaster",
                nullable: true);
        }
    }
}
