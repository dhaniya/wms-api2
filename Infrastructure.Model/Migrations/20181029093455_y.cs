﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Infrastructure.Model.Migrations
{
    public partial class y : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "BarcodeGenerate",
                columns: table => new
                {
                    Pk_BarcodeID = table.Column<Guid>(nullable: false),
                    CustomerCode = table.Column<string>(nullable: true),
                    BarCode = table.Column<string>(nullable: true),
                    InvoiceNo = table.Column<string>(nullable: true),
                    IsActive = table.Column<bool>(nullable: false),
                    IsDelete = table.Column<bool>(nullable: false),
                    CreatedBy = table.Column<Guid>(nullable: true),
                    ModifiedBy = table.Column<Guid>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BarcodeGenerate", x => x.Pk_BarcodeID);
                });
        }
    }
}
