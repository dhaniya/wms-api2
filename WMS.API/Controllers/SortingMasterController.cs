﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using System.Net.Http;
using Domain.Engine.Access.Impl;
using Domain.Engine.Access.Interfaces;
using Domain.Objects.Models;
using WMS.API.Models;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;

namespace WMS.API.Controllers
{
    [Authorize]
    public class SortingMasterController : Controller
    {
        private readonly ISortingMasterService _Sortingmasterservice;
        public SortingMasterController(ISortingMasterService Sortingmasterservice)
        {
            _Sortingmasterservice = Sortingmasterservice;
        }

        [HttpPost]
        [Route("/SortingMaster/InsertUpdateSortingMaster")]
        public ResponseviewModel InsertUpdateSortingMaster([FromBody]SortingMasterModel sortingmastermodel)
        {
            string userid = User.FindFirst(ClaimTypes.Name)?.Value;
            sortingmastermodel.CreatedBy = Guid.Parse(userid);
            sortingmastermodel.ModifiedBy = Guid.Parse(userid);
            return _Sortingmasterservice.InsertUpdateSortingMaster(sortingmastermodel);
        }


        [HttpGet]
        [Route("/SortingMaster/DeleteSortingMasterbyID")]
        public ResponseviewModel DeleteSortingMasterbyID(Guid id)
        {
            SortingMasterModel sortingm = new SortingMasterModel();
            string userid = User.FindFirst(ClaimTypes.Name)?.Value;
            sortingm.ModifiedBy = Guid.Parse(userid);
            sortingm.Pk_SortingID = id;
            return _Sortingmasterservice.DeleteSortingMasterbyID(sortingm);
        }

        [HttpGet]
        [Route("/SortingMaster/GetAllSortingMaster")]
        public ResponseviewModel GetAllSortingMaster()
        {
            return _Sortingmasterservice.GetAllSortingMaster();
        }


        [HttpPost]
        [Route("/SortingMaster/AddPutAwayBox")]
        public ResponseviewModel AddPutAwayBox([FromBody]PutAwayViewModel putawayviewmodel)
        {
            string userid = User.FindFirst(ClaimTypes.Name)?.Value;
            putawayviewmodel.ModifiedBy = Guid.Parse(userid);
            return _Sortingmasterservice.AddPutAwayBox(putawayviewmodel);
        }


        [HttpGet]
        [Route("/SortingMaster/GetPutAwayList")]
        public ResponseviewModel GetPutAwayList()
        {
            return _Sortingmasterservice.GetPutAwayList();
        }

        [HttpGet]
        [Route("/SortingMaster/GetPutAwayListByDate")]
        public ResponseviewModel GetPutAwayListByDate(DateTime fromdate , DateTime todate)
        {
            return _Sortingmasterservice.GetPutAwayListByDate(fromdate,todate);
        }

        [HttpGet]
        [Route("/SortingMaster/GetPutAwayListByInvoice")]
        public ResponseviewModel GetPutAwayListInvoice(string Invoice)
        {
            return _Sortingmasterservice.GetPutAwayListInvoice(Invoice);
        }


    }
}