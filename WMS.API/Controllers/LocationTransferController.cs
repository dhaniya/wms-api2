﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using System.Net.Http;
using Domain.Engine.Access.Impl;
using Domain.Engine.Access.Interfaces;
using Domain.Objects.Models;
using WMS.API.Models;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;

namespace WMS.API.Controllers
{
    [Authorize]
    [Produces("application/json")]
    [Route("api/LocationTransfer")]
    public class LocationTransferController : Controller
    {
        private readonly ILocationTransferService _LocationTransferService;
        public LocationTransferController(ILocationTransferService locationtransferservice)
        {
            _LocationTransferService = locationtransferservice;
        }

        [HttpGet]
        [Route("/LocationTransfer/GetLocationTransferlist")]
        public ResponseviewModel GetLocationTransferlist(searchlocationtransfer searchlocationtransfer)
        {
            return _LocationTransferService.GetLocationTransferlist(searchlocationtransfer);
        }

        [HttpGet]
        [Route("/LocationTransfer/GetDropdownofRepackedCaseNo")]
        public ResponseviewModel GetDropdownofRepackedCaseNo()
        {
            return _LocationTransferService.GetDropdownofRepackedCaseNo();
        }

        [HttpGet]
        [Route("/LocationTransfer/GetDropdownofLocationId")]
        public ResponseviewModel GetDropdownofLocationId()
        {
            return _LocationTransferService.GetDropdownofLocationId();
        }

        [HttpPost]
        [Route("/LocationTransfer/UpdateLocationBysortingid")]
        public ResponseviewModel UpdateLocationBysortingid([FromBody]UpdateLocationTransferModel updatelocationtransfermodel)
        {
            string userid = User.FindFirst(ClaimTypes.Name)?.Value;
            updatelocationtransfermodel.ModifiedBy = Guid.Parse(userid);
            return _LocationTransferService.UpdateLocationBysortingid(updatelocationtransfermodel);
        }
    }
}