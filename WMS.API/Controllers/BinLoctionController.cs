using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using System.Net.Http;
using Domain.Engine.Access.Impl;
using Domain.Engine.Access.Interfaces;
using Domain.Objects.Models;
using WMS.API.Models;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;

namespace WMS.API.Controllers
{
    [Authorize]
    [Produces("application/json")]
    [Route("api/BinLocation")]
    public class BinLocationController : Controller
    {
        private readonly IBinLocationService _BinLocationService;
        public BinLocationController(IBinLocationService binlocationservice)
        {
            _BinLocationService = binlocationservice;
        }


        [HttpGet]
        [Route("/BinLocation/GetDDLRepackedCaseNo")]
        public ResponseviewModel GetDDLRepackedCaseNo()
        {
            return _BinLocationService.GetDDLRepackedCaseNo();
        }

        [HttpGet]
        [Route("/BinLocation/GetDDLItemCodeByRepackedcaseNo")]
        public ResponseviewModel GetDDLItemCodeByRepackedcaseNo(string repackedcaseno)
        {
            return _BinLocationService.GetDDLItemCodeByRepackedcaseNo(repackedcaseno);
        }

        [HttpGet]
        [Route("/BinLocation/GetNoofPiecesAndQtyByItemCode")]
        public ResponseviewModel GetNoofPiecesAndQtyByItemCode(string caseno,string itemcode)
        {
            return _BinLocationService.GetNoofPiecesAndQtyByItemCode(caseno,itemcode);
        }

        [HttpPost]
        [Route("/BinLocation/InsertBinLocation")]
        public ResponseviewModel InsertBinLocation([FromBody]BinLocationModel binlocationmodel)
        {
            string userid = User.FindFirst(ClaimTypes.Name)?.Value;
            binlocationmodel.CreatedBy = Guid.Parse(userid);

            return _BinLocationService.InsertBinLocation(binlocationmodel);
        }

        [HttpPost]
        [Route("/BinLocation/GetSortedBinLocation")]
        public ResponseviewModel GetSortedBinLocation([FromBody]SortedBinLocationModel model)
        {
            return _BinLocationService.GetSortedBinLocation(model);
        }

        [HttpGet]
        [Route("/BinLocation/GetSortedBinLocationExcel")]
        public ResponseviewModel GetSortedBinLocationExcel()
        {
            return _BinLocationService.GetSortedBinLocationExcel();
        }

        [HttpGet]
        [Route("/BinLocation/GetSortedBinLocationItemCode")]
        public ResponseviewModel GetSortedBinLocationItemCode(string ItemCode)
        {
            return _BinLocationService.GetSortedBinLocationItemCode(ItemCode);
        }

        [HttpGet]
        [Route("/BinLocation/GetBinAvailableQTYByItemCode")]
        public ResponseviewModel GetBinAvailableQTYByItemCode(string ItemCode,string locationid)
        {
            return _BinLocationService.GetBinAvailableQTYByItemCode(ItemCode,locationid);
        }

        [HttpPost]
        [Route("/BinLocationHistory/InsertBinLocationHistory")]
        public ResponseviewModel InsertBinLocationHistory ([FromBody]BinLocationHistoryModel binlocationhistorymodel)
        {
            string userid = User.FindFirst(ClaimTypes.Name)?.Value;
            binlocationhistorymodel.CreatedBy = Guid.Parse(userid);
            binlocationhistorymodel.ModifiedBy = Guid.Parse(userid);
            return _BinLocationService.InsertBinLocationHistory(binlocationhistorymodel);
        }
        [HttpGet]
        [Route("/BinLocationHistory/GetBinHistorylist")]
        public ResponseviewModel GetBinHistorylist()
        {
            return _BinLocationService.GetBinPickedItem();
        }
        [HttpGet]
        [Route("/BinLocationHistory/GetLooseHistorylist")]
        public ResponseviewModel GetLooseHistorylist()
        {
            return _BinLocationService.GetLoosePickedItem();
        }
        [HttpPost]
        [Route("/BinLocationHistory/InsertBinLocationHistoryLooseItem")]
        public ResponseviewModel InsertBinLocationHistoryLooseItem([FromBody]BinLocationHistoryModel binlocationhistorymodel)
        {
            string userid = User.FindFirst(ClaimTypes.Name)?.Value;
            binlocationhistorymodel.CreatedBy = Guid.Parse(userid);
            binlocationhistorymodel.ModifiedBy = Guid.Parse(userid);
            return _BinLocationService.InsertBinLocationHistoryLooseItem(binlocationhistorymodel);
        }
        [HttpGet]
        [Route("/BinLocationHistory/getRePackbyCode")]
        public ResponseviewModel getRePackbyCode(string custcode)
        {
            return _BinLocationService.getRePackbyCode(custcode);
        }

    [HttpGet]
    [Route("/BinLocation/GetBinItemLocationHistory")]
    public ResponseviewModel GetBinItemLocationHistory(string locationId , string itemcode)
    {
      return _BinLocationService.GetBinItemLocationHistory(locationId,itemcode);
    }

    [HttpGet]
    [Route("/BinLocationHistory/DeleteBinItemLocationbyID")]
    public ResponseviewModel DeleteBinItemLocationbyID(Guid id)
    {
      BinLocationHistoryModel binLocationHistory = new BinLocationHistoryModel();
      string userid = User.FindFirst(ClaimTypes.Name)?.Value;
      binLocationHistory.ModifiedBy = Guid.Parse(userid);
      binLocationHistory.PK_BinLocationHistoryId = id;
      return _BinLocationService.DeleteBinItemLocationbyID(binLocationHistory);
    }

    [HttpGet]
    [Route("/BinLocationHistory/DeleteBinInLocationbyID")]
    public ResponseviewModel DeleteBinInLocationbyID(Guid id)
    {
      SortingMasterModel sortingmaster = new SortingMasterModel();
      string userid = User.FindFirst(ClaimTypes.Name)?.Value;
      sortingmaster.ModifiedBy = Guid.Parse(userid);
      sortingmaster.Pk_SortingID = id;
      return _BinLocationService.DeleteBinInLocationbyID(sortingmaster);
    }


  }
}
