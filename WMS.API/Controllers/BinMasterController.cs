﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using System.Net.Http;
using Domain.Engine.Access.Impl;
using Domain.Engine.Access.Interfaces;
using Domain.Objects.Models;
using WMS.API.Models;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;

namespace WMS.API.Controllers
{
    [Authorize]
    [Produces("application/json")]
    [Route("api/BinMaster")]
    public class BinMasterController : Controller
    {
        private readonly IBinMasterService _BinService;
        public BinMasterController(IBinMasterService binservice)
        {
            _BinService = binservice;
        }

        [HttpPost]
        [Route("/BinMaster/InsertBinMaster")]
        public ResponseviewModel InsertBinMaster([FromBody]List<BinMasterModel> binmaster)
        {
            string userid = User.FindFirst(ClaimTypes.Name)?.Value;
            var id = Guid.Parse(userid);
            binmaster.ForEach(k => k.CreatedBy = id);
            return _BinService.InsertBinMaster(binmaster);
        }

        [HttpGet]
        [Route("/BinMaster/GetAllBinMasterList")]
        public ResponseviewModel GetAllBinMasterList()
        {
            return _BinService.GetAllBinMasterList();
        }

        [HttpGet]
        [Route("/BinMaster/GetRackName")]
        public ResponseviewModel GetRackName()
        {
            return _BinService.GetRackName();
        }

        [HttpGet]
        [Route("/BinMaster/GetDLLOfBinLocationWithLocationTransfer")]
        public ResponseviewModel GetDLLOfBinLocationWithLocationTransfer()
        {
            return _BinService.GetDLLOfBinLocationWithLocationTransfer();
        }

        [HttpGet]
        [Route("/BinMaster/Deleteitemtransfer")]
        public ResponseviewModel Deleteitemtransfer(Guid id)
        {
            return _BinService.Deleteitemtransfer(id);
        }
    }
}