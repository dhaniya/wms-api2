﻿using Domain.Engine.Access.Interfaces;
using Domain.Objects.Models;
using Infrastructure.Model.Main.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;


namespace WMS.API.Controllers
{
    [Authorize]
    public class StockCountController : ControllerBase
    {
        private readonly IStockCountService _StockCountService;
        public StockCountController(IStockCountService stockcountService)
        {
            _StockCountService = stockcountService;
        }

        [HttpPost]
        [Route("/StockCount/InsertUpdateStockCount")]
        public ResponseviewModel InsertUpdateStockCount([FromBody]StockCountModel stockcountmodel)
        {
            string userid = User.FindFirst(ClaimTypes.Name)?.Value;
            stockcountmodel.CreatedBy = Guid.Parse(userid);
            stockcountmodel.ModifiedBy = Guid.Parse(userid);
            return _StockCountService.InsertUpdateStockCount(stockcountmodel);
        }

        [HttpGet]
        [Route("/StockCount/GetAllSortingMaster")]
        public ResponseviewModel GetStockCountList()
        {
            StockCountView sortingm = new StockCountView();
            string userid = User.FindFirst(ClaimTypes.Name)?.Value;
            sortingm.ModifiedBy = Guid.Parse(userid);
            //sortingm.UserName = modelview.UserName;
            return _StockCountService.GetStockCountList();
        }

        [HttpGet]
        [Route("/StockCount/DeleteStockCountbyID")]
        public ResponseviewModel DeleteStockCountbyID(Guid id)
        {
            StockCountModel stockd = new StockCountModel();
            string userid = User.FindFirst(ClaimTypes.Name)?.Value;
            stockd.ModifiedBy = Guid.Parse(userid);
            stockd.Pk_StockCountID = id;
            return _StockCountService.DeleteStockCountbyID(stockd);
        }

        [HttpGet]
        [Route("/StockCount/GetRunningNumber")]
        public ResponseviewModel GetRunningNumber()
        {
            return _StockCountService.GetRunningNumber();
        }

        [HttpPost]
        [Route("/StockCount/GetStockDiscrepancyData")]
        public ResponseviewModel GetStockDiscrepancyData([FromBody]StockCountFilter filterdata)
        {
            return _StockCountService.GetStockDiscrepancyData(filterdata);
        }

        [HttpGet]
        [Route("/StockCount/GetAllRunningNumber")]
        public ResponseviewModel GetAllRunningNumber()
        {
            return _StockCountService.GetAllRunningNumber();
        }
    }
}
