﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using System.Net.Http;
using Domain.Engine.Access.Impl;
using Domain.Engine.Access.Interfaces;
using Domain.Objects.Models;
using WMS.API.Models;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;

namespace WMS.API.Controllers
{
    [Authorize]
    public class CustomerMasterController : Controller
    {
        private readonly ICustomerMasterService _CustomerMasterService;
        public CustomerMasterController(ICustomerMasterService customermasterService)
        {
            _CustomerMasterService = customermasterService;
        }


        [HttpPost]
        [Route("/CustomerMaster/InsertUpdateCustomerMaster")]
        public ResponseviewModel InsertUpdateCustomerMaster([FromBody]CustomerMasterViewModel customermasterviewmodel)
        {
            string userid = User.FindFirst(ClaimTypes.Name)?.Value;
            customermasterviewmodel.CreatedBy= Guid.Parse(userid);
            customermasterviewmodel.ModifiedBy = Guid.Parse(userid);
            return _CustomerMasterService.InsertUpdateCustomerMaster(customermasterviewmodel);
        }

        [HttpGet]
        [Route("/CustomerMaster/GetCustomerMasterByID")]
        public ResponseviewModel GetCustomerMasterByID(Guid id)
        {
            return _CustomerMasterService.GetCustomerMasterByID(id);
        }

        [HttpGet]
        [Route("/CustomerMaster/GetAllListCustomerMaster")]
        public ResponseviewModel GetAllListCustomerMaster()
        {
            return _CustomerMasterService.GetAllListCustomerMaster();
        }

        [HttpGet]
        [Route("/CustomerMaster/DeleteCustomerMasterByID")]
        public ResponseviewModel DeleteCustomerMasterByID(Guid id)
        {
            CustomerMasterViewModel customer = new CustomerMasterViewModel();
            string userid = User.FindFirst(ClaimTypes.Name)?.Value;
            customer.ModifiedBy = Guid.Parse(userid);
            customer.Pk_CustomerMasterId = id;
            return _CustomerMasterService.DeleteCustomerMasterByID(customer);
        }
        [HttpGet]
        [Route("/CustomerMaster/GetDropdownofCustomerMaster")]
        public ResponseviewModel GetDropdownofCustomerMaster()
        {
            return _CustomerMasterService.GetDropdownofCustomerMaster();
        }

        [HttpGet]
        [Route("/CustomerMaster/GetDropdownofCustomerCodeByID")]
        public ResponseviewModel GetDropdownofCustomerCodeByID(Guid id)
        {
            return _CustomerMasterService.GetDropdownofCustomerCodeByID(id);
        }

        [HttpGet]
        [Route("/CustomerMaster/GetDuplicateCustomerCodeCheck")]
        public ResponseviewModel GetDuplicateCustomerCodeCheck(string code,string custid )
        {
            return _CustomerMasterService.GetDuplicateCustomerCodeCheck(code,custid);
        }
    }
}