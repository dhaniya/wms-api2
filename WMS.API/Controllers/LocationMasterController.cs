﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using System.Net.Http;
using Domain.Engine.Access.Impl;
using Domain.Engine.Access.Interfaces;
using Domain.Objects.Models;
using WMS.API.Models;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;

namespace WMS.API.Controllers
{
    [Authorize]
    public class LocationMasterController : Controller
    {
        private readonly ILocationMasterService _locationmasterservice;
        public LocationMasterController(ILocationMasterService locationmasterservice)
        {
            _locationmasterservice = locationmasterservice;
        }

        [HttpPost]
        [Route("/LocationMaster/InsertLocationMaster")]
        public ResponseviewModel InsertUpdateAsnMaster([FromBody]List<LocationMasterModel> listlocationmaster)
        {
            string userid = User.FindFirst(ClaimTypes.Name)?.Value;
            var locatiobid = Guid.Parse(userid);
            listlocationmaster.ForEach(k => k.CreatedBy = locatiobid);
            return _locationmasterservice.InsertLocationMaster(listlocationmaster);
        }

        [HttpGet]
        [Route("/LocationMaster/GetAllLocationMasterList")]
        public ResponseviewModel GetAllLocationMasterList()
        {
            return _locationmasterservice.GetAllLocationMasterList();
        }

        [HttpGet]
        [Route("/LocationMaster/GetDLLRackName")]
        public ResponseviewModel GetDLLRackName()
        {
            return _locationmasterservice.GetDLLRackName();
        }


        [HttpGet]
        [Route("/LocationMaster/GetLocationMasterListbyRackName")]
        public ResponseviewModel GetLocationMasterListbyRackName(string RackName)
        {
            return _locationmasterservice.GetLocationMasterListbyRackName(RackName);
        }

        [HttpGet]
        [Route("/LocationMaster/GetDLLLocationMaster")]
        public ResponseviewModel GetDLLLocationMaster(int LocationType, int ZoneType)
        {
            return _locationmasterservice.GetDLLLocationMaster(LocationType, ZoneType);
        }



    }
}