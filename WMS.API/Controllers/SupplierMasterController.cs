﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using System.Net.Http;
using Domain.Engine.Access.Impl;
using Domain.Engine.Access.Interfaces;
using Domain.Objects.Models;
using WMS.API.Models;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;

namespace WMS.API.Controllers
{
    [Authorize]
    public class SupplierMasterController : Controller
    {
        private readonly ISupplierMasterService _SupplierService;
        public SupplierMasterController(ISupplierMasterService supplierservice)
        {
            _SupplierService = supplierservice;
        }

        [HttpPost]
        [Route("/SupplierMaster/InsertUpdateSupplierMaster")]
        public ResponseviewModel InsertUpdateSupplierMaster([FromBody]SupplierMasterModels suppliermastermodel)
        {
            string userid = User.FindFirst(ClaimTypes.Name)?.Value;
            suppliermastermodel.CreatedBy = Guid.Parse(userid);
            suppliermastermodel.ModifiedBy = Guid.Parse(userid);
            return _SupplierService.InsertUpdateSupplierMaster(suppliermastermodel);
        }

        [HttpGet]
        [Route("/SupplierMaster/GetSupplierMasterByID")]
        public ResponseviewModel GetSupplierMasterByID(Guid id)
        {
            return _SupplierService.GetSupplierMasterByID(id);
        }

        [HttpGet]
        [Route("/SupplierMaster/GetAllListSupplierMaster")]
        public ResponseviewModel GetAllListSupplierMaster()
        {
            return _SupplierService.GetAllListSupplierMaster();
        }

        [HttpGet]
        [Route("/SupplierMaster/DeleteSupplierMasterByID")]
        public ResponseviewModel DeleteSupplierMasterByID(Guid id)
        {
            SupplierMasterModels supplier = new SupplierMasterModels();
            string userid = User.FindFirst(ClaimTypes.Name)?.Value;
            supplier.ModifiedBy = Guid.Parse(userid);
            supplier.Pk_SupplierID = id;
            return _SupplierService.DeleteSupplierMasterByID(supplier);
        }

        [HttpGet]
        [Route("/SupplierMaster/GetDropdownofSupplierMaster")]
        public ResponseviewModel GetDropdownofSupplierMaster()
        {
            return _SupplierService.GetDropdownofSupplierMaster();
        }

    }
}