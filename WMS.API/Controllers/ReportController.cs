using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using System.Net.Http;
using Domain.Engine.Access.Impl;
using Domain.Engine.Access.Interfaces;
using Domain.Objects.Models;
using WMS.API.Models;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;

namespace WMS.API.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    public class ReportController : Controller
    {
        private readonly ISortingMasterService _Sortingmasterservice;
        private readonly IReportService _reportService;
        public ReportController(ISortingMasterService Sortingmasterservice, IReportService reportService)
        {
            _Sortingmasterservice = Sortingmasterservice;
            _reportService = reportService;
        }
        [HttpPost]
        [Route("/Reports/InventorySearchForRack")]
        public ResponseviewModel InventorySearchForRack([FromBody]InventorySearchViewModel inventorySearchView)
        {
            return _Sortingmasterservice.GetInventorySearchForRack(inventorySearchView);
        }

        // Repack case Inventory Export Excel
        [HttpPost]
        [Route("/Reports/InventorySearchForRackExcelExport")]
        public ResponseviewModel InventorySearchForRackExcelExport([FromBody]InventorySearchViewModel inventorySearchView)
        {
            return _Sortingmasterservice.InventorySearchForRackExcelExport(inventorySearchView);
        }

        [HttpPost]
        [Route("/Reports/GetInventorySearchForBin")]
        public ResponseviewModel GetInventorySearchForBin([FromBody]InventorySearchViewModel inventorySearchView)
        {
            return _Sortingmasterservice.GetInventorySearchForBin(inventorySearchView);
        }

        // Get Inventory Search For Bin Excel Export
        [HttpPost]
        [Route("/Reports/GetInventorySearchForBinExcelExport")]
        public ResponseviewModel GetInventorySearchForBinExcelExport([FromBody]InventorySearchViewModel inventorySearchView)
        {
            return _Sortingmasterservice.GetInventorySearchForBinExcelExport(inventorySearchView);
        }
        [HttpPost]
        [Route("/Reports/GetPartReport")]
        public ResponseviewModel GetPartReport([FromBody]PartReportDetailsModel searchmodel)
        {
            return _reportService.PartReportDetails(searchmodel);
        }

        //Part Report Excel Export
        [HttpPost]
        [Route("/Reports/GetPartReportExcelExport")]
        public ResponseviewModel GetPartReportExcelExport([FromBody]PartReportDetailsModelExcelExport searchmodel)
        {
            return _reportService.GetPartReportExcelExport(searchmodel);
        }

        [HttpPost]
        [Route("/Reports/Discrepancy")]
        public ResponseviewModel Discrepancy([FromBody]DiscrepancyFilterModel searchmodel)
        {
            return _reportService.Discrepancy(searchmodel);
        }

        //Asn Discrepancy Excel Export
        [HttpPost]
        [Route("/Reports/DiscrepancyExcelExport")]
        public ResponseviewModel DiscrepancyExcelExport([FromBody]DiscrepancyFilterModel searchmodel)
        {
            return _reportService.DiscrepancyExcelExport(searchmodel);
        }

        //Incoming Shipment Summary Report
        [HttpPost]
        [Route("/Reports/GetIncomingShipmentSummaryReport")]
        public ResponseviewModel GetIncomingShipmentSummaryReport([FromBody]IncomingShipmentSummaryReportFilterModel searchmodel)
        {
            return _reportService.GetIncomingShipmentSummaryReport(searchmodel);
        }

        //Incoming Shipment Summary Report Excel Export
        [HttpPost]
        [Route("/Reports/GetIncomingShipmentSummaryReportExcelExport")]
        public ResponseviewModel GetIncomingShipmentSummaryReportExcelExport([FromBody]IncomingShipmentSummaryReportFilterExcelExportModel searchmodel)
        {
            return _reportService.GetIncomingShipmentSummaryReportExcelExport(searchmodel);
        }

        // Shipment Report
        [HttpPost]
        [Route("/Reports/GetShipmentReport")]
        public ResponseviewModel GetShipmentReport([FromBody]ShipmentReportFilterView filterdata)
        {
            return _reportService.GetShipmentReport(filterdata);
        }

        // Shipment Report Excel Export
        [HttpPost]
        [Route("/Reports/GetShipmentReportExcelExport")]
        public ResponseviewModel GetShipmentReportExcelExport([FromBody]ShipmentReportFilterExcelView filterdata)
        {
            return _reportService.GetShipmentReportExcelExport(filterdata);
        }

        //Delivery Listing Report
        [HttpPost]
        [Route("/Reports/GetDeliveryListingReport")]
        public ResponseviewModel GetDeliveryListingReport([FromBody]DeliveryListingReportFilterView filterdata)
        {
            return _reportService.GetDeliveryListingReport(filterdata);
        }

        //Delivery Listing Report Excel Export
        [HttpPost]
        [Route("/Reports/GetDeliveryListingReportExcelExport")]
        public ResponseviewModel GetDeliveryListingReportExcelExport([FromBody]DeliveryListingReportFilterExcelExportView filterdata)
        {
            return _reportService.GetDeliveryListingReportExcelExport(filterdata);
        }

        //Delivery Listing Report
        [HttpPost]
        [Route("/Reports/GetDeliveryListingSummaryReport")]
        public ResponseviewModel GetDeliveryListingSummaryReport([FromBody]DeliveryListingReportFilterView filterdata)
        {
            return _reportService.GetDeliveryListingSummaryReport(filterdata);
        }

        //Delivery Listing Summary Report Excel Export
        [HttpPost]
        [Route("/Reports/GetDeliveryListingSummaryReportExcelExport")]
        public ResponseviewModel GetDeliveryListingSummaryReportExcelExport([FromBody]DeliveryListingReportFilterExcelExportView filterdata)
        {
            return _reportService.GetDeliveryListingSummaryReportExcelExport(filterdata);
        }

        [HttpPost]
        [Route("/Reports/GetPartsTransactionsList")]
        public ResponseviewModel GetPartsTransactionsList([FromBody]GetPartsTransactionsListView filterdata)
        {
            return _reportService.GetPartsTransactionsList(filterdata);
        }

        [HttpPost]
        [Route("/Reports/GetPartsTransactionsListExcelExport")]
        public ResponseviewModel GetPartsTransactionsListExcelExport([FromBody]GetPartsTransactionsListView filterdata)
        {
            return _reportService.GetPartsTransactionsListExcelExport(filterdata);
        }


        [HttpPost]
        [Route("/Reports/GetPartsTransactionsBinList")]
        public ResponseviewModel GetPartsTransactionsBinList([FromBody]GetPartsTransactionsBinList filterdata)
        {
            return _reportService.GetPartsTransactionsBinList(filterdata);
        }
        [HttpPost]
        [Route("/Reports/GetPartsTransactionsBinDoList")]
        public ResponseviewModel GetPartsTransactionsBinDoList([FromBody]GetPartsTransactionsBinDoList filterdata)
        {
            return _reportService.GetPartsTransactionsBinDoList(filterdata);
        }
        [HttpPost]
        [Route("/Reports/GetReallocationList")]
        public ResponseviewModel GetReallocationList([FromBody]GetRealocationsListView filterdata)
        {
            return _reportService.GetReallocationList(filterdata);
        }
        [HttpGet]
        [Route("/Reports/GetRealocationsListExcelExport")]
        public ResponseviewModel GetRealocationsListExcelExport()
        {
            return _reportService.GetRealocationsListExcelExport();
        }

        [HttpPost]
        [Route("/Reports/GetStockAgingReports")]
        [AllowAnonymous]
        public ResponseviewModel GetStockAgingReports([FromBody]GetStockAgingReportsListView filterdata)
        {
            return _reportService.GetStockAgingReports(filterdata);
        }

        [HttpGet]
        [Route("/Reports/GetStockAgingReportsExcelExport")]
        public ResponseviewModel GetStockAgingReportsExcelExport()
        {
            return _reportService.GetStockAgingReportsExcelExport();
        }
        [HttpGet]
        [Route("/Reports/GetStockAgingFeatchSP")]
        [AllowAnonymous]
        public ResponseviewModel GetStockAgingFeatchSP()
        {
            return _reportService.GetStockAgingFeatchSP();
        }
    }
}
