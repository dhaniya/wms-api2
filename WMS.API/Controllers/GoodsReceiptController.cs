﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using System.Net.Http;
using Domain.Engine.Access.Impl;
using Domain.Engine.Access.Interfaces;
using Domain.Objects.Models;
using WMS.API.Models;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;

namespace WMS.API.Controllers
{
    [Authorize]
    public class GoodsReceiptController : Controller
    {
        private readonly IGoodReceiptService _GoodReceiptService;
        private readonly IASNMasterService _AsnService;


        public GoodsReceiptController(IGoodReceiptService goodreceiptservice, IASNMasterService asnservice)
        {
            _GoodReceiptService = goodreceiptservice;
            _AsnService = asnservice;
        }

        [HttpGet]
        [Route("/GoodsReceipt/GetDropdownofInvoiceNoByDateofArrival")]
        public ResponseviewModel GetDropdownofInvoiceNoByDateofArrival(DateTime DateOfArrival)
        {
            return _AsnService.GetDropdownofInvoiceNoByDateofArrival(DateOfArrival);
        }

        [HttpGet]
        [Route("/GoodsReceipt/GetDropdownofInvoiceforGoodReceipt")]
        public ResponseviewModel GetDropdownofInvoiceforGoodReceipt()
        {
            return _AsnService.GetDropdownofInvoiceforGoodReceipt();
        }

        [HttpPost]
        [Route("/GoodsReceipt/AddGoodsReceipts")]
        public ResponseviewModel AddGoodsReceipts([FromBody]GoodsReceiptsModel goodsreceiptsmodel)
        {
            string userid = User.FindFirst(ClaimTypes.Name)?.Value;
            goodsreceiptsmodel.CreatedBy = Guid.Parse(userid);

            return _AsnService.AddGoodsReceipts(goodsreceiptsmodel);
        }

        [HttpGet]
        [Route("/GoodsReceipt/GetGoodsReceiptsByDateOfArrival")]
        public ResponseviewModel GetGoodsReceiptsByDateOfArrival(DateTime DateOfArrival)
        {
            return _AsnService.GetGoodsReceiptsByDateOfArrival(DateOfArrival);
        }

        [HttpGet]
        [Route("/GoodsReceipt/GetGoodsReceiptsByInvoiceNo")]
        public ResponseviewModel GetGoodsReceiptsByInvoiceNo(string InvoiceNo)
        {
            return _AsnService.GetGoodsReceiptsByInvoiceNo(InvoiceNo);
        }


        [HttpGet]
        [Route("/GoodsReceipt/GetGoodReceiptList")]
        public ResponseviewModel GetGoodReceiptList(GoodReceiptListViewModel goodreceiptlistviewmodel)
        {
            return _AsnService.GetGoodReceiptList(goodreceiptlistviewmodel);
        }

        [HttpGet]
        [Route("/GoodsReceipt/DeleteGoodsReciptByID")]
        public ResponseviewModel DeleteGoodReciptByID(string invnumber, string palletnumber, string containernumber)
        {
            string userid = User.FindFirst(ClaimTypes.Name)?.Value;
            return _AsnService.DeleteGoodReciptByID(invnumber, palletnumber, containernumber, Guid.Parse(userid));
        }
    }
}