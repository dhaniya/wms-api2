﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using System.Net.Http;
using Domain.Engine.Access.Impl;
using Domain.Engine.Access.Interfaces;
using Domain.Objects.Models;
using WMS.API.Models;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;

namespace WMS.API.Controllers
{
   [Authorize]
    public class CompanyMasterController : Controller
    {
        private readonly ICompanyMasterService _CompanyService;
        public CompanyMasterController(ICompanyMasterService companyService)
        {
            _CompanyService = companyService;
        }

        [HttpPost]
        [Route("/CompanyMaster/InsertUpdateCompanyMaster")]
        public ResponseviewModel InsertUpdateCompanyMaster([FromBody]CompanyMasterModels companymodel)
        {
            string userid = User.FindFirst(ClaimTypes.Name)?.Value;
            companymodel.CreatedBy = Guid.Parse(userid);
            companymodel.ModifiedBy = Guid.Parse(userid);
            return _CompanyService.InsertUpdateCompanyMaster(companymodel);
        }

        [HttpGet]
        [Route("/CompanyMaster/GetCompanyMasterByID")]
        public ResponseviewModel GetCompanyMasterByID(Guid id)
        {
            return _CompanyService.GetCompanyMasterByID(id);
        }

        [HttpGet]
        [Route("/CompanyMaster/GetAllListCompanyMaster")]
        public ResponseviewModel GetAllListCompanyMaster()
        {
            return _CompanyService.GetAllListCompanyMaster();
        }

        [HttpGet]
        [Route("/CompanyMaster/DeleteCompanyMasterByID")]
        public ResponseviewModel DeleteCompanyMasterByID(Guid id)
        {
            CompanyMasterModels compny = new CompanyMasterModels();
            string userid = User.FindFirst(ClaimTypes.Name)?.Value;
            compny.ModifiedBy = Guid.Parse(userid);
            compny.PK_CompanyID = id;
            return _CompanyService.DeleteCompanyMasterByID(compny);
        }

        [HttpGet]
        [Route("/CompanyMaster/GetDropdownofCompanyMaster")]
        public ResponseviewModel GetDropdownofCompanyMaster()
        {
            return _CompanyService.GetDropdownofCompanyMaster();
        }


    }
}