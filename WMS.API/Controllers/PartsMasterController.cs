﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using System.Net.Http;
using Domain.Engine.Access.Impl;
using Domain.Engine.Access.Interfaces;
using Domain.Objects.Models;
using WMS.API.Models;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;

namespace WMS.API.Controllers
{
  //  [Authorize]
    public class PartsMasterController : Controller
    {
        private readonly IPartsMasterService _PartsmasterService;
        public PartsMasterController(IPartsMasterService partsmasterservice)
        {
            _PartsmasterService = partsmasterservice;
        }

        [HttpPost]
        [Route("/PartsMaster/InsertUpdatePartsMaster")]
        public ResponseviewModel InsertUpdatePartsMaster([FromBody]PartsMasterModel partsmastermodel)
        {
            //if(partsmastermodel.Pk_PartsMasterID.HasValue)

            //  b.CreatedBy.HasValue? b.CreatedBy.Value : Guid.Empty,
           string userid =  User.FindFirst(ClaimTypes.Name)?.Value;
            partsmastermodel.CreatedBy = Guid.Parse(userid);
            partsmastermodel.ModifiedBy = Guid.Parse(userid);
            return _PartsmasterService.InsertUpdatePartsMaster(partsmastermodel);
        }

        [HttpGet]
        [Route("/PartsMaster/GetPartsMasterByID")]
        public ResponseviewModel GetPartsMasterByID(Guid id)
        {
            return _PartsmasterService.GetPartsMasterByID(id);
        }

        [HttpGet]
        [Route("/PartsMaster/CheckPartNumberExistsbyPartno")]
        public ResponseviewModel CheckPartNumberExistsbyPartno(string PartNumber)
        {
            return _PartsmasterService.CheckPartNumberExistsbyPartno(PartNumber);
        }

        [HttpPost]
        [Route("/PartsMaster/GetAllListPartsMaster")]
        public ResponseviewModel GetAllListPartsMaster([FromBody]PartsMasterModellist filter)
        {
            return _PartsmasterService.GetAllListPartsMaster(filter);
        }

        [HttpGet]
        [Route("/PartsMaster/DeletePartsMasterByID")]
        public ResponseviewModel DeletePartsMasterByID(Guid id)
        {
            PartsMasterModel part = new PartsMasterModel();
            string userid = User.FindFirst(ClaimTypes.Name)?.Value;
            part.ModifiedBy = Guid.Parse(userid);
            part.Pk_PartsMasterID = id;
            return _PartsmasterService.DeletePartsMasterByID(part);
        }

        [HttpPost]
        [Route("/PartsMaster/CheckPartsMasterDetails")]
        public ResponseviewModel CheckPartsMasterDetails([FromBody]List<PartsMasterModel> lstpartsmaster)
        {
            return _PartsmasterService.CheckPartsMasterDetails(lstpartsmaster);
        }
        [HttpPost]
        [Route("/PartsMaster/UploadPartsMaster")]
        public ResponseviewModel UploadPartsMaster([FromBody]List<PartsMasterModel> lstpartsmaster)
        {
            return _PartsmasterService.UploadPartsMaster(lstpartsmaster);
        }
    }
}