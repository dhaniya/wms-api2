﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using System.Net.Http;
using Domain.Engine.Access.Impl;
using Domain.Engine.Access.Interfaces;
using Domain.Objects.Models;
using WMS.API.Models;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;

namespace WMS.API.Controllers
{
    [Produces("application/json")]
    [Route("api/BarCode")]
   [Authorize]
    public class BarCodeController : Controller
    {
        private readonly IBarCodeService _BarCodeService;
        public BarCodeController(IBarCodeService barcodeservice)
        {
            _BarCodeService = barcodeservice;
        }

        [HttpGet]
        [Route("/BarCode/GetBarCodeGenerate")]
        public ResponseviewModel GetBarCodeGenerate(string customercode, int number)
        {
            string userid = User.FindFirst(ClaimTypes.Name)?.Value;
            var barcode = Guid.Parse(userid);
            
            return _BarCodeService.GetBarCodeGenerate( customercode, number, barcode);

        }

        [HttpGet]
        [Route("/BarCode/GetBarCodeList")]
        public ResponseviewModel GetBarCodeList(string customercode)
        {
            return _BarCodeService.GetBarCodeList(customercode);
        }

        [HttpPost]
        [Route("/BarCode/ReprintBarcode")]
        public ResponseviewModel RePrintBarcodeList([FromBody]List<BarCodeModel> lstbarcode)
        {
            return _BarCodeService.RePrintBarcode(lstbarcode);
        }

        [HttpGet]
        [Route("/BarCode/GetDDLBarcodeByCustomerCodeandinvoiceno")]
        public ResponseviewModel GetDDLBarcodeByCustomerCodeandinvoiceno(string customercode)
        {
            return _BarCodeService.GetDDLBarcodeByCustomerCodeandinvoiceno(customercode);
        }

        [HttpGet]
        [Route("/BarCode/CheckBarcodeValidOrNot")]
        public ResponseviewModel CheckBarcodeValidOrNot(string barcode)
        {
            return _BarCodeService.CheckBarcodeValidOrNot(barcode);
        }

        


    }
}