using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using System.Net.Http;
using Domain.Engine.Access.Impl;
using Domain.Engine.Access.Interfaces;
using Domain.Objects.Models;
using WMS.API.Models;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;

namespace WMS.API.Controllers
{
   [Authorize]
    public class PickingMasterController : Controller
    {
        private readonly IPickingMasterService _PickingMasterService;
        public PickingMasterController(IPickingMasterService pickingmasterservice)
        {
            _PickingMasterService = pickingmasterservice;
        }

        [HttpGet]
        [Route("/PickingMaster/GetDropdownofCustomerMaster")]
        public ResponseviewModel GetDropdownofCustomerMaster()
        {
            return _PickingMasterService.GetDropdownofCustomerMaster();
        }

        [HttpGet]
        [Route("/PickingMaster/GetDropdownofCustomerCodeByID")]
        public ResponseviewModel GetDropdownofCustomerCodeByID(Guid id)
        {
            return _PickingMasterService.GetDropdownofCustomerCodeByID(id);
        }

        [HttpGet]
        [Route("/PickingMaster/GetDropdownofPutAwayInvoiceNoByCustomerCode")]
        public ResponseviewModel GetDropdownofPutAwayInvoiceNoByCustomerCode(string CustomerCode)
        {
            return _PickingMasterService.GetDropdownofPutAwayInvoiceNoByCustomerCode(CustomerCode);
        }

        
        [HttpGet]
        [Route("/PickingMaster/GetDropdownofRepackedCaseNoByInvoiceno")]
        public ResponseviewModel GetDropdownofRepackedCaseNoByInvoiceno(string invoiceno,string customercode)
        {
            return _PickingMasterService.GetDropdownofRepackedCaseNoByInvoiceno(invoiceno,customercode);
        }
        
        [HttpGet]
        [Route("/PickingMaster/GetItemCodeByRepackedCaseNo")]
        public ResponseviewModel GetItemCodeByRepackedCaseNo(string repackedcaseno,string invoiceno)
        {
            return _PickingMasterService.GetItemCodeByRepackedCaseNo(repackedcaseno,invoiceno);
        }

        [HttpGet]
        [Route("/PickingMaster/GetAvailableQuantityBySortingDetail")]
        public ResponseviewModel GetAvailableQuantityBySortingDetail(string itemcode, string caseno, string invoiceno)
        {
            return _PickingMasterService.GetAvailableQuantityBySortingDetail(itemcode, caseno, invoiceno);
        }

        
        [HttpGet]
        [Route("/PickingMaster/GetSortingdetailbyRepackedCaseno")]
        public ResponseviewModel GetSortingdetailbyRepackedCaseno(string caseno,string itemcode, int pickingtype, string invoiceno)
        {
            return _PickingMasterService.GetSortingdetailbyRepackedCaseno(caseno,itemcode, pickingtype, invoiceno);
        }

        [HttpPost]
        [Route("/PickingMaster/InsertPickingMaster")]
        public ResponseviewModel InsertPickingMaster([FromBody]PickingViewModel pickingviewmodel)
        {
            string userid = User.FindFirst(ClaimTypes.Name)?.Value;
            pickingviewmodel.CreatedBy = Guid.Parse(userid);
            pickingviewmodel.ModifiedBy = Guid.Parse(userid);
           pickingviewmodel.listpickingdetail.ForEach(k => k.CreatedBy = pickingviewmodel.CreatedBy);
            return _PickingMasterService.InsertPickingMaster(pickingviewmodel);
        }

        [HttpPost]
        [Route("/PickingMaster/GetPickingList")]
        public ResponseviewModel GetPickingList([FromBody]SearchPickingViewModel searchpickingviewmodel)
        {
           
            var d = _PickingMasterService.GenerateOrderNo();
            return _PickingMasterService.GetPickingList(searchpickingviewmodel);
        }
        [HttpPost]
        [Route("/PickingMaster/GetPickingListALL")]
        public ResponseviewModel GetPickingListALL([FromBody]SearchPickingViewModel searchpickingviewmodel)
        {

            var d = _PickingMasterService.GenerateOrderNo();
            return _PickingMasterService.GetPickingListALL(searchpickingviewmodel);
        }


        [HttpGet]
        [Route("/PickingMaster/GetPickingListbyCustomerCode")]
        public ResponseviewModel GetPickinGetPickingListbyCustomerCodegList(string customercode)
        {
            return _PickingMasterService.GetPickingListbyCustomerCode(customercode);
        }        

        [HttpGet]
        [Route("/PickingMaster/GetPickingDetailsByPickingId")]
        public ResponseviewModel GetPickingDetailsByPickingId(Guid id)
        {
            return _PickingMasterService.GetPickingDetailsByPickingId(id);
        }

        [HttpGet]
        [Route("/PickingMaster/GetPendingPickingByPickingId")]
        public ResponseviewModel GetPendingPickingByPickingId(Guid id)
        {
            return _PickingMasterService.GetPendingPickingByPickingId(id);
        }

        [HttpGet]
        [Route("/PickingMaster/GetPickingDetailByOrderNo")]
        public ResponseviewModel GetPickingDetailByOrderNo(string orderno)
        {
            return _PickingMasterService.GetPickingDetailByOrderNo(orderno);
        }

        [HttpPost]
        [Route("/PickingMaster/UpdatePickingStatus")]
        public ResponseviewModel UpdatePickingStatus([FromBody]UpdatePickingViewModel updatepickingviewmodel)
        {
            string userid = User.FindFirst(ClaimTypes.Name)?.Value;
            updatepickingviewmodel.ModifiedBy = Guid.Parse(userid);
            return _PickingMasterService.UpdatePickingStatus(updatepickingviewmodel);
        }


    [HttpPost]
    [Route("/PickingMaster/UpdatePickingMasterList")]
    public ResponseviewModel UpdatePickingMasterList([FromBody] PickingDetailViewModel PickingViewModel)
    {
      string userid = User.FindFirst(ClaimTypes.Name)?.Value;
      PickingViewModel.CreatedBy = Guid.Parse(userid);
      PickingViewModel.ModifiedBy = Guid.Parse(userid);
      return _PickingMasterService.UpdatePickingMasterList(PickingViewModel);
    }

    [HttpGet]
    [Route("/PickingMaster/DeletePickingbyID")]
    public ResponseviewModel DeletePickingbyID(Guid id)
    {
      PickingMasterViewModel pickingmaster = new PickingMasterViewModel();
      string userid = User.FindFirst(ClaimTypes.Name)?.Value;
      pickingmaster.ModifiedBy = Guid.Parse(userid);
      pickingmaster.Pk_PickingMasterID = id;
      return _PickingMasterService.DeletePickingbyID(pickingmaster);
    }

  }
}
