using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using System.Net.Http;
using Domain.Engine.Access.Impl;
using Domain.Engine.Access.Interfaces;
using Domain.Objects.Models;
using WMS.API.Models;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;

namespace WMS.API.Controllers
{
    [Authorize]

    public class DeliveryOrderController : Controller
    {
        private readonly IDeliveryOrderService _deliveryorderservice;
        public DeliveryOrderController(IDeliveryOrderService deliveryorderservice)
        {
            _deliveryorderservice = deliveryorderservice;
        }

        [HttpGet]
        [Route("/DeliveryOrder/GetPickedList")]
        public ResponseviewModel GetPickedList(SearchPickedViewModel searchpickedviewmodel)
        {
            return _deliveryorderservice.GetPickedList(searchpickedviewmodel);
        }

        [HttpGet]
        [Route("/DeliveryOrder/GetAllPickedDetailsByPickingId")]
        public ResponseviewModel GetAllPickedDetailsByPickingId(Guid id)
        {
            return _deliveryorderservice.GetAllPickedDetailsByPickingId(id);
        }

        [HttpGet]
        [Route("/DeliveryOrder/GetAllpickedDetailById")]
        public ResponseviewModel GetAllPickedDetailsByPickingIdExcelExport(Guid id)
        {
            return _deliveryorderservice.GetAllpickedDetailById(id);
        }

        [HttpGet]
        [Route("/DeliveryOrder/GetAllPickedDetailsByPickingIdForEdit")]
        public ResponseviewModel GetAllPickedDetailsByPickingIdForEdit(Guid id)
        {
            return _deliveryorderservice.GetAllPickedDetailsByPickingIdForEdit(id);
        }

        [HttpPost]
        [Route("/DeliveryOrder/CreateDeliveryOrder")]
        public ResponseviewModel CreateDeliveryOrder([FromBody]CreateDeliveryOrderViewModel createdeliveryorderviewmodel)
        {
            string userid = User.FindFirst(ClaimTypes.Name)?.Value;
            createdeliveryorderviewmodel.CreatedBy = Guid.Parse(userid);

            return _deliveryorderservice.CreateDeliveryOrder(createdeliveryorderviewmodel);
        }

        [HttpPost]
        [Route("/DeliveryOrder/UpdateDeliveryOrder")]
        public ResponseviewModel UpdateDeliveryOrder([FromBody]CreateDeliveryOrderViewModel createdeliveryorderviewmodel)
        {
            string userid = User.FindFirst(ClaimTypes.Name)?.Value;
            createdeliveryorderviewmodel.CreatedBy = Guid.Parse(userid);

            return _deliveryorderservice.UpdateDeliveryOrder(createdeliveryorderviewmodel);
        }

        [HttpPost]
        [Route("/DeliveryOrder/GetDeliveryOrderList")]
        public ResponseviewModel GetDeliveryOrderList([FromBody]DeliveryOrderFilterViewModel searchmodel)
        {
            return _deliveryorderservice.GetDeliveryOrderList(searchmodel);
        }

        [HttpGet]
        [Route("/DeliveryOrder/GetDeliveryOrderDropdownList")]
        public ResponseviewModel GetDeliveryOrderDropdownList()
        {
            return _deliveryorderservice.GetDeliveryOrderDropdownList();
        }

        [HttpGet]
        [Route("/DeliveryOrder/GetDeliveryOrderDetailList")]
        public ResponseviewModel GetDeliveryOrderDetailList(string doorder)
        {
            return _deliveryorderservice.GetDeliveryOrderDetailList(doorder);
        }
        [HttpGet]
        [Route("/DeliveryOrder/ConfirmDO")]
        public ResponseviewModel ConfirmDO(string doorder)
        {
            return _deliveryorderservice.ConfirmDO(doorder);
        }
        [HttpPost]
        [Route("/DeliveryOrder/UpdateLocationPacking")]
        public ResponseviewModel UpdatePackingLocation([FromBody]PickingLocationUpdate model)
        {
            return _deliveryorderservice.UpdatePackingLocation(model);
        }

        [HttpGet]
        [Route("/DeliveryOrder/Address")]
        public ResponseviewModel Address()
        {
            return _deliveryorderservice.AddressDeliveryOrder();
        }
    }
}
