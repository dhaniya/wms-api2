﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using System.Net.Http;
using Domain.Engine.Access.Impl;
using Domain.Engine.Access.Interfaces;
using Domain.Objects.Models;
using WMS.API.Models;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;

namespace WMS.API.Controllers
{
    [Authorize]
    public class SortingDetailController : Controller
    {
        private readonly ISortingDetailService _Sortingdetailservice;
        public SortingDetailController(ISortingDetailService Sortingdetailservice)
        {
            _Sortingdetailservice = Sortingdetailservice;
        }

        [HttpPost]
        [Route("/SortingDetail/UpdateSortingDetailList")]
        public ResponseviewModel UpdateSortingDetailList([FromBody]List<SortingDetailModels> sortingdetaillist)
        {
            string userid = User.FindFirst(ClaimTypes.Name)?.Value;
            var ui = Guid.Parse(userid);
            sortingdetaillist.ForEach(k => k.ModifiedBy = ui);
            return _Sortingdetailservice.UpdateSortingDetailList(sortingdetaillist);
        }


        [HttpGet]
        [Route("/SortingDetail/GetAllSortingDetailList")]
        public ResponseviewModel GetAllSortingDetailList()
        {
            return _Sortingdetailservice.GetAllSortingDetailList();
        }

        [HttpGet]
        [Route("/SortingDetail/GetSortingDetailByRepackedCaseNo")]//Add New parameter in this api(invoicenumber)
        public ResponseviewModel GetSortingDetailByRepackedCaseNo(string RepackedCaseNo,string invoicenumber)
        {
            return _Sortingdetailservice.GetSortingDetailByRepackedCaseNo(RepackedCaseNo, invoicenumber);
        }

        [HttpGet]
        [Route("/SortingDetail/GetSortingDetailByInvoiceno")]
        public ResponseviewModel GetSortingDetailByInvoiceno(string invoiceno , bool removed)
        {
            return _Sortingdetailservice.GetSortingDetailByInvoiceNo(invoiceno, removed);
        }



        [HttpGet]
        [Route("/SortingDetail/GetSortingDetailsDemo")]
        public ResponseviewModel GetSortingDetailsDemo()
        {
            return _Sortingdetailservice.GetSortingDetailsDemo();
        }
    }
}