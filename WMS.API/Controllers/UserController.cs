﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using System.Diagnostics;
using System.Net.Http;
using Domain.Engine.Access.Impl;
using Domain.Engine.Access.Interfaces;
using Domain.Objects.Models;
using WMS.API.Models;
using Microsoft.Extensions.Options;
using System.Security.Claims;

namespace WMS.API.Controllers
{
   [Authorize]
    [Produces("application/json")]
    [Route("api/User")]
    public class UserController : Controller
    {
        private readonly IUserService _UserService;
        private readonly IUserAccessService _UserAccess;
        public UserController(IUserService userservice, IUserAccessService userAccess)
        {
            _UserService = userservice;
            _UserAccess = userAccess;
        }

        [HttpPost]
        [Route("/User/Login")]
        [AllowAnonymous]
        public ResponseviewModel Login([FromBody]LoginViewModel loginviewmodel)
        {
            return _UserService.Login(loginviewmodel);
        }

        [HttpPost]
        [Route("/User/InsertUpdateUser")]
        [AllowAnonymous]
        public ResponseviewModel InsertUpdateUser([FromBody]UserMasterViewModel usermasterviewmodel)
        {
          //  string userid = User.FindFirst(ClaimTypes.Name)?.Value;
           // usermasterviewmodel.CreatedBy = Guid.Parse(userid);
          //  usermasterviewmodel.ModifiedBy = Guid.Parse(userid);
            return _UserService.InsertUpdateUser(usermasterviewmodel);
        }

        [HttpGet]
        [Route("/User/GetDDLRoleType")]
        [AllowAnonymous]
        public ResponseviewModel GetDDLRoleType()
        {
            return _UserService.GetDDLRoleType();
        }

        [HttpGet]
        [Route("/User/GetUserDetailById")]
        public ResponseviewModel GetUserDetailById(Guid id)
        {
            return _UserService.GetUserDetailById(id);
        }

        [HttpGet]
        [Route("/User/GetAllUser")]
        public ResponseviewModel GetAllUser()
        {
            return _UserService.GetAllUser();
        }

        [HttpGet]
        [Route("/User/DeleteUserById")]
        public ResponseviewModel DeleteUserById(Guid id)
        {
            UserMasterViewModel useru = new UserMasterViewModel();
            string userid = User.FindFirst(ClaimTypes.Name)?.Value;
            useru.ModifiedBy = Guid.Parse(userid);
            useru.Pk_UserId = id;
            return _UserService.DeleteUserById(useru);
        }
        #region UserAccess
        [HttpGet]
        [Route("/UserAccess/GetAccessList")]
        public ResponseviewModel GetAccessList()
        {
            return _UserAccess.GetListRoutes();
        }
        [HttpPost]
        [Route("/UserAccess/AddAccess")]
        public ResponseviewModel AddAccess([FromBody]List<UserAccessViewModel> model)
        {
            return _UserAccess.AddUserAccess(model);
        }

        [HttpPost]
        [Route("/UserAccess/GetByUserid")]
        public ResponseviewModel GetAccess([FromBody]UserAccessViewModel model)
        {
            return _UserAccess.GetUserAccessByid(model.UserId);
        }

        [HttpPost]
        [Route("/User/ChangePassword")]
        [AllowAnonymous]
        public ResponseviewModel ChangePassword([FromBody]ChangePasswordViewModel usermasterviewmodel)
        {
            string userid = User.FindFirst(ClaimTypes.Name)?.Value;
            return _UserService.ChangePassword(usermasterviewmodel);
        }
        #endregion




    }
}
