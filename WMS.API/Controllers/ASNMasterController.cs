using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using System.Diagnostics;
using System.Net.Http;
using Domain.Engine.Access.Impl;
using Domain.Engine.Access.Interfaces;
using Domain.Objects.Models;
using WMS.API.Models;
using Microsoft.Extensions.Options;
using System.Security.Claims;

namespace WMS.API.Controllers
{
    [Authorize]
    public class ASNMasterController : ControllerBase
    {
        private readonly IASNMasterService _AsnService;
        public ASNMasterController(IASNMasterService asnservice)
        {
            _AsnService = asnservice;
        }

        [Authorize]
        [HttpPost]
        [Route("/ASNMaster/InsertUpdateAsnMaster")]
        public ResponseviewModel InsertUpdateAsnMaster([FromBody]ASNMasterModels asnmastermodel)
        {
            string userid = User.FindFirst(ClaimTypes.Name)?.Value;
            asnmastermodel.CreatedBy = Guid.Parse(userid);
            asnmastermodel.ModifiedBy = Guid.Parse(userid);
            return _AsnService.InsertUpdateAsnMaster(asnmastermodel);
        }

        [HttpPost]
        [Route("/ASNMaster/UploadASNandItemDetail")]
        public ResponseviewModel UploadASNandItemDetail([FromBody]ASNandlistasnitemsModel asnandlistasnitemsmodel)
        {
            string userid = User.FindFirst(ClaimTypes.Name)?.Value;
            asnandlistasnitemsmodel.CreatedBy = Guid.Parse(userid);
            asnandlistasnitemsmodel.ModifiedBy = Guid.Parse(userid);
            return _AsnService.UploadASNandItemDetail(asnandlistasnitemsmodel);
        }

        [HttpGet]
        [Route("/ASNMaster/GetDropdownofInvoiceNo")]
        public ResponseviewModel GetDropdownofInvoiceNo()
        {
            return _AsnService.GetDropdownofInvoiceNo();
        }

        [HttpGet]
        [Route("/ASNMaster/GetASNMasterDetailByInvoiceNo")]
        public ResponseviewModel GetASNMasterDetailByInvoiceNo(string InvoiceNo)
        {
            return _AsnService.GetASNMasterDetailByInvoiceNo(InvoiceNo);
        }

        [HttpGet]
        [Route("/ASNMaster/GetContainerNoByAsndata")]
        public ResponseviewModel GetContainerNoByAsndata(string InvoiceNo,string CaseNo)
        {
            return _AsnService.GetContainerNoByAsndata(InvoiceNo,CaseNo);
        }

        [HttpGet]
        [Route("/ASNMaster/GetDuplicateInvoiceNoCheck")]
        public ResponseviewModel GetDuplicateInvoiceNoCheck(string InvoiceNo)
        {
            return _AsnService.GetDuplicateInvoiceNoCheck(InvoiceNo);
        }
        [HttpGet]
        [Route("/ASNMaster/GetCustomerCodes")]
        public ResponseviewModel GetCustomerCodes(string InvoiceNo)
        {
            return _AsnService.GetCustomerCodes(InvoiceNo);
        }
        #region Sorting

        [HttpGet]
        [Route("/ASNMaster/GetDropdownOfReceivedInvoiceNo")]
        public ResponseviewModel GetDropdownOfReceivedInvoiceNo()
        {
            return _AsnService.GetDropdownOfReceivedInvoiceNo();
        }

        [HttpGet]
        [Route("/ASNMaster/GetDropdownOfCaseNoByInvoiceNo")]
        public ResponseviewModel GetDropdownOfCaseNoByInvoiceNo(string invoiceno)
        {
            return _AsnService.GetDropdownOfCaseNoByInvoiceNo(invoiceno);
        }

        [HttpGet]
        [Route("/ASNMaster/GetDropdownOfCustomerCodeByAsnDetail")]
        public ResponseviewModel GetDropdownOfCustomerCodeByAsnDetail(SortingItemDetail sortingitemdetail)
        {
            return _AsnService.GetDropdownOfCustomerCodeByAsnDetail(sortingitemdetail);
        }

        [HttpGet]
        [Route("/ASNMaster/GetCustomerDataByInvoiceAndCaseNo")]
        public ResponseviewModel GetCustomerCodeByInvoiceAndCaseNo(string invoiceno , string caseno)
        {
            return _AsnService.GetCustomerDataByInvoiceAndCaseNo(invoiceno,caseno);
        }

        [HttpGet]
        [Route("/ASNMaster/GetLocationIdByAsnDetail")]
        public ResponseviewModel GetLocationIdByAsnDetail(SortingItemDetail sortingitemdetail)
        {
            return _AsnService.GetLocationIdByAsnDetail(sortingitemdetail);
        }

        [HttpGet]
        [Route("/ASNMaster/GetAvailableQuantityByAsnDetail")]
        public ResponseviewModel GetAvailableQuantityByAsnDetail(SortingItemDetail sortingitemdetail)
        {
            return _AsnService.GetAvailableQuantityByAsnDetail(sortingitemdetail);
        }
        [HttpPost]
        [Route("/ASNMaster/CheckASNDetails")]
        public ResponseviewModel CheckASNDetails([FromBody]List<ASNItemDetailModels> lstasndetails)
        {
            return _AsnService.CheckASNDetails(lstasndetails);

        }
        #endregion

        [HttpGet]
        [Route("/ASNMaster/GetTodayAsnMaster")]
        public ResponseviewModel GetTodayAsnMaster(TodayAsnMasterViewModel todayasnmasterviewmodel)
        {
            return _AsnService.GetTodayAsnMaster(todayasnmasterviewmodel);
        }
        [HttpGet]
        [Route("/ASNMaster/DeleteASN")]
        public ResponseviewModel DeleteASN(Guid asnid)
        {
            return _AsnService.DeleteASN(asnid);
        }
        // Get Detail of ASN
        [HttpGet]
        [Route("/ASNMaster/ASNInfoByInvoiceNo")]
        public ResponseviewModel ASNInfoByInvoiceNo(string invoiceno)
        {
            return _AsnService.ASNInfoByInvoiceNo(invoiceno);
        }

    }
}
