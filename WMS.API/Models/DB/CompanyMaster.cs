﻿using System;
using System.Collections.Generic;

namespace WMS.API.Models.DB
{
    public partial class CompanyMaster
    {
        public Guid PK_CompanyID { get; set; }
        public string CompanyName { get; set; }
        public string CompanyID { get; set; }
        public string ContactNo { get; set; }
        public string EmailID { get; set; }
        public string Address { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public bool IsDelete { get; set; }
        public Guid? CreatedBy { get; set; }
        public Guid? ModifiedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
    }
}
