﻿using AutoMapper;
using Domain.Objects.Models;
using Infrastructure.Model.Main.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WMS.API.Helper
{
    public class MapperProfile : Profile
    {
        public MapperProfile()
        {
            CreateMap<ASNItemDetailModels, ASNDetailsMismatch>();
            CreateMap<DeliveryOrder, DeliveryOrderViewResponceModel>();
            CreateMap<DeliveryOrderDetail, DeliveryOrderDetailsViewModel>();

        }
    }
}
