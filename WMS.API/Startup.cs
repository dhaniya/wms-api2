using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using Infrastructure.Model.Main;
using Microsoft.AspNetCore.Identity;
using Infrastructure.Model.Main.Entities;
using WMS.API.Models.DB;
using Domain.Engine.Access.Impl;
using Domain.Engine.Access.Interfaces;
using Swashbuckle.AspNetCore.Swagger;
using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using Domain.Objects.Models;
using System.Text;
using Microsoft.Extensions.FileProviders;
using System.IO;

namespace WMS.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "My API", Version = "v1" });
            });
            services.AddAutoMapper();
            services.AddMvc();
            //services.AddDbContext<MainDbContext>(options =>
            //options.UseSqlServer(Configuration.GetConnectionString("wms")));

            services.AddDbContext<MainDbContext>(options =>
            options.UseSqlServer(Configuration.GetConnectionString("wms")).EnableSensitiveDataLogging());
            services.AddTransient<IMainDbContext, MainDbContext>();
            services.AddTransient<ICompanyMasterService, Domain.Engine.Access.Impl.CompanyMasterService>();
            services.AddTransient<ISupplierMasterService, Domain.Engine.Access.Impl.SupplierMasterService>();
            services.AddTransient<IASNMasterService, Domain.Engine.Access.Impl.ASNMasterService>();
            services.AddTransient<IASNItemDetailService, Domain.Engine.Access.Impl.ASNItemDetailService>();
            services.AddTransient<ISortingMasterService, Domain.Engine.Access.Impl.SortingMasterService>();
            services.AddTransient<ISortingDetailService, Domain.Engine.Access.Impl.SortingDetailService>();
            services.AddTransient<IGoodReceiptService, Domain.Engine.Access.Impl.GoodReceiptService>();
            services.AddTransient<ICustomerMasterService, Domain.Engine.Access.Impl.CustomerMasterService>();
            services.AddTransient<IPickingMasterService, Domain.Engine.Access.Impl.PickingMasterService>();
            services.AddTransient<IDeliveryOrderService, Domain.Engine.Access.Impl.DeliveryOrderService>();
            services.AddTransient<IBarCodeService, Domain.Engine.Access.Impl.BarCodeService>();
            services.AddTransient<ILocationTransferService, Domain.Engine.Access.Impl.LocationTransferService>();
            services.AddTransient<ILocationMasterService, Domain.Engine.Access.Impl.LocationMasterService>();
            services.AddTransient<IPartsMasterService, Domain.Engine.Access.Impl.PartsMasterService>();
            services.AddTransient<IBinMasterService, Domain.Engine.Access.Impl.BinMasterService>();
            services.AddTransient<IBinLocationService, Domain.Engine.Access.Impl.BinLocationService>();
            services.AddTransient<IUserService, Domain.Engine.Access.Impl.UserService>();
            services.AddTransient<IStockTransactionService, Domain.Engine.Access.Impl.StockTransactionService>();
            services.AddTransient<IReportService, Domain.Engine.Access.Impl.ReportServices>();
            services.AddTransient<IUserAccessService, Domain.Engine.Access.Impl.UserAccessService>();
            services.AddTransient<IStockCountService, Domain.Engine.Access.Impl.StockCountService>();

            services.AddCors(options =>
            {
                options.AddPolicy("AllowAllHeaders",
                      builder =>
                      {
                              builder.AllowAnyOrigin()
                                     .AllowAnyHeader()
                                     .AllowAnyMethod();
                          });
            });

            // configure strongly typed settings objects
            var connectionStringsSection = Configuration.GetSection("connectionStrings");
            services.Configure<ConnectionStrings>(connectionStringsSection);
            var appSettingsSection = Configuration.GetSection("AppSettings");
            services.Configure<AppSettings>(appSettingsSection);

            // configure jwt authentication
            var appSettings = appSettingsSection.Get<AppSettings>();
            var key = Encoding.ASCII.GetBytes(appSettings.Secret);
            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
             .AddJwtBearer(x =>
             {
                 x.RequireHttpsMetadata = false;
                 x.SaveToken = true;
                 x.TokenValidationParameters = new TokenValidationParameters
                 {
                     ValidateIssuerSigningKey = true,
                     IssuerSigningKey = new SymmetricSecurityKey(key),
                     ValidateIssuer = false,
                     ValidateAudience = false
                 };
             });

            // configure DI for application services
            services.AddScoped<IUserService, UserService>();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {

            if (env.IsDevelopment())
            {
                app.UseBrowserLink();

            }
            else
            {

                //  app.UseExceptionHandler("/Home/Error");
            }
            app.UseDeveloperExceptionPage();
            app.UseDatabaseErrorPage();
            app.UseSwagger();


            // app.AddEfDiagrams<MainDbContext>();
            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), 
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
                    {
                        c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
                    });
            // Shows UseCors with CorsPolicyBuilder.
            app.UseCors("AllowAllHeaders");
            app.UseCors(builder =>
               builder.WithOrigins("http://localhost:4200"));

            app.UseStaticFiles();
            app.UseStaticFiles(new StaticFileOptions
            {
                FileProvider = new PhysicalFileProvider(
             Path.Combine(Directory.GetCurrentDirectory(), "Barcodes")),
                RequestPath = "/StaticFiles"
            });
            app.UseAuthentication();
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });

            // app.UseMvc();
        }
    }
}
